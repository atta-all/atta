/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import it.uniud.atta.common.issue.Issue;
import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.common.issue.IssueCollector;
import it.uniud.atta.common.issue.IssueLeaf;
import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.metamodel.PublishableBuilder;
import it.uniud.atta.metamodel.exception.EntityBuilderHasErrorsRuntimeException;

public abstract class EntityBuilderBase<T extends Entity> implements EntityBuilder<T> {
    
    protected final IssueCollection surfaceIssues;
    
	protected BuildingStage buildingStage;
	protected IntegrityStage integrityStage;
    
    private final List<EntityBuilder<?>> components;
    
    private PublishableBuilder<?> wrapper;
	
	protected EntityBuilderBase() {
		surfaceIssues = new IssueCollection(this);
		components = new ArrayList<>();
		
		integrityStage = IntegrityStage.DESCRIBED;
        buildingStage = BuildingStage.DESCRIBED;
	}
	
    @Override
    public final PublishableBuilder<?> getWrapper() {
        return wrapper;
    }
    
	@Override
	public final void setWrapper(PublishableBuilder<?> wrapper) {
		this.wrapper = wrapper;
		for (EntityBuilder<?> child : getComponents()) {
			child.setWrapper(wrapper);
		}
	}
    
    protected void addComponent(EntityBuilder<?> component) {
    	if (component != null)
    		components.add(component);
    }
    
    protected void removeComponent(EntityBuilder<?> component) {
    	components.remove(component);
    }
    
    protected void addComponents(Collection<? extends EntityBuilder<?>> components) {
    	if (components != null)
    		this.components.addAll(components);
    }
    
    @Override
    public final List<EntityBuilder<?>> getComponents() {
    	return components;
    }
    
    @Override
    public Set<EntityReference> getReferences() {
        Set<EntityReference> result = new HashSet<>();
        
        for (EntityBuilder<?> component : components) {
        	if (component instanceof ReferencedEntityBuilder)
        		result.add(((ReferencedEntityBuilder)component).makeReference());
        	else
        		result.addAll(component.getReferences());
        }
        
        return result;
    }
    
    @Override
    public IssueCollection getIssues() {
    	IssueCollection result = new IssueCollection(surfaceIssues);
    	for (IssueCollector child : components)
    		result.append(child.getIssues());
    	return result;
    }
	
	@Override
	public final void appendIssue(Issue issue) {
		surfaceIssues.append(new IssueLeaf(issue));
	}
	
	@Override
	public final void appendIssue(Issue issue, String reason) {
		surfaceIssues.append(new IssueLeaf(issue,reason));
	}
	
	@Override
    public final void appendChildIssues(IssueCollection child) {
	    surfaceIssues.append(child);
    }
	
	@Override
	public void resolveInternalReferences() {
		if (!buildingStage.greaterThan(BuildingStage.DESCRIBED)) {
			
			if (getIssues().getSummary() != IssueLevel.ERROR) {
		    	internalResolutionHandler();
		    	for (EntityBuilder<?> component : components)
		    		component.resolveInternalReferences();
			}
			
			if (getIssues().getSummary() != IssueLevel.ERROR)
				integrityStage = IntegrityStage.INTERNAL;
			
			buildingStage = BuildingStage.INTERNALLY_RESOLVED;
		}
	}
	
	@Override
	public void resolveExternalReferences() {		
		if (!buildingStage.greaterThan(BuildingStage.INTERNALLY_RESOLVED)) {
			resolveInternalReferences();
			
			if (getIssues().getSummary() != IssueLevel.ERROR) {
				externalResolutionHandler();
		    	for (EntityBuilder<?> component : components)
		    		component.resolveExternalReferences();
			}
			
			if (getIssues().getSummary() != IssueLevel.ERROR)
				integrityStage = IntegrityStage.EXTERNAL;
			
			buildingStage = BuildingStage.EXTERNALLY_RESOLVED; 
		}
	}
	
	@Override
	public final void checkCorrectness() {
		if (!buildingStage.greaterThan(BuildingStage.EXTERNALLY_RESOLVED)) {
			resolveExternalReferences();
			
			if (getIssues().getSummary() != IssueLevel.ERROR) {
			    checkUnrecognizedFields();
				checkingHandler();
		    	for (EntityBuilder<?> component : components)
		    		component.checkCorrectness();
			}
			
			if (getIssues().getSummary() != IssueLevel.ERROR)
				integrityStage = IntegrityStage.CHECKED;
			
			buildingStage = BuildingStage.CHECKED; 
		}		
	}
	
	@Override
	public final T build() {
		
		checkCorrectness();
    	
        if (getIntegrityStage() != IntegrityStage.CHECKED)
            throw new EntityBuilderHasErrorsRuntimeException(getIssues().toString());
		
		return buildHandler();
	}
	
	protected void internalResolutionHandler() { 
		// Defaults to empty, in order to avoid to override as empty for most cases
	}
	
	protected void externalResolutionHandler() { 
		// Defaults to empty, in order to avoid to override as empty for most cases
	}
	
	protected void checkingHandler() {
		// Defaults to empty, in order to avoid to override as empty for most cases		
	}
	
	protected abstract T buildHandler();
	
	private void checkUnrecognizedFields() {
	    
	}
	
	@Override
	public final BuildingStage getBuildingStage() {
		return buildingStage;
	}
	
	@Override
	public final IntegrityStage getIntegrityStage() {
		return integrityStage;
	}
}

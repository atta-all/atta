/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel;

import it.uniud.atta.metamodel.locator.LocatorBuilder;

import java.util.HashMap;
import java.util.Map;

public final class PublishedBuilderCache {
    
    private final static PublishedBuilderCache INSTANCE = new PublishedBuilderCache();
    
    private final Map<LocatorBuilder,PublishableBuilder<?>> content = new HashMap<>();
    
    public static PublishedBuilderCache getInstance() {
        return INSTANCE;
    }
    
    private PublishedBuilderCache() { }
    
    public PublishableBuilder<?> get(LocatorBuilder remoteBuilder) {
    	return content.get(remoteBuilder);
    }
    
    public void put(LocatorBuilder remoteBuilder, PublishableBuilder<?> builder) {
    	content.put(remoteBuilder, builder);
    }
}

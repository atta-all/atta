/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior.runner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.uniud.atta.metamodel.entity.EntityBuilderBase;
import it.uniud.atta.metamodel.entity.EntityField;
import it.uniud.atta.metamodel.entity.EntityFieldKind;
import it.uniud.atta.metamodel.verteximplementation.behavior.constraint.WorkerConstraint;
import it.uniud.atta.metamodel.verteximplementation.behavior.constraint.WorkerConstraintBuilder;

public final class RunnerBuilder extends EntityBuilderBase<Runner> {
    
    private static enum Field implements EntityField {

        COMMAND("command",EntityFieldKind.IDENTIFYING),
        ARGS("args",EntityFieldKind.OPTIONAL), 
        CONSTRAINTS("constraints",EntityFieldKind.OPTIONAL),
        ;
        
        private final String name;
        private final EntityFieldKind kind;
        
        Field(String name, EntityFieldKind kind) {
            this.name = name;
            this.kind = kind;
        }
            
        @Override
        public String toString() {
            return name;
        }
        
        @Override
        public EntityFieldKind getKind() {
            return kind;
        }
    }
    
	private final String command;
	private final String[] args;
	private final List<WorkerConstraintBuilder> constraintBuilders;
	
	public RunnerBuilder(Map<String,Object> raw) {
    	super();
		
    	Object commandObj = raw.get(Field.COMMAND.toString());
    	Object argsObj = raw.get(Field.ARGS.toString());
    	Object constraintsObj = raw.get(Field.CONSTRAINTS.toString());
    	
        command = acquireCommand(commandObj);
        args = acquireArgs(argsObj);
        constraintBuilders = acquireConstraintBuilders(constraintsObj);
        
        addComponents(constraintBuilders);
	}
	
	public String getCommand() {
		return command;
	}
	
	public String[] getArgs() {
		return args;
	}
	
	public List<WorkerConstraintBuilder> getConstraintBuilders() {
		return constraintBuilders;
	}
    
    private String[] acquireArgs(Object argsObj) {
    	String[] result = new String[0];
    	
		if (argsObj != null)
			result = ((String)argsObj).split(" ");
		
		return result;
    }
    
    private String acquireCommand(Object commandObj) {
		return commandObj.toString();
    }
    
    private List<WorkerConstraintBuilder> acquireConstraintBuilders(Object constraintsObj) {
    	
        List<WorkerConstraintBuilder> result = new ArrayList<>();
    	
        if (constraintsObj != null) {
	        @SuppressWarnings("unchecked")
	        Map<String,Object> rawConstraints = (Map<String,Object>)constraintsObj;
	        
	        for (Map.Entry<String,Object> rawConstraint : rawConstraints.entrySet()) {
	        	WorkerConstraintBuilder constraintBuilder = new WorkerConstraintBuilder(rawConstraint.getKey(),rawConstraint.getValue().toString());
	            constraintBuilder.setWrapper(this.getWrapper());
	            result.add(constraintBuilder);
	        }
        } 
        return result;
    }

	@Override
    protected Runner buildHandler() {
		
		List<WorkerConstraint> constraints = new ArrayList<>();
		for (WorkerConstraintBuilder constraintBuilder : constraintBuilders)
			constraints.add(constraintBuilder.build());
		
		return new Runner(command,args,constraints);
    }
}

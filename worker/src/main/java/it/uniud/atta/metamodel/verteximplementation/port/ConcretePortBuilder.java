/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.port;

import java.util.Map;
import java.util.Set;

import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.metamodel.PublishableBuilder;
import it.uniud.atta.metamodel.PublishedBuilderFactory;
import it.uniud.atta.metamodel.edgetype.abstr.AbstractTypeBuilder;
import it.uniud.atta.metamodel.edgetype.concr.ConcreteAtomicTypeBuilderFactory;
import it.uniud.atta.metamodel.edgetype.concr.ConcreteTypeBuilder;
import it.uniud.atta.metamodel.edgetype.concr.ReferencedConcreteTypeBuilder;
import it.uniud.atta.metamodel.edgetype.concr.api.ConcreteType;
import it.uniud.atta.metamodel.entity.EntityLabel;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.verteximplementation.behavior.BehaviorLanguage;
import it.uniud.atta.metamodel.verteximplementation.port.api.PortDirection;
import it.uniud.atta.metamodel.verteximplementation.port.api.PortSensitivity;

public final class ConcretePortBuilder extends PortBuilderBase<ConcretePort> {
    
	final private BehaviorLanguage language;
	
	public ConcretePortBuilder(Map<String,Object> raw, BehaviorLanguage language) {
    	super(raw);
        
    	this.language = language;
    	
        Object typeBuilderObj = raw.get(Field.TYPE.toString());

        typeBuilder = acquireTypeBuilder(typeBuilderObj);
        
        addComponent(typeBuilder);
	}
    
    private ConcreteTypeBuilder<?> acquireTypeBuilder(Object typeBuilderObj) {
    	ConcreteTypeBuilder<?> result = null;
		if (typeBuilderObj != null) {
	    	result = ConcreteAtomicTypeBuilderFactory.makeFrom((String)typeBuilderObj,language);	    	
	    	if (result != null) {
	    		result.checkCorrectness();
	    		if (result.getIssues().getSummary() == IssueLevel.ERROR)
	    			result = new ReferencedConcreteTypeBuilder((String)typeBuilderObj);
	    	}
		}
		return result;
    }
	
    @Override
    protected PortSensitivity getDefaultSensitivity() {
    	return (getDirection() == PortDirection.IN ? PortSensitivity.SENSITIVE : PortSensitivity.UNDEFINED);
    }
    	
	@Override
    protected void internalResolutionHandler() {
	    if (typeBuilder instanceof ReferencedConcreteTypeBuilder) {
	    	removeComponent(typeBuilder);
            Set<EntityLabel> internals = getWrapper().findLabelsFor(ConcreteTypeBuilder.class,((ReferencedConcreteTypeBuilder)typeBuilder).makeReference());
            typeBuilder = (ConcreteTypeBuilder<?>)internals.iterator().next().getEntityBuilder();
            addComponent(typeBuilder);
	    }
    }

	@Override
    protected void externalResolutionHandler() {
	    if (typeBuilder instanceof ReferencedConcreteTypeBuilder) {
	    	removeComponent(typeBuilder);
            Set<EntityLabel> remotes = getWrapper().findLabelsFor(LocatorBuilder.class,((ReferencedConcreteTypeBuilder)typeBuilder).makeReference());
            PublishableBuilder<?> publishedBuilder = PublishedBuilderFactory.makeFrom((LocatorBuilder)remotes.iterator().next().getEntityBuilder());
            publishedBuilder.resolveExternalReferences();
            typeBuilder = (AbstractTypeBuilder<?>)publishedBuilder;
            addComponent(typeBuilder);
	    }
    }
	
	@Override
    protected ConcretePort buildHandler() {
	    return new ConcretePort(getName(),getDirection(),(ConcreteType)typeBuilder.build(),getSensitivity(),getDefaultValue());
	}
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.port.api;

public enum PortSensitivity {

	SENSITIVE("true"), // The port is sensitive
	INSENSITIVE("false"), // The port is not sensitive
	UNDEFINED("undefined"), // No sensitivity is defined
	;
	
	private String str;
	
	PortSensitivity(String str) {
		this.str = str;
	}
	
	@Override
	public String toString() {
		return str;
	}
	
    public static PortSensitivity fromString(String str) {
        
    	PortSensitivity result = null;
        for (PortSensitivity kind: PortSensitivity.values()) {
             if (kind.toString().equals(str)) {
                  result = kind;
                  break;
             }
        }
        return result;
    }
}

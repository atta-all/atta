/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.entity;

import it.uniud.atta.metamodel.edgetype.abstr.composite.ComponentBuilder;
import it.uniud.atta.metamodel.edgetype.abstr.composite.CompositeTypeBuilder;
import it.uniud.atta.metamodel.edgetype.abstr.enumeration.EnumTypeBuilder;
import it.uniud.atta.metamodel.edgetype.abstr.map.MapTypeBuilder;
import it.uniud.atta.metamodel.edgetype.abstr.set.SetTypeBuilder;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.verteximplementation.behavior.BehaviorBuilder;
import it.uniud.atta.metamodel.verteximplementation.behavior.build.BuildSpecificationBuilder;
import it.uniud.atta.metamodel.verteximplementation.behavior.property.CustomWorkerPropertyBuilder;
import it.uniud.atta.repository.RepositoryBuilderImpl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public enum RecognizedEntityKind {
    
	MAP_ABSTRACT_TYPE("Map Abstract Type", MapTypeBuilder.class),
	ENUM_ABSTRACT_TYPE("Enum Abstract Type", EnumTypeBuilder.class),
	SET_ABSTRACT_TYPE("Set Abstract Type", SetTypeBuilder.class),
	COMPOSITE_ABSTRACT_TYPE("Composite Abstract Type", CompositeTypeBuilder.class),
	COMPONENT("Composite Type Component", ComponentBuilder.class),
	BEHAVIORAL_IMPLEMENTATION("Behavioral Implementation", BehaviorBuilder.class),
	WORKER_PROPERTY("Worker Property", CustomWorkerPropertyBuilder.class),
	REPOSITORY("Repository", RepositoryBuilderImpl.class),
	LOCATOR("Locator", LocatorBuilder.class),
	BUILD_SPECIFICATION("Build Specification", BuildSpecificationBuilder.class)
	;
	    
	private final String str;
	private final Class<? extends EntityBuilder<?>> cls;
	    
	RecognizedEntityKind(String str, Class<? extends EntityBuilder<?>> cls) {
	    this.str = str;
	    this.cls = cls;
	}
	
	Class<? extends EntityBuilder<?>> getBuilderClass() {
	    return cls;
	}
	
    @Override
    public String toString() {
        return str;
    }
    
    public static Set<RecognizedEntityKind> identifyFrom(Map<String,Object> raw) {
        
        Set<RecognizedEntityKind> result = new HashSet<>();
        
        for (RecognizedEntityKind val : values()) {

            Set<EntityField> fields = val.getAllFields();
            
            boolean matches = false;
            for (EntityField field : fields)
                if (field.getKind() == EntityFieldKind.IDENTIFYING && raw.get(field.toString()) != null) {
                    matches = true;
                    break;
                }
            if (matches)
                result.add(val);
        }
        
        return result;
    }
    
    public Set<EntityField> getAllFields() {
        return getFields(null);
    }
    
    public Set<EntityField> getFields(EntityFieldKind restrictionLevel) {
        return getFields(cls,restrictionLevel);
    }
    
    private static Set<EntityField> getFields(Class<?> cls, EntityFieldKind restrictionLevel) {
        Set<EntityField> result = new HashSet<>();
        try {
            Class<?>[] internalClasses = cls.getClasses();
            for (Class<?> internalClass : internalClasses)
                if (internalClass.isEnum() && EntityField.class.isAssignableFrom(internalClass)) {
                    Method method = internalClass.getMethod("values");
                    method.setAccessible(true);
                    EntityField[] fields = (EntityField[]) method.invoke(null);
                    for (EntityField field : fields)
                        if (restrictionLevel == null || field.getKind().ordinal() >= restrictionLevel.ordinal())
                            result.add(field);
                    break;
                }
            Class<?> superClass = cls.getSuperclass();
            if (superClass != null)
                result.addAll(getFields(superClass, restrictionLevel));
            
        } catch (Exception e) { }
        
        return result;
    }
    
    public EntityBuilder<?> constructFrom(Map<String,Object> raw) {
        
        EntityBuilder<?> result = null;
        try {
            Constructor<? extends EntityBuilder<?>> constructor = cls.getConstructor(Map.class);
            result = constructor.newInstance(raw);
        } catch (Exception e) {
            // Never thrown
        }
        return result;
    }
}
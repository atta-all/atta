/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.port;

import java.util.Map;

import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.common.lang.StrictNameChecker;
import it.uniud.atta.metamodel.edgetype.api.EdgeType;
import it.uniud.atta.metamodel.edgetype.api.EdgeTypeBuilder;
import it.uniud.atta.metamodel.entity.EntityBuilderBase;
import it.uniud.atta.metamodel.entity.EntityField;
import it.uniud.atta.metamodel.entity.EntityFieldKind;
import it.uniud.atta.metamodel.verteximplementation.port.api.Port;
import it.uniud.atta.metamodel.verteximplementation.port.api.PortBuilder;
import it.uniud.atta.metamodel.verteximplementation.port.api.PortDirection;
import it.uniud.atta.metamodel.verteximplementation.port.api.PortSensitivity;

public abstract class PortBuilderBase<T extends Port> extends EntityBuilderBase<T> implements PortBuilder<T> {
    
    public static enum Field implements EntityField {

        NAME("name",EntityFieldKind.MANDATORY),
        DIRECTION("direction",EntityFieldKind.IDENTIFYING),
        TYPE("type",EntityFieldKind.MANDATORY),
        SENSITIVE("sensitive",EntityFieldKind.OPTIONAL), 
        DEFAULT_VALUE("default",EntityFieldKind.OPTIONAL),
        ;
        
        private final String name;
        private final EntityFieldKind kind;
        
        Field(String name, EntityFieldKind kind) {
            this.name = name;
            this.kind = kind;
        }
            
        @Override
        public String toString() {
            return name;
        }
        
        @Override
        public EntityFieldKind getKind() {
            return kind;
        }
    }
    
    final private String name;
	final private PortDirection direction;
	final private PortSensitivity sensitivity;
	final private Object defaultValue;
	protected EdgeTypeBuilder<? extends EdgeType> typeBuilder;
	
	public PortBuilderBase(Map<String,Object> raw) {
    	super();
    	
    	name = acquireName(raw.get(Field.NAME.toString()));
        
        Object directionObj = raw.get(Field.DIRECTION.toString());
        Object sensitivityObj = raw.get(Field.SENSITIVE.toString());
        Object defaultValueObj = raw.get(Field.DEFAULT_VALUE.toString());

        direction = acquireDirection(directionObj);
		sensitivity = acquireSensitivity(sensitivityObj);
		defaultValue = defaultValueObj;
	}
	
	@Override
	public final String getName() {
	    return name;
	}
	
	public final PortDirection getDirection() {
		return direction;
	}
	
	public final PortSensitivity getSensitivity() {
		return sensitivity;
	}
	
	public final Object getDefaultValue() {
		return defaultValue;
	}
	
	public EdgeTypeBuilder<?> getTypeBuilder() {
		return typeBuilder;
	}
	
    private static String acquireName(Object nameObj) {
        return nameObj.toString();
    }
    
    private void checkName() {
		if (!StrictNameChecker.validate(getName()))
			appendIssue(PortIssue.NAME_NOT_COMPLIANT);
    }
    
    private PortDirection acquireDirection(Object directionObj) {
		return PortDirection.fromString((String)directionObj);
    }
    
    private PortSensitivity acquireSensitivity(Object sensitiveObj) {
    	
    	PortSensitivity result = getDefaultSensitivity();
    	
    	if (sensitiveObj != null)
    		result = PortSensitivity.fromString(sensitiveObj.toString());
    	
    	return result;
    }
    
    @Override
    protected void checkingHandler() {
    	checkName();
    	
    	if (direction == PortDirection.UNRECOGNIZED)
    		appendIssue(PortIssue.UNRECOGNIZED_KIND);
    	
    	if (sensitivity == null)
    		appendIssue(PortIssue.UNRECOGNIZED_SENSITIVITY);
    	else if (direction == PortDirection.OUT && sensitivity != PortSensitivity.UNDEFINED)
			appendIssue(PortIssue.IMPROPER_SENSITIVITY);
    	
		if (defaultValue != null && typeBuilder.getIssues().getSummary() != IssueLevel.ERROR)
			appendChildIssues(typeBuilder.build().validate(defaultValue));
    }
 
    protected abstract PortSensitivity getDefaultSensitivity();
}

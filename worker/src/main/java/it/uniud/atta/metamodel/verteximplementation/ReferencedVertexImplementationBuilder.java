/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation;

import java.util.List;

import it.uniud.atta.metamodel.ReferencedPublishedBuilder;
import it.uniud.atta.metamodel.entity.EntityReference;
import it.uniud.atta.metamodel.entity.EntityReferenceImpl;
import it.uniud.atta.metamodel.verteximplementation.api.VertexImplementation;
import it.uniud.atta.metamodel.verteximplementation.port.api.PortBuilder;

final public class ReferencedVertexImplementationBuilder extends ReferencedPublishedBuilder<VertexImplementation> implements VertexImplementationBuilder<VertexImplementation> {

    public ReferencedVertexImplementationBuilder(String typeName) {
        super(typeName);
    }

	@Override
    public List<PortBuilder<?>> getPortBuilders() {
	    return null;
    }

	@Override
    public VertexSecurity getSecurity() {
	    return null;
    }
	
    @Override
    public final EntityReference makeReference() {
        return new EntityReferenceImpl(getName(),VertexImplementationBuilder.class);
    }
}

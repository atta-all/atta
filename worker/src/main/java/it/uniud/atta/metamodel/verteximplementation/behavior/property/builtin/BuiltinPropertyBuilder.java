/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior.property.builtin;

import it.uniud.atta.metamodel.entity.EntityBuilderBase;
import it.uniud.atta.metamodel.verteximplementation.behavior.property.WorkerProperty;
import it.uniud.atta.metamodel.verteximplementation.behavior.property.WorkerPropertyBuilder;
import it.uniud.atta.metamodel.verteximplementation.behavior.property.WorkerPropertyIssue;

public final class BuiltinPropertyBuilder extends EntityBuilderBase<WorkerProperty> implements WorkerPropertyBuilder {

    private final String name;
    private final BuiltinProperty value;
    
    public BuiltinPropertyBuilder(String name) {
        super();
        this.name = name;
        value = BuiltinProperty.fromString(name);
    }
    
    @Override
    protected void checkingHandler() {
        if (value == null)
            surfaceIssues.append(WorkerPropertyIssue.UNRECOGNIZED_BUILTIN,name);
    }
    
    @Override
    protected WorkerProperty buildHandler() {
        return value;
    }
}

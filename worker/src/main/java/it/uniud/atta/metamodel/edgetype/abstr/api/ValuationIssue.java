/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.api;

import it.uniud.atta.common.issue.Issue;
import it.uniud.atta.common.issue.IssueLevel;

public enum ValuationIssue implements Issue {
	 
	INVALID_VALUE, // The type does not match the valuation in terms of value
	VALUE_NOT_INCLUDED, // The value is valid, but is not included in the enum/set
	NOT_ATOMIC, // The valuation is not atomic
	NOT_MAP_PAIR, // The valuation is not a map pair
	MULTIPLE_PAIRS, // There are multiple pairs when one is expected 
	IS_LIST, // There is a list when a single entry is expected
	IS_ATOMIC, // There is an atomic value when a list or map is expected
	MISSING_FIELD, // A value for the given field of the abstract type has not been provided 
	UNRECOGNIZED_FIELD, // A value for an unrecognized field of the abstract type has been provided
	VALUE_NOT_SINGLETON, // The value should be a singleton, not an array
	INVALID_ARRAY_SIZES, // The array is either too large or too small compared to the specification bounds
	ARRAY_IS_JAGGED, // The number of values is incomplete, making the multidimensional array jagged
    ;
    
    private IssueLevel level;
    
    ValuationIssue() {
        this(IssueLevel.ERROR);
    }
    
    ValuationIssue(IssueLevel level) {
        this.level = level;
    }
    
    @Override
    public IssueLevel getLevel() {
        return level;
    }
}

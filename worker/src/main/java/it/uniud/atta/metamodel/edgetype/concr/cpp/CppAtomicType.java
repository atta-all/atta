/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.concr.cpp;

import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicAbstractType;
import it.uniud.atta.metamodel.edgetype.concr.api.ConcreteAtomicType;

public enum CppAtomicType implements ConcreteAtomicType {
    
	BOOL("bool",AtomicAbstractType.BOOL,1,1),
	SIGNED_CHAR("char",AtomicAbstractType.INT8,8,8),
	UNSIGNED_CHAR("unsigned char",AtomicAbstractType.UINT8,8,8),
	SIGNED_SHORT("short",AtomicAbstractType.INT16,16,-1),
	UNSIGNED_SHORT("unsigned short",AtomicAbstractType.UINT16,16,-1),
	SIGNED_INTEGER("int",AtomicAbstractType.INT32,16,-1),
	UNSIGNED_INTEGER("unsigned int",AtomicAbstractType.UINT32,16,-1),
	SIGNED_LONG("long",AtomicAbstractType.INT64,32,-1),
	UNSIGNED_LONG("unsigned long",AtomicAbstractType.UINT64,32,-1),
	LONG_LONG("long long",AtomicAbstractType.INTEGER,64,-1), 
	UNSIGNED_LONG_LONG("unsigned long long",AtomicAbstractType.INTEGER,64,-1),
	FLOAT("float",AtomicAbstractType.FLOAT,32,32),
	DOUBLE("double",AtomicAbstractType.DOUBLE,64,64);
	    
	private String name;
	private AtomicAbstractType abstractType;
	private int minBitWidth;
	private int maxBitWidth;
	    
	CppAtomicType(String str, AtomicAbstractType abstractType, int minBitWidth, int maxBitWidth) {
	    this.name = str;
	    this.abstractType = abstractType;
	    this.minBitWidth = minBitWidth;
	    this.maxBitWidth = maxBitWidth;
	}

    @Override
    public String toString() {
    	return new StringBuilder("(")
    							 .append(name)
    							 .append(",")
    							 .append(abstractType)
    							 .append("[")
    							 .append(minBitWidth)
    							 .append(",")
    							 .append(maxBitWidth)
    							 .append("])").toString();
    }
    
    @Override
    public AtomicAbstractType getAbstractType() {
    	return abstractType;
    }
    
    @Override
    public int getMinBitWidth() {
    	return minBitWidth;
    }
    
    @Override
    public int getMaxBitWidth() {
    	return maxBitWidth;
    }
    
    public static CppAtomicType fromString(String str) {
            
        CppAtomicType result = null;
        for (CppAtomicType kind: CppAtomicType.values()) {
             if (kind.name.equals(str)) {
                  result = kind;
                  break;
             }
        }
            
        return result;
    }

	@Override
	public IssueCollection validate(Object valuation) {
		return abstractType.validate(valuation);
	}

    @Override
    public String getName() {
        return name;
    }
}

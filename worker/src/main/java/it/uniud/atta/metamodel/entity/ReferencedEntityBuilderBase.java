/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.entity;

import it.uniud.atta.metamodel.entity.EntityBuilderBase;
import it.uniud.atta.metamodel.entity.ReferencedEntityBuilder;
import it.uniud.atta.metamodel.exception.EntityBuilderHasErrorsRuntimeException;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public abstract class ReferencedEntityBuilderBase<T extends Entity> extends EntityBuilderBase<T> implements ReferencedEntityBuilder {

    private final String name;
    
    public ReferencedEntityBuilderBase(String name) {
        super();
        this.name = name;
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17,31).append(name).build();
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof ReferencedEntityBuilderBase))
            return false;
        @SuppressWarnings("unchecked")
        ReferencedEntityBuilderBase<T> otherRef = (ReferencedEntityBuilderBase<T>)other;
        
        return name.equals(otherRef.name);
    }
    
    @Override
    protected final T buildHandler() {
        throw new EntityBuilderHasErrorsRuntimeException(getIssues().toString());
    }

    @Override
    public String getName() {
        return name;
    }
}

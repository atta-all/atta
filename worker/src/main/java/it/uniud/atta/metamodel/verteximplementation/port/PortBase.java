/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.port;

import it.uniud.atta.metamodel.verteximplementation.port.api.Port;
import it.uniud.atta.metamodel.verteximplementation.port.api.PortDirection;
import it.uniud.atta.metamodel.verteximplementation.port.api.PortSensitivity;

public abstract class PortBase implements Port {
    
	final private String name;
	final private PortDirection kind;
	final private PortSensitivity sensitivity;
	final private Object defaultValue;
	
	protected PortBase(String name, PortDirection direction, PortSensitivity sensitivity, Object defaultValue) {
		this.name = name;
		this.kind = direction;
		this.sensitivity = sensitivity;
		this.defaultValue = defaultValue;
	}
	
	@Override
	public final String getName() {
		return name;
	}
	
	@Override
	public final PortDirection getDirection() {
		return kind;
	}
	
	@Override
	public final PortSensitivity getSensitivity() {
		return sensitivity;
	}
	
	@Override
	public final Object getDefaultValue() {
		return defaultValue;
	}
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior;

import it.uniud.atta.metamodel.entity.EntityField;
import it.uniud.atta.metamodel.entity.EntityFieldKind;
import it.uniud.atta.metamodel.verteximplementation.VertexImplementationBuilderBase;
import it.uniud.atta.metamodel.verteximplementation.api.Behavior;
import it.uniud.atta.metamodel.verteximplementation.behavior.build.BuildSpecification;
import it.uniud.atta.metamodel.verteximplementation.behavior.build.BuildSpecificationBuilder;
import it.uniud.atta.metamodel.verteximplementation.port.ConcretePort;
import it.uniud.atta.metamodel.verteximplementation.port.ConcretePortBuilder;
import it.uniud.atta.metamodel.verteximplementation.port.api.PortBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

final public class BehaviorBuilder extends VertexImplementationBuilderBase<Behavior>  {
    
    public static enum Field implements EntityField {

        LANGUAGE("language",EntityFieldKind.IDENTIFYING),
        BUILD("build",EntityFieldKind.OPTIONAL),
        ;
            
        private String name;
        private EntityFieldKind kind;
        
        Field(String name, EntityFieldKind kind) {
            this.name = name;
            this.kind = kind;
        }
            
        @Override
        public String toString() {
            return name;
        }
        
        @Override
        public EntityFieldKind getKind() {
            return kind;
        }
    }
    
    private final BehaviorLanguage language;
    private final BuildSpecificationBuilder buildSpecificationBuilder;
    
    public BehaviorBuilder(Map<String, Object> raw) {
        super(raw);
        
        language = acquireLanguage(raw);
        if (language != null)
        	portBuilders = acquirePortBuilders(raw);
        
        buildSpecificationBuilder = acquireBuildSpecificationBuilder(raw);
        
        addComponents(portBuilders);
        addComponent(buildSpecificationBuilder);
    }

    public BehaviorLanguage getLanguage() {
    	return language;
    }
    
    public BuildSpecificationBuilder getBuildSpecificationBuilder() {
    	return buildSpecificationBuilder;
    }
    
    @Override
    protected void checkingHandler() {
    	super.checkingHandler();

		if (language == null)
			surfaceIssues.append(BehaviorIssue.UNRECOGNIZED_LANGUAGE_VALUE);
    }

	@Override
    protected Behavior buildHandler() {
		
	    BuildSpecification buildSpecification = (buildSpecificationBuilder != null ? buildSpecificationBuilder.build() : null);
		
	    return new BehaviorImpl(buildPorts(),getSecurity(),language,buildSpecification);
    }
	
    protected List<ConcretePort> buildPorts() {
        List<ConcretePort> result = new ArrayList<>();
    	
        for (PortBuilder<?> portBuilder : getPortBuilders())
            result.add(((ConcretePortBuilder)portBuilder).build());
    	
        return result;
    }
	
	private BehaviorLanguage acquireLanguage(Map<String, Object> raw) {
        return BehaviorLanguage.fromString((String)raw.get(Field.LANGUAGE.toString()));
	}
	
	@Override
    protected List<PortBuilder<?>> acquirePortBuilders(Map<String, Object> raw) {
    	
    	List<PortBuilder<?>> result = new ArrayList<>();
    	
    	if (raw.containsKey(VertexImplementationBuilderBase.Field.PORTS.toString())) {
            @SuppressWarnings("unchecked")
            List<Map<String,Object>> rawPortBuilderList = (List<Map<String,Object>>)raw.get(VertexImplementationBuilderBase.Field.PORTS.toString());
            
            for (Map<String,Object> rawPortBuilder : rawPortBuilderList)
                result.add(new ConcretePortBuilder(rawPortBuilder,language));
    	}
    	
    	return result;
    }
	
    protected BuildSpecificationBuilder acquireBuildSpecificationBuilder(Map<String, Object> raw) {
    	
		BuildSpecificationBuilder result = null;
    	
    	if (raw.containsKey(Field.BUILD.toString())) {
    		@SuppressWarnings("unchecked")
            Map<String,Object> rawBuildSpecificationBuilder = (Map<String,Object>)raw.get(Field.BUILD.toString());
            result = new BuildSpecificationBuilder(rawBuildSpecificationBuilder);
    	}
    	
    	return result;
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior.property.builtin;

public class NumLogicalCoresChecker implements BuiltinPropertyChecker {

	private static NumLogicalCoresChecker INSTANCE = new NumLogicalCoresChecker();
    
	private NumLogicalCoresChecker() { }
	
	public static NumLogicalCoresChecker getInstance() {
		return INSTANCE;
	}
	
	@Override
    public String check() {
	    return Integer.toString(Runtime.getRuntime().availableProcessors());
    }

    @Override
    public boolean validates(String value) {
        try {
            int val = Integer.parseInt(value);
            return (val > 0);
        } catch (NumberFormatException e) {
            return false;
        }
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel;

import java.util.Set;

import it.uniud.atta.common.issue.IssueCollector;
import it.uniud.atta.metamodel.entity.EntityBuilder;
import it.uniud.atta.metamodel.entity.EntityReference;
import it.uniud.atta.metamodel.entity.EntityLabel;
import it.uniud.atta.metamodel.entity.Staged;
import it.uniud.atta.metamodel.locator.LocatorBuilder;

public interface PublishableBuilder<T extends Publishable> extends EntityBuilder<T>, IssueCollector, Staged {
    
	public T build();
	
    public LocatorBuilder getSourceLocatorBuilder();
    
    public void setSourceLocatorBuilder(LocatorBuilder remoteBuilder);
    
    public Set<EntityLabel> getLabels();

    public Set<EntityLabel> findLabelsFor(Class<?> cls);
    
    public Set<EntityLabel> findLabelsFor(Class<?> cls, EntityReference reference);
}

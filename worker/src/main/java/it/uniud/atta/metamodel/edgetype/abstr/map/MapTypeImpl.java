/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.map;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.metamodel.edgetype.abstr.AbstractTypeBase;
import it.uniud.atta.metamodel.edgetype.abstr.api.AbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.MapType;
import it.uniud.atta.metamodel.edgetype.abstr.api.ValuationIssue;
import it.uniud.atta.metamodel.edgetype.api.EdgeDataConvertibility;
import it.uniud.atta.metamodel.edgetype.api.EdgeTypeConversionSafety;

final class MapTypeImpl extends AbstractTypeBase implements MapType {
    
    private final AbstractType keyType;
    private final AbstractType valueType;
    
    MapTypeImpl(AbstractType keyType, AbstractType valueType) {
        this.keyType = keyType;
        this.valueType = valueType;
    }
    
    @Override
    public final AbstractType getKeyType() {
        return keyType;
    }
    
    @Override
    public final AbstractType getValueType() {
        return valueType;
    }

	@Override
    public IssueCollection validate(Object valuation) {
		IssueCollection result = new IssueCollection(valuation);
		
		if (!(valuation instanceof List || valuation instanceof Map))
			result.append(ValuationIssue.NOT_MAP_PAIR);
		else {
			if (valuation instanceof List) {
				@SuppressWarnings("unchecked")
                List<Object> listValuation = (List<Object>)valuation;
				for (Object listItem : listValuation) {
					if (!(listItem instanceof Map))
						result.append(ValuationIssue.NOT_MAP_PAIR,listItem.toString());
					else 
						validateMapPair(listItem, result);
				}
			} else
				validateMapPair(valuation, result);
		}
	    
	    return result;
    }
	
	private void validateMapPair(Object valuation, IssueCollection issues) {
		@SuppressWarnings("unchecked")
        Map<String,Object> entrySet = (Map<String,Object>)valuation;
		if (entrySet.size() > 1)
			issues.append(ValuationIssue.MULTIPLE_PAIRS,entrySet.toString());
		else {
			issues.appendAll(keyType.validate(entrySet.keySet().iterator().next()));
			issues.appendAll(valueType.validate(entrySet.values().iterator().next()));
		}		
	}
	
	@Override
    public Map<AbstractType, EdgeTypeConversionSafety> findSubsetsEqualTo(AbstractType other) {
		Map<AbstractType, EdgeTypeConversionSafety> result = new HashMap<>();
		if (other instanceof MapType) {
			if (this == other)
				result.put(this, EdgeTypeConversionSafety.SAFE);
			else {
				if (keyType.convertibilityTo(((MapType) other).getKeyType()) == EdgeDataConvertibility.EQUAL &&
					valueType.convertibilityTo(((MapType) other).getValueType()) == EdgeDataConvertibility.EQUAL)
					result.put(this, EdgeTypeConversionSafety.SAFE);
			}
		}
		
	    return result;
    }
}

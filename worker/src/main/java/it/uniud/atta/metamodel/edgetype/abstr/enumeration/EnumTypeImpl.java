/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.enumeration;

import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.metamodel.edgetype.abstr.AbstractTypeBase;
import it.uniud.atta.metamodel.edgetype.abstr.api.AbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.EnumType;
import it.uniud.atta.metamodel.edgetype.abstr.api.ValuationIssue;
import it.uniud.atta.metamodel.edgetype.api.EdgeTypeConversionSafety;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class EnumTypeImpl extends AbstractTypeBase implements EnumType {

    private final Set<String> values;

    EnumTypeImpl(Set<String> values) {
        this.values = values;
    }
    
    @Override
    public Set<String> getValues() {
        return values;
    }

	@Override
    public IssueCollection validate(Object valuation) {
	    IssueCollection result = new IssueCollection(valuation);
	    
	    if (valuation instanceof List || valuation instanceof Map)
	    	result.append(ValuationIssue.NOT_ATOMIC);
	    else {
	    	String stringValuation = valuation.toString();
	    	boolean found = false;
	    	for (String value : values)
	    		if (value.equals(stringValuation)) {
	    			found = true;
	    			break;
	    		}
	    	
	    	if (!found)
	    		result.append(ValuationIssue.VALUE_NOT_INCLUDED);
	    }
	    	
	    return result;
    }
	
	@Override
	public Map<AbstractType, EdgeTypeConversionSafety> findSubsetsEqualTo(AbstractType other) {
		Map<AbstractType, EdgeTypeConversionSafety> result = new HashMap<>();
		if (other instanceof EnumType) {
			if (this == other)
				result.put(this, EdgeTypeConversionSafety.SAFE);
			else {
				if (((EnumType)other).getValues().containsAll(values))
					result.put(this, EdgeTypeConversionSafety.SAFE);
			}
		}
		
	    return result;
	}
}

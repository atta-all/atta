/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.port;

import it.uniud.atta.common.issue.Issue;
import it.uniud.atta.common.issue.IssueLevel;

public enum PortIssue implements Issue {
    
	NAME_NOT_COMPLIANT(IssueLevel.ERROR), // The name of the port is not compliant to Java naming specification
	UNRECOGNIZED_KIND(IssueLevel.ERROR), // The direction is incorrect
    UNKNOWN_TYPE(IssueLevel.ERROR), // The type is unknown
    UNRECOGNIZED_SENSITIVITY(IssueLevel.ERROR), // The value for the sensitivity is not a boolean
    IMPROPER_SENSITIVITY(IssueLevel.WARNING), // The sensitive specification does not apply to the port direction
    UNHANDLED_LANGUAGE_FOR_BINDING(IssueLevel.ERROR), // The concrete port has no valid language value, hence the concrete type cannot be bound
    ;
    
    private IssueLevel level;
    
    PortIssue(IssueLevel level) {
        this.level = level;
    }
    
    @Override
    public IssueLevel getLevel() {
        return level;
    }
}

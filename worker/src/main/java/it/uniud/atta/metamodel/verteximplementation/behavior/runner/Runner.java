/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior.runner;

import java.util.List;

import it.uniud.atta.metamodel.entity.Entity;
import it.uniud.atta.metamodel.verteximplementation.behavior.constraint.WorkerConstraint;

public final class Runner implements Entity {
	
	private final String command;
	private final String[] args;
	private final List<WorkerConstraint> constraints;
	
	public Runner(String command, String[] args, List<WorkerConstraint> constraints) {
		this.command = command;
		this.args = args;
		this.constraints = constraints;
	}
	
	public String getCommand() {
		return command;
	}
	
	public String[] getArgs() {
		return args;
	}
	
	public List<WorkerConstraint> getConstraints() {
		return constraints;
	}
}

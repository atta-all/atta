/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.composite;

import it.uniud.atta.metamodel.PublishableBuilderBase;
import it.uniud.atta.metamodel.edgetype.abstr.AbstractTypeBuilder;
import it.uniud.atta.metamodel.edgetype.abstr.api.CompositeType;
import it.uniud.atta.metamodel.edgetype.abstr.api.Component;
import it.uniud.atta.metamodel.entity.EntityField;
import it.uniud.atta.metamodel.entity.EntityFieldKind;

import java.util.*;

final public class CompositeTypeBuilder extends PublishableBuilderBase<CompositeType> implements AbstractTypeBuilder<CompositeType> {

    public static enum Field implements EntityField {

        COMPONENTS("components",EntityFieldKind.IDENTIFYING)
        ;
            
        private final String name;
        private final EntityFieldKind kind;

        Field(String name, EntityFieldKind kind) {
            this.name = name;
            this.kind = kind;
        }
            
        @Override
        public String toString() {
            return name;
        }
        
        @Override
        public EntityFieldKind getKind() {
            return kind;
        }
    }
    
    private final List<ComponentBuilder> fieldBuilders;

    public CompositeTypeBuilder(Map<String, Object> raw) {
        super(raw);
        
        fieldBuilders = acquireFieldBuilders(raw);
        
        addComponents(fieldBuilders);
    }
    
    private List<ComponentBuilder> acquireFieldBuilders(Map<String, Object> raw) {
    	
        @SuppressWarnings("unchecked")
        List<Map<String,Object>> rawFields = (List<Map<String,Object>>)raw.get(Field.COMPONENTS.toString());
    	
        List<ComponentBuilder> result = new ArrayList<>();
        for (Map<String,Object> rawField : rawFields) {
            ComponentBuilder fieldBuilder = new ComponentBuilder(rawField);
            fieldBuilder.setWrapper(this.getWrapper());
            result.add(fieldBuilder);
        }
        
        return result;
    }
    
    public List<ComponentBuilder> getFieldBuilders() {
        return fieldBuilders;
    }
	
    @Override
    protected CompositeType buildHandler() {
    	
        List<Component> builtFields = new ArrayList<>();
        for (ComponentBuilder fieldBuilder : fieldBuilders)
            builtFields.add(fieldBuilder.build());
        
        return new CompositeTypeImpl(builtFields);
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior.build;

import it.uniud.atta.common.issue.Issue;
import it.uniud.atta.common.issue.IssueLevel;

public enum BuildSpecificationIssue implements Issue {
    
	UNRECOGNIZED_KIND, // THe specified kind is not recognized
    ;
    
    private IssueLevel level;
    
    private BuildSpecificationIssue() {
    	this(IssueLevel.ERROR);
    }
    
    private BuildSpecificationIssue(IssueLevel level) {
        this.level = level;
    }
    
    @Override
    public IssueLevel getLevel() {
        return level;
    }
}

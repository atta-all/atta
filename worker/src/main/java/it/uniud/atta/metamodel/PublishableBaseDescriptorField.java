/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel;

import it.uniud.atta.metamodel.entity.EntityField;
import it.uniud.atta.metamodel.entity.EntityFieldKind;

public enum PublishableBaseDescriptorField implements EntityField {

    LABELS("labels",EntityFieldKind.OPTIONAL),
    ;
        
    private String name;
    private EntityFieldKind kind;
    
    PublishableBaseDescriptorField(String name, EntityFieldKind kind) {
        this.name = name;
        this.kind = kind;
    }
        
    @Override
    public String toString() {
        return name;
    }
    
    @Override
    public EntityFieldKind getKind() {
        return kind;
    }
}
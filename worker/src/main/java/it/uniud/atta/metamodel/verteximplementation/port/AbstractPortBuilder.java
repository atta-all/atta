/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.port;

import java.util.Map;
import java.util.Set;

import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.metamodel.PublishableBuilder;
import it.uniud.atta.metamodel.PublishedBuilderFactory;
import it.uniud.atta.metamodel.edgetype.abstr.AbstractTypeBuilder;
import it.uniud.atta.metamodel.edgetype.abstr.ReferencedAbstractTypeBuilder;
import it.uniud.atta.metamodel.edgetype.abstr.api.AbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.atomic.AtomicTypeBuilder;
import it.uniud.atta.metamodel.entity.EntityLabel;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.verteximplementation.port.api.PortSensitivity;

public final class AbstractPortBuilder extends PortBuilderBase<AbstractPort> {
	
	public AbstractPortBuilder(Map<String,Object> raw) {
    	super(raw);
        
        Object typeBuilderObj = raw.get(Field.TYPE.toString());

        typeBuilder = acquireTypeBuilder(typeBuilderObj);
        
        addComponent(typeBuilder);
	}
    
    private AbstractTypeBuilder<?> acquireTypeBuilder(Object typeBuilderObj) {
    	AbstractTypeBuilder<?> result = null;
		if (typeBuilderObj != null) {
	    	result = new AtomicTypeBuilder((String)typeBuilderObj);
	    	result.checkCorrectness();
	        if (result.getIssues().getSummary() == IssueLevel.ERROR)
	        	result = new ReferencedAbstractTypeBuilder((String)typeBuilderObj);
		}
		return result;
    }
	
    @Override
    protected PortSensitivity getDefaultSensitivity() {
    	return PortSensitivity.UNDEFINED;
    }

	@Override
    protected void internalResolutionHandler() {
	    if (typeBuilder instanceof ReferencedAbstractTypeBuilder) {
	    	removeComponent(typeBuilder);
            Set<EntityLabel> internals = getWrapper().findLabelsFor(AbstractTypeBuilder.class,((ReferencedAbstractTypeBuilder)typeBuilder).makeReference());
            typeBuilder = (AbstractTypeBuilder<?>)internals.iterator().next().getEntityBuilder();
            addComponent(typeBuilder);
	    }
    }

	@Override
    protected void externalResolutionHandler() {
	    if (typeBuilder instanceof ReferencedAbstractTypeBuilder) {
	    	removeComponent(typeBuilder);
            Set<EntityLabel> remotes = getWrapper().findLabelsFor(LocatorBuilder.class,((ReferencedAbstractTypeBuilder)typeBuilder).makeReference());
            PublishableBuilder<?> publishedBuilder = PublishedBuilderFactory.makeFrom((LocatorBuilder)remotes.iterator().next().getEntityBuilder());
            publishedBuilder.resolveExternalReferences();
            typeBuilder = (AbstractTypeBuilder<?>)publishedBuilder;
            addComponent(typeBuilder);
	    }
    }
	
	@Override
    protected AbstractPort buildHandler() {
	    return new AbstractPort(getName(),getDirection(),(AbstractType)typeBuilder.build(),getSensitivity(),getDefaultValue());
	}
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.time;

import java.math.BigInteger;

import it.uniud.atta.metamodel.entity.EntityBuilderBase;

public final class SimulationTimeBuilder extends EntityBuilderBase<SimulationTime> {

    private final String val;
    
    public SimulationTimeBuilder(String val) {
        super();
        this.val = val;
    }
    
    @Override
    protected void checkingHandler() {
        if (!isValueValid())
            surfaceIssues.append(SimulationTimeIssue.INVALID_VALUE,val);
    }
    
    private boolean isValueValid() {
        return parseValue(val) != null;
    }
    
    private BigInteger parseValue(String val) {
        BigInteger result = null;
        String unit = val.substring(val.length()-2, val.length());
        if (SimulationTimeUnit.fromString(unit) != null) {
            try {
                result = new BigInteger(val.substring(0,val.length()-2));
            } catch (NumberFormatException e) { }
        }
        return result;
    }
    
    @Override
    protected SimulationTime buildHandler() {
        return new SimulationTime(parseValue(val));
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.structure.api;

public enum GuardOperator {
    
    GEQ(">="),
    GT(">"),
    EQ("="),
    NEQ("!="),
    LT("<"),
    LEQ("<=")
    ;
        
    private String name;
        
    GuardOperator(String str) {
        this.name = str;
    }
        
    public static GuardOperator fromString(String str) {
        
        GuardOperator result = null;
        for (GuardOperator kind: GuardOperator.values()) {
             if (kind.name.equals(str)) {
                  result = kind;
                  break;
             }
        }
            
        return result;
    }
    
    public String toString() {
        return name;
    }
}



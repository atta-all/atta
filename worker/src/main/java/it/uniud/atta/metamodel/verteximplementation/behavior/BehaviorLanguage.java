/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior;

import it.uniud.atta.common.lang.Named;

public enum BehaviorLanguage implements Named {

	JAVA("java"), 
	CPP("cpp"),
	C("c"),
	PYTHON("python"),
	CSHARP("csharp"), 
	;
	    
	private String name;
	    
	BehaviorLanguage(String name) {
	    this.name = name;
	}
	    
    @Override
    public String toString() {
        return name;
    }
    
    public static BehaviorLanguage fromString(String str) {
    	BehaviorLanguage result = null;
        for (BehaviorLanguage language: BehaviorLanguage.values()) {
             if (language.toString().equals(str)) {
                  result = language;
                  break;
             }
        }
        return result;
    }

    @Override
    public String getName() {
        return name;
    }
    
}
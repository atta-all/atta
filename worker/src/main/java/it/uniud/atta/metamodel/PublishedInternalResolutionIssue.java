/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel;

import it.uniud.atta.common.issue.Issue;
import it.uniud.atta.common.issue.IssueLevel;

public enum PublishedInternalResolutionIssue implements Issue {
    
    UNBOUND_REFERENCE, // A reference has no corresponding internal or published entity declared
    UNUSED_LABEL(IssueLevel.WARNING), // A label is not used anywhere
    CYCLIC_DEPENDENCY, // The reference is in a cycle within the descriptor
    INVALID_REFERENCE_NAME, // The name provided for the reference is invalid
    MISSING_TYPE_NAME, // A type has no name
    TYPE_NAME_NOT_COMPLIANT, // A type name is not compliant to the naming specification
    RESERVED_ATOMIC_TYPE_NAME, // The name used is that of an atomic type hence reserved
    RESERVED_BUILTIN_PROPERTY_NAME, // The name used is that of a builtin property hence reserved    
    MISSING_REPOSITORY_NAME, // A repository has no name
    DEFINED_NAME_NOT_COMPLIANT, // The name defined for the artifact is not compliant  
    MISMATCHING_LABEL_CONTENT, // The reference has a label with incompatible builder type
    UNRECOGNIZED_FIELD, // The field is not part of the entity
    ;
    
    private IssueLevel level;
    
    PublishedInternalResolutionIssue() {
        this(IssueLevel.ERROR);
    }
    
    PublishedInternalResolutionIssue(IssueLevel level) {
        this.level = level;
    }
    
    @Override
    public IssueLevel getLevel() {
        return level;
    }
}

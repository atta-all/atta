/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.concr.cpp;

import it.uniud.atta.common.lang.Named;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicTypeIssue;
import it.uniud.atta.metamodel.edgetype.concr.ConcreteTypeBuilder;
import it.uniud.atta.metamodel.entity.EntityBuilderBase;

public final class CppPrimitiveTypeBuilder extends EntityBuilderBase<CppAtomicType> implements ConcreteTypeBuilder<CppAtomicType>, Named {

    private final CppAtomicType value;
    private final String name;
    
    public CppPrimitiveTypeBuilder(String name) {
        super();
        this.name = name;
        value = CppAtomicType.fromString(name);
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    @Override
    protected void checkingHandler() {
    	if (value == null)
    		surfaceIssues.append(AtomicTypeIssue.UNKNOWN_TYPE_VALUE,name);
    }
    
    @Override
    protected CppAtomicType buildHandler() {
        return value;
    }
}

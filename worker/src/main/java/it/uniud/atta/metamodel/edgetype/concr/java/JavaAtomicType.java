/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.concr.java;

import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicAbstractType;
import it.uniud.atta.metamodel.edgetype.concr.api.ConcreteAtomicType;

public enum JavaAtomicType implements ConcreteAtomicType {
    
	BOOLEAN("boolean",AtomicAbstractType.BOOL,1,1), // Boolean
	BYTE("byte",AtomicAbstractType.INT8,8,8), // Signed 8 bit integer
	SHORT("short",AtomicAbstractType.INT16,16,16), // Signed 16 bit integer
	INTEGER("int",AtomicAbstractType.INT32,32,32), // Signed 32 bit integer
	LONG("long",AtomicAbstractType.INT64,64,64), // Signed 64 bit integer
	FLOAT("float",AtomicAbstractType.FLOAT,32,32), // IEEE 754 single precision floating point
	DOUBLE("double",AtomicAbstractType.DOUBLE,64,64), // IEEE 754 double precision floating point
	BIG_INTEGER("BigInteger",AtomicAbstractType.INTEGER,32,Integer.MAX_VALUE), // Arbitrary precision integer
	DECIMAL("Decimal",AtomicAbstractType.DECIMAL,32,Integer.MAX_VALUE), // Arbitrary precision decimal
	STRING("String",AtomicAbstractType.TEXT,8,Integer.MAX_VALUE); // UTF16 string
	    
	private String name;
	private AtomicAbstractType abstractType;
	private int minBitWidth;
	private int maxBitWidth;
	    
	JavaAtomicType(String str, AtomicAbstractType abstractType, int minBitWidth, int maxBitWidth) {
	    this.name = str;
	    this.abstractType = abstractType;
	    this.minBitWidth = minBitWidth;
	    this.maxBitWidth = maxBitWidth;
	}

    @Override
    public String toString() {
    	return new StringBuilder("(")
    							 .append(name)
    							 .append(",")
    							 .append(abstractType)
    							 .append("[")
    							 .append(minBitWidth)
    							 .append(",")
    							 .append(maxBitWidth)
    							 .append("])").toString();
    }

    @Override
    public AtomicAbstractType getAbstractType() {
    	return abstractType;
    }
    
    @Override
    public int getMinBitWidth() {
    	return minBitWidth;
    }
    
    @Override
    public int getMaxBitWidth() {
    	return maxBitWidth;
    }
    
    public static JavaAtomicType fromString(String str) {
            
        JavaAtomicType result = null;
        for (JavaAtomicType kind: JavaAtomicType.values()) {
             if (kind.getName().equals(str)) {
                  result = kind;
                  break;
             }
        }
            
        return result;
    }

    @Override
    public String getName() {
        return name;
    }

	@Override
    public IssueCollection validate(Object valuation) {
	    return getAbstractType().validate(valuation);
    }
}

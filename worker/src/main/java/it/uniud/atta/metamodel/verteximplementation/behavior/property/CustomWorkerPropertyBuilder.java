/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior.property;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.uniud.atta.metamodel.PublishableBuilderBase;
import it.uniud.atta.metamodel.entity.EntityField;
import it.uniud.atta.metamodel.entity.EntityFieldKind;
import it.uniud.atta.metamodel.verteximplementation.behavior.build.BuildSpecificationBuilder;
import it.uniud.atta.metamodel.verteximplementation.behavior.runner.Runner;
import it.uniud.atta.metamodel.verteximplementation.behavior.runner.RunnerBuilder;

public final class CustomWorkerPropertyBuilder extends PublishableBuilderBase<WorkerProperty> implements WorkerPropertyBuilder {

    public static enum Field implements EntityField {

        CHECK("check",EntityFieldKind.IDENTIFYING),
        BUILD("build",EntityFieldKind.OPTIONAL)
        ;
        
        private final String name;
        private final EntityFieldKind kind;
        
        Field(String name, EntityFieldKind kind) {
            this.name = name;
            this.kind = kind;
        }
            
        @Override
        public String toString() {
            return name;
        }
        
        @Override
        public EntityFieldKind getKind() {
            return kind;
        }
    }
    
	private final BuildSpecificationBuilder buildSpecificationBuilder;
	private final List<RunnerBuilder> checkerBuilders;
	
	public CustomWorkerPropertyBuilder(Map<String,Object> raw) {
        super(raw);
        
        buildSpecificationBuilder = acquireBuildSpecificationBuilder(raw);
        checkerBuilders = acquireCheckerBuilders(raw);
        addComponent(buildSpecificationBuilder);
        addComponents(checkerBuilders);
	}

	@Override
    protected WorkerProperty buildHandler() {
		
		List<Runner> checkers = new ArrayList<>();
		for (RunnerBuilder checkerBuilder : checkerBuilders) {
			checkers.add(checkerBuilder.build());
		}
	    return new CustomWorkerProperty(buildSpecificationBuilder.build(),checkers);
    }
	
	public List<RunnerBuilder> getCheckerBuilders() {
		return checkerBuilders;
	}
	
	public BuildSpecificationBuilder getBuildSpecificationBuilder() {
		return buildSpecificationBuilder;
	}
	
    private List<RunnerBuilder> acquireCheckerBuilders(Map<String, Object> raw) {
    	
        List<RunnerBuilder> result = new ArrayList<>();
    	
        Object rawRunnersObj = raw.get(Field.CHECK.toString());

        @SuppressWarnings("unchecked")
        List<Map<String,Object>> rawRunners = (List<Map<String,Object>>)rawRunnersObj;
        
        for (Map<String,Object> rawRunner : rawRunners) {
        	RunnerBuilder runnerBuilder = new RunnerBuilder(rawRunner);
            runnerBuilder.setWrapper(this.getWrapper());
            result.add(runnerBuilder);
        }

        return result;
    }
    
    private BuildSpecificationBuilder acquireBuildSpecificationBuilder(Map<String, Object> raw) {
    	
		BuildSpecificationBuilder result = null;
    	
    	if (raw.containsKey(Field.BUILD.toString())) {
    		@SuppressWarnings("unchecked")
            Map<String,Object> rawBuildSpecificationBuilder = (Map<String,Object>)raw.get(Field.BUILD.toString());
            result = new BuildSpecificationBuilder(rawBuildSpecificationBuilder);
    	}
    	
    	return result;
    }
}

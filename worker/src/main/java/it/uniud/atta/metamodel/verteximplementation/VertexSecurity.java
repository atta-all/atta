/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation;

import it.uniud.atta.common.lang.Named;

public enum VertexSecurity implements Named {

	FORCED_ON("true"), // Data security is forced to be On from the applied element downward the hierarchy
	FORCED_OFF("false"), // Data security is forced to be Off from the applied element downward the hierarchy
	UNFORCED("unforced") // Data security is not forced (default)
	;
	    
	private String name;
	    
	VertexSecurity(String name) {
	    this.name = name;
	}
	    
    @Override
    public String toString() {
        return name;
    }
    
    public static VertexSecurity fromString(String str) {
    	VertexSecurity result = null;
        for (VertexSecurity language: VertexSecurity.values()) {
             if (language.toString().equals(str)) {
                  result = language;
                  break;
             }
        }
        return result;
    }

    @Override
    public String getName() {
        return name;
    }
}

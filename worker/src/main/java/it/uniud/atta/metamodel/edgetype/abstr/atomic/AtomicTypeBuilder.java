/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.atomic;

import it.uniud.atta.metamodel.edgetype.abstr.AbstractTypeBuilder;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicAbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicTypeIssue;
import it.uniud.atta.metamodel.entity.EntityBuilderBase;

public final class AtomicTypeBuilder extends EntityBuilderBase<AtomicAbstractType> implements AbstractTypeBuilder<AtomicAbstractType> {

    private final String name;
    private final AtomicAbstractType value;
    
    public AtomicTypeBuilder(String name) {
        super();
        this.name = name;
        value = AtomicAbstractType.fromString(name);
    }
    
    @Override
    protected void checkingHandler() {
        if (value == null)
            surfaceIssues.append(AtomicTypeIssue.UNKNOWN_TYPE_VALUE,name);
    }
    
    @Override
    protected AtomicAbstractType buildHandler() {
        return value;
    }
}

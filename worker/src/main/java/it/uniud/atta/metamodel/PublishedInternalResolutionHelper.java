/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import it.uniud.atta.common.lang.StrictNameChecker;
import it.uniud.atta.common.math.FloydWarshall;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicAbstractType;
import it.uniud.atta.metamodel.edgetype.api.EdgeTypeBuilder;
import it.uniud.atta.metamodel.entity.BuildingStage;
import it.uniud.atta.metamodel.entity.EntityReference;
import it.uniud.atta.metamodel.entity.EntityLabel;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.verteximplementation.behavior.property.builtin.BuiltinProperty;
import it.uniud.atta.repository.RepositoryBuilder;

final public class PublishedInternalResolutionHelper {
    
    private PublishedInternalResolutionHelper() { }
    
    public static void check(PublishableBuilder<?> builder) {
    	if (!builder.getBuildingStage().greaterThan(BuildingStage.DESCRIBED) && builder.getWrapper() == builder) {
    		checkLabelNames(builder);
    		checkIncompatibleLabelsForReferences(builder);
	        checkMissingReferences(builder);
	        checkUnusedLabels(builder);
	        checkInternalCycles(builder);
	        checkUnrecognizedFields(builder);
    	}
    }
    
    private static void checkLabelNames(PublishableBuilder<?> builder) {
        
        for (EntityLabel typeLabel : builder.findLabelsFor(EdgeTypeBuilder.class))  {
            String typeName = typeLabel.getName();
            if (typeName.isEmpty())
                builder.appendIssue(PublishedInternalResolutionIssue.MISSING_TYPE_NAME);
            else if (!StrictNameChecker.validate(typeLabel.getName()))
            	builder.appendIssue(PublishedInternalResolutionIssue.TYPE_NAME_NOT_COMPLIANT,typeName);
            else if (AtomicAbstractType.fromString(typeLabel.getName()) != null) 
            	builder.appendIssue(PublishedInternalResolutionIssue.RESERVED_ATOMIC_TYPE_NAME,typeName);
        }
        	
        for (EntityLabel repoLabel : builder.findLabelsFor(RepositoryBuilder.class))
            if (repoLabel.getName().isEmpty())
                builder.appendIssue(PublishedInternalResolutionIssue.MISSING_REPOSITORY_NAME);
        
        for (EntityLabel locatorLabel : builder.findLabelsFor(LocatorBuilder.class)) {
        	String locatorName = locatorLabel.getName();
        	if (AtomicAbstractType.includes(locatorName))
        	    builder.appendIssue(PublishedInternalResolutionIssue.RESERVED_ATOMIC_TYPE_NAME,locatorName);
        	else if (BuiltinProperty.includes(locatorName))
        	    builder.appendIssue(PublishedInternalResolutionIssue.RESERVED_BUILTIN_PROPERTY_NAME,locatorName);
        }
    }
    
    private static void checkIncompatibleLabelsForReferences(PublishableBuilder<?> builder) {
        Set<EntityLabel> labels = builder.getLabels();
        for (EntityReference ref : builder.getReferences())
            for (EntityLabel label : labels)
                if (ref.hasNameEqualTo(label) && (!(label.getEntityBuilder() instanceof LocatorBuilder)))
                    if (!ref.getBuilderClass().isAssignableFrom(label.getEntityBuilder().getClass()))
                        builder.appendIssue(PublishedInternalResolutionIssue.MISMATCHING_LABEL_CONTENT,ref.getName());
    }
        
    private static void checkMissingReferences(PublishableBuilder<?> builder) {
        Set<EntityLabel> labels = builder.getLabels();
        for (EntityReference ref : builder.getReferences()) {
        	boolean found = false;
        	for (EntityLabel label : labels)
        		if (ref.hasNameEqualTo(label)) {
        			found = true;
        			break;
        		}
        	
        	if (!found)
        		builder.appendIssue(PublishedInternalResolutionIssue.UNBOUND_REFERENCE,ref.getName());
        }
    }
 
    private static void checkUnusedLabels(PublishableBuilder<?> builder) {
        Set<EntityReference> refs = builder.getReferences();
    	for (EntityLabel label : builder.getLabels()) {
    	    boolean found = false;
    	    for (EntityReference ref : refs)
                if (ref.hasNameEqualTo(label)) {
                    found = true;
                    break;
                }
    	    
    	    if (!found)
    	        builder.appendIssue(PublishedInternalResolutionIssue.UNUSED_LABEL,label.getName());
    	}  
    }
    
    private static void checkUnrecognizedFields(PublishableBuilder<?> builder) {
        
        
    }
    
    private static void checkInternalCycles(PublishableBuilder<?> builder) {
    	
        Map<String,Set<String>> dependencies = new HashMap<>();
        
        Set<String> allReferenceNames = new HashSet<>();
        
        for (EntityLabel label : builder.getLabels()) {
        	Set<String> referenceNames = new HashSet<>();
        	for (EntityReference ref : label.getEntityBuilder().getReferences())
        	    referenceNames.add(ref.getName());
            allReferenceNames.addAll(referenceNames);
            dependencies.put(label.getName(), referenceNames);        	
        }
        
        Map<String,Map<String,Double>> distances = new FloydWarshall<String>().getDistances(allReferenceNames,dependencies);
        
        Set<String> verticesInCycles = new HashSet<>();  
        for (String vertex : allReferenceNames) 
            if (!verticesInCycles.contains(vertex) && !distances.get(vertex).get(vertex).isInfinite()) {
                verticesInCycles.add(vertex);
                Set<String> verticesInThisCycle = new HashSet<>();
                for (String otherVertex : allReferenceNames)
                    if (!verticesInCycles.contains(otherVertex) && distances.get(vertex).get(otherVertex) > 0) {
                        verticesInCycles.add(otherVertex);
                        verticesInThisCycle.add(otherVertex);
                    }
                        
                builder.appendIssue(PublishedInternalResolutionIssue.CYCLIC_DEPENDENCY,Arrays.toString(verticesInThisCycle.toArray()));
            }
    }
}

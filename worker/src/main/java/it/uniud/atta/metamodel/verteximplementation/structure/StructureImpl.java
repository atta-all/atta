/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.structure;

import java.util.List;

import it.uniud.atta.metamodel.verteximplementation.VertexImplementationBase;
import it.uniud.atta.metamodel.verteximplementation.VertexSecurity;
import it.uniud.atta.metamodel.verteximplementation.api.Structure;
import it.uniud.atta.metamodel.verteximplementation.port.AbstractPort;

public class StructureImpl extends VertexImplementationBase implements Structure {

    protected StructureImpl(List<AbstractPort> ports, VertexSecurity security) {
        super(ports,security);
        // TODO Auto-generated constructor stub
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation;

import it.uniud.atta.metamodel.entity.EntityIssue;
import it.uniud.atta.metamodel.entity.RecognizedEntityKind;
import it.uniud.atta.metamodel.exception.UnhandledRecognizedEntityKindRuntimeException;
import it.uniud.atta.metamodel.verteximplementation.behavior.BehaviorBuilder;

import java.util.Map;
import java.util.Set;

public final class VertexImplementationBuilderFactory {
    
    public static VertexImplementationBuilder<?> makeFrom(Map<String,Object> raw) {

    	VertexImplementationBuilder<?> result = null;
        
        Set<RecognizedEntityKind> recognizedKinds = RecognizedEntityKind.identifyFrom(raw);
        
        switch (recognizedKinds.size()) {
            case 0:
                result = new ReferencedVertexImplementationBuilder("");
                result.appendIssue(EntityIssue.UNDEFINED_KIND);
                break;
            case 1:
                RecognizedEntityKind kind = recognizedKinds.iterator().next();
                switch (kind) {
                    case BEHAVIORAL_IMPLEMENTATION:
                        result = new BehaviorBuilder(raw);
                        break;
                    default:
                        throw new UnhandledRecognizedEntityKindRuntimeException(kind.toString());
                }
                break;
            default:
                result = new ReferencedVertexImplementationBuilder("");
                result.appendIssue(EntityIssue.AMBIGUOUS_KIND);
        }
        
        return result; 
    }
}

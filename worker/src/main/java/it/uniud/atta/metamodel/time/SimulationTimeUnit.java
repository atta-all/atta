/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.time;

import java.math.BigInteger;

public enum SimulationTimeUnit {

    FEMTOS("fs",1),
    PICOS("ps",1000),
    NANOS("ns",1000),
    MICROS("us",1000),
    MILLIS("ms",1000),
    SEC("ss",1000),
    MIN("mm",60),
    HOUR("hh",60),
    DAY("dd",24),
    ;
    
    private final String name;
    private final int scale;
    
    SimulationTimeUnit(String name, int scale) {
        this.name = name;
        this.scale = scale;
    }
    
    @Override
    public String toString() {
        return name;
    }
    
    public static SimulationTimeUnit fromString(String str) {
        
        SimulationTimeUnit result = null;
        for (SimulationTimeUnit kind: SimulationTimeUnit.values()) {
             if (kind.name.equals(str)) {
                  result = kind;
                  break;
             }
        }
            
        return result;
    }
    
    public double dividedBy(SimulationTimeUnit other) {
        double result = 1;
        
        int diff = this.ordinal() - other.ordinal(); 
        
        if (diff == 0)
            return result;
        else {
            SimulationTimeUnit[] values = values();
            if (diff < 0) {
                for (int i = this.ordinal()+1; i <= other.ordinal(); ++i)
                    result = result / values[i].scale;
            } else {
                for (int i = this.ordinal(); i > other.ordinal(); --i)
                    result = result * values[i].scale;
            }
        }
        
        return result;
    }
    
    public static String getNormalizedAccurateTimeFormatFor(BigInteger fsecs) {
        int ordinal;
        BigInteger amount = new BigInteger("1000");
        SimulationTimeUnit[] values = values();
        for (ordinal=1; ordinal< values.length; ++ordinal) {
            BigInteger[] divisionAndRem = fsecs.divideAndRemainder(amount);
            if (divisionAndRem[0].intValue() == 0 || (divisionAndRem[0].intValue() > 0 && divisionAndRem[1].intValue() > 0))
                break;
            else
                amount = amount.multiply(new BigInteger(String.valueOf(values[ordinal].scale)));
        }
        ordinal--;
        
        StringBuilder result = new StringBuilder(fsecs.divide(amount.divide(new BigInteger("1000"))).toString());
        result.append(values[ordinal]);
        
        return result.toString();
    }

    public static String getNormalizedApproximateTimeFormatFor(BigInteger fsecs) {
        int ordinal;
        BigInteger amount = new BigInteger("1000");
        SimulationTimeUnit[] values = values();
        for (ordinal=1; ordinal< values.length; ++ordinal) {
            if (fsecs.divide(amount).doubleValue() < 1)
                break;
            else
                amount = amount.multiply(new BigInteger(String.valueOf(values[ordinal].scale)));
        }
        ordinal--;
        
        StringBuilder result = new StringBuilder(fsecs.divide(amount.divide(new BigInteger("1000"))).toString());
        result.append(values[ordinal]);
        
        return result.toString();
    }
}

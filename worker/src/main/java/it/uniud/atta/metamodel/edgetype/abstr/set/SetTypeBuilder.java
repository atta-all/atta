/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.set;

import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.metamodel.PublishableBuilder;
import it.uniud.atta.metamodel.PublishableBuilderBase;
import it.uniud.atta.metamodel.PublishedBuilderFactory;
import it.uniud.atta.metamodel.edgetype.abstr.AbstractTypeBuilder;
import it.uniud.atta.metamodel.edgetype.abstr.ReferencedAbstractTypeBuilder;
import it.uniud.atta.metamodel.edgetype.abstr.api.AbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.SetType;
import it.uniud.atta.metamodel.edgetype.abstr.atomic.AtomicTypeBuilder;
import it.uniud.atta.metamodel.entity.EntityField;
import it.uniud.atta.metamodel.entity.EntityFieldKind;
import it.uniud.atta.metamodel.entity.EntityLabel;
import it.uniud.atta.metamodel.locator.LocatorBuilder;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

final public class SetTypeBuilder extends PublishableBuilderBase<SetType> implements AbstractTypeBuilder<SetType> {

    public static enum Field implements EntityField {

        SET("set",EntityFieldKind.IDENTIFYING),
        TYPE("type",EntityFieldKind.MANDATORY)
        ;
            
        private final String name;
        private final EntityFieldKind kind;
        
        Field(String name, EntityFieldKind kind) {
            this.name = name;
            this.kind = kind;
        }
            
        @Override
        public String toString() {
            return name;
        }
        
        @Override
        public EntityFieldKind getKind() {
            return kind;
        }
    }
    
    private AbstractTypeBuilder<?> typeBuilder;
    private final List<Object> values;

    @SuppressWarnings("unchecked")
    public SetTypeBuilder(Map<String, Object> raw) {
        super(raw);
        
        typeBuilder = acquireTypeBuilder(raw);
        values = (List<Object>)raw.get(Field.SET.toString());
        
        addComponent(typeBuilder);
    }
    
    public AbstractTypeBuilder<?> getTypeBuilder() {
        return typeBuilder;
    }
    
    public List<Object> getListedValues() {
        return values;
    }
    
    private AbstractTypeBuilder<?> acquireTypeBuilder(Map<String,Object> raw) {
    	
    	AbstractTypeBuilder<?> result = new AtomicTypeBuilder((String)raw.get(Field.TYPE.toString()));
    	result.checkCorrectness();
        if (result.getIssues().getSummary() == IssueLevel.ERROR)
            result = new ReferencedAbstractTypeBuilder((String)raw.get(Field.TYPE.toString()));
        return result;
    }
    
	@Override
    protected void internalResolutionHandler() {
        if (typeBuilder instanceof ReferencedAbstractTypeBuilder) {
        	removeComponent(typeBuilder);
            Set<EntityLabel> internals = getWrapper().findLabelsFor(AbstractTypeBuilder.class,((ReferencedAbstractTypeBuilder)typeBuilder).makeReference());
            if (!internals.isEmpty())
                typeBuilder = (AbstractTypeBuilder<?>)internals.iterator().next().getEntityBuilder();
            addComponent(typeBuilder);
        }
    }
	
	@Override
    protected void externalResolutionHandler() {
    	if (typeBuilder instanceof ReferencedAbstractTypeBuilder) {
        	removeComponent(typeBuilder);
            Set<EntityLabel> remotes = getWrapper().findLabelsFor(LocatorBuilder.class,((ReferencedAbstractTypeBuilder)typeBuilder).makeReference());
            PublishableBuilder<?> publishedBuilder = PublishedBuilderFactory.makeFrom((LocatorBuilder)remotes.iterator().next().getEntityBuilder());
            publishedBuilder.resolveExternalReferences();
            typeBuilder = (AbstractTypeBuilder<?>)publishedBuilder;
            addComponent(typeBuilder);
        }
    }
    
    @Override
    protected void checkingHandler() {
    	Set<Object> setValues = new HashSet<>(values);
    	
    	if (setValues.size() != values.size())
    		appendIssue(SetTypeIssue.DUPLICATE_VALUES);
    	if (typeBuilder.getIssues().getSummary() != IssueLevel.ERROR) {
    		AbstractType type = typeBuilder.build();
	    	for (Object value : values) {
	    		if (type.validate(value).getSummary() == IssueLevel.ERROR)
	    			appendIssue(SetTypeIssue.INVALID_VALUES);
	    	}
    	}
    }
    
    @Override
    protected SetType buildHandler() {
        return new SetTypeImpl(typeBuilder.build(),new HashSet<>(values));
    }
}

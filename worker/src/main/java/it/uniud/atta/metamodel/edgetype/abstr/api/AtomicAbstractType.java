/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.metamodel.edgetype.abstr.atomic.*;
import it.uniud.atta.metamodel.edgetype.api.EdgeDataConvertibility;
import it.uniud.atta.metamodel.edgetype.api.EdgeTypeConversionSafety;

public enum AtomicAbstractType implements AbstractType {
	    
	BOOL("bool",BoolTypeHandler.getInstance()), // Boolean
	UINT8("uint8",Uint8TypeHandler.getInstance()), // Unsigned 8 bit integer
	INT8("int8",Int8TypeHandler.getInstance()), // Signed 8 bit integer
	UINT16("uint16",Uint16TypeHandler.getInstance()), // Unsigned 16 bit integer
	INT16("int16",Int16TypeHandler.getInstance()), // Signed 16 bit integer
	UINT32("uint32",Uint32TypeHandler.getInstance()), // Unsigned 32 bit integer
	INT32("int32",Int32TypeHandler.getInstance()), // Signed 32 bit integer
	UINT64("uint64",Uint64TypeHandler.getInstance()), // Unsigned 64 bit integer
	INT64("int64",Int64TypeHandler.getInstance()), // Signed 64 bit integer
	FLOAT("float",FloatTypeHandler.getInstance()), // IEEE 754 single precision floating point
	DOUBLE("double",DoubleTypeHandler.getInstance()), // IEEE 754 double precision floating point
	INTEGER("integer",IntegerTypeHandler.getInstance()), // Alias for maximum precision integer
	DECIMAL("decimal",DecimalTypeHandler.getInstance()), // Alias for generic decimal number
	TEXT("text",TextTypeHandler.getInstance()); // UTF8 string
	    
	private String name;
	private AtomicAbstractTypeHandler handler;
	    
	AtomicAbstractType(String str, AtomicAbstractTypeHandler handler) {
	    this.name = str;
	    this.handler = handler;
	}
	    
    public static AtomicAbstractType fromString(String str) {
        
    	AtomicAbstractType result = null;
        for (AtomicAbstractType kind: AtomicAbstractType.values()) {
             if (kind.name.equals(str)) {
                  result = kind;
                  break;
             }
        }
            
        return result;
    }
    
    public static boolean includes(String str) {
    	return (fromString(str) != null);
    }
	
    @Override
    public String toString() {
    	return new StringBuilder("[")
    							 .append(name)
    							 .append(",")
    							 .append(handler.getCategory())
    							 .append("]")
    							 .toString();
    }
    
    public AtomicTypeCategory getCategory() {
    	return handler.getCategory();
    }
    
    public String getName() {
    	return name;
    }
    
    @Override
    public IssueCollection validate(Object valuation) {
    	
		IssueCollection result = new IssueCollection(valuation);
		
		if (valuation instanceof Map || valuation instanceof List)
			result.append(ValuationIssue.NOT_ATOMIC,valuation.toString());
		else {
		    if (!handler.validValue(valuation.toString()))
		    	result.append(ValuationIssue.INVALID_VALUE,valuation.toString());
		}
	    return result;
    }

    @Override
    public Map<AbstractType, EdgeTypeConversionSafety> findSubsetsEqualTo(AbstractType other) {
		Map<AbstractType, EdgeTypeConversionSafety> result = new HashMap<>();
		if (other instanceof AtomicAbstractType) {
			if (this == other)
				result.put(this, EdgeTypeConversionSafety.SAFE);
			else {
				EdgeDataConvertibility convertibility = handler.getConvertibilityToDifferent((AtomicAbstractType)other);
				if (convertibility != EdgeDataConvertibility.NONE)
					result.put(this, convertibility.getSafety());
			}
		}
		
	    return result;
    }
    
    @Override
    public EdgeDataConvertibility convertibilityTo(AbstractType other) {
		if (other instanceof AtomicAbstractType) {
			if (this == other)
				return EdgeDataConvertibility.EQUAL;
			else
				return handler.getConvertibilityToDifferent((AtomicAbstractType)other);
		} else
			return EdgeDataConvertibility.NONE; 
    }
}

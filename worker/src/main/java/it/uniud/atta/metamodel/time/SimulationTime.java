/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.time;

import it.uniud.atta.metamodel.entity.Entity;

import java.math.BigInteger;

public class SimulationTime implements Entity {

    private final BigInteger value;
    
    public SimulationTime(BigInteger value) {
        this.value = value;
    }

    public BigInteger getValue() {
        return value;
    }

    @Override
    public String toString() {
        return SimulationTimeUnit.getNormalizedAccurateTimeFormatFor(value);
    }
    
    public String getPreciseValue() {
        return toString();
    }
    
    public String getApproximateValue() {
        return SimulationTimeUnit.getNormalizedApproximateTimeFormatFor(value);
    }
    
    public SimulationTime add(SimulationTime other) {
        return new SimulationTime(value.add(other.value));
    }
    
}

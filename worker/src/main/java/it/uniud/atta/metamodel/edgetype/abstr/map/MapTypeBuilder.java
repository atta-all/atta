/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.map;

import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.metamodel.*;
import it.uniud.atta.metamodel.edgetype.abstr.AbstractTypeBuilder;
import it.uniud.atta.metamodel.edgetype.abstr.ReferencedAbstractTypeBuilder;
import it.uniud.atta.metamodel.edgetype.abstr.api.MapType;
import it.uniud.atta.metamodel.edgetype.abstr.atomic.AtomicTypeBuilder;
import it.uniud.atta.metamodel.entity.EntityField;
import it.uniud.atta.metamodel.entity.EntityFieldKind;
import it.uniud.atta.metamodel.entity.EntityLabel;
import it.uniud.atta.metamodel.locator.LocatorBuilder;

import java.util.Map;
import java.util.Set;

final public class MapTypeBuilder extends PublishableBuilderBase<MapType> implements AbstractTypeBuilder<MapType> {
    
    public static enum Field implements EntityField {

        KEYS("keys",EntityFieldKind.IDENTIFYING),
        VALUES("values",EntityFieldKind.IDENTIFYING)
        ;
            
        private final String name;
        private final EntityFieldKind kind;

        Field(String name, EntityFieldKind kind) {
            this.name = name;
            this.kind = kind;
        }
            
        @Override
        public String toString() {
            return name;
        }
        
        @Override
        public EntityFieldKind getKind() {
            return kind;
        }
    }
    
    private AbstractTypeBuilder<?> keyTypeBuilder;
    private AbstractTypeBuilder<?> valueTypeBuilder;

    public MapTypeBuilder(Map<String, Object> raw) {
        super(raw);
        
        keyTypeBuilder = setTypeBuilder(raw,Field.KEYS.toString());
        valueTypeBuilder = setTypeBuilder(raw,Field.VALUES.toString());
        
        addComponent(keyTypeBuilder);
        addComponent(valueTypeBuilder);
    }
    
    private AbstractTypeBuilder<?> setTypeBuilder(Map<String, Object> raw, String field) {
    	AbstractTypeBuilder<?> result = new AtomicTypeBuilder((String)raw.get(field));
    	result.checkCorrectness();
        if (result.getIssues().getSummary() == IssueLevel.ERROR)
            result = new ReferencedAbstractTypeBuilder((String)raw.get(field));
        return result;
    }
    
    public AbstractTypeBuilder<?> getKeyTypeDescriptor() {
        return keyTypeBuilder;
    }

    public AbstractTypeBuilder<?> getValueTypeDescriptor() {
        return valueTypeBuilder;
    }

	@Override
    protected void internalResolutionHandler() {
        keyTypeBuilder = resolveInternalType(keyTypeBuilder);
        valueTypeBuilder = resolveInternalType(valueTypeBuilder);
    }
	
	@Override
    protected void externalResolutionHandler() {
    	keyTypeBuilder = resolveExternalType(keyTypeBuilder);
    	valueTypeBuilder = resolveExternalType(valueTypeBuilder);
    }
    
    @Override
    protected MapType buildHandler() {    	
        return new MapTypeImpl(keyTypeBuilder.build(),valueTypeBuilder.build());
    }
    
    private AbstractTypeBuilder<?> resolveInternalType(AbstractTypeBuilder<?> typeBuilder) {
        
    	AbstractTypeBuilder<?> result = typeBuilder;
    	
        if (typeBuilder instanceof ReferencedAbstractTypeBuilder) {
        	
        	removeComponent(typeBuilder);
            
            Set<EntityLabel> internals = getWrapper().findLabelsFor(AbstractTypeBuilder.class,((ReferencedAbstractTypeBuilder)typeBuilder).makeReference());
            if (!internals.isEmpty())
                result = (AbstractTypeBuilder<?>)internals.iterator().next().getEntityBuilder();
            
            addComponent(result);
        }
        return result;
    }
    
    private AbstractTypeBuilder<?> resolveExternalType(AbstractTypeBuilder<?> typeBuilder) {
        
    	AbstractTypeBuilder<?> result = typeBuilder;
    	
        if (typeBuilder instanceof ReferencedAbstractTypeBuilder) {
        	
        	removeComponent(typeBuilder);
            
            Set<EntityLabel> remotes = getWrapper().findLabelsFor(LocatorBuilder.class,((ReferencedAbstractTypeBuilder)typeBuilder).makeReference());
            PublishableBuilder<?> publishedBuilder = PublishedBuilderFactory.makeFrom((LocatorBuilder)remotes.iterator().next().getEntityBuilder());
            publishedBuilder.resolveExternalReferences();
            result = (AbstractTypeBuilder<?>)publishedBuilder;
            addComponent(result);
        }
        return result;
    }
}

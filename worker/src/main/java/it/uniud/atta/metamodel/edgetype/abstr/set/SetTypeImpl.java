/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.set;

import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.metamodel.edgetype.abstr.AbstractTypeBase;
import it.uniud.atta.metamodel.edgetype.abstr.api.AbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.SetType;
import it.uniud.atta.metamodel.edgetype.abstr.api.ValuationIssue;
import it.uniud.atta.metamodel.edgetype.api.EdgeDataConvertibility;
import it.uniud.atta.metamodel.edgetype.api.EdgeTypeConversionSafety;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

final class SetTypeImpl extends AbstractTypeBase implements SetType {

    private final AbstractType type;
    private final Set<Object> values;
    
    SetTypeImpl(AbstractType type, Set<Object> values) {
        this.type = type;
        this.values = values;
    }
    
    @Override
    public Set<Object> getValues() {
        return values;
    }
    
    @Override
    public AbstractType getType() {
        return type;
    }

	@Override
    public IssueCollection validate(Object valuation) {
		if (!values.contains(valuation)) {
			IssueCollection result = new IssueCollection(valuation);
			result.append(ValuationIssue.VALUE_NOT_INCLUDED);
			return result;
		} else
			return type.validate(valuation);
    }

	@Override
    public Map<AbstractType, EdgeTypeConversionSafety> findSubsetsEqualTo(AbstractType other) {
		Map<AbstractType, EdgeTypeConversionSafety> result = new HashMap<>();
		if (other instanceof SetType) {
			if (this == other)
				result.put(this, EdgeTypeConversionSafety.SAFE);
			else if (type.convertibilityTo(((SetType)other).getType()) == EdgeDataConvertibility.EQUAL) {
				if (((SetType) other).getValues().containsAll(values))
					result.put(this, EdgeTypeConversionSafety.SAFE);
			}
		} else {
			switch (type.convertibilityTo(other)) {
			case EQUAL:
				result.put(this, EdgeTypeConversionSafety.SAFE);
				break;
			case EQUAL_UNSAFE:
				result.put(this, EdgeTypeConversionSafety.UNSAFE);
				break;
			default:
			}
		}
		
	    return result;
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.enumeration;

import it.uniud.atta.common.lang.StrictNameChecker;
import it.uniud.atta.metamodel.PublishableBuilderBase;
import it.uniud.atta.metamodel.edgetype.abstr.AbstractTypeBuilder;
import it.uniud.atta.metamodel.edgetype.abstr.api.EnumType;
import it.uniud.atta.metamodel.entity.EntityField;
import it.uniud.atta.metamodel.entity.EntityFieldKind;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class EnumTypeBuilder extends PublishableBuilderBase<EnumType> implements AbstractTypeBuilder<EnumType> {
    
    public static enum Field implements EntityField {

        ENUM("enum",EntityFieldKind.IDENTIFYING)
        ;
            
        private final String name;
        private final EntityFieldKind kind;

        Field(String name, EntityFieldKind kind) {
            this.name = name;
            this.kind = kind;
        }
            
        @Override
        public String toString() {
            return name;
        }
        
        @Override
        public EntityFieldKind getKind() {
            return kind;
        }
    }
    
    private final List<String> values;

    @SuppressWarnings("unchecked")
    public EnumTypeBuilder(Map<String, Object> raw) {
        super(raw);
        
        values = (List<String>)raw.get(Field.ENUM.toString());
    }
    
    public List<String> getValues() {
        return values;
    }
    
    @Override
    protected void checkingHandler() {
    	Set<String> setValues = new HashSet<>();
    	
    	for (String value : values) {
    		if (!StrictNameChecker.validate(value))
    			appendIssue(EnumTypeIssue.INVALID_VALUE,value);
    		if (setValues.contains(value))
    			appendIssue(EnumTypeIssue.DUPLICATE_VALUE,value);
    		setValues.add(value);
    	}
    }
    
    @Override
    protected EnumType buildHandler() {
    	
        return new EnumTypeImpl(new HashSet<>(values));
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior.constraint;

import java.util.Set;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.metamodel.PublishableBuilder;
import it.uniud.atta.metamodel.PublishedBuilderFactory;
import it.uniud.atta.metamodel.entity.EntityBuilderBase;
import it.uniud.atta.metamodel.entity.EntityLabel;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.verteximplementation.behavior.property.ReferencedWorkerPropertyBuilder;
import it.uniud.atta.metamodel.verteximplementation.behavior.property.WorkerPropertyBuilder;
import it.uniud.atta.metamodel.verteximplementation.behavior.property.builtin.BuiltinProperty;
import it.uniud.atta.metamodel.verteximplementation.behavior.property.builtin.BuiltinPropertyBuilder;

public final class WorkerConstraintBuilder extends EntityBuilderBase<WorkerConstraint> {

	private WorkerPropertyBuilder propertyBuilder;
	private final String expression;
	
	public WorkerConstraintBuilder(String property, String expression) {
        super();

        this.propertyBuilder = acquirePropertyBuilder(property);
        this.expression = expression;
        
        addComponent(propertyBuilder);
	}
	
	@Override
	protected void checkingHandler() {
		if (propertyBuilder instanceof BuiltinPropertyBuilder) {
		    propertyBuilder.checkCorrectness();
		    if (propertyBuilder.getIssues().getSummary() != IssueLevel.ERROR) {
    		    BuiltinProperty property = (BuiltinProperty)propertyBuilder.build();
    		    if (!property.validates(expression))
    		        appendIssue(WorkerConstraintIssue.VALUE_NOT_INCLUDED);
		    }
		}
		
		try {
			Pattern.compile(expression);
		} catch (PatternSyntaxException e) {
			appendIssue(WorkerConstraintIssue.INVALID_VALUE_PATTERN);
		}
	}

	@Override
    protected WorkerConstraint buildHandler() {
	    return new WorkerConstraintImpl(propertyBuilder.build(),Pattern.compile(expression));
    }
	
	public String getExpression() {
		return expression;
	}
	
	public WorkerPropertyBuilder getPropertyBuilder() {
		return propertyBuilder;
	}
    
    private WorkerPropertyBuilder acquirePropertyBuilder(String propertyName) {
    	
    	WorkerPropertyBuilder result = null;
    		
    	if (BuiltinProperty.includes(propertyName))
    		result = new BuiltinPropertyBuilder(propertyName);
        else
        	result = new ReferencedWorkerPropertyBuilder(propertyName);

    	return result;
    }

	@Override
    protected void externalResolutionHandler() {
	    if (propertyBuilder instanceof ReferencedWorkerPropertyBuilder) {
	    	removeComponent(propertyBuilder);
            Set<EntityLabel> remotes = getWrapper().findLabelsFor(LocatorBuilder.class,((ReferencedWorkerPropertyBuilder)propertyBuilder).makeReference());
            PublishableBuilder<?> publishedBuilder = PublishedBuilderFactory.makeFrom((LocatorBuilder)remotes.iterator().next().getEntityBuilder());
            publishedBuilder.resolveExternalReferences();
            propertyBuilder = (WorkerPropertyBuilder)publishedBuilder;
            addComponent(propertyBuilder);
	    }
    }
}

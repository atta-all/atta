/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.composite;

import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.metamodel.edgetype.abstr.AbstractTypeBase;
import it.uniud.atta.metamodel.edgetype.abstr.api.AbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.CompositeType;
import it.uniud.atta.metamodel.edgetype.abstr.api.Component;
import it.uniud.atta.metamodel.edgetype.abstr.api.ValuationIssue;
import it.uniud.atta.metamodel.edgetype.api.EdgeDataConvertibility;
import it.uniud.atta.metamodel.edgetype.api.EdgeTypeConversionSafety;
import it.uniud.atta.metamodel.edgetype.api.EdgeTypeInclusion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

class CompositeTypeImpl extends AbstractTypeBase implements CompositeType {

    private final List<Component> fields;
    
    CompositeTypeImpl(List<Component> fields) {
        this.fields = fields;
    }
    
    @Override
    public final Component getField(String key) {
        for (Component field : fields)
            if (field.getName().equals(key))
                return field;
        return null;
    }
    
    @Override
    public final List<Component> getFields() {
        return fields;
    }
    
	@Override
    public IssueCollection validate(Object valuation) {
		IssueCollection result = new IssueCollection(valuation);
		
		if (valuation instanceof List)
			result.append(ValuationIssue.IS_LIST);
		else if (!(valuation instanceof Map))
			result.append(ValuationIssue.IS_ATOMIC);
		else {
			@SuppressWarnings("unchecked")
	        Map<String,Object> entrySet = (Map<String,Object>)valuation;
			Set<String> valuationFieldNames = entrySet.keySet();
			for (Component field : fields) {
				if (!field.getArraySpecification().isOptional() && !valuationFieldNames.contains(field.getName()))
					result.append(ValuationIssue.MISSING_FIELD,field.getName());
				else if (valuationFieldNames.contains(field.getName())) {
					Object fieldValue = entrySet.get(field.getName());
					if (isValueStructureCorrect(field, fieldValue, result) )
						validateAll(field.getArraySpecification().getNumDimensions()-1,field.getType(),fieldValue,result);
				}
			}
			checkUnrecognizedFields(valuationFieldNames, result);
		}
	    
	    return result;
    }
	
	private void checkUnrecognizedFields(Set<String> valuationFieldNames, IssueCollection issues) {
		if (valuationFieldNames.size() != fields.size()) {
			for (String valuationFieldName : valuationFieldNames) {
				boolean found = false;
				for (Component field : fields)
					if (field.getName().equals(valuationFieldName)) {
						found = true;
						break;
					}
				if (!found)
					issues.append(ValuationIssue.UNRECOGNIZED_FIELD,valuationFieldName);
			}
		}
	}
	
	private boolean isValueStructureCorrect(Component field, Object fieldValue, IssueCollection issues) {
		
		int[] sizes = getSizes(fieldValue);
		
		if (!field.getArraySpecification().isSingleton() && isJagged(sizes.length-1, fieldValue, sizes)) {
			issues.append(ValuationIssue.ARRAY_IS_JAGGED);
			return false;
		}
		
		if (!field.getArraySpecification().covers(sizes)) {
			if (field.getArraySpecification().isSingleton())
				issues.append(ValuationIssue.VALUE_NOT_SINGLETON);
			else
				issues.append(ValuationIssue.INVALID_ARRAY_SIZES);
			return false;
		} else
			return true;
	}
	
	private boolean isJagged(int currentDim, Object fieldValue, int[] sizes) {
		
		if (!(fieldValue instanceof List))
			return true;

		@SuppressWarnings("unchecked")
        List<Object> lst = (List<Object>)fieldValue;
		
		if (currentDim == 0)
			return lst.size() != sizes[0];
		else {
			for (int i=0; i<lst.size(); ++i)
				if (isJagged(currentDim-1, lst.get(i), sizes))
					return true;
					
			return false;
		}
	}
	
	private int[] getSizes(Object fieldValue) {
		
		Object val = fieldValue;
		
		List<Integer> sizes = new ArrayList<>();
		
		while (val instanceof List) {
			@SuppressWarnings("unchecked")
            List<Object> lst = (List<Object>)val;
			sizes.add(lst.size());
			val = lst.get(0);
		}
		
		int[] result = new int[sizes.size()];
		
		for (int i=0; i<sizes.size(); ++i)
			result[i] = sizes.get(sizes.size()-i-1);
		
		return result;
	}
	
	private void validateAll(int currentDim, AbstractType type, Object fieldValue, IssueCollection issues) {
		if (currentDim <= 0)
			if (!(fieldValue instanceof List))
				issues.appendAll(type.validate(fieldValue));
		else {
			@SuppressWarnings("unchecked")
	        List<Object> lst = (List<Object>)fieldValue;
			for (int i=0; i<lst.size(); ++i)
				validateAll(currentDim-1, type, lst.get(i), issues);
		}
	}
	
	@Override
    public Map<AbstractType, EdgeTypeConversionSafety> findSubsetsEqualTo(AbstractType other) {
		Map<AbstractType, EdgeTypeConversionSafety> result = new HashMap<>();
		if (other instanceof CompositeType) {
			if (this == other)
				result.put(this, EdgeTypeConversionSafety.SAFE);
			else {
				
				CompositeType otherComposite = (CompositeType)other;
				List<Map<Component,EdgeDataConvertibility>> convertibleFields = new ArrayList<>();
				boolean inconvertible = false;
				for (Component otherField : otherComposite.getFields()) {
					Map<Component,EdgeDataConvertibility> convertibles = this.findFieldsConvertibleInto(otherField);
					if (convertibles.isEmpty()) {
						inconvertible = true;
						break;
					}
					convertibleFields.add(convertibles); 
				}
					
				if (!inconvertible)
					result.putAll(getCombinations(convertibleFields));
			}
		}
		
	    return result;
    }
	
	private Map<Component,EdgeDataConvertibility> findFieldsConvertibleInto(Component dstField) {
		Map<Component,EdgeDataConvertibility> result = new HashMap<>();
		for (Component srcField : fields)
			if (srcField.getArraySpecification().isIncludedWithin(dstField.getArraySpecification())) {
				EdgeDataConvertibility convertibility = srcField.getType().convertibilityTo(dstField.getType());  
				if (convertibility.getInclusion() == EdgeTypeInclusion.EQUAL)
					result.put(srcField,convertibility);
			}
		return result;
	}
	
	private Map<CompositeType,EdgeTypeConversionSafety> getCombinations(List<Map<Component,EdgeDataConvertibility>> convertibleFields) {
		Map<CompositeType,EdgeTypeConversionSafety> result = new HashMap<>();
		
		int numTargetFields = convertibleFields.size();
		
		int[] sizes = new int[numTargetFields];
		for (int i=0; i<numTargetFields; ++i)
			sizes[i] = convertibleFields.get(i).size();
		
		List<Component[]> sequences = new ArrayList<>();
		
		int[] dividers = new int[numTargetFields];
		for (int i=0; i<numTargetFields; ++i) {
			int product = 1;
			for (int j=0; j<i; ++j)
				product *= sizes[j];
			dividers[i] = product;
		}
		
		int numSequences = dividers[numTargetFields-1]*sizes[numTargetFields-1];
		for (int s=0; s<numSequences; ++s) {
			Component[] sequence = new Component[numTargetFields]; 
			for (int i=0; i<numTargetFields; ++i) {
				int index = ((s/dividers[i]) % sizes[i]);
				sequence[i] = new ArrayList<Component>(convertibleFields.get(i).keySet()).get(index);
			}
			sequences.add(sequence);
		}
		
		for (Component[] sequence : sequences) {
			Set<Component> availableFields = new HashSet<>(fields); 
			
			boolean validEntry = true;
			boolean isSafe = true;
			for (int i=0; i<numTargetFields; ++i) {
				Component field = sequence[i];
				if (!availableFields.contains(field)) {
					validEntry = false;
					break;
				} else
					availableFields.remove(field);
					
				EdgeDataConvertibility convertibility = convertibleFields.get(i).get(field);
				if (convertibility.getSafety() == EdgeTypeConversionSafety.UNSAFE)
					isSafe = false;
				
			}
			
			if (validEntry)
				result.put(new CompositeTypeImpl(Arrays.asList(sequence)), (isSafe ? EdgeTypeConversionSafety.SAFE : EdgeTypeConversionSafety.UNSAFE));
		}
		
		return result;
	}
}

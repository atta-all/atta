/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.composite;

import java.util.Map;

import it.uniud.atta.common.interval.ArraySpecification;
import it.uniud.atta.metamodel.edgetype.abstr.api.AbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.Component;
import it.uniud.atta.metamodel.edgetype.api.EdgeDataConvertibility;
import it.uniud.atta.metamodel.edgetype.api.EdgeTypeConversionSafety;

final class ComponentImpl implements Component {
    
    private final String name;
    private final AbstractType type;
    private final ArraySpecification arraySpecification;
    
    ComponentImpl(String name, AbstractType type, ArraySpecification arraySpecification) {
        this.name = name;
        this.type = type;
        this.arraySpecification = arraySpecification;
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    @Override
    public AbstractType getType() {
        return type;
    }
    
    @Override
    public ArraySpecification getArraySpecification() {
        return arraySpecification;
    }
    
    @Override
    public String toString() {
    	StringBuilder strb = new StringBuilder("[");
    	strb.append(name)
    		.append(",")
    		.append(type);
    	if (!arraySpecification.isSingleton())
    		strb.append(",").append(arraySpecification);
    	strb.append("]");
    	return strb.toString();
    }
    
    public final EdgeDataConvertibility convertibilityTo(Component other) {
    	if (arraySpecification.isIncludedWithin(other.getArraySpecification())) {
			Map<AbstractType, EdgeTypeConversionSafety> subsetsEqual = type.findSubsetsEqualTo(other.getType());
			
			switch (subsetsEqual.size()) {
			case 0:
				return EdgeDataConvertibility.NONE;
			case 1:
				if (subsetsEqual.values().contains(EdgeTypeConversionSafety.SAFE))
					return (subsetsEqual.containsKey(this) ? EdgeDataConvertibility.EQUAL : EdgeDataConvertibility.SUBSET);
				else
					return (subsetsEqual.containsKey(this) ? EdgeDataConvertibility.EQUAL_UNSAFE : EdgeDataConvertibility.SUBSET_UNSAFE);
			default:
				return (subsetsEqual.values().contains(EdgeTypeConversionSafety.SAFE)) ? 
						EdgeDataConvertibility.SUBSET : EdgeDataConvertibility.SUBSET_UNSAFE;
			}
    	} else
    		return EdgeDataConvertibility.NONE;
    }
}

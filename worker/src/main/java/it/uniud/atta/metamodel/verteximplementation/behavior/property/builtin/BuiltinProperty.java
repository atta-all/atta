/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior.property.builtin;

import it.uniud.atta.metamodel.verteximplementation.behavior.property.WorkerProperty;

public enum BuiltinProperty implements WorkerProperty {
	    
	OS_KIND("os.kind",OsKindChecker.getInstance()), 
	OS_VERSION("os.version",OsVersionChecker.getInstance()),
	OS_ARCH("os.arch",OsArchChecker.getInstance()),
	NUM_CORES("num_cores",NumLogicalCoresChecker.getInstance()),
	;
	    
	private String name;
	private BuiltinPropertyChecker checker;
	    
	BuiltinProperty(String str, BuiltinPropertyChecker checker) {
	    this.name = str;
	    this.checker = checker;
	}
	    
    public static BuiltinProperty fromString(String str) {
        
    	BuiltinProperty result = null;
        for (BuiltinProperty kind: BuiltinProperty.values()) {
             if (kind.name.equals(str)) {
                  result = kind;
                  break;
             }
        }
            
        return result;
    }
    
    public boolean validates(String value) {
        return checker.validates(value);
    }
    
    public static boolean includes(String str) {
    	return (fromString(str) != null);
    }
	
    @Override
    public String toString() {
    	return name;
    }

	@Override
    public String check() {
	    return checker.check();
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior.build;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.uniud.atta.metamodel.entity.EntityBuilderBase;
import it.uniud.atta.metamodel.entity.EntityField;
import it.uniud.atta.metamodel.entity.EntityFieldKind;
import it.uniud.atta.metamodel.verteximplementation.behavior.runner.Runner;
import it.uniud.atta.metamodel.verteximplementation.behavior.runner.RunnerBuilder;

public final class BuildSpecificationBuilder extends EntityBuilderBase<BuildSpecification> {
	
    public static enum Field implements EntityField {

        KIND("kind",EntityFieldKind.MANDATORY),
        RUNNERS("runners",EntityFieldKind.MANDATORY)
        ;
        
        private final String name;
        private final EntityFieldKind kind;
        
        Field(String name, EntityFieldKind kind) {
            this.name = name;
            this.kind = kind;
        }
            
        @Override
        public String toString() {
            return name;
        }
        
        @Override
        public EntityFieldKind getKind() {
            return kind;
        }
    }
    
	private final BuildKind kind;
	private final List<RunnerBuilder> runnerBuilders;
	
	public BuildSpecificationBuilder(Map<String,Object> raw) {
    	super();
		
        kind = acquireKind(raw);
        runnerBuilders = acquireRunnerBuilders(raw);
        
        addComponents(runnerBuilders);
	}
	
	public final List<RunnerBuilder> getRunnerBuilders() {
		return runnerBuilders;
	}
	
	public BuildKind getBuildKind() {
		return kind;
	}
    
    private BuildKind acquireKind(Map<String,Object> raw) {
    	Object kindObj = raw.get(Field.KIND.toString());
		return BuildKind.fromString((String)kindObj);
    }
    
    private List<RunnerBuilder> acquireRunnerBuilders(Map<String, Object> raw) {
    	
        List<RunnerBuilder> result = new ArrayList<>();
    	
        Object rawRunnersObj = raw.get(Field.RUNNERS.toString());
    	
        if (rawRunnersObj != null) {
	        @SuppressWarnings("unchecked")
	        List<Map<String,Object>> rawRunners = (List<Map<String,Object>>)rawRunnersObj;
	        
	        for (Map<String,Object> rawRunner : rawRunners) {
	        	RunnerBuilder runnerBuilder = new RunnerBuilder(rawRunner);
	            runnerBuilder.setWrapper(this.getWrapper());
	            result.add(runnerBuilder);
	        }
        } 
        return result;
    }
    
    @Override
    protected void checkingHandler() {
    	if (kind == null)
    		appendIssue(BuildSpecificationIssue.UNRECOGNIZED_KIND);
    }
	
	@Override
    protected BuildSpecification buildHandler() {
		
        List<Runner> runners = new ArrayList<>();
        
        for (RunnerBuilder runnerBuilder : getRunnerBuilders())
            runners.add(runnerBuilder.build());
        
        switch (kind) {
        case MAVEN:
        	return new MavenSpecification(runners);
        default:
        	throw new BuilderKindUnhandledRuntimeException(kind);
        }
    }
}

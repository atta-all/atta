/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior;

import java.util.List;

import it.uniud.atta.metamodel.verteximplementation.VertexImplementationBase;
import it.uniud.atta.metamodel.verteximplementation.VertexSecurity;
import it.uniud.atta.metamodel.verteximplementation.api.Behavior;
import it.uniud.atta.metamodel.verteximplementation.behavior.build.BuildSpecification;
import it.uniud.atta.metamodel.verteximplementation.port.ConcretePort;

public final class BehaviorImpl extends VertexImplementationBase implements Behavior {

	private final BehaviorLanguage language;	
	private final BuildSpecification buildSpecification;
	
    protected BehaviorImpl(List<ConcretePort> ports, VertexSecurity security, BehaviorLanguage language, BuildSpecification buildSpecification) {
        super(ports,security);
        
        this.language = language;
        this.buildSpecification = buildSpecification;
    }
    
    @Override
    public BehaviorLanguage getLanguage() {
    	return language;
    }
    
    @Override
    public BuildSpecification getBuildSpecification() {
    	return buildSpecification;
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr;

import it.uniud.atta.metamodel.edgetype.abstr.api.AbstractType;
import it.uniud.atta.metamodel.edgetype.api.EdgeDataConvertibility;
import it.uniud.atta.metamodel.edgetype.api.EdgeTypeConversionSafety;

import java.util.Map;

public abstract class AbstractTypeBase implements AbstractType {

	@Override
    public final EdgeDataConvertibility convertibilityTo(AbstractType other) {
		Map<AbstractType, EdgeTypeConversionSafety> subsetsEqual = findSubsetsEqualTo(other);
		
		switch (subsetsEqual.size()) {
		case 0:
			return EdgeDataConvertibility.NONE;
		case 1:
			if (subsetsEqual.values().contains(EdgeTypeConversionSafety.SAFE))
				return (subsetsEqual.containsKey(this) ? EdgeDataConvertibility.EQUAL : EdgeDataConvertibility.SUBSET);
			else
				return (subsetsEqual.containsKey(this) ? EdgeDataConvertibility.EQUAL_UNSAFE : EdgeDataConvertibility.SUBSET_UNSAFE);
		default:
			return (subsetsEqual.values().contains(EdgeTypeConversionSafety.SAFE)) ? 
					EdgeDataConvertibility.SUBSET_AMBIGUOUS : EdgeDataConvertibility.SUBSET_AMBIGUOUS_UNSAFE;
		}
    }
}

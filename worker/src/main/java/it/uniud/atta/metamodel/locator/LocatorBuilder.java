/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.locator;

import it.uniud.atta.metamodel.entity.EntityBuilderBase;
import it.uniud.atta.metamodel.entity.EntityField;
import it.uniud.atta.metamodel.entity.EntityFieldKind;
import it.uniud.atta.metamodel.entity.EntityLabel;
import it.uniud.atta.repository.RepositoryBuilder;
import it.uniud.atta.repository.RepositoryBuilderImpl;
import it.uniud.atta.repository.ReferencedRepositoryBuilder;

import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class LocatorBuilder extends EntityBuilderBase<Locator> {
    
    public static enum Field implements EntityField {

        REPO("repo",EntityFieldKind.IDENTIFYING),
        PATH("path",EntityFieldKind.IDENTIFYING),
        REVISION("revision",EntityFieldKind.OPTIONAL);
        
        private final String name;
        private final EntityFieldKind kind;
        
        Field(String name, EntityFieldKind kind) {
            this.name = name;
            this.kind = kind;
        }
            
        @Override
        public String toString() {
            return name;
        }
        
        @Override
        public EntityFieldKind getKind() {
            return kind;
        }
    }
    
    private RepositoryBuilder repositoryBuilder;
    private final String path;
    private final String revision;
    
    LocatorBuilder(RepositoryBuilder repoBuilder, String path, String revision) {
        super();
        this.repositoryBuilder = repoBuilder;
        this.path = path;
        this.revision = revision;
    }
    
    public LocatorBuilder(Map<String,Object> raw) {
    	super();
    	this.repositoryBuilder = acquireRepositoryBuilder(raw);
        this.path = acquirePath(raw);
        this.revision = acquireRevision(raw);
        
        addComponent(repositoryBuilder);
    }
    
    public RepositoryBuilder getRepositoryBuilder() {
    	return repositoryBuilder;
    }
    
    private RepositoryBuilder acquireRepositoryBuilder(Map<String,Object> raw) {
        
        RepositoryBuilder result = null;
        
        Object repoObj = raw.get(Field.REPO.toString());
        
        if (repoObj != null) {
            if (repoObj instanceof Map) {
            	@SuppressWarnings("unchecked")
            	Map<String,Object> internalRawRepo = (Map<String,Object>)repoObj;
            	result = new RepositoryBuilderImpl(internalRawRepo);
            } else
            	result = new ReferencedRepositoryBuilder((String)repoObj);
        }
        
        return result;
    }
    
    private String acquirePath(Map<String,Object> raw) {
        String result = null;
        
        Object pathObj = raw.get(Field.PATH.toString());
        if (pathObj != null)
            result = pathObj.toString();
        
        return result;
    }
    
    private String acquireRevision(Map<String,Object> raw) {
        Object revisionObj = raw.get(Field.REVISION.toString());
        
        return (revisionObj == null ? "" : revisionObj.toString());
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17,31).append(repositoryBuilder).append(path).append(revision).build();
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof LocatorBuilder))
            return false;
        LocatorBuilder otherRemoteArtifactBuilder = (LocatorBuilder)other;
        
        if (!this.repositoryBuilder.equals(otherRemoteArtifactBuilder.repositoryBuilder))
            return false;
        
        if (!this.path.equals(otherRemoteArtifactBuilder.path))
            return false;

        if (!this.revision.equals(otherRemoteArtifactBuilder.revision))
            return false;
        
        return true;
    }

	@Override
    protected void internalResolutionHandler() {
        if (repositoryBuilder instanceof ReferencedRepositoryBuilder) {
        	removeComponent(repositoryBuilder);
            Set<EntityLabel> repoLabels = getWrapper().findLabelsFor(RepositoryBuilder.class, ((ReferencedRepositoryBuilder)repositoryBuilder).makeReference());
            repositoryBuilder = (RepositoryBuilder)repoLabels.iterator().next().getEntityBuilder();
            addComponent(repositoryBuilder);
        }
    }
	
	@Override
	protected void checkingHandler() {
		if (path == null)
			appendIssue(LocatorIssue.MISSING_PATH);
		if (repositoryBuilder == null)
			appendIssue(LocatorIssue.MISSING_REPOSITORY);
	}
    
    @Override
    protected Locator buildHandler() {
        return new LocatorImpl(repositoryBuilder.build(),path,revision);
    }
}

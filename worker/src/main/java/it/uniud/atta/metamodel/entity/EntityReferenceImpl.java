/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.entity;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class EntityReferenceImpl implements EntityReference {

    private final String name;
    private final Class<?> builderClass;

    @Override
    public String getName() {
        return name;
    }
    
    @Override
    public Class<?> getBuilderClass() {
        return builderClass;
    }
    
    public EntityReferenceImpl(String name, Class<?> builderClass) {
        this.name = name;
        this.builderClass = builderClass;
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17,31).append(name).build();
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof EntityReferenceImpl))
            return false;
        EntityReferenceImpl otherRef = (EntityReferenceImpl)other;
        
        return this.name.equals(otherRef.name);
    }

	@Override
    public boolean hasNameEqualTo(EntityLabel label) {
	    return this.name.equals(label.getName());
    }
}

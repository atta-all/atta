/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.common.yaml.AmbiguousDescriptorFileException;
import it.uniud.atta.common.yaml.DescriptorFileNotFoundException;
import it.uniud.atta.common.yaml.MalformedDescriptorException;
import it.uniud.atta.common.yaml.YamlLoader;
import it.uniud.atta.metamodel.entity.EntityBuilder;
import it.uniud.atta.metamodel.entity.EntityField;
import it.uniud.atta.metamodel.entity.EntityFieldKind;
import it.uniud.atta.metamodel.entity.EntityIssue;
import it.uniud.atta.metamodel.entity.RecognizedEntityKind;
import it.uniud.atta.metamodel.exception.EntityBuilderHasErrorsRuntimeException;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.locator.PublishedIssue;
import it.uniud.atta.repository.api.Checkout;
import it.uniud.atta.repository.exception.*;

public final class PublishedBuilderFactory {
    
	public static PublishableBuilder<?> makeFrom(LocatorBuilder remoteBuilder) {
		
		PublishableBuilder<?> result = PublishedBuilderCache.getInstance().get(remoteBuilder);
        if (result != null)
        	return result;
        
	    if (remoteBuilder.getIssues().getSummary() == IssueLevel.ERROR)
	        throw new EntityBuilderHasErrorsRuntimeException(remoteBuilder.getIssues().toString());
	    
        try {
            Checkout checkout = remoteBuilder.build().retrieve();
            result = (PublishableBuilder<?>)makeFrom(YamlLoader.getInstance().loadFromPathAsMap(checkout.getPath()),true);
        } catch (MalformedDescriptorException e) {
            result = new EmptyPublishedBuilder();
            result.appendIssue(PublishedIssue.MALFORMED_DESCRIPTOR_FILE,remoteBuilder.toString());
        } catch (DescriptorFileNotFoundException e) {
            result = new EmptyPublishedBuilder();
            result.appendIssue(PublishedIssue.DESCRIPTOR_FILE_NOT_FOUND,remoteBuilder.toString());
        } catch (AmbiguousDescriptorFileException e) {
            result = new EmptyPublishedBuilder();
            result.appendIssue(PublishedIssue.AMBIGUOUS_DESCRIPTOR_FILE,remoteBuilder.toString());        
        } catch (RepositoryPathMissingException e) {
            result = new EmptyPublishedBuilder();
            result.appendIssue(PublishedIssue.PATH_NOT_FOUND,remoteBuilder.toString());
        } catch (UnknownRevisionException e) {
            result = new EmptyPublishedBuilder();
            result.appendIssue(PublishedIssue.REVISION_NOT_FOUND,remoteBuilder.toString());
        } catch (NoAvailableAddressException e) {
            result = new EmptyPublishedBuilder();
            result.appendIssue(PublishedIssue.UNREACHABLE_SOURCES,remoteBuilder.toString());
        } catch (RepositoryOperationException e) {
            result = new EmptyPublishedBuilder();
            result.appendIssue(PublishedIssue.MISCELLANEOUS_ERROR,remoteBuilder.toString());
        }
        
        result.setWrapper(result);
        result.setSourceLocatorBuilder(remoteBuilder);
        PublishedBuilderCache.getInstance().put(remoteBuilder, result);
        
        return result;
	}
	
	public static EntityBuilder<?> makeFrom(Map<String,Object> raw) {
	    return makeFrom(raw,false);
	}
	
    public static EntityBuilder<?> makeFrom(Map<String,Object> raw, boolean isPublished) {

        EntityBuilder<?> result = null;
        
        Set<RecognizedEntityKind> publishableKinds = RecognizedEntityKind.identifyFrom(raw);
        
        switch (publishableKinds.size()) {
            case 0:
                result = new EmptyPublishedBuilder();
                result.appendIssue(EntityIssue.UNDEFINED_KIND);
                break;
            case 1:
                RecognizedEntityKind kind = publishableKinds.iterator().next();
                
                Set<String> missingFieldNames = getMissingFieldNames(raw, kind);
                Set<String> unrecognizedFieldNames = getUnrecognizedFieldNames(raw, kind, isPublished);
                
                if (!missingFieldNames.isEmpty() || !unrecognizedFieldNames.isEmpty()) {
                    result = new EmptyPublishedBuilder();
                    for (String fieldName : missingFieldNames)
                        result.appendIssue(EntityIssue.MISSING_FIELD, fieldName);
                    for (String fieldName : unrecognizedFieldNames)
                        result.appendIssue(EntityIssue.UNRECOGNIZED_FIELD, fieldName);    
                } else
                    result = kind.constructFrom(raw);
                break;
            default:
                result = new EmptyPublishedBuilder();
                result.appendIssue(EntityIssue.AMBIGUOUS_KIND);
        }
        
        return result; 
    }
    
    private static Set<String> getMissingFieldNames(Map<String,Object> raw, RecognizedEntityKind kind) {
        
        Set<String> result = new HashSet<>();
        
        Set<EntityField> mandatoryFields = kind.getFields(EntityFieldKind.MANDATORY);
        
        for (EntityField field : mandatoryFields) {
            if (raw.get(field.toString()) == null)
                result.add(field.toString());
        }
        
        return result;
    }
    
    private static Set<String> getUnrecognizedFieldNames(Map<String,Object> raw, RecognizedEntityKind kind, boolean isPublished) {
        
        Set<String> result = new HashSet<>();
        
        Set<EntityField> fields = kind.getAllFields();
        if (isPublished)
            fields.add(PublishableBaseDescriptorField.LABELS);
        
        Set<String> fieldNames = new HashSet<>();
        for (EntityField field : fields)
            fieldNames.add(field.toString());
        
        for (String key : raw.keySet())
            if (!fieldNames.contains(key))
                result.add(key);
        
        return result;
    }
}

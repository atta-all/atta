/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.locator;

import it.uniud.atta.common.issue.Issue;
import it.uniud.atta.common.issue.IssueLevel;

public enum PublishedIssue implements Issue {

    UNREACHABLE_SOURCES, // If was not possible to reach the address of the published on any source
    REVISION_NOT_FOUND, // The declared revision has not been found
    PATH_NOT_FOUND, // The declared path has not been found
    DESCRIPTOR_FILE_NOT_FOUND, // No descriptor file exists at the supposed location 
    AMBIGUOUS_DESCRIPTOR_FILE, // There are more than one descriptor file at the given path
    MALFORMED_DESCRIPTOR_FILE, // The file could not be successfully parsed
    MISCELLANEOUS_ERROR, // Anything else not currently handled explicitly
    ;
    
    private final IssueLevel level;
    
    PublishedIssue() {
        this(IssueLevel.ERROR);
    }
    
    PublishedIssue(IssueLevel level) {
        this.level = level;
    }
    
    @Override
    public IssueLevel getLevel() {
        return level;
    }
}

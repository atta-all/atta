/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation;

import it.uniud.atta.metamodel.PublishableBuilderBase;
import it.uniud.atta.metamodel.entity.EntityField;
import it.uniud.atta.metamodel.entity.EntityFieldKind;
import it.uniud.atta.metamodel.verteximplementation.api.VertexImplementation;
import it.uniud.atta.metamodel.verteximplementation.port.api.PortBuilder;

import java.util.List;
import java.util.Map;

abstract public class VertexImplementationBuilderBase<T extends VertexImplementation> extends PublishableBuilderBase<T> implements VertexImplementationBuilder<T> {

    public static enum Field implements EntityField {

        PORTS("ports",EntityFieldKind.MANDATORY),
        SECURITY("security",EntityFieldKind.OPTIONAL),
        ;
            
        private final String name;
        private final EntityFieldKind kind;
        
        Field(String name, EntityFieldKind kind) {
            this.name = name;
            this.kind = kind;
        }
            
        @Override
        public String toString() {
            return name;
        }
        
        @Override
        public EntityFieldKind getKind() {
            return kind;
        }
    }

    private final VertexSecurity security;
    protected List<PortBuilder<?>> portBuilders;
    
    public VertexImplementationBuilderBase(Map<String, Object> raw) {
        super(raw);
        
        security = acquireSecurity(raw);
    }
    
    public final VertexSecurity getSecurity() {
    	return security;
    }
    
    @Override
    public final List<PortBuilder<?>> getPortBuilders() {
    	return portBuilders;
    }
    
    private final VertexSecurity acquireSecurity(Map<String, Object> raw) {
        
        VertexSecurity result = VertexSecurity.UNFORCED;
        
        Object securityObj = raw.get(Field.SECURITY.toString());
        
        if (securityObj != null) {
        	VertexSecurity parsedValue = VertexSecurity.fromString(securityObj.toString());
            if (parsedValue == null)
            	result = null;
            else
            	result = parsedValue;
        }
        return result;
    }
    
    @Override
    protected void checkingHandler() {
    	if (security == null)
    		surfaceIssues.append(VertexImplementationIssue.UNRECOGNIZED_SECURITY_VALUE);
    }
	
    protected abstract List<PortBuilder<?>> acquirePortBuilders(Map<String, Object> raw);
}

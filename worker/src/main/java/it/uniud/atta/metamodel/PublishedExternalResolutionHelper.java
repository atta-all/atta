/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel;

import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.common.math.FloydWarshall;
import it.uniud.atta.metamodel.entity.BuildingStage;
import it.uniud.atta.metamodel.entity.EntityReference;
import it.uniud.atta.metamodel.entity.EntityLabel;
import it.uniud.atta.metamodel.locator.LocatorBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class PublishedExternalResolutionHelper {
    
    private PublishedExternalResolutionHelper() { }
	
    public static void check(PublishableBuilder<?> builder) {
    	if (builder.getSourceLocatorBuilder() != null && builder.getBuildingStage() != BuildingStage.EXTERNALLY_RESOLVED) {
	        checkDependenciesTree(builder);
	        checkMismatchingLocatorLabelContent(builder);
    	}
    }
	
	private static void checkDependenciesTree(PublishableBuilder<?> root) {
	    
        Map<LocatorBuilder,PublishableBuilder<?>> evaluated = new HashMap<>();
        Map<LocatorBuilder,Set<LocatorBuilder>> dependencies = new HashMap<>();
		
        Set<LocatorBuilder> toBeEvaluated = new HashSet<>();
        Set<LocatorBuilder> encountered = new HashSet<>();
        
        LocatorBuilder rootLocatorBuilder = root.getSourceLocatorBuilder();
        encountered.add(rootLocatorBuilder);
        
        rootLocatorBuilder.checkCorrectness();
        
        if (rootLocatorBuilder.getIssues().getSummary() != IssueLevel.ERROR) {
            
            evaluated.put(rootLocatorBuilder, root);
            
            Set<LocatorBuilder> usedLocatorBuilders = getUsedLocatorBuilders(root);
            
            dependencies.put(rootLocatorBuilder, usedLocatorBuilders);
            encountered.addAll(usedLocatorBuilders);
            for (LocatorBuilder usedRemoteBuilder : usedLocatorBuilders)
                if (!evaluated.containsKey(usedRemoteBuilder))
                    toBeEvaluated.add(usedRemoteBuilder);
            checkForCycles(root,encountered,dependencies);
        }
        
        while (!toBeEvaluated.isEmpty()) {
            LocatorBuilder currentLocator = toBeEvaluated.iterator().next();
            toBeEvaluated.remove(currentLocator);
            
            currentLocator.checkCorrectness();
            
            if (currentLocator.getIssues().getSummary() != IssueLevel.ERROR) {
                
                PublishableBuilder<?> dependencyBuilder = PublishedBuilderFactory.makeFrom(currentLocator);
                
                evaluated.put(currentLocator, dependencyBuilder);
                
                Set<LocatorBuilder> usedRemoteBuilders = getUsedLocatorBuilders(dependencyBuilder);
                
                dependencies.put(currentLocator, usedRemoteBuilders);
                encountered.addAll(usedRemoteBuilders);
                for (LocatorBuilder usedRemoteBuilder : usedRemoteBuilders)
                    if (!evaluated.containsKey(usedRemoteBuilder))
                        toBeEvaluated.add(usedRemoteBuilder);
                
                checkForCycles(root,encountered,dependencies);
                
                if (dependencyBuilder.getIssues().getSummary() != IssueLevel.OK)
                	root.appendChildIssues(dependencyBuilder.getIssues());
            }
        }	    
	}
	
	private static void checkMismatchingLocatorLabelContent(PublishableBuilder<?> builder) {
	    
        Set<EntityReference> refs = builder.getReferences();
        for (EntityLabel locatorLabel : builder.findLabelsFor(LocatorBuilder.class)) {
            for (EntityReference ref : refs) 
                if (ref.hasNameEqualTo(locatorLabel)) {
                    PublishableBuilder<?> content = PublishedBuilderCache.getInstance().get((LocatorBuilder)locatorLabel.getEntityBuilder());
                    if (!(content instanceof EmptyPublishedBuilder) && !ref.getBuilderClass().isAssignableFrom(content.getClass()))
                        builder.appendIssue(PublishedExternalResolutionIssue.MISMATCHING_LOCATOR_LABEL_CONTENT, locatorLabel.getName());
                    break;
                }
        }
	}
	
    private static Set<LocatorBuilder> getUsedLocatorBuilders(PublishableBuilder<?> builder) {
        Set<LocatorBuilder> result = new HashSet<>();
        
        Set<EntityReference> refs = builder.getReferences();
        for (EntityLabel locatorLabel : builder.findLabelsFor(LocatorBuilder.class)) {
            for (EntityReference ref : refs) 
                if (ref.hasNameEqualTo(locatorLabel)) {
                    result.add((LocatorBuilder)locatorLabel.getEntityBuilder());
                    break;
                }
        }

        return result;
    }
	
	private static void checkForCycles(PublishableBuilder<?> root, Set<LocatorBuilder> vertices, Map<LocatorBuilder,Set<LocatorBuilder>> edges) {
	    
	    Map<LocatorBuilder,Map<LocatorBuilder,Double>> distances = new FloydWarshall<LocatorBuilder>().getDistances(vertices,edges);
	    
        Set<LocatorBuilder> verticesInCycles = new HashSet<>();  
        for (LocatorBuilder vertex : vertices) 
            if (!verticesInCycles.contains(vertex) && !distances.get(vertex).get(vertex).isInfinite()) {
                verticesInCycles.add(vertex);
                Set<LocatorBuilder> verticesInThisCycle = new HashSet<>();
                for (LocatorBuilder otherVertex : vertices)
                    if (!verticesInCycles.contains(otherVertex) && distances.get(vertex).get(otherVertex) > 0) {
                        verticesInCycles.add(otherVertex);
                        verticesInThisCycle.add(otherVertex);
                    }
                        
                root.appendIssue(PublishedExternalResolutionIssue.CYCLIC_DEPENDENCY,Arrays.toString(verticesInThisCycle.toArray()));
            }
	}
	
}

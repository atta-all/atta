/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.concr.cpp;

import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.metamodel.edgetype.abstr.api.AbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicAbstractType;
import it.uniud.atta.metamodel.edgetype.concr.api.ConcreteAtomicType;

public class StringType implements ConcreteAtomicType {

    @Override
    public String toString() {
    	return new StringBuilder("(")
    							 .append(getName())
    							 .append(",")
    							 .append(getAbstractType())
    							 .append("[")
    							 .append(getMinBitWidth())
    							 .append(",")
    							 .append(getMaxBitWidth())
    							 .append("])").toString();
    }
    
    @Override
    public AbstractType getAbstractType() {
    	return AtomicAbstractType.TEXT;
    }
    
    @Override
    public int getMinBitWidth() {
    	return 8;
    }
    
    @Override
    public int getMaxBitWidth() {
    	return Integer.MAX_VALUE;
    }

    @Override
    public String getName() {
        return "string";
    }

	@Override
    public IssueCollection validate(Object valuation) {
	    // TODO Auto-generated method stub
	    return null;
    }
}

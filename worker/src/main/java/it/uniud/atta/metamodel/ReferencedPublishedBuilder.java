/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel;

import java.util.HashSet;
import java.util.Set;

import it.uniud.atta.metamodel.entity.EntityReference;
import it.uniud.atta.metamodel.entity.EntityLabel;
import it.uniud.atta.metamodel.entity.ReferencedEntityBuilderBase;
import it.uniud.atta.metamodel.locator.LocatorBuilder;


public abstract class ReferencedPublishedBuilder<T extends Publishable> extends ReferencedEntityBuilderBase<T> implements PublishableBuilder<T> {

    public ReferencedPublishedBuilder(String name) {
        super(name);
    }
    
    @Override
    public final LocatorBuilder getSourceLocatorBuilder() {
        return null;
    }

	@Override
    public final void setSourceLocatorBuilder(LocatorBuilder remoteBuilder) {
    }

	@Override
    public final Set<EntityLabel> getLabels() {
	    return new HashSet<>();
    }

	@Override
    public final Set<EntityLabel> findLabelsFor(Class<?> cls) {
	    return new HashSet<>();
    }

	@Override
    public final Set<EntityLabel> findLabelsFor(Class<?> cls, EntityReference reference) {
	    return new HashSet<>();
    }
}

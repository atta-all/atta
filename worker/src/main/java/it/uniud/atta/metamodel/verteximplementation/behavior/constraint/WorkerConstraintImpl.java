/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior.constraint;

import java.util.regex.Pattern;

import it.uniud.atta.metamodel.verteximplementation.behavior.property.WorkerProperty;

public final class WorkerConstraintImpl implements WorkerConstraint {

	private final WorkerProperty property;
	private final Pattern pattern;
	
	public WorkerConstraintImpl(WorkerProperty property, Pattern pattern) {
	    this.property = property;
	    this.pattern = pattern;
    }
	
	@Override
	public WorkerProperty getProperty() {
		return property;
	}
	
	@Override
	public Pattern getPattern() {
		return pattern;
	}
}

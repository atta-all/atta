/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.atomic;

import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicAbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicTypeCategory;
import it.uniud.atta.metamodel.edgetype.api.EdgeDataConvertibility;

public class Int64TypeHandler extends NumericAtomicAbstractTypeHandlerBase {

	private static Int64TypeHandler INSTANCE = new Int64TypeHandler();
	    
	private Int64TypeHandler() {
		super(AtomicTypeCategory.SIGNED_INTEGER,64);
	}
	
	public static Int64TypeHandler getInstance() {
		return INSTANCE;
	}
	
	@Override
    public boolean validValue(String value) {
	    try {  
	    	@SuppressWarnings("unused")
	    	long v = Long.parseLong(value);
	    	return true;
	    } catch(NumberFormatException e) {  
	    	return false;
	    }
	}
	
	@Override
	public EdgeDataConvertibility getConvertibilityToDifferent(AtomicAbstractType other) {
		switch (other) {
		case INTEGER:
		case DECIMAL:
			return EdgeDataConvertibility.EQUAL;
		case BOOL:
		case TEXT:
			return EdgeDataConvertibility.NONE;
		default:
			return EdgeDataConvertibility.EQUAL_UNSAFE;
		}
    }
}

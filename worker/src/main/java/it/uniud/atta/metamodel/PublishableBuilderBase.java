/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel;

import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.metamodel.entity.BuildingStage;
import it.uniud.atta.metamodel.entity.EntityBuilder;
import it.uniud.atta.metamodel.entity.EntityBuilderBase;
import it.uniud.atta.metamodel.entity.IntegrityStage;
import it.uniud.atta.metamodel.entity.EntityReference;
import it.uniud.atta.metamodel.entity.EntityLabel;
import it.uniud.atta.metamodel.entity.EntityLabelImpl;
import it.uniud.atta.metamodel.locator.LocatorBuilder;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public abstract class PublishableBuilderBase<T extends Publishable> extends EntityBuilderBase<T> implements PublishableBuilder<T> {

    private LocatorBuilder sourceLocatorBuilder;
    private Set<EntityLabel> labels;
    
    public PublishableBuilderBase(Map<String,Object> raw) {
        super();

		labels = new HashSet<>();
		
		setWrapper(this);
		
		acquireLabels(raw);
    }

    @Override
    public final LocatorBuilder getSourceLocatorBuilder() {
    	return sourceLocatorBuilder;
    }
    
    @Override
    public final void setSourceLocatorBuilder(LocatorBuilder source) {
    	this.sourceLocatorBuilder = source;
    }
    
    private final void acquireLabels(Map<String,Object> raw) {
        if (raw.containsKey(PublishableBaseDescriptorField.LABELS.toString())) {
            @SuppressWarnings("unchecked")
            Map<String,Object> rawLabelMap = (Map<String,Object>)raw.get(PublishableBaseDescriptorField.LABELS.toString());
            for (Map.Entry<String,Object> rawEntry : rawLabelMap.entrySet()) {
                @SuppressWarnings("unchecked")
                EntityBuilder<?> builder = PublishedBuilderFactory.makeFrom((Map<String,Object>)rawEntry.getValue());
                builder.setWrapper(this);
                addLabelFrom(rawEntry.getKey(),builder);
            }   
        }
    }
	
    @Override
    public final Set<EntityLabel> getLabels() {
    	return labels;
    }
	
	@Override
    public final Set<EntityLabel> findLabelsFor(Class<?> cls, EntityReference reference) {
    	Set<EntityLabel> result = new HashSet<>();
    	
    	for (EntityLabel label : getLabels()) {
    		if (cls.isInstance(label.getEntityBuilder()) && (reference == null || reference.hasNameEqualTo(label)))
    			result.add(label);
    	}
    	return result;
    }
	
	@Override
    public final Set<EntityLabel> findLabelsFor(Class<?> cls) {
    	return findLabelsFor(cls,null);
    }
	
	@Override
	public final void resolveInternalReferences() {
		if (!buildingStage.greaterThan(BuildingStage.DESCRIBED)) {
			
			if (getIssues().getSummary() != IssueLevel.ERROR)
		    	PublishedInternalResolutionHelper.check(this);
			
		    if (getIssues().getSummary() != IssueLevel.ERROR) {
		        internalResolutionHandler();
	            for (EntityBuilder<?> component : getComponents())
	                component.resolveInternalReferences();
	        }
			
			if (getIssues().getSummary() != IssueLevel.ERROR)
				integrityStage = IntegrityStage.INTERNAL;
			
			buildingStage = BuildingStage.INTERNALLY_RESOLVED;
		}
	}
	
	@Override
	public final void resolveExternalReferences() {		
		if (!buildingStage.greaterThan(BuildingStage.INTERNALLY_RESOLVED)) {
			resolveInternalReferences();
			
			if (getIssues().getSummary() != IssueLevel.ERROR)
				PublishedExternalResolutionHelper.check(this);
			
	        if (getIssues().getSummary() != IssueLevel.ERROR) {
	            externalResolutionHandler();
	            for (EntityBuilder<?> component : getComponents())
	                component.resolveExternalReferences();
	        }
			
			if (getIssues().getSummary() != IssueLevel.ERROR)
				integrityStage = IntegrityStage.EXTERNAL;
			
			buildingStage = BuildingStage.EXTERNALLY_RESOLVED; 
		}
	}

    @Override
    public final IssueCollection getIssues() {
        IssueCollection result = super.getIssues();
    	for (EntityLabel label : labels)
    		result.append(label.getEntityBuilder().getIssues());
    	return result;
    }
	
    public void addLabelFrom(String name, EntityBuilder<?> builder) {
    	if (builder != null)
    		labels.add(new EntityLabelImpl(name,builder));
    }
    
    @Override
    public final Set<EntityReference> getReferences() {
    	Set<EntityReference> result = super.getReferences();
    	
    	for (EntityLabel label : labels)
    		result.addAll(label.getEntityBuilder().getReferences());
    	
    	return result;
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.composite;

import java.util.Map;
import java.util.Set;

import it.uniud.atta.common.interval.ArraySpecification;
import it.uniud.atta.common.interval.exception.InvalidArraySpecificationException;
import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.common.lang.Named;
import it.uniud.atta.common.lang.StrictNameChecker;
import it.uniud.atta.metamodel.*;
import it.uniud.atta.metamodel.edgetype.abstr.AbstractTypeBuilder;
import it.uniud.atta.metamodel.edgetype.abstr.ReferencedAbstractTypeBuilder;
import it.uniud.atta.metamodel.edgetype.abstr.api.Component;
import it.uniud.atta.metamodel.edgetype.abstr.atomic.AtomicTypeBuilder;
import it.uniud.atta.metamodel.entity.*;
import it.uniud.atta.metamodel.locator.LocatorBuilder;

final public class ComponentBuilder extends EntityBuilderBase<Component> implements Named {
    
    public static enum Field implements EntityField {
        
        NAME("name",EntityFieldKind.MANDATORY),
        TYPE_REF("type",EntityFieldKind.MANDATORY),
        ARRAY("array",EntityFieldKind.OPTIONAL);
            
        private final String name;
        private final EntityFieldKind kind;

        Field(String name, EntityFieldKind kind) {
            this.name = name;
            this.kind = kind;
        }
            
        @Override
        public String toString() {
            return name;
        }
        
        @Override
        public EntityFieldKind getKind() {
            return kind;
        }
    }
    
    private final String name;
    private AbstractTypeBuilder<?> typeBuilder;
    private final ArraySpecification arraySpecification;
    
    ComponentBuilder(Map<String,Object> raw) {
    	super();
    	
    	name = acquireName(raw.get(Field.NAME.toString()));
        typeBuilder = acquireTypeBuilder(raw.get(Field.TYPE_REF.toString()));
        arraySpecification = acquireSpecification(raw.get(Field.ARRAY.toString()));
        
        addComponent(typeBuilder);
    }
    
    @Override
    public String getName() {
        return name;
    }
    
    public AbstractTypeBuilder<?> getTypeBuilder() {
        return typeBuilder;
    }
    
    public ArraySpecification getArraySpecification() {
        return arraySpecification;
    }
    
    private String acquireName(Object rawName) {
        String result = null;
        if (rawName != null) {
            result = rawName.toString();
        }
        return result;
    }
    
    private AbstractTypeBuilder<?> acquireTypeBuilder(Object rawType) {
    	AbstractTypeBuilder<?> result = null;
        
        if (rawType != null) {
            String typeString = (String)rawType;
            
            result = new AtomicTypeBuilder(typeString);
            result.checkCorrectness();
            if (result.getIssues().getSummary() == IssueLevel.ERROR)
                result = new ReferencedAbstractTypeBuilder(typeString);
        }
        return result;
    }
    
    private ArraySpecification acquireSpecification(Object rawArraySpec) {
        ArraySpecification result = null;
        
        String textualSpecification = rawArraySpec == null ? "1" : (String)rawArraySpec; 
        
        try {
            result = new ArraySpecification(textualSpecification);
        } catch (InvalidArraySpecificationException e) { }   
        
        return result;
    }
	
	@Override
    protected void internalResolutionHandler() {
        if (typeBuilder instanceof ReferencedAbstractTypeBuilder) {
        	removeComponent(typeBuilder);
            Set<EntityLabel> internalLabels = getWrapper().findLabelsFor(AbstractTypeBuilder.class,((ReferencedAbstractTypeBuilder)typeBuilder).makeReference());
            if (!internalLabels.isEmpty())
                typeBuilder = (AbstractTypeBuilder<?>)internalLabels.iterator().next().getEntityBuilder();
            addComponent(typeBuilder);
        }
    }
	
	@Override
    protected void externalResolutionHandler() {
    	if (typeBuilder instanceof ReferencedAbstractTypeBuilder) {
        	removeComponent(typeBuilder);
            Set<EntityLabel> locatorLabels = getWrapper().findLabelsFor(LocatorBuilder.class,((ReferencedAbstractTypeBuilder)typeBuilder).makeReference());
            PublishableBuilder<?> publishedBuilder = PublishedBuilderFactory.makeFrom((LocatorBuilder)locatorLabels.iterator().next().getEntityBuilder());
            publishedBuilder.resolveExternalReferences();
            typeBuilder = (AbstractTypeBuilder<?>)publishedBuilder;
            addComponent(typeBuilder);
        }
    }
	
	@Override
	protected void checkingHandler() {
    	if (name == null)
            surfaceIssues.append(ComponentIssue.MISSING_NAME);
    	else if (!StrictNameChecker.validate(name))
    	    surfaceIssues.append(ComponentIssue.INVALID_NAME);
    	if (typeBuilder == null)
        	surfaceIssues.append(ComponentIssue.MISSING_TYPE);
    	if (arraySpecification == null)
            surfaceIssues.append(ComponentIssue.INVALID_ARRAY_SPECIFICATION);
	}
    
    @Override
    protected Component buildHandler() {    	
    	return new ComponentImpl(name,typeBuilder.build(),getArraySpecification());
    }
}

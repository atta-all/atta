/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.entity;

public enum BuildingStage {

	NONE, // The entity cannot be described, yet an object can be created
	DESCRIBED, // The entity has been described, but no internal or external resolution has been performed
	INTERNALLY_RESOLVED, // The entity has been internally resolved  
	EXTERNALLY_RESOLVED, // The entity has been externally resolved
	CHECKED, // The entity has been checked
    ;
	
    public boolean greaterThan(BuildingStage other) {
		return (this.ordinal() > other.ordinal());
	}
}

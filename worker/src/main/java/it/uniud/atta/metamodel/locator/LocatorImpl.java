/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.locator;

import it.uniud.atta.repository.api.Checkout;
import it.uniud.atta.repository.api.Repository;
import it.uniud.atta.repository.exception.RepositoryOperationException;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class LocatorImpl implements Locator {
	
	private final Repository repository;
	private final String path;
	private final String revision;
	
	public LocatorImpl(Repository repository, String path, String revision) {
		this.repository = repository;
		this.path = path;
		this.revision = revision;
	}
	
	@Override
	public Repository getRepository() {
		return repository;
	}
	
	@Override
	public String getPath() {
		return path;
	}
	
	@Override
	public String getRevision() {
		return revision;
	}

    @Override
    public Checkout retrieve() throws RepositoryOperationException {
        return repository.retrieve(path, revision);
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17,31).append(repository).append(path).append(revision).build();
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof LocatorImpl))
            return false;
        LocatorImpl otherLocator = (LocatorImpl)other;
        
        if (!this.repository.equals(otherLocator.repository))
            return false;
        if (!this.path.equals(otherLocator.path))
            return false;
        if (!this.revision.equals(otherLocator.revision))
            return false;
        
        return true;
    }
}

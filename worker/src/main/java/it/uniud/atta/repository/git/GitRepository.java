/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository.git;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.CreateBranchCommand.SetupUpstreamMode;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import it.uniud.atta.repository.DistributedRepositoryBase;
import it.uniud.atta.repository.RevisionBuilder;
import it.uniud.atta.repository.api.RepositoryAddress;
import it.uniud.atta.repository.api.RepositoryKind;
import it.uniud.atta.repository.api.Revision;
import it.uniud.atta.repository.api.RevisionKind;
import it.uniud.atta.repository.exception.*;

public final class GitRepository extends DistributedRepositoryBase {
    
    public GitRepository(RepositoryAddress address, Set<RepositoryAddress> mirrors) {
        super(RepositoryKind.GIT,address,mirrors);
    }
	
	private File getDotGitDirectory() {
	    return new File(getDownloadDirectory(),".git");
	}

    @Override
    protected void switchInto(Revision revision) throws RepositoryOperationException {
        
        org.eclipse.jgit.lib.Repository repo = null;
        try {
            repo = FileRepositoryBuilder.create(getDotGitDirectory());
            
            new Git(repo).checkout().setName(revision.getUid()).call();

        } catch (Exception e) {
            throw new RepositorySwitchException(revision.getName(),e);
        } finally {
            if (repo != null)
                repo.close();
        }
    }
    
    @Override
    protected void download() throws RepositoryOperationException {
        Git gitClient = null;
        boolean hasFailed = false;
        try {
            gitClient = Git.cloneRepository()
            .setCloneAllBranches(true)
            .setURI(getRetrievalAddress().getName())
            .setDirectory(getDotGitDirectory().getParentFile())
            .call();
            
            createHeadsForAllRemoteBranches(gitClient);

        } catch (Exception e) {
            hasFailed = true;
            throw new RepositoryDownloadException(this.toString(),e);
        } finally {
            if (gitClient != null)
                gitClient.getRepository().close();
            if (hasFailed) {
                try {
                    FileUtils.deleteDirectory(getUsedCacheDirectory());    
                } catch (IOException e) {
                    throw new RepositoryCacheDeletionException(getUsedCacheDirectory(),e);
                }
            }
        }
    }
    
    private static void createHeadsForAllRemoteBranches(Git git) throws GitAPIException {
        Map<String,Ref> refs = git.getRepository().getAllRefs();
        for (Entry<String,Ref> ref : refs.entrySet()) {
            String name = ref.getKey();
            if (name.startsWith(Constants.R_REMOTES)) {
                String id = name.substring(name.lastIndexOf("/")+1);
                if (!refs.containsKey(Constants.R_HEADS + id)) {
                    String tracked = name.substring(13);
                    git.branchCreate() 
                    .setName(id)
                    .setUpstreamMode(SetupUpstreamMode.SET_UPSTREAM)
                    .setStartPoint(tracked)
                    .call();
                }
            }
        }
    }

    @Override
    protected void validateRevision(String revisionString) throws RepositoryOperationException {
        if (revisionString == null || revisionString.isEmpty())
            revisionString = Constants.MASTER;
        
        checkIncompleteHash(revisionString);
        
        org.eclipse.jgit.lib.Repository repo = null;
 
        try {
            
            repo = FileRepositoryBuilder.create(getDotGitDirectory());
            ObjectId revObjId = repo.resolve(revisionString);
            
            if (revObjId == null)
                throw new UnknownRevisionException(revisionString);
                
            RevWalk rw = new RevWalk(repo);
            rw.parseCommit(revObjId);
        } catch (MissingObjectException e) {
            throw new UnknownRevisionException(revisionString,e);
        } catch (IOException e) {
            throw new RepositoryRevisionValidationException(revisionString,e);
        } finally {
            if (repo != null)
                repo.close();
        }         
    }
    
    @Override
    protected Revision getRevision(String revisionString) throws RepositoryOperationException {
        if (revisionString.isEmpty())
            revisionString = Constants.MASTER;
        
        checkIncompleteHash(revisionString);
        
        org.eclipse.jgit.lib.Repository repo = null;
        
        try {
            
            repo = FileRepositoryBuilder.create(getDotGitDirectory());
            ObjectId revObjId = repo.resolve(revisionString);
            
            if (revObjId == null)
                throw new UnknownRevisionException(revisionString);
                
            RevWalk rw = new RevWalk(repo);
            RevCommit commit = rw.parseCommit(revObjId);
            
            Map<String,Ref> refPairs = repo.getAllRefs();
            String uid = commit.getName();
            
            RevisionKind kind = RevisionKind.COMMIT;
            for (Entry<String,Ref> refPair : refPairs.entrySet()) {
                String name = refPair.getKey();
                if (name.startsWith(Constants.R_HEADS)) {
                    if (name.substring(11).equals(revisionString)) {
                        kind = RevisionKind.TIP;
                        break;
                    }
                } else if (name.startsWith(Constants.R_TAGS)) {
                    if (name.substring(10).equals(revisionString)) {
                        kind = RevisionKind.TAG;
                        break;
                    }
                }
            }
            
            return new RevisionBuilder().name(revisionString).kind(kind).uid(uid).build();
        } catch (MissingObjectException e) {
            throw new UnknownRevisionException(revisionString,e);
        } catch (IOException e) {
            throw new RepositoryRevisionValidationException(revisionString,e);
        } finally {
            if (repo != null)
                repo.close();
        }
    }

	@Override
    protected void pull() throws RepositoryOperationException {
        org.eclipse.jgit.lib.Repository repo = null;
        try {
            repo = FileRepositoryBuilder.create(getDotGitDirectory());
            
            Git git = new Git(repo);
            
            if (!repo.getFullBranch().startsWith(Constants.R_HEADS))
            	git.checkout().setName(Constants.MASTER).call(); // Pull wouldn't work on a detached head
            git.pull().call();

        } catch (Exception e) {
            throw new RepositoryPullException(e);
        } finally {
            if (repo != null)
                repo.close();
        }    
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository.hg;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.tmatesoft.hg.core.HgCheckoutCommand;
import org.tmatesoft.hg.core.HgCloneCommand;
import org.tmatesoft.hg.core.HgException;
import org.tmatesoft.hg.core.HgPullCommand;
import org.tmatesoft.hg.core.Nodeid;
import org.tmatesoft.hg.core.SessionContext;
import org.tmatesoft.hg.internal.BasicSessionContext;
import org.tmatesoft.hg.internal.StreamLogFacility;
import org.tmatesoft.hg.repo.HgBranches;
import org.tmatesoft.hg.repo.HgBranches.BranchInfo;
import org.tmatesoft.hg.repo.HgLookup;
import org.tmatesoft.hg.repo.HgRemoteRepository;
import org.tmatesoft.hg.repo.HgTags;
import org.tmatesoft.hg.util.LogFacility.Severity;

import it.uniud.atta.repository.DistributedRepositoryBase;
import it.uniud.atta.repository.RevisionBuilder;
import it.uniud.atta.repository.api.RepositoryAddress;
import it.uniud.atta.repository.api.RepositoryKind;
import it.uniud.atta.repository.api.Revision;
import it.uniud.atta.repository.api.RevisionKind;
import it.uniud.atta.repository.exception.*;

public final class HgRepository extends DistributedRepositoryBase {

    private final static SessionContext CONTEXT = new BasicSessionContext(new StreamLogFacility(Severity.Error,false,System.out)); 
	
    public HgRepository(RepositoryAddress address, Set<RepositoryAddress> mirrors) {
        super(RepositoryKind.HG,address,mirrors);
    }
    
    @Override
    protected void download() throws RepositoryOperationException {
        boolean hasFailed = false;
        try {
            File downloadDir = getDownloadDirectory();
            if (getRetrievalAddress().isLocal()) {
                FileUtils.copyDirectory(new File(getRetrievalAddress().getName()), downloadDir);
            } else {
                HgLookup lookup = new HgLookup(CONTEXT);
                URI address = new URI(this.getRetrievalAddress().getName());
                HgRemoteRepository remote = lookup.detectRemote(address);
                HgCloneCommand cmd = new HgCloneCommand().source(remote);
                cmd.destination(downloadDir).execute();
            }
        } catch (Exception e) {
            hasFailed = true;
            throw new RepositoryDownloadException(this.toString(),e);
        } finally {
            if (hasFailed) {
                try {
                    FileUtils.deleteDirectory(getUsedCacheDirectory());    
                } catch (IOException e) {
                    throw new RepositoryCacheDeletionException(getUsedCacheDirectory(),e);
                }
            }
        }        
    }

    @Override
    protected void switchInto(Revision revision) throws RepositoryOperationException {
        
        try {
            HgLookup lookup = new HgLookup(CONTEXT);
            org.tmatesoft.hg.repo.HgRepository repo = lookup.detect(getDownloadDirectory());
            HgCheckoutCommand co = new HgCheckoutCommand(repo);
            co.clean(true).changeset(Nodeid.fromAscii(revision.getUid())).execute();
        } catch (Exception e) {
            throw new RepositorySwitchException(revision.getName(),e);
        }
    }

    @Override
    protected void validateRevision(String revisionString) throws RepositoryOperationException {
        
        if (revisionString == null || revisionString.isEmpty())
            revisionString = "default";
       
        try {
            // Checks if the revision is a local changeset id
            Long.valueOf(revisionString); 
            throw new LocalRevisionFormatException(revisionString);
        } catch (NumberFormatException e) {
            checkIncompleteHash(revisionString);
        }
        
        try {
            HgLookup lookup = new HgLookup(CONTEXT);
            org.tmatesoft.hg.repo.HgRepository repo = lookup.detect(getDownloadDirectory());
            HgCheckoutCommand co = new HgCheckoutCommand(repo);
            co.clean(true).changeset(Nodeid.fromAscii(revisionString)).execute();
        } catch (HgException e) {
            throw new UnknownRevisionException(revisionString,e);
        } catch (Exception e) {
            throw new RepositoryOperationException(revisionString,e);
        }
    }
    
    @Override
    protected Revision getRevision(String revisionString) throws RepositoryOperationException {
        
        if (revisionString.isEmpty())
            revisionString = "default";
        
        checkIncompleteHash(revisionString);
       
        try {
            
            RevisionKind kind = null;
            Nodeid id = null;
            
            HgLookup lookup = new HgLookup(CONTEXT);
            org.tmatesoft.hg.repo.HgRepository repo = lookup.detect(getDownloadDirectory());
            
            HgTags tags = repo.getTags();
            List<Nodeid> tagged = tags.tagged(revisionString);
            if (!tagged.isEmpty() && tagged.size() == 1) {
                id = tagged.get(0);
                kind = RevisionKind.TAG;
            }
            
            HgBranches branches = repo.getBranches();
            BranchInfo branchInfo = branches.getBranch(revisionString);
            if (branchInfo != null) {
                id = branchInfo.getHeads().get(0);
                kind = RevisionKind.TIP;
            }
            
            if (kind == null) {
                id = Nodeid.fromAscii(revisionString);
                kind = RevisionKind.COMMIT;
            }
         
            return new RevisionBuilder().name(revisionString).uid(id.toString()).kind(kind).build();
        } catch (HgException e) {
            throw new UnknownRevisionException(revisionString,e);
        } catch (Exception e) {
            throw new RepositoryOperationException(revisionString,e);
        }
    }

	@Override
    protected void pull() throws RepositoryOperationException {
        try {
            if (getRetrievalAddress().isLocal()) {
                FileUtils.copyDirectory(new File(getRetrievalAddress().getName()), getDownloadDirectory());
            } else {
                HgLookup lookup = new HgLookup(CONTEXT);
                org.tmatesoft.hg.repo.HgRepository repo = lookup.detect(getDownloadDirectory());
                URI address = new URI(this.getRetrievalAddress().getName());
                HgRemoteRepository remote = lookup.detectRemote(address);
                HgPullCommand pull = new HgPullCommand(repo);
                pull.source(remote).execute();
            }
        } catch (Exception e) {
            throw new RepositoryPullException(e);
        }
    }
}

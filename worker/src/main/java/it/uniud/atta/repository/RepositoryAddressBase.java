/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import it.uniud.atta.repository.api.RepositoryAddress;

public abstract class RepositoryAddressBase implements RepositoryAddress {

    final private String name;
    
    public RepositoryAddressBase(String name) {
        this.name = name;
    }
    
    @Override
    public final String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17,31).append(name).build();
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof RepositoryAddressBase))
            return false;
        RepositoryAddressBase otherAddr = (RepositoryAddressBase)other;
        
        return this.name.equals(otherAddr.name);
    }
    
    protected final static String completeLocalAddress(String name) {
        if (!(name.contains("://"))) {
            String currentPath = System.getProperty("user.dir");
            currentPath = currentPath.replace("\\", "/");
            if (!currentPath.startsWith("/"))
                currentPath = "/" + currentPath;
            name = "file://" + currentPath + "/" + name;
        } 
        return name;
    }
    
    @Override
    final public boolean isLocal() {
        return (!(name.contains("://")) || (name.contains("file://")));
    }
}

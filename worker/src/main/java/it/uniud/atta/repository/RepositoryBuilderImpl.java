/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import it.uniud.atta.metamodel.entity.EntityBuilderBase;
import it.uniud.atta.metamodel.entity.EntityField;
import it.uniud.atta.metamodel.entity.EntityFieldKind;
import it.uniud.atta.repository.api.Repository;
import it.uniud.atta.repository.api.RepositoryAddress;
import it.uniud.atta.repository.api.RepositoryKind;
import it.uniud.atta.repository.git.GitRepository;
import it.uniud.atta.repository.hg.HgRepository;
import it.uniud.atta.repository.svn.SvnRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class RepositoryBuilderImpl extends EntityBuilderBase<Repository> implements RepositoryBuilder {

    public static enum Field implements EntityField {

        KIND("kind",EntityFieldKind.MANDATORY),
        ADDRESS("address",EntityFieldKind.IDENTIFYING),
        MIRRORS("mirrors",EntityFieldKind.OPTIONAL),
        ;
        
        private final String name;
        private final EntityFieldKind kind;
        
        Field(String name, EntityFieldKind kind) {
            this.name = name;
            this.kind = kind;
        }
            
        @Override
        public String toString() {
            return name;
        }
        
        @Override
        public EntityFieldKind getKind() {
            return kind;
        }
    }
    
    private final RepositoryKind kind;
    private final RepositoryAddress address;
    private final Set<RepositoryAddress> mirrors;
    
    public RepositoryBuilderImpl(RepositoryKind kind, RepositoryAddress address, Set<RepositoryAddress> mirrors) {
        super();
        this.kind = kind;
        this.address = address;
        this.mirrors = mirrors;
    }
    
    public RepositoryBuilderImpl(Map<String,Object> raw) {
    	super();
        
        Object kindObj = raw.get(RepositoryDescriptorField.KIND.toString());
        Object addressObj = raw.get(RepositoryDescriptorField.ADDRESS.toString());
        
        kind = acquireKind(kindObj);
        address = acquireAddress(addressObj,kind);
        mirrors = acquireMirrors(raw,kind);
    }
    
    @Override
    protected void checkingHandler() {
    	if (kind == null)
    		appendIssue(RepositoryIssue.MISSING_KIND);
    	else {
    		if (kind == RepositoryKind.UNRECOGNIZED)
        		appendIssue(RepositoryIssue.UNRECOGNIZED_KIND);
        	if (address == null)
        		appendIssue(RepositoryIssue.MISSING_ADDRESS);    			
    	}
    }

    @Override
    protected Repository buildHandler() {
        
        Repository result = null;
        
        switch (kind) {
            case GIT:
                result = new GitRepository(address,mirrors);
                break;
            case HG:
                result = new HgRepository(address,mirrors);
                break;
            case SVN:
                result = new SvnRepository(address,mirrors);
                break;
            case UNRECOGNIZED:
        }
        
        return result;
    }
    
    private final RepositoryKind acquireKind(Object kindObj) {
        
        RepositoryKind result = null;
        
        if (kindObj != null)
        	result = RepositoryKind.fromString((String)kindObj);           
        
        return result;
    }
    
    private final RepositoryAddress acquireAddress(Object addressObj, RepositoryKind kind) {
        
        RepositoryAddress result = null;
        
        if (addressObj != null && kind != null)
            result = RepositoryAddressFactory.makeFrom(addressObj.toString(),kind);
    
        return result;
    }
    
    @SuppressWarnings("unchecked")
    private final Set<RepositoryAddress> acquireMirrors(Map<String,Object> raw, RepositoryKind kind) {
        
        Set<RepositoryAddress> result = new HashSet<>(); 
        
        Object mirrorsObj = raw.get(RepositoryDescriptorField.MIRRORS.toString());
        
        List<String> mirrorAddressList = null;
        if (mirrorsObj != null) {
            mirrorAddressList = (List<String>) mirrorsObj;
        } else
            mirrorAddressList = new ArrayList<>();
            
        if (kind != null)
            for (String addr : mirrorAddressList) 
                result.add(RepositoryAddressFactory.makeFrom(addr,kind));
        
        return result;
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17,31).append(kind).append(address).append(mirrors).build();
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof RepositoryBuilderImpl))
            return false;
        RepositoryBuilderImpl otherRepoBase = (RepositoryBuilderImpl)other;
        
        if (!this.kind.equals(otherRepoBase.kind))
            return false;
        
        Set<RepositoryAddress> allAddresses = new HashSet<>();
        allAddresses.add(this.address);
        allAddresses.addAll(this.mirrors);
        Set<RepositoryAddress> allOtherAddresses = new HashSet<>();
        allOtherAddresses.add(otherRepoBase.address);
        allOtherAddresses.addAll(otherRepoBase.mirrors);
        allAddresses.retainAll(allOtherAddresses);
        
        if (allAddresses.isEmpty())
            return false;
        
        return true;
    }
}

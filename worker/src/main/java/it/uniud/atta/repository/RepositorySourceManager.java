/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import it.uniud.atta.repository.api.Repository;
import it.uniud.atta.repository.api.RepositoryAddress;
import it.uniud.atta.repository.api.RepositoryKind;
import it.uniud.atta.repository.exception.NoAvailableAddressException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class RepositorySourceManager {
    
    private final Map<Integer,Set<RepositorySource>> idSources;
    private final Map<Integer,RepositorySource> idCurrentSource;
    private final Map<Repository,Integer> repoIds;
    
    private int counter = 0;
    
    private final static long BACKOFF_INTERVAL_MS = 300000;
    
    private static RepositorySourceManager INSTANCE = new RepositorySourceManager();
    
    public static RepositorySourceManager getInstance() {
        return INSTANCE;
    }
    
    private RepositorySourceManager() { 
        idSources = new HashMap<>();
        idCurrentSource = new HashMap<>();
        repoIds = new HashMap<>();
    }
    
    public void clearAll() {
        idSources.clear();
        idCurrentSource.clear();
        repoIds.clear();
    }
    
    public int getId(Repository repo) {
        
        Integer result = repoIds.get(repo);
        
        if (result != null)
            return result;
        
        Set<RepositorySource> repoSources = computeSources(repo);
                 
        for (Map.Entry<Integer,Set<RepositorySource>> sourceSet : idSources.entrySet()) {
           Set<RepositorySource> sources = sourceSet.getValue();
           for (RepositorySource repoSource : repoSources) {
               if (sources.contains(repoSource)) {
                   sources.addAll(repoSources);
                   repoIds.put(repo, sourceSet.getKey());
                   return sourceSet.getKey();
               }
           }
        }
        
        result = ++counter;
        idSources.put(result, repoSources);
        repoIds.put(repo,result);
        idCurrentSource.put(result,getPrimarySource(repo));
        
        return result;
    }  
    
    RepositorySource getCurrentSource(Repository repo) {
        Integer id = getId(repo);
        return idCurrentSource.get(id);
    }
    
    private RepositorySource getPrimarySource(Repository repo) {
        RepositorySource result = null;
        
        Integer id = getId(repo);
        Set<RepositorySource> sources = idSources.get(id);
        
        for (RepositorySource source : sources) {
            if (source.getAddress() == repo.getAddress()) {
                result = source;
                break;
            }
        }
        return result;
    }
    
    private Set<RepositorySource> computeSources(Repository repo) {
        RepositoryKind kind = ((RepositoryBase)repo).getKind();
        Set<RepositorySource> result = new HashSet<>();
        result.add(new RepositorySource(kind,repo.getAddress()));
        for (RepositoryAddress mirror : repo.getMirrors())
            result.add(new RepositorySource(kind,mirror));

        return result;
    }
    
    void tryChangeSourceToCachedAvailableOne(Repository repo) {
        
        Integer id = getId(repo);
        
        Set<RepositorySource> sources = idSources.get(id);
        RepositorySource currentSource = idCurrentSource.get(id);
        
        for (RepositorySource source : sources) {
            if (!source.equals(currentSource) && source.getDirectory().exists() && source.isAvailable()) {
                idCurrentSource.put(id, source);
                break;
            }  
        }
    }
    
    void tryChangeSourceToAvailableOne(Repository repo) throws NoAvailableAddressException {

        Integer id = getId(repo);
        
        Set<RepositorySource> sources = idSources.get(id);
        RepositorySource currentSource = idCurrentSource.get(id);
        
        currentSource.backoff(BACKOFF_INTERVAL_MS);
        boolean updated = false;
        for (RepositorySource source : sources) {
            if (source.isAvailable()) {
                idCurrentSource.put(id, source);
                updated = true;
                if (source.getDirectory().exists()) {
                    break;
                }
            }
        }
        
        if (!updated)
            throw new NoAvailableAddressException(repo);
    }
}

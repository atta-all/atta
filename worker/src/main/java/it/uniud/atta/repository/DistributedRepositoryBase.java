/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import it.uniud.atta.repository.api.Checkout;
import it.uniud.atta.repository.api.RepositoryAddress;
import it.uniud.atta.repository.api.RepositoryKind;
import it.uniud.atta.repository.api.Revision;
import it.uniud.atta.repository.api.RevisionKind;
import it.uniud.atta.repository.exception.InvalidRevisionFormatException;
import it.uniud.atta.repository.exception.LocalRevisionFormatException;
import it.uniud.atta.repository.exception.RepositoryDownloadException;
import it.uniud.atta.repository.exception.RepositoryOperationException;
import it.uniud.atta.repository.exception.RepositoryPathMissingException;
import it.uniud.atta.repository.exception.RepositorySwitchException;
import it.uniud.atta.repository.exception.UnknownRevisionException;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.apache.commons.io.FileUtils;

public abstract class DistributedRepositoryBase extends RepositoryBase {
    
    private static final String DOWNLOAD_DIR_NAME = "download";

    protected DistributedRepositoryBase(RepositoryKind kind, RepositoryAddress address, Set<RepositoryAddress> mirrors) {
        super(kind,address,mirrors);
    }
    
    protected final File getDownloadDirectory() {
        return new File(getUsedCacheDirectory(),DOWNLOAD_DIR_NAME);
    }

    /**
     * Switches the repository into the given revision.
     * Precondition: the repository has already been downloaded.
     */    
    abstract protected void switchInto(Revision revision) throws RepositoryOperationException;
    
    /**
     * Downloads the repository.
     * Precondition: the repository has not been downloaded yet.
     */     
    abstract protected void download() throws RepositoryOperationException;
    
    /**
     * Updates the repository to the latest version.
     * Precondition: the repository has already been downloaded.
     */
    abstract protected void pull() throws RepositoryOperationException;
    
    @Override
    public final Checkout retrieve(String pathString, String revisionString) throws RepositoryOperationException {
        
        Revision revision = null;
        File checkoutDirectory = null;

        try {

            tryChangeSourceToCachedAvailableOne();
            
            if (!getDownloadDirectory().exists())
                download();
            
            try {
                
            	revision = getRevision(revisionString);

            	if (revision.getKind() != RevisionKind.COMMIT) {
            	    pull();
            	    revision = getRevision(revisionString);
            	}
            } catch (UnknownRevisionException e) {
            	pull();
            	revision = getRevision(revisionString);
            }
            
            checkoutDirectory = getCheckoutDirectory(pathString,revision);
            
            if (!checkoutDirectory.exists())
                checkout(pathString,revision,checkoutDirectory);
        } catch (InvalidRevisionFormatException|LocalRevisionFormatException|RepositoryPathMissingException e) {
            throw e;
        } catch (RepositoryDownloadException e) {
            // If it passes, then we try to change the source and retry recursively
            tryChangeSourceToAvailableOne();
            return retrieve(pathString, revisionString);
        } catch (RepositorySwitchException e) {
            // Checks if the error stems for a missing revision
            validateRevision(revisionString);
        }
        
        return new CheckoutBuilder(this).path(checkoutDirectory).revision(revision).build();
    }

    @Override
    public void checkout(String pathString, Revision revision, File destination) throws RepositoryOperationException {
        switchInto(revision);
        pathCopy(pathString,destination);
    }
    
    protected void checkIncompleteHash(String revisionString) throws LocalRevisionFormatException {
        if (revisionString.matches("^[0-9A-Fa-f]+$") && revisionString.length() < 40) {
            throw new LocalRevisionFormatException(revisionString);
        }
    }
    
    private void pathCopy(String pathString, File destination) throws RepositoryOperationException {
        
        File completeSourceDirectory = new File(getDownloadDirectory(),pathString);

        if (!completeSourceDirectory.exists())
            throw new RepositoryPathMissingException(pathString);
        
        try {
           FileUtils.copyDirectory(completeSourceDirectory, destination);
        } catch (IOException e) {
           throw new RepositoryOperationException("Impossible to copy " 
                   + completeSourceDirectory.getAbsolutePath() + " to " + destination.getAbsolutePath(),e);
        }
   }
}

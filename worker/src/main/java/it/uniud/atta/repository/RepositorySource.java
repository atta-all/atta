/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import it.uniud.atta.common.lang.IdGenerator;
import it.uniud.atta.repository.api.RepositoryAddress;
import it.uniud.atta.repository.api.RepositoryKind;

import java.io.File;

import org.apache.commons.lang3.builder.HashCodeBuilder;

final class RepositorySource {

    private final RepositoryKind kind;
    private final RepositoryAddress address;
    private File directory;
    private long backoffTimeMillis;
    
    RepositorySource(RepositoryKind kind, RepositoryAddress address) {
        this.kind = kind;
        this.address = address;
        this.backoffTimeMillis = System.currentTimeMillis();
    }
    
    RepositoryAddress getAddress() {
        return address;
    }
    
    File getDirectory() {
        if (directory == null)
            directory = new File(RepositoryBase.BASE_CACHE_DIR,computeIdentifier());
        return directory;
    }
    
    void backoff(long time) {
        this.backoffTimeMillis = System.currentTimeMillis() + time;
    }
    
    boolean isAvailable() {
        return (System.currentTimeMillis() >= backoffTimeMillis);
    }
    
    private final String computeIdentifier() {

        StringBuilder input = new StringBuilder();
        input.append(kind).append(address.getName());
         
        return IdGenerator.sha1(input.toString());
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17,31).append(kind).append(address).build();
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof RepositorySource))
            return false;
        RepositorySource otherSource = (RepositorySource)other;
        
        if (this.kind != otherSource.kind)
            return false;
        if (!this.address.equals(otherSource.address))
            return false;
        
        return true;
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import it.uniud.atta.repository.api.RepositoryAddress;
import it.uniud.atta.repository.api.RepositoryKind;
import it.uniud.atta.repository.git.GitRepositoryAddress;
import it.uniud.atta.repository.hg.HgRepositoryAddress;
import it.uniud.atta.repository.svn.SvnRepositoryAddress;

public final class RepositoryAddressFactory {
    
    public static RepositoryAddress makeFrom(String addressString, RepositoryKind kind) {
        
        RepositoryAddress result = null;

        switch (kind) {
            case GIT:
                result = new GitRepositoryAddress(addressString);
                break;
            case HG:
                result = new HgRepositoryAddress(addressString);
                break;
            case SVN:
                result = new SvnRepositoryAddress(addressString);
                break;
            case UNRECOGNIZED:
        }
            
        return result;
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import java.io.File;

import it.uniud.atta.repository.api.Checkout;
import it.uniud.atta.repository.api.Repository;
import it.uniud.atta.repository.api.Revision;
import it.uniud.atta.repository.exception.CheckoutBuildingException;

public final class CheckoutBuilder {

    private Repository repo = null;
    private File path = null;
    private Revision revision = null;

    public CheckoutBuilder(Repository repo) {
        this.repo = repo;
    }
    
    public CheckoutBuilder path(File path) {
        this.path = path;
        return this;
    }
    
    public CheckoutBuilder revision(Revision revision) {
        this.revision = revision;
        return this;
    }
    
    public Checkout build() {
        if (repo == null || path == null || revision == null)
            throw new CheckoutBuildingException();

        return new CheckoutImpl(repo,path,revision);
    }
}

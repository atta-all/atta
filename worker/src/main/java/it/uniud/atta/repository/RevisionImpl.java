/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import it.uniud.atta.repository.api.Revision;
import it.uniud.atta.repository.api.RevisionKind;

final class RevisionImpl implements Revision {
    
    private final String name;
    private final RevisionKind kind;
    private final String uid;
    
    @Override
    public String getName() {
        return name;
    }

    @Override
    public RevisionKind getKind() {
        return kind;
    }
    
    @Override
    public String getUid() {
        return uid;
    }
    
    RevisionImpl(String name, RevisionKind kind, String uid) {
        this.name = name;
        this.kind = kind;
        this.uid = uid;
    }
}

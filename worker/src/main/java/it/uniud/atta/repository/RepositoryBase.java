/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import it.uniud.atta.common.lang.IdGenerator;
import it.uniud.atta.repository.api.Repository;
import it.uniud.atta.repository.api.RepositoryAddress;
import it.uniud.atta.repository.api.RepositoryKind;
import it.uniud.atta.repository.api.Revision;
import it.uniud.atta.repository.exception.*;

import java.io.File;
import java.security.GeneralSecurityException;
import java.util.*;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public abstract class RepositoryBase implements Repository {

    static final String BASE_CACHE_DIR = "target/repocache";
    static final String CHECKOUTS_DIR_NAME = "checkouts";
    
    private final RepositoryKind kind;
    private final RepositoryAddress address;
    private final Set<RepositoryAddress> mirrors;
	
    private final RepositorySourceManager sourceIdManager;
	
    static {
        installAllTrustingTrustManager();
    }
	
	protected RepositoryBase(RepositoryKind kind, RepositoryAddress address, Set<RepositoryAddress> mirrors) {
	    this.kind = kind;
	    this.address = address;
	    this.mirrors = mirrors;
	    
	    sourceIdManager = RepositorySourceManager.getInstance();
	}
	
	final RepositoryKind getKind() {
		return kind;
	}
	
	@Override
	public final RepositoryAddress getAddress() {
		return address;
	}
	
	@Override
	public final Set<RepositoryAddress> getMirrors() {
		return mirrors;
	}
	
	protected final RepositoryAddress getRetrievalAddress() {
	    return sourceIdManager.getCurrentSource(this).getAddress();
	}
	
	protected final File getUsedCacheDirectory() {
	    return sourceIdManager.getCurrentSource(this).getDirectory();
	}
	
	protected final void tryChangeSourceToCachedAvailableOne() {
	    sourceIdManager.tryChangeSourceToCachedAvailableOne(this);
	}
	
	protected final void tryChangeSourceToAvailableOne() throws NoAvailableAddressException {
	    sourceIdManager.tryChangeSourceToAvailableOne(this);
	}
	
	protected final File getCheckoutDirectory(String pathString, Revision revision) {

	    StringBuilder input = new StringBuilder();
	    input.append(pathString).append(revision.getUid());
	    
	    return new File(new File(getUsedCacheDirectory(),CHECKOUTS_DIR_NAME),IdGenerator.sha1(input.toString()));
	}
	
	abstract protected Revision getRevision(String revisionString) throws RepositoryOperationException;
   
	abstract protected void validateRevision(String revisionString) throws RepositoryOperationException;
	
    abstract protected void checkout(String pathString, Revision revision, File destinationDirectory) throws RepositoryOperationException;
    
    private static void installAllTrustingTrustManager() {
        TrustManager[] trustAllCerts = new TrustManager[] {
            new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                }
            }
        };

        // Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (GeneralSecurityException e) {
    
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17,31).append(kind).append(address).append(mirrors).build();
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof RepositoryBase))
            return false;
        RepositoryBase otherRepoBase = (RepositoryBase)other;
        
        if (!this.kind.equals(otherRepoBase.kind))
            return false;
        
        Set<RepositoryAddress> allAddresses = new HashSet<>();
        allAddresses.add(this.address);
        allAddresses.addAll(this.mirrors);
        Set<RepositoryAddress> allOtherAddresses = new HashSet<>();
        allOtherAddresses.add(otherRepoBase.address);
        allOtherAddresses.addAll(otherRepoBase.mirrors);
        allAddresses.retainAll(allOtherAddresses);
        
        if (allAddresses.isEmpty())
            return false;
        
        return true;
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        
        builder.append("Kind: ").append(kind).append(" ")
               .append("Address: ").append(address.getName()).append(" ");
        
        return builder.toString();
    }
}

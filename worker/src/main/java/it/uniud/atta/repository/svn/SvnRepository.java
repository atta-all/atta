/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository.svn;

import java.io.File;
import java.io.IOException;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.tmatesoft.svn.core.*;
import org.tmatesoft.svn.core.wc.*;

import it.uniud.atta.repository.CheckoutBuilder;
import it.uniud.atta.repository.RepositoryBase;
import it.uniud.atta.repository.RevisionBuilder;
import it.uniud.atta.repository.api.Checkout;
import it.uniud.atta.repository.api.RepositoryAddress;
import it.uniud.atta.repository.api.RepositoryKind;
import it.uniud.atta.repository.api.Revision;
import it.uniud.atta.repository.api.RevisionKind;
import it.uniud.atta.repository.exception.*;

public final class SvnRepository extends RepositoryBase {

    public SvnRepository(RepositoryAddress address, Set<RepositoryAddress> mirrors) {
        super(RepositoryKind.SVN,address,mirrors);
    }
	
    @Override
    public final Checkout retrieve(String pathString, String revisionString) throws RepositoryOperationException {

        Revision revision = null;
        File checkoutDirectory = null;
        
        try {
            tryChangeSourceToCachedAvailableOne();
            
            revision = getRevision(revisionString);
            
            checkoutDirectory = getCheckoutDirectory(pathString,revision);
            
            if (!checkoutDirectory.exists())
                checkout(pathString,revision,checkoutDirectory);
        } catch (InvalidRevisionFormatException e) {
            throw e;
        } catch (RepositoryOperationException e) {
            if (isCurrentSourceAvailable()) {
                // Checks either if the error stems for a missing revision or path
                validateRevision(revisionString);
                validatePath(pathString,revisionString);
                // If no cause has been identified, throws
                throw e;
            } else {
                tryChangeSourceToAvailableOne();
                return retrieve(pathString, revisionString);
            }
        }
        
        return new CheckoutBuilder(this).path(checkoutDirectory).revision(revision).build();
    }
	
	@Override
	protected void checkout(String pathString, Revision revision, File directory) throws RepositoryOperationException {
	    SVNClientManager clientManager = SVNClientManager.newInstance();
	    boolean hasFailed = false;
        try {
            SVNRevision svnRevision = getSvnRevision(revision.getUid());
            
            SVNUpdateClient updateClient = clientManager.getUpdateClient();
            String uriString = getRetrievalAddress().getName()+pathString;
            updateClient.doCheckout(SVNURL.parseURIEncoded(uriString), directory, 
                                    svnRevision, svnRevision, SVNDepth.INFINITY, true);
        } catch (Exception e) {
            hasFailed = true;
            throw new RepositoryDownloadException(this.toString(),e);
        } finally {
            clientManager.dispose();
            if (hasFailed) {
                try {
                    FileUtils.deleteDirectory(directory);    
                } catch (IOException e) {
                    throw new RepositoryCacheDeletionException(directory,e);
                }
            }
        }	    
	}
	
	private SVNRevision getSvnRevision(String revisionString) throws InvalidRevisionFormatException {
	    SVNRevision result = null;
        
        if (revisionString.isEmpty()) {
            result = SVNRevision.HEAD;
        } else {
            try {
                result = SVNRevision.create(Long.parseLong(revisionString));                
            } catch (NumberFormatException e) {
                throw new InvalidRevisionFormatException(revisionString,e);
            }
        }
        return result;
    }

	@Override
    protected void validateRevision(String revisionString) throws RepositoryOperationException {
        SVNClientManager clientManager = SVNClientManager.newInstance();
        SVNRevision svnRevision = getSvnRevision(revisionString);
        SVNWCClient wcClient = clientManager.getWCClient();
        
        try {
            wcClient.doInfo(SVNURL.parseURIEncoded(getRetrievalAddress().getName()), svnRevision, svnRevision);
        } catch (SVNException e) {
            throw new UnknownRevisionException(revisionString,e);
        } finally {
            clientManager.dispose();
        }
    }
	
	protected Revision getRevision(String revisionString) throws RepositoryOperationException {
        SVNClientManager clientManager = SVNClientManager.newInstance();
        SVNRevision svnRevision = getSvnRevision(revisionString);
        SVNWCClient wcClient = clientManager.getWCClient();
        
        SVNInfo info = null;
        try {
            info = wcClient.doInfo(SVNURL.parseURIEncoded(getRetrievalAddress().getName()), svnRevision, svnRevision);
        } catch (SVNException e) {
            throw new UnknownRevisionException(revisionString,e);
        } finally {
            clientManager.dispose();
        }
        
        String uid = String.valueOf(info.getCommittedRevision().getNumber());
        
        boolean isHead = revisionString.isEmpty();
        RevisionKind kind = (isHead ? RevisionKind.TIP : RevisionKind.COMMIT);
        
        return new RevisionBuilder().kind(kind).name(isHead ? "head" : revisionString).uid(uid).build();
	}
	
    private boolean isCurrentSourceAvailable() {
        SVNClientManager clientManager = SVNClientManager.newInstance();
        SVNRevision svnRevision = SVNRevision.HEAD;
        SVNWCClient wcClient = clientManager.getWCClient();
        
        boolean result = true;
        try {
            wcClient.doInfo(SVNURL.parseURIEncoded(getRetrievalAddress().getName()), svnRevision, svnRevision);
        } catch (SVNException e) {
            result = false;
        } finally {
            clientManager.dispose();
        }
        return result;
    }
    
    private void validatePath(String pathString, String revisionString) throws RepositoryOperationException {
        SVNClientManager clientManager = SVNClientManager.newInstance();
        try {
            SVNRevision svnRevision = getSvnRevision(revisionString); 
            String uri = getRetrievalAddress().getName() + pathString;
            SVNWCClient wcClient = clientManager.getWCClient();
            wcClient.doInfo(SVNURL.parseURIEncoded(uri), svnRevision, svnRevision);
        } catch (SVNException e) {
            throw new RepositoryPathMissingException(pathString,e);
        } finally {
            clientManager.dispose();
        }  
    }
}

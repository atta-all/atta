/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.codegen;

import java.io.*;

import org.apache.commons.exec.*;
import org.apache.commons.io.FileUtils;

public class ThriftCompiler {

    public static final File outputDirectory = new File("target/generated-sources/thrift");
    
    public static void compile(ThriftGeneratorKind generator, File thriftFile) throws IOException {
        
        FileUtils.forceMkdir(outputDirectory);
        
        CommandLine commandLine = new CommandLine("thrift")
                                      .addArgument("--gen")
                                      .addArgument(generator.getId())
                                      .addArgument(thriftFile.getAbsolutePath());
        DefaultExecutor executor = new DefaultExecutor();

        ExecuteStreamHandler streamHandler = new PumpStreamHandler();
        executor.setStreamHandler(streamHandler);
        executor.setWorkingDirectory(outputDirectory);
        executor.execute(commandLine);
    }
}

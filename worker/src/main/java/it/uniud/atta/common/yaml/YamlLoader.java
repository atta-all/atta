/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common.yaml;

import java.io.*;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.SafeConstructor;
import org.yaml.snakeyaml.nodes.MappingNode;
import org.yaml.snakeyaml.nodes.Node;
import org.yaml.snakeyaml.nodes.NodeTuple;
import org.yaml.snakeyaml.nodes.ScalarNode;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.parser.ParserException;

public final class YamlLoader {

    private final Yaml yaml;
    
    private final static YamlLoader INSTANCE = new YamlLoader(); 
    
    private YamlLoader() {
        yaml = new Yaml(new DuplicateCheckingConstructor());
    }
    
    public static YamlLoader getInstance() {
        return INSTANCE;
    }
    
    public Object load(File file) throws DescriptorFileNotFoundException, MalformedDescriptorException {
        Object result = null;
    	try {
    	    result = yaml.load(new FileInputStream(file));
    	} catch (ParserException e) {
    	    throw new MalformedDescriptorException(e);
    	} catch (FileNotFoundException e) {
    	    throw new DescriptorFileNotFoundException(e);
    	}
    	
    	return result;
    }
    
    @SuppressWarnings("unchecked")
    public Map<String,Object> loadAsMap(File file) throws DescriptorFileNotFoundException, MalformedDescriptorException {
    	return (Map<String,Object>) load(file);
    }
    
    public Object loadFromPath(File path) throws DescriptorFileNotFoundException, AmbiguousDescriptorFileException, MalformedDescriptorException {
    	File inputFile = null;
    	
    	File [] files = path.listFiles(new FilenameFilter() {
    	    @Override
    	    public boolean accept(File dir, String name) {
    	        return name.endsWith(".yaml");
    	    }
    	});
    	switch (files.length) {
    	case 0:
    		throw new DescriptorFileNotFoundException();
    	case 1:
	        inputFile = files[0];
    		break;
    	default:
    		throw new AmbiguousDescriptorFileException();
    	}
    	
        return load(inputFile);
    }
    
    @SuppressWarnings("unchecked")
    public Map<String,Object> loadFromPathAsMap(File file) throws DescriptorFileNotFoundException, AmbiguousDescriptorFileException, MalformedDescriptorException {
    	return (Map<String,Object>) loadFromPath(file);
    }
    
    private class DuplicateCheckingConstructor extends SafeConstructor {
        
        DuplicateCheckingConstructor() {
            this.yamlConstructors.put(Tag.MAP, new DuplicateCheckingConstructYamlMap());
        }
        
        class DuplicateCheckingConstructYamlMap extends ConstructYamlMap {
            public Object construct(Node node) {
                List<NodeTuple> tuples = ((MappingNode)node).getValue();
                Set<String> found = new HashSet<>();
                for (NodeTuple tuple : tuples) {
                    String key = ((ScalarNode)tuple.getKeyNode()).getValue();
                    if (found.contains(key))
                        throw new ParserException("while parsing a map", node.getStartMark(),
                                "found duplicate key '" + key + "'", tuple.getKeyNode().getStartMark());
                    found.add(key);
                }
                return super.construct(node);
            }
        }

    }
}

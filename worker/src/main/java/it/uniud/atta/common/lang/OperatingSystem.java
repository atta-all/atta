/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common.lang;


public final class OperatingSystem {
    
    private final OperatingSystemKind kind; 
    
    private static OperatingSystem INSTANCE = new OperatingSystem();
    
    private OperatingSystem() {
        String name = System.getProperty("os.name").toLowerCase();
        OperatingSystemKind kindValue = null;
        if (isWindows(name))
        	kindValue = OperatingSystemKind.WINDOWS;
        else if (isMac(name))
        	kindValue = OperatingSystemKind.OSX;
        else if (isUnix(name))
        	kindValue = OperatingSystemKind.NIX;
        else if (isLinux(name))
        	kindValue = OperatingSystemKind.LINUX;
        else if (isSolaris(name))
        	kindValue = OperatingSystemKind.SOLARIS;
        
        kind = kindValue;
    }
    
    public static OperatingSystem getInstance() {
        return INSTANCE;
    }
    
    public OperatingSystemKind getKind() {
    	return kind;
    }
 
    private static boolean isWindows(String name) {
        return (name.contains("win"));
    }
 
    private static boolean isMac(String name) {
        return (name.contains("mac"));
    }
 
    private static boolean isUnix(String name) {
        return (name.contains("nix") || name.contains("aix"));
    }
    
    private static boolean isLinux(String name) {
        return (name.contains("nux"));
    }
 
    private static boolean isSolaris(String name) {
        return (name.contains("sunos"));
    }
}

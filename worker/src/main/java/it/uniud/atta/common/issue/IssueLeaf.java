/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common.issue;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class IssueLeaf implements IssueNode {

    private final Issue issue;
    private final String reason;

    public IssueLeaf(Issue issue) {
        this(issue,"");
    }
    
    public IssueLeaf(Issue issue, String reason) {
        this.issue = issue;
        this.reason = reason;
    }
    
    public Issue getIssue() {
        return issue;
    }
    
    public String getReason() {
        return reason;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17,31).append(issue).append(reason).build();
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof IssueLeaf))
            return false;
        IssueLeaf otherIssueNode = (IssueLeaf)other;
        
        if (!this.issue.equals(otherIssueNode.issue))
            return false;
        
        if (!this.reason.equals(otherIssueNode.reason))
            return false;
        
        return true;
    }
    
    @Override
    public String toString() {
        StringBuilder strb = new StringBuilder("[");

        strb.append(issue);
        
        if (!reason.isEmpty())
            strb.append(":")
                .append(reason);
         
        return strb.append("]").toString();
    }
}

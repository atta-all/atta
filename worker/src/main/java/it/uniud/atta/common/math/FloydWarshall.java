/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common.math;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FloydWarshall<T> {
    
    public Map<T,Map<T,Double>> getDistances(Set<T> vertices, Map<T,Set<T>> edges) {
        
        Map<T,Map<T,Double>> current = initDistances(vertices,edges);
        
        for (T vk : vertices) {
            for (T vi : vertices) {
                for (T vj : vertices) { 
                    if (current.get(vi).get(vj) > current.get(vi).get(vk) + current.get(vk).get(vj))
                        current.get(vi).put(vj,current.get(vi).get(vk) + current.get(vk).get(vj));
                }
            }
        }
        
        return current;
    }
    
    private Map<T,Map<T,Double>> initDistances(Set<T> vertices, Map<T,Set<T>> edges) {
        
        Map<T,Map<T,Double>> result = new HashMap<>();
        
        for (T rowEntity : vertices) {
            
            Map<T,Double> vals = new HashMap<>();
            for (T columnEntity : vertices) {
                Set<T> targets = edges.get(rowEntity);
                Double value = (targets != null && targets.contains(columnEntity)) ? 1.0 : Double.POSITIVE_INFINITY;
                vals.put(columnEntity, value);
            }
            result.put(rowEntity, vals);
        }
        return result;
    }
}

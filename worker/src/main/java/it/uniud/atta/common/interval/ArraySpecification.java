/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common.interval;

import org.apache.commons.lang.StringUtils;

import it.uniud.atta.common.interval.SizeInterval;
import it.uniud.atta.common.interval.exception.*;

public final class ArraySpecification {

    private final static String DIVIDER = ",";
    
    private final SizeInterval[] specification;
    
    public ArraySpecification(String textual) throws InvalidArraySpecificationException {
        
        if (textual.isEmpty())
            throw new EmptyArraySpecificationException();
        
        String[] intervalsTextual = textual.split(DIVIDER,-1);
        
        int numDimensions = intervalsTextual.length;
        
        specification = new SizeInterval[numDimensions];
        
        try {
            for (int i=0;i<numDimensions;++i) {
            	
            	String[] boundsTextual = intervalsTextual[i].split(SizeInterval.DIVIDER,-1);
            	
            	int lowerBound = 0;
            	int upperBound = 0;
            	
            	switch (boundsTextual.length) {
            	case 2:
            		lowerBound = getLowerBound(boundsTextual[0], (i==0));
            		upperBound = getUpperBound(boundsTextual[1]);	
            		break;
            	case 1:
            		lowerBound = getLowerBound(boundsTextual[0], (i==0));
            		upperBound = lowerBound;
            		break;
            	default:
            		throw new InvalidSizeIntervalException();
            	}
            	
                specification[i] = new SizeInterval(lowerBound,upperBound);
                if (specification[i].isSingleton() && numDimensions>1)
                	throw new SingletonOnMultidimensionalArrayException();
                
            	if (specification[i].getLowerBound() == 0 && i > 0)
            		throw new LowerBoundIsZeroForHigherDimensionException();
            }
        }
        catch (InvalidSizeIntervalException e) {
            throw new InvalidArraySpecificationException(e);
        }
    }
    
    @Override
    public String toString() {
    	String[] intervalStrings = new String[specification.length];
    	for (int i=0; i < specification.length; ++i) {
    		SizeInterval thisInterval = specification[i];
    		StringBuilder builder = new StringBuilder();
    		if (thisInterval.isSingleton())
    			builder.append(thisInterval.getLowerBound());
    		else {
    			if (thisInterval.getLowerBound() != (i > 0 ? 1 : 0)) {
    				builder.append(thisInterval.getLowerBound());
    			}
    			builder.append(SizeInterval.DIVIDER);
    			if (thisInterval.getUpperBound() != Integer.MAX_VALUE) {
    				builder.append(thisInterval.getUpperBound());
    			}
    		}
    		intervalStrings[i] = builder.toString();
    	}
    	return StringUtils.join(intervalStrings, DIVIDER);
    }
    
    public SizeInterval get(int index) {
        return specification[index];
    }
    
    public int getNumDimensions() {
        return specification.length;
    }
    
    public boolean isSingleton() {
    	return specification.length == 1 && specification[0].isSingleton();
    }
    
    public boolean isOptional() {
    	return specification[0].getLowerBound() == 0;
    }
    
    public boolean covers(int[] sizes) {
    	if (specification.length < sizes.length)
    		return false;
    	
    	for (int i=0; i<specification.length; ++i) {
    		SizeInterval dimension = specification[i];
    		int size = (i >= sizes.length ? 1 : sizes[i]);
    		if (dimension.getUpperBound() < size)
    			return false;
    		else if (dimension.getLowerBound() > size)
    			return false;
    	}
    	
    	return true;
    }
    
    public boolean isIncludedWithin(ArraySpecification other) { 
    	
    	if (other.specification.length < this.specification.length)
    		return false;
    	else {
    		for (int i=0; i< other.specification.length; ++i) {
    			SizeInterval source = null;
    			if (i>= this.specification.length)
	                try {
	                    source = new SizeInterval(1);
                    } catch (InvalidSizeIntervalException e) {
                    }
                else 
    				source = specification[i];
    			
    			if (!source.isIncludedWithin(other.specification[i]))
    				return false;
    		}
    	}
    	
    	return true;
    }
    
    private int getLowerBound(String textual, boolean isFirstDimension) throws InvalidSizeIntervalException {
    	int result = 0;
    	if (textual.isEmpty()) { 
    		if (!isFirstDimension)
    			result = 1;
    	} else {
    		try {
    			result = Integer.parseInt(textual);
    		} catch (NumberFormatException e) {
    			throw new InvalidLowerBoundException(e);
    		}
    	}
    	return result;
    }
    
    private int getUpperBound(String textual) throws InvalidSizeIntervalException {
    	int result = Integer.MAX_VALUE;
    	if (!textual.isEmpty()) {
    		try {
    			result = Integer.parseInt(textual);
    		} catch (NumberFormatException e) {
    			throw new InvalidUpperBoundException(e);
    		}
    	}
    	return result;
    }
    
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common.lang;


public enum OperatingSystemKind implements Named {

	LINUX("linux"),
	OSX("osx"),
	SOLARIS("solaris"),
	WINDOWS("windows"),
	NIX("*nix"),
	ANY("any"); // OS-independent
	
	private String name;
	
	private OperatingSystemKind(String name) {
		this.name = name;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
    public static OperatingSystemKind fromString(String str) {
        
    	OperatingSystemKind result = null;
        for (OperatingSystemKind kind: OperatingSystemKind.values()) {
             if (kind.getName().equals(str)) {
                  result = kind;
                  break;
             }
        }
            
        return result;
    }
}

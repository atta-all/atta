/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common.lang;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public final class StrictNameChecker {
    
    private static final Set<String> KEYWORDS = new HashSet<>(
            Arrays.asList(
                "abstract",     "assert",        "boolean",      "break",           "byte",
                "case",         "catch",         "char",         "class",           "const",
                "continue",     "default",       "do",           "double",          "else",
                "enum",         "extends",       "false",        "final",           "finally",
                "float",        "for",           "goto",         "if",              "implements",
                "import",       "instanceof",    "int",          "interface",       "long",
                "native",       "new",           "null",         "package",         "private",
                "protected",    "public",        "return",       "short",           "static",
                "strictfp",     "super",         "switch",       "synchronized",    "this",
                "throw",        "throws",        "transient",    "true",            "try",
                "void",         "volatile",      "while"
            ));

        private static final Pattern NAME_PATTERN = Pattern.compile("[A-Za-z_]+[\\w]*");

        public static boolean validate(String text) {
            return !(KEYWORDS.contains(text) || !NAME_PATTERN.matcher(text).matches());
        }
}

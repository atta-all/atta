/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common.issue;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class IssueCollection implements IssueNode {

	private final Object responsible;
    private final Set<IssueNode> collection;
    
    public IssueCollection(Object responsible) {
    	this.responsible = responsible;
    	this.collection = new HashSet<>();
    }
    
    public IssueCollection(IssueCollection other) {
	    this(other.getResponsible());
	    this.collection.addAll(other.getAll());
    }

	public Object getResponsible() {
    	return responsible;
    }

    public IssueCollection append(Issue issue) {
        collection.add(new IssueLeaf(issue));
        return this;
    }

    public IssueCollection append(Issue issue, String reason) {
        collection.add(new IssueLeaf(issue,reason));
        return this;
    }

    public IssueCollection append(IssueNode child) {
    	if (child instanceof IssueLeaf || (child instanceof IssueCollection && !((IssueCollection)child).isEmpty()))
    		collection.add(child);
        return this;
    }
    
    public IssueCollection appendAll(IssueCollection other) {
        collection.addAll(other.getAll());
        return this;
    }
    
    public boolean contains(Issue issue) {
        for (IssueNode node : collection) {
        	if (node instanceof IssueLeaf && ((IssueLeaf)node).getIssue().equals(issue))
        		return true;
        	if (node instanceof IssueCollection && ((IssueCollection)node).contains(issue))
        		return true;
        }
        return false;
    }
    
    public boolean hasIssuesFor(IssueCollector responsible) {
    	if (this.responsible == responsible)
    		return true;
    	
        for (IssueNode node : collection) {
        	if (node instanceof IssueCollection && ((IssueCollection)node).hasIssuesFor(responsible))
        		return true;
        }
        return false;    	
    }
    
    public Set<IssueNode> getAll() {
        return collection;
    }
    
    public boolean isEmpty() {
        return collection.isEmpty();
    }
    
    public int size() {
        return collection.size();
    }

    public IssueLevel getSummary() {
        IssueLevel result = IssueLevel.OK;
        
        for (IssueNode node : collection) {
        	IssueLevel level = null;
        	if (node instanceof IssueLeaf) {
        		level = ((IssueLeaf)node).getIssue().getLevel();
        	} else if (node instanceof IssueCollection) {
        		level = ((IssueCollection)node).getSummary();
        	}
    		switch (level) {
    		case ERROR:
    			return IssueLevel.ERROR;
    		case WARNING:
    			result = IssueLevel.WARNING;
    		case OK:
    		}
        }
            
        return result;
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder(17,31).append(responsible).append(collection).build();
    }
    
    @Override
    public boolean equals(Object other) {
        if (other == null || !(other instanceof IssueCollection))
            return false;
        IssueCollection otherIssueCollection = (IssueCollection)other;
        
        if (!this.responsible.equals(otherIssueCollection.responsible))
        	return false;
        
        if (!this.collection.equals(otherIssueCollection.collection))
            return false;
        
        return true;
    }
    
    @Override
    public String toString() {
        StringBuilder strb = new StringBuilder("{");
        
        for (IssueNode pair : collection)
            strb.append(pair);
         
        return strb.append("}").toString();
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common.interval;

import it.uniud.atta.common.interval.exception.*;

public final class SizeInterval {
    
	final static String DIVIDER = ":";
	
    private int lowerBound;
    private int upperBound;
    
    public SizeInterval(int fixedValue) throws InvalidSizeIntervalException {
    	this(fixedValue,fixedValue);
    }
    
    public SizeInterval(int lowerBound, int upperBound) throws InvalidSizeIntervalException {
    	if (lowerBound < 0)
    		throw new NegativeLowerBoundException(); 
    	if (lowerBound > upperBound)
    		throw new EmptySizeIntervalException();
    	if (upperBound == 0)
    		throw new ZeroSizeIntervalException();
    	this.lowerBound = lowerBound;
    	this.upperBound = upperBound;
    }
    
    public boolean isSingleton() {
        return (isFixed() && upperBound == 1);
    }
    
    public boolean isFixed() {
    	return (lowerBound == upperBound);
    }
    
    public boolean isBounded() {
        return (upperBound != Integer.MAX_VALUE);
    }
    
    public int getLowerBound() {
        return lowerBound;
    }
    
    public int getUpperBound() {
        return upperBound;
    }
    
    public boolean isIncludedWithin(SizeInterval other) {
    	return this.lowerBound >= other.lowerBound && this.upperBound <= other.upperBound;
    }
    
    @Override
    public String toString() {
    	StringBuilder strb = new StringBuilder();
    	strb.append(lowerBound)
    		.append(DIVIDER)
    		.append(upperBound);
    	return strb.toString();
    }
    
}

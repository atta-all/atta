/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common.lang;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;

public class ResourceLoader {

    private final StringBuilder resourceStringBuilder;
    
    public ResourceLoader(String root) {
        resourceStringBuilder = new StringBuilder(root); 
    }
    
    public ResourceLoader(ResourceLoader other, String tail) {
        resourceStringBuilder = new StringBuilder(other.resourceStringBuilder);
        appendSeparatorAndText(tail);
    }
    
    public ResourceLoader append(String path) {
        appendSeparatorAndText(path);
        return this;
    }
    
    public File getFile() throws FileNotFoundException {
        
        URL resource = this.getClass().getClassLoader().getResource(resourceStringBuilder.toString());
        if (resource == null)
            throw new FileNotFoundException(resourceStringBuilder.toString());
        File result = new File(resource.getFile()); 
        if (!result.isFile())
            throw new FileNotFoundException(resourceStringBuilder.toString());
        return result;
    }
    
    private void appendSeparatorAndText(String text) {
        resourceStringBuilder.append("/").append(text);
    }
}

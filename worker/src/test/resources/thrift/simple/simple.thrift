namespace java atta

struct FullName {
    1: string firstName,
    2: string middleName,
    3: string surname
}
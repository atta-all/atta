/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import it.uniud.atta.common.lang.ResourceLoader;
import it.uniud.atta.repository.RepositoryBase;
import it.uniud.atta.repository.RepositorySourceManager;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.After;

public class RepositoryTestingBase {
    
    static final ResourceLoader DESCRIPTORS_BASE_DIR = new ResourceLoader("yaml/repo");
    static final String PATH_TO_CHECKOUT = "types/abstract/2dcoord";
    
    @After
    public void deleteDirectory() throws IOException {
        FileUtils.deleteDirectory(new File(RepositoryBase.BASE_CACHE_DIR));
        RepositorySourceManager.getInstance().clearAll();
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import static org.junit.Assert.*;
import it.uniud.atta.common.lang.ResourceLoader;
import it.uniud.atta.common.yaml.YamlLoader;
import it.uniud.atta.repository.RepositoryBuilderImpl;
import it.uniud.atta.repository.api.*;
import it.uniud.atta.repository.exception.*;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestHgRepository extends RepositoryTestingBase {
    
    private static Repository repo;
    
    private static ResourceLoader DESCRIPTORS_DIR = new ResourceLoader(DESCRIPTORS_BASE_DIR,"hg");
    
    @BeforeClass
    public static void makeRepo() throws Exception {
        repo = new RepositoryBuilderImpl(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_DIR,"default.yaml").getFile())).build();
    }
    
    @Test
    public void testCommitRevisionKind() throws Exception {
        Checkout checkout = repo.retrieve(PATH_TO_CHECKOUT, "8ef591d494737b1d714f09934a09420ba96c98ba");
        assertThat(checkout.getRevision().getKind(),is(RevisionKind.COMMIT));
    }
    
    @Test
    public void testTagRevisionKind() throws Exception {
        Checkout checkout = repo.retrieve(PATH_TO_CHECKOUT, "1.0");
        assertThat(checkout.getRevision().getKind(),is(RevisionKind.TAG));
    }    
    
    @Test
    public void testTipRevisionKind() throws Exception {
        Checkout checkout = repo.retrieve(PATH_TO_CHECKOUT, "mybranch");
        assertThat(checkout.getRevision().getKind(),is(RevisionKind.TIP));
    }    
    
    @Test
    public void testDefaultRevision() throws Exception {
        Checkout checkout = repo.retrieve(PATH_TO_CHECKOUT, "");
        assertThat(checkout.getRevision().getName(),is("default"));
        assertThat(checkout.getRevision().getKind(),is(RevisionKind.TIP));
    }

    @Test(expected = UnknownRevisionException.class)
    public void testUnknownRevisionException() throws Exception {
        repo.retrieve(PATH_TO_CHECKOUT, "abc11c084d25d65ab024a2779dd03069900a8a19");
    }
    
    @Test(expected = LocalRevisionFormatException.class)
    public void testLocalChangesetFormatException() throws Exception {
        repo.retrieve(PATH_TO_CHECKOUT, "2");
    }

    @Test(expected = LocalRevisionFormatException.class)
    public void testPartialGlobalChangesetFormatException() throws Exception {
        repo.retrieve(PATH_TO_CHECKOUT, "8ef591d494737b1d");
    }
    
    @Test(expected = RepositoryPathMissingException.class)
    public void testRepositoryPathMissingException() throws Exception {
        repo.retrieve("inexistent","8ef591d494737b1d714f09934a09420ba96c98ba");
    }
}

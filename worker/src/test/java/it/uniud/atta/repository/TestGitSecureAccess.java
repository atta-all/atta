/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import it.uniud.atta.common.lang.ResourceLoader;

import java.io.*;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.transport.*;
import org.eclipse.jgit.util.FS;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

/**
 * This is an inactive test suite. Credentials are fake.
 */
public class TestGitSecureAccess {
    
    private static String HTTPS_URI = "https://bitbucket.org/atta-repo-auth/git-auth.git";
    private static String SSH_URI = "git@bitbucket.org:atta-repo-auth/git-auth.git";
    private static ResourceLoader KEYFILE = new ResourceLoader("auth/testkey_rsa1024_passless");
    private static File TARGET = new File("target/authrepo");
    private static String USER = "user";
    private static String PASSWORD = "pass";
    
    public class CustomJschConfigSessionFactory extends JschConfigSessionFactory {
        @Override
        protected void configure(OpenSshConfig.Host host, Session session) {
            session.setConfig("StrictHostKeyChecking", "yes");
        }
        
        @Override
        protected JSch getJSch(final OpenSshConfig.Host hc, FS fs) throws JSchException {
            JSch jsch = super.getJSch(hc, fs);
            try {
                jsch.addIdentity(KEYFILE.getFile().toString());
            } catch (JSchException e) {
                e.printStackTrace();  
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            return jsch;
        }
    }
    
    //@Test
    public void testAsymmetricKey() throws Exception {
        
        SshSessionFactory.setInstance(new CustomJschConfigSessionFactory());

        Git gitClient = null;
        try {
            gitClient = Git.cloneRepository()
            .setCloneAllBranches(true)
            .setURI(SSH_URI)
            .setDirectory(TARGET)
            .call();

        } finally {
            if (gitClient != null) {
                gitClient.getRepository().close();
                FileUtils.deleteDirectory(TARGET); 
            }
        }
    }
    
    //@Test
    public void testUserAndPassword() throws Exception {
        Git gitClient = null;
        try {
            UsernamePasswordCredentialsProvider user = new UsernamePasswordCredentialsProvider(USER, PASSWORD);
            
            gitClient = Git.cloneRepository()
            .setCloneAllBranches(true)
            .setURI(HTTPS_URI)
            .setDirectory(TARGET)
            .setCredentialsProvider(user)
            .call();

        } finally {
            if (gitClient != null) {
                gitClient.getRepository().close();
                FileUtils.deleteDirectory(TARGET); 
            }
        }
    }
    
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import java.io.*;

import org.apache.commons.io.FileUtils;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.auth.BasicAuthenticationManager;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;

/**
 * This is an inactive test suite. Credentials are fake.
 */
public class TestSvnSecureAccess {
    
    private static String HTTPS_URI = "https://riouxsvn.com/svn/svn-auth";
    private static File TARGET = new File("target/authrepo");
    private static String USER = "user";
    private static String PASS = "password";
      
    //@Test
    public void testUserPassword() throws Exception {

        SVNClientManager clientManager = SVNClientManager.newInstance();
        try {
            SVNRevision svnRevision = SVNRevision.HEAD;
            SVNURL url = SVNURL.parseURIEncoded(HTTPS_URI);
            
            BasicAuthenticationManager authManager = new BasicAuthenticationManager(USER,PASS); 
            clientManager.setAuthenticationManager(authManager);
            SVNUpdateClient updateClient = clientManager.getUpdateClient();
            updateClient.doCheckout(url, TARGET,svnRevision, svnRevision, SVNDepth.INFINITY, true);

        } finally {
            clientManager.dispose();
            FileUtils.deleteDirectory(TARGET);
        }    
    }
}
/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import static org.junit.Assert.*;
import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.common.lang.ResourceLoader;
import it.uniud.atta.common.yaml.YamlLoader;
import it.uniud.atta.repository.RepositoryBuilderImpl;
import it.uniud.atta.repository.RepositoryIssue;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestRepositoryBuilder {
    
    static final ResourceLoader DESCRIPTORS_BASE_DIR = new ResourceLoader(RepositoryTestingBase.DESCRIPTORS_BASE_DIR,"git");
    
    private IssueCollection issues;
    
    @Test
    public void testCorrect() throws Exception {
        issues = getRepositoryBuilderIssuesFrom("default.yaml");
        assertTrue(issues.isEmpty());
    }
    
    @Test
    public void testMissingKind() throws Exception {
        issues = getRepositoryBuilderIssuesFrom("missingkind.yaml");
        assertThat(issues.size(),is(1));
        assertTrue(issues.contains(RepositoryIssue.MISSING_KIND));
    }
    
    @Test
    public void testMissingAddress() throws Exception {
        issues = getRepositoryBuilderIssuesFrom("missingaddress.yaml");
        assertThat(issues.size(),is(1));
        assertTrue(issues.contains(RepositoryIssue.MISSING_ADDRESS));
    }
    
    private IssueCollection getRepositoryBuilderIssuesFrom(String fileName) throws Exception {
    	RepositoryBuilder builder = new RepositoryBuilderImpl(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_BASE_DIR,fileName).getFile()));
    	builder.checkCorrectness();
    	return builder.getIssues();
    }
}

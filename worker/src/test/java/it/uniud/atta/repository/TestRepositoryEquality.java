/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import static org.junit.Assert.*;
import it.uniud.atta.common.lang.ResourceLoader;
import it.uniud.atta.common.yaml.YamlLoader;
import it.uniud.atta.repository.RepositoryBuilderImpl;
import it.uniud.atta.repository.api.*;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestRepositoryEquality {
    
    static final ResourceLoader DESCRIPTORS_BASE_DIR = new ResourceLoader(RepositoryTestingBase.DESCRIPTORS_BASE_DIR,"git");
    
    private Repository firstRepo, secondRepo;
    
    @Test
    public void testSame() throws Exception {
        firstRepo = getRepositoryFrom("default.yaml");
        secondRepo = getRepositoryFrom("default.yaml");
        assertThat(firstRepo,equalTo(secondRepo));
    }

    @Test
    public void testHavingSwitchedAddressAndMirrors() throws Exception {
        firstRepo = getRepositoryFrom("default.yaml");
        secondRepo = getRepositoryFrom("alternative.yaml");
        assertThat(firstRepo,equalTo(secondRepo));
    }
    
    @Test
    public void testOneInCommon() throws Exception {
        firstRepo = getRepositoryFrom("wrongfirstmirror.yaml");
        secondRepo = getRepositoryFrom("alternative.yaml");
        assertThat(firstRepo,equalTo(secondRepo));
    }
    
    @Test
    public void testNotEqualEvenIfSecondWouldAssociateFirstWithThird() throws Exception {
        firstRepo = getRepositoryFrom("default.yaml");
        secondRepo = getRepositoryFrom("wrongsecondmirror.yaml");
        Repository thirdRepo = getRepositoryFrom("wrongbothmirrors.yaml");
        assertThat(firstRepo,not(equalTo(thirdRepo)));
    }
    
    @Test
    public void testNoneInCommon() throws Exception {
        firstRepo = getRepositoryFrom("wrongbothmirrors.yaml");
        secondRepo = getRepositoryFrom("alternative.yaml");
        
        assertThat(firstRepo,not(equalTo(secondRepo)));
    }
    		
    private Repository getRepositoryFrom(String fileName) throws Exception {
    	return new RepositoryBuilderImpl(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_BASE_DIR,fileName).getFile())).build();
    }
}

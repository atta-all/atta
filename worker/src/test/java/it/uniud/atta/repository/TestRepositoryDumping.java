/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import it.uniud.atta.common.lang.ResourceLoader;
import it.uniud.atta.common.yaml.YamlLoader;
import it.uniud.atta.repository.RepositoryBuilderImpl;
import it.uniud.atta.repository.api.Repository;
import it.uniud.atta.repository.api.RepositoryKind;

import java.beans.IntrospectionException;
import java.util.*;

import org.junit.*;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.introspector.BeanAccess;
import org.yaml.snakeyaml.introspector.Property;
import org.yaml.snakeyaml.introspector.PropertyUtils;
import org.yaml.snakeyaml.nodes.NodeTuple;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;

public class TestRepositoryDumping {
	
    @Test
    public void testDumpRepository() throws Exception {
        
        Repository repo = new RepositoryBuilderImpl(YamlLoader.getInstance().loadAsMap(
        									        new ResourceLoader(RepositoryTestingBase.DESCRIPTORS_BASE_DIR,"git/default.yaml").getFile()))
                                               		.build();
        
        Representer repr = new ConvertRepositoryKindRepresenter();
        repr.setPropertyUtils(new UnsortedPropertyUtils());
        Yaml yaml = new Yaml(repr);
        @SuppressWarnings("unused")
        // TODO: perform checking on the result
        String output = yaml.dump(repo);
    }
    
    private class SkipNullOrEmptyRepresenter extends Representer {
        @SuppressWarnings("rawtypes")
        @Override
        protected NodeTuple representJavaBeanProperty(Object javaBean, Property property,
                Object propertyValue, Tag customTag) {
            if (propertyValue == null || 
                    (property.getType() == List.class && ((List)propertyValue).isEmpty()) || 
                    (property.getType() == Map.class && ((Map)propertyValue).isEmpty())) {
                return null;
            } else {
                return super.representJavaBeanProperty(javaBean, property, propertyValue, customTag);
            }
        }
    }
    
    private class ConvertRepositoryKindRepresenter extends SkipNullOrEmptyRepresenter {
        @Override
        protected NodeTuple representJavaBeanProperty(Object javaBean, Property property,
                Object propertyValue, Tag customTag) {

            if (propertyValue instanceof RepositoryKind) 
                return super.representJavaBeanProperty(javaBean, property, propertyValue.toString(), customTag);
                
            return super.representJavaBeanProperty(javaBean, property, propertyValue, customTag);
        }
    }
    
    private class UnsortedPropertyUtils extends PropertyUtils {
        @Override
        protected Set<Property> createPropertySet(Class<? extends Object> type, BeanAccess bAccess)
                throws IntrospectionException {
            Set<Property> result = new LinkedHashSet<Property>(getPropertiesMap(type,
                    BeanAccess.FIELD).values());
            return result;
        }
    }
}

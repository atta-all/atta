/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import static org.junit.Assert.*;
import it.uniud.atta.common.lang.OperatingSystem;
import it.uniud.atta.common.lang.OperatingSystemKind;
import it.uniud.atta.common.lang.ResourceLoader;
import it.uniud.atta.common.yaml.YamlLoader;
import it.uniud.atta.repository.RepositoryBuilderImpl;
import it.uniud.atta.repository.api.*;
import it.uniud.atta.repository.exception.*;

import java.io.*;

import net.lingala.zip4j.core.ZipFile;

import org.apache.commons.io.FileUtils;
import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestSvnRepository extends RepositoryTestingBase {
    
    private static Repository repo;
    private static String EXTENSION = ".yaml";
    private static ResourceLoader SOURCE = new ResourceLoader("repos/svn-artifacts-win.zip");
    private static String DESTINATION = "target/svn-repos";
    private static ResourceLoader DESCRIPTORS_DIR = new ResourceLoader(DESCRIPTORS_BASE_DIR,"svn");
    
    @BeforeClass
    public static void makeRepo() throws Exception {
        if (OperatingSystem.getInstance().getKind() == OperatingSystemKind.WINDOWS) {
            EXTENSION = "-win.yaml";
            extractRepos();
        }
        repo = new RepositoryBuilderImpl(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_DIR,"default" + EXTENSION).getFile())).build();
    }
    
    @AfterClass
    public static void cleanZippedRepos() throws Exception {
        if (OperatingSystem.getInstance().getKind() == OperatingSystemKind.WINDOWS) {
            FileUtils.deleteDirectory(new File(DESTINATION));
        }        
    }
    
    @Test
    public void testCommitRevisionKind() throws Exception {
        Checkout checkout = repo.retrieve(PATH_TO_CHECKOUT, "3");
        assertThat(checkout.getRevision().getKind(),is(RevisionKind.COMMIT));
    } 
    
    @Test
    public void testTipRevisionKind() throws Exception {
        Checkout checkout = repo.retrieve(PATH_TO_CHECKOUT, "");
        assertThat(checkout.getRevision().getName(),is("head"));
        assertThat(checkout.getRevision().getKind(),is(RevisionKind.TIP));
    }
    
    @Test(expected = InvalidRevisionFormatException.class)
    public void testInvalidRevisionFormatException() throws Exception {
        repo.retrieve(PATH_TO_CHECKOUT,"5f");
    }
    
    @Test(expected = UnknownRevisionException.class)
    public void testUnknownRevisionException() throws Exception {
        repo.retrieve(PATH_TO_CHECKOUT,"85");
    }
    
    @Test(expected = RepositoryPathMissingException.class)
    public void testRepositoryPathMissingException() throws Exception {
        repo.retrieve("inexistent","3");
    }
    
    private static void extractRepos() throws Exception {
        ZipFile zipFile = new ZipFile(SOURCE.getFile());
        zipFile.extractAll(DESTINATION);     
        FileUtils.copyDirectory(new File(DESTINATION,"svn-artifacts"), new File(DESTINATION,"svn-artifacts-mirror"));
    }
}

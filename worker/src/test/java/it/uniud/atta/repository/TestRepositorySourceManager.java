/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import static org.junit.Assert.*;
import it.uniud.atta.common.lang.ResourceLoader;
import it.uniud.atta.common.yaml.YamlLoader;
import it.uniud.atta.repository.RepositoryBase;
import it.uniud.atta.repository.RepositoryBuilderImpl;
import it.uniud.atta.repository.api.*;
import it.uniud.atta.repository.exception.*;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestRepositorySourceManager extends RepositoryTestingBase {
    
    private Repository repo1;
    private Repository repo2;
    private static ResourceLoader DESCRIPTORS_DIR = new ResourceLoader(DESCRIPTORS_BASE_DIR,"git");
    private static String revisionString = "1a75e2b5d2807ade5f52af0939dc5a75968eec66"; 

    @Test
    public void testChangeToWorkingMirror() throws Exception {
        repo1 = getRepositoryFrom("wrongfirstmirror.yaml"); 
        repo1.retrieve(PATH_TO_CHECKOUT, revisionString);
        assertThat(((RepositoryBase)repo1).getRetrievalAddress(),equalTo(repo1.getMirrors().iterator().next()));
    }
    
    @Test(expected = NoAvailableAddressException.class)
    public void testNoAvailableRepositoryException() throws Exception {
        repo1 = getRepositoryFrom("wrongbothmirrors.yaml"); 
        repo1.retrieve(PATH_TO_CHECKOUT, revisionString);        
    }
    
    @Test
    public void testReuseSourceWithSameSources() throws Exception {
        repo1 = getRepositoryFrom("default.yaml");
        repo1.retrieve(PATH_TO_CHECKOUT, revisionString);
        repo2 = getRepositoryFrom("alternative.yaml"); 
        repo2.retrieve(PATH_TO_CHECKOUT, revisionString);
        assertThat(((RepositoryBase)repo2).getRetrievalAddress(),equalTo(repo1.getAddress()));
    }
    
    @Test
    public void testReuseSourceWithInferredEquality() throws Exception {
        repo1 = getRepositoryFrom("default.yaml");
        repo1.retrieve(PATH_TO_CHECKOUT, revisionString);
        repo2 = getRepositoryFrom("wrongfirstmirror.yaml"); 
        repo2.retrieve(PATH_TO_CHECKOUT, revisionString);
        assertThat(((RepositoryBase)repo2).getRetrievalAddress(),equalTo(repo1.getAddress()));
    }
    
    private Repository getRepositoryFrom(String fileName) throws Exception {
    	return new RepositoryBuilderImpl(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_DIR,fileName).getFile())).build();
    }
}

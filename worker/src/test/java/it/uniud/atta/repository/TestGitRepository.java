/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.repository;

import static org.junit.Assert.*;
import it.uniud.atta.common.lang.ResourceLoader;
import it.uniud.atta.common.yaml.YamlLoader;
import it.uniud.atta.repository.RepositoryBuilderImpl;
import it.uniud.atta.repository.api.*;
import it.uniud.atta.repository.exception.*;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestGitRepository extends RepositoryTestingBase {
    
    private static Repository repo;
    private static ResourceLoader DESCRIPTORS_DIR = new ResourceLoader(DESCRIPTORS_BASE_DIR,"git");
    
    @BeforeClass
    public static void makeRepo() throws Exception {
        repo = new RepositoryBuilderImpl(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_DIR,"default.yaml").getFile())).build();
    }
    
    @Test
    public void testCommitRevisionKind() throws Exception {
        Checkout checkout = repo.retrieve(PATH_TO_CHECKOUT, "1a75e2b5d2807ade5f52af0939dc5a75968eec66");
        assertThat(checkout.getRevision().getKind(),is(RevisionKind.COMMIT));
    }
    
    @Test
    public void testTagRevisionKind() throws Exception {
        Checkout checkout = repo.retrieve(PATH_TO_CHECKOUT, "1.0");
        assertThat(checkout.getRevision().getKind(),is(RevisionKind.TAG));
    }    
    
    @Test
    public void testTipRevisionKind() throws Exception {
        Checkout checkout = repo.retrieve(PATH_TO_CHECKOUT, "mybranch");
        assertThat(checkout.getRevision().getKind(),is(RevisionKind.TIP));
    }    
    
    @Test
    public void testMasterRevision() throws Exception {
        Checkout checkout = repo.retrieve("types/abstract/composite/2dcoord", "");
        assertThat(checkout.getRevision().getName(),is("master"));
        assertThat(checkout.getRevision().getKind(),is(RevisionKind.TIP));
    }
    
    @Test(expected = UnknownRevisionException.class)
    public void testUnknownRefException() throws Exception {
        repo.retrieve(PATH_TO_CHECKOUT, "wrong");
    }
    
    @Test(expected = UnknownRevisionException.class)
    public void testUnknownHashException() throws Exception {
        repo.retrieve(PATH_TO_CHECKOUT, "2a75e2b5d2807ade5f52af0939dc5a75968eec66");
    }
    
    @Test(expected = LocalRevisionFormatException.class)
    public void testLocalRevisionFormatException() throws Exception {
        repo.retrieve(PATH_TO_CHECKOUT, "1a75e2b5d2");
    }
    
    @Test(expected = RepositoryPathMissingException.class)
    public void testRepositoryPathMissingException() throws Exception {
        repo.retrieve("types/abstr", "mybranch");        
    }
}

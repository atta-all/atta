/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.persistence;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static org.hamcrest.CoreMatchers.*;

public class TestByteArrayPersistence
{
    private static String DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    private static String PROTOCOL = "jdbc:derby:";
    private static String PATH = TestByteArrayPersistence.class.getResource("").getPath();
    private static String DBNAME = "derbyDB";
    
    private static Connection conn = null;
    
    private static Logger log;
    
    private static int NUMINTS = 1000;
        
    @BeforeClass
    public static void initialize() throws Exception {
        
        cleanup();
        
        log = LoggerFactory.getLogger(TestByteArrayPersistence.class);
        
        System.setProperty("derby.system.home", PATH); 
        
        Class.forName(DRIVER).newInstance();
        
        conn = DriverManager.getConnection(PROTOCOL + DBNAME + ";create=true");

        conn.createStatement().execute(
                "CREATE TABLE ByteArrays (id smallint not null primary key, hash blob not null)"
        );
    }
    
    @Test
    public void testSerializable() throws Exception {
            
        log.info("Testing a Serializable object:");
        
        SerializableObject originalSerializable = new SerializableObject();
        originalSerializable.name = "Luca Geretti";
        originalSerializable.intarray = new int[NUMINTS];
        
        SerializableObject recoveredSerializable = (SerializableObject) storeAndRetrieve(originalSerializable);
        
        assertThat(recoveredSerializable.name,equalTo(originalSerializable.name));
        assertThat(recoveredSerializable.intarray,equalTo(originalSerializable.intarray));
    }
    
    @Test
    public void testExternalizable() throws Exception {
        
        log.info("Testing an Externalizable object:");
        
        ExternalizableObject originalExternalizable = new ExternalizableObject();
        originalExternalizable.name = "Luca Geretti";
        originalExternalizable.intarray = new int[NUMINTS];
        
        ExternalizableObject recoveredExternalizable = (ExternalizableObject) storeAndRetrieve(originalExternalizable);
        
        assertThat(recoveredExternalizable.name,equalTo(originalExternalizable.name));
        assertThat(recoveredExternalizable.intarray,equalTo(originalExternalizable.intarray));
    }
    
    private Serializable storeAndRetrieve(Serializable original) 
                throws SQLException, IOException, ClassNotFoundException {

        String query = "INSERT INTO ByteArrays (id,hash) VALUES (?,?)";
        PreparedStatement pstmt = conn.prepareStatement(query);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        
        long initialTime = System.nanoTime();

        oos.writeObject(original);
        
        pstmt.setShort(1, (short)1);
        
        log.info("Serialization took " + (System.nanoTime() - initialTime)/1e3 + " micros");

        initialTime = System.nanoTime();
        
        pstmt.setBytes(2, bos.toByteArray());
        
        log.info("ByteArray extraction took " + (System.nanoTime() - initialTime)/1e3 + " micros");
        
        initialTime = System.nanoTime();
        
        pstmt.execute();
        
        log.info("Insertion of ByteArray took " + (System.nanoTime() - initialTime)/1e3 + " micros");

        initialTime = System.nanoTime();
        
        ResultSet results = conn.createStatement().executeQuery("SELECT hash FROM ByteArrays");
        
        assertTrue(results.next());
        
        byte[] retrievedBytes = results.getBytes("hash");
        
        log.info("Retrieval into ByteArray took " + (System.nanoTime() - initialTime)/1e3 + " micros");
        
        initialTime = System.nanoTime();
        
        ByteArrayInputStream bis = new ByteArrayInputStream(retrievedBytes);
        ObjectInputStream ois = new ObjectInputStream(bis);
        Serializable retrievedObject = (Serializable)ois.readObject();
        
        log.info("Deserialization took " + (System.nanoTime() - initialTime)/1e3 + " micros");
        
        return retrievedObject;
    }
    
    
    @After
    public void deleteTuples() throws Exception {
        conn.createStatement().execute("DELETE FROM ByteArrays");
    }
    
    @AfterClass
    public static void closeConnection() throws Exception {
        conn.close();
    }
    
    public static void cleanup() throws Exception {
        FileUtils.deleteDirectory(new File(PATH + DBNAME));
    }
}

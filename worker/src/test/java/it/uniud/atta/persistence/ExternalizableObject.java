/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.persistence;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class ExternalizableObject implements Externalizable {

    public String name;
    
    public int[] intarray;
    
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        
        out.writeUTF(name);
        out.writeInt(intarray.length);
        
        for (int val : intarray)
            out.writeInt(val);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException,
            ClassNotFoundException {
        
        this.name = in.readUTF();
        
        int arraylength = in.readInt();
        this.intarray = new int[arraylength];
        
        for (int i=0;i<arraylength;i++)
            this.intarray[i] = in.readInt();
    }

}

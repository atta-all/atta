/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.codegen.velocity;

import java.util.*;
import java.io.*;

import javax.xml.parsers.*;

import org.xml.sax.*;
import org.apache.velocity.app.*;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.apache.velocity.*;

public class ClassGenerator {

    private static List<ClassDescriptor> classes;
    private static ClassDescriptorImporter cdImporter;
    private static XMLReader xmlReader;

    static void init() throws Exception {

        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setNamespaceAware(true);
        SAXParser saxParser = spf.newSAXParser();
        xmlReader = saxParser.getXMLReader();

        cdImporter = new ClassDescriptorImporter();
        xmlReader.setContentHandler(cdImporter);

        Velocity.addProperty(RuntimeConstants.RUNTIME_LOG, "target/velocity.log");
        Velocity.addProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        Velocity.addProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        Velocity.init();
    }

    public static void start(String modelFile, String templateFile) throws Exception {

        FileInputStream input = new FileInputStream(modelFile);
        xmlReader.parse(new InputSource(input));
        input.close();
        classes = cdImporter.getClasses();

        GeneratorUtility utility = new GeneratorUtility();
        for (int i = 0; i < classes.size(); i++) {

            VelocityContext context = new VelocityContext();
            ClassDescriptor cl = (ClassDescriptor) classes.get(i);
            context.put("class", cl);
            context.put("utility", utility);

            Template template = Velocity.getTemplate(templateFile);

            BufferedWriter writer = new BufferedWriter(new FileWriter("target/" + cl.getName()+".java"));

            template.merge(context, writer);
            writer.flush();
            writer.close();
        }
    }
}
/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.codegen.velocity;

import java.util.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

public class ClassDescriptorImporter extends DefaultHandler {

    private List<ClassDescriptor> classes = new ArrayList<>();

    public List<ClassDescriptor> getClasses() {
        return classes;
    }

    @Override
    public void startElement(String uri, String name, String qName, Attributes attr) throws SAXException {

        if (name.equals("Class")) {
            ClassDescriptor cl = new ClassDescriptor();
            cl.setName(attr.getValue("name"));
            classes.add(cl);
        } else if (name.equals("Attribute")) {
            AttributeDescriptor at = new AttributeDescriptor();
            at.setName(attr.getValue("name"));
            at.setType(attr.getValue("type"));
            ClassDescriptor parent = (ClassDescriptor) classes.get(classes.size()-1);
            parent.addAttribute(at);
        } else if (name.equals("Content")) {
        
        } else 
            throw new SAXException("Element " + name + " not valid");
    }

    @Override
    public void endElement(String uri, String name, String qName) throws SAXException { }
}
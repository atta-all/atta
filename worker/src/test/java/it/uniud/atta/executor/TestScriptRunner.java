/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.executor;

import it.uniud.atta.repository.RepositoryAddressFactory;
import it.uniud.atta.repository.RepositoryBuilderImpl;
import it.uniud.atta.repository.api.Checkout;
import it.uniud.atta.repository.api.Repository;
import it.uniud.atta.repository.api.RepositoryKind;

import java.io.File;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.exec.Executor;
import org.apache.commons.exec.LogOutputStream;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteStreamHandler;
import org.apache.commons.exec.PumpStreamHandler;
import org.junit.*;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class TestScriptRunner {

    public class CollectingLogOutputStream extends LogOutputStream {
        private final List<String> lines = new LinkedList<>();
        
        @Override protected void processLine(String line, int level) {
            lines.add(line);
        }   
        public List<String> getLines() {
            return lines;
        }
    }
    
    @Test
    public void testRunScript() throws Exception {
        
        Repository repository = new RepositoryBuilderImpl(RepositoryKind.GIT,RepositoryAddressFactory.makeFrom("src/test/resources/repos/git-artifacts",RepositoryKind.GIT),new HashSet<>()).build();
        Checkout checkout = repository.retrieve("properties/find-tools/cmake", "");
        File outputDirectory = checkout.getPath();
        CommandLine commandLine = new CommandLine("sh")
                                      .addArgument("findCMake.sh");
        
        Executor executor = new DefaultExecutor();

        Map<String,String> environment = EnvironmentForTesting.get();
        
        CollectingLogOutputStream collecting = new CollectingLogOutputStream();
        ExecuteStreamHandler streamHandler = new PumpStreamHandler(collecting);
        
        executor.setStreamHandler(streamHandler);
        executor.setWorkingDirectory(outputDirectory);
        int result = executor.execute(commandLine,environment);
        assertThat(result,is(0));
        assertThat(collecting.getLines().size(),is(1));
    }
    
    @Test
    public void testRunScriptEnvironmentDependent() throws Exception {
        
        Repository repository = new RepositoryBuilderImpl(RepositoryKind.GIT,RepositoryAddressFactory.makeFrom("src/test/resources/repos/git-artifacts",RepositoryKind.GIT),new HashSet<>()).build();
        Checkout checkout = repository.retrieve("properties/find-tools/env-example", "");
        File outputDirectory = checkout.getPath();
        CommandLine commandLine = new CommandLine("sh")
                                      .addArgument("findCMakeOrJava.sh");
        
        Executor executor = new DefaultExecutor();

        CollectingLogOutputStream collecting = new CollectingLogOutputStream();
        
        executor.setStreamHandler(new PumpStreamHandler(collecting));
        executor.setWorkingDirectory(outputDirectory);
        
        Map<String,String> environment = EnvironmentForTesting.get();
        
        environment.put("EXECUTABLE", "cmake");
        int result = executor.execute(commandLine,environment);
        assertThat(result,is(0));
        assertThat(collecting.getLines().size(),is(1));
        String cmakeVersion = collecting.getLines().get(0);
        assertFalse(cmakeVersion.isEmpty());
        environment.put("EXECUTABLE", "java");
        collecting = new CollectingLogOutputStream();
        executor.setStreamHandler(new PumpStreamHandler(collecting));
        result = executor.execute(commandLine,environment);
        assertThat(result,is(0));
        assertThat(collecting.getLines().size(),is(1));
        String javaVersion = collecting.getLines().get(0);
        assertFalse(javaVersion.isEmpty());
        assertThat(cmakeVersion,is(not(javaVersion)));
    }
}

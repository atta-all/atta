/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common.issue;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import it.uniud.atta.common.issue.Issue;
import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.common.issue.IssueCollector;
import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.metamodel.PublishedInternalResolutionIssue;
import it.uniud.atta.metamodel.locator.LocatorIssue;

import org.junit.Before;
import org.junit.Test;

public class TestIssueCollection {
    
    private IssueCollection collection;
    
    private IssueCollector testCollector = new IssueCollector() {

		@Override
        public IssueCollection getIssues() {
            return null;
        }

		@Override
        public void appendIssue(Issue issue) {
        }

		@Override
        public void appendIssue(Issue issue, String reason) {
        }

		@Override
        public void appendChildIssues(IssueCollection child) {
        } };
    
    @Before
    public void reset() {
        collection = new IssueCollection(testCollector);
    }
    
    @Test
    public void testOk() {
        assertThat(collection.getSummary(),is(IssueLevel.OK));
    }
    
    @Test
    public void testWarning() {
        collection.append(PublishedInternalResolutionIssue.UNUSED_LABEL);
        assertThat(collection.getSummary(),is(IssueLevel.WARNING));
    }
    
    @Test
    public void testError() {
        collection.append(PublishedInternalResolutionIssue.UNUSED_LABEL)
                  .append(PublishedInternalResolutionIssue.INVALID_REFERENCE_NAME);
        assertThat(collection.getSummary(),is(IssueLevel.ERROR));
    }
    
    @Test
    public void testDuplicates() {
        collection.append(PublishedInternalResolutionIssue.UNUSED_LABEL)
                  .append(PublishedInternalResolutionIssue.UNUSED_LABEL);
        assertThat(collection.getAll().size(),is(1));
    }
    
    @Test
    public void testContainsSameLevel() {
    	collection.append(PublishedInternalResolutionIssue.CYCLIC_DEPENDENCY);
    	assertTrue(collection.contains(PublishedInternalResolutionIssue.CYCLIC_DEPENDENCY));
    	assertFalse(collection.contains(PublishedInternalResolutionIssue.UNBOUND_REFERENCE));
    }
    
    @Test
    public void testContainsOtherLevel() {
    	collection.append(PublishedInternalResolutionIssue.DEFINED_NAME_NOT_COMPLIANT);
        IssueCollection other = new IssueCollection(testCollector)
        .append(PublishedInternalResolutionIssue.CYCLIC_DEPENDENCY)
        .append(PublishedInternalResolutionIssue.UNBOUND_REFERENCE);
    	collection.append(other);
    	assertFalse(collection.contains(PublishedInternalResolutionIssue.INVALID_REFERENCE_NAME));
    	assertTrue(collection.contains(PublishedInternalResolutionIssue.UNBOUND_REFERENCE));
    }
    
    @Test
    public void testHasIssuesForGivenResponsible() {
    	collection.append(PublishedInternalResolutionIssue.INVALID_REFERENCE_NAME);
        IssueCollection other = new IssueCollection(testCollector)
        .append(PublishedInternalResolutionIssue.CYCLIC_DEPENDENCY)
        .append(PublishedInternalResolutionIssue.UNBOUND_REFERENCE);
    	collection.append(other);
    	assertTrue(collection.hasIssuesFor(testCollector));
    }
    
    @Test
    public void testChildren() {
        
        IssueCollection repoIssues = new IssueCollection(testCollector)
                                .append(LocatorIssue.MISSING_REPOSITORY)
                                .append(LocatorIssue.MISSING_PATH);
        
        collection.append(repoIssues);
        
        assertThat(collection.getAll().size(),is(1));
        assertThat(((IssueCollection)collection.getAll().iterator().next()).getAll().size(),is(2));
    }
}

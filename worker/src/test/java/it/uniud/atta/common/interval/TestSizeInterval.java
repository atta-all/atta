/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common.interval;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import it.uniud.atta.common.interval.SizeInterval;
import it.uniud.atta.common.interval.exception.*;

import org.junit.Test;

public class TestSizeInterval {
    
    @Test(expected = EmptySizeIntervalException.class) 
    public void testEmptySizeIntervalException() throws InvalidSizeIntervalException {
        
        new SizeInterval(2,1);
    }     

    @Test 
    public void testUnboundedSizeIntervalException() throws InvalidSizeIntervalException {
        
        SizeInterval s = new SizeInterval(2,Integer.MAX_VALUE);
        assertFalse(s.isBounded());
    }    

    @Test(expected = NegativeLowerBoundException.class) 
    public void testNegativeLowerBoundException() throws InvalidSizeIntervalException {
        
        new SizeInterval(-1,2);
    }
    
    @Test(expected = ZeroSizeIntervalException.class) 
    public void testZeroSizeIntervalException() throws InvalidSizeIntervalException {
        
        new SizeInterval(0,0);
    }
    
    @Test
    public void testCanonicSizeInterval() throws InvalidSizeIntervalException {
        
        SizeInterval s = new SizeInterval(0,5);
        assertTrue(s.isBounded());
        assertFalse(s.isSingleton());
        assertThat(s.getLowerBound(),is(0));
        assertThat(s.getUpperBound(),is(5));
    }
    
    @Test
    public void testFixedSizeInterval() throws InvalidSizeIntervalException {
        
        SizeInterval s = new SizeInterval(3);
        assertTrue(s.isBounded());
        assertTrue(s.isFixed());
        assertThat(s.getLowerBound(),is(3));
        assertThat(s.getUpperBound(),is(3));
    }
    
    @Test
    public void testSingleton() throws InvalidSizeIntervalException {
        
        SizeInterval s = new SizeInterval(1);
        assertTrue(s.isSingleton());
        assertThat(s.getLowerBound(),is(1));
        assertThat(s.getUpperBound(),is(1));
    }
    
    @Test
    public void testAnySizeInterval() throws InvalidSizeIntervalException {
        
        SizeInterval s = new SizeInterval(0,Integer.MAX_VALUE);
        assertFalse(s.isBounded());
        assertFalse(s.isSingleton());
        assertThat(s.getLowerBound(),is(0));
    }
    
    @Test
    public void testSameIsIncludedWithin() throws InvalidSizeIntervalException {
        
        SizeInterval s1 = new SizeInterval(3,5);
        SizeInterval s2 = new SizeInterval(3,5);
        assertTrue(s1.isIncludedWithin(s2));
    }
    
    @Test
    public void testIsNotIncludedWithin() throws InvalidSizeIntervalException {
        
        SizeInterval s1 = new SizeInterval(3,5);
        SizeInterval s2 = new SizeInterval(2,5);
        SizeInterval s3 = new SizeInterval(3,6);
        SizeInterval s4 = new SizeInterval(4,6);
        assertFalse(s2.isIncludedWithin(s1));
        assertFalse(s3.isIncludedWithin(s1));
        assertFalse(s4.isIncludedWithin(s1));
    }
    
    @Test
    public void testIsIncludedWithin() throws InvalidSizeIntervalException {
        
        SizeInterval s1 = new SizeInterval(1);
        SizeInterval s2 = new SizeInterval(0,5);
        SizeInterval s3 = new SizeInterval(0,6);
        SizeInterval s4 = new SizeInterval(0,Integer.MAX_VALUE);
        assertTrue(s1.isIncludedWithin(s2));
        assertTrue(s2.isIncludedWithin(s3));
        assertTrue(s4.isIncludedWithin(s4));
    }
}

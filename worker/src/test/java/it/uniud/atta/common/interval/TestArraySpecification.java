/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common.interval;

import static org.junit.Assert.*;
import it.uniud.atta.common.interval.ArraySpecification;
import it.uniud.atta.common.interval.SizeInterval;
import it.uniud.atta.common.interval.exception.*;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;

public class TestArraySpecification {
    
    @Test
    public void testCanonicArraySpecification() throws Exception {
        
        ArraySpecification s = new ArraySpecification(":5,2:3");
        assertThat(s.getNumDimensions(),is(2));
        SizeInterval int1 = s.get(0);
        SizeInterval int2 = s.get(1);
        assertThat(int1.getLowerBound(),is(0));
        assertThat(int1.getUpperBound(),is(5));
        assertThat(int2.getLowerBound(),is(2));
        assertThat(int2.getUpperBound(),is(3));
    }
    
    @Test
    public void testWidthOneInterval() throws Exception {
        
        ArraySpecification s = new ArraySpecification("3");
        assertThat(s.getNumDimensions(),is(1));
        assertThat(s.get(0).getLowerBound(),is(3));
        assertThat(s.get(0).getUpperBound(),is(3));
    }
    
    @Test
    public void testLowerBoundOnHigherDimensions() throws Exception {
        
        ArraySpecification s = new ArraySpecification("3,:");
        assertThat(s.get(1).getLowerBound(),is(1));
    }
    
    @Test
    public void testImplicitSizes() throws Exception {
        
        ArraySpecification s = new ArraySpecification(":5,1:3");
        int[] sizes = new int[]{4};
        assertTrue(s.covers(sizes));
    }
    
    @Test
    public void testSizesAreTooLarge() throws Exception {
        
        ArraySpecification s = new ArraySpecification(":5,2:3");
        int[] sizes = new int[]{2,4};
        assertFalse(s.covers(sizes));
    }
    
    @Test
    public void testSizesAreTooSmall() throws Exception {
        
        ArraySpecification s = new ArraySpecification(":5,2:3");
        int[] sizes = new int[]{2,1};
        assertFalse(s.covers(sizes));
    }
    
    @Test
    public void testPrinting() throws Exception {
    	ArraySpecification s = new ArraySpecification("0:2147483647,1:3");
    	assertThat(s.toString(),is(":,:3"));
    }
    
    @Test(expected = EmptyArraySpecificationException.class)
    public void testEmptyArraySpecificationException() throws Exception {
        
        new ArraySpecification("");
    }
    
    @Test(expected = InvalidArraySpecificationException.class)
    public void testInvalidArraySpecificationException() throws Exception {
        
        new ArraySpecification("-1");
    }
    
    @Test(expected = InvalidArraySpecificationException.class)
    public void testLowerBoundIsZeroForHigherDimensionException() throws Exception {
        
        new ArraySpecification(":5,0:3");
    }
    
    @Test(expected = SingletonOnMultidimensionalArrayException.class)
    public void testSingletonOnMultidimensionalArrayException() throws Exception {
        
        new ArraySpecification(":5,1:1");
    }
    
    @Test
    public void testIsNotIncludedWithin() throws Exception {
    	ArraySpecification s1 = new ArraySpecification(":3,2:6");
    	ArraySpecification s2 = new ArraySpecification("1:3,2:6");
    	ArraySpecification s3 = new ArraySpecification("1:3");
    	ArraySpecification s4 = new ArraySpecification(":");
    	
    	assertFalse(s1.isIncludedWithin(s2));
    	assertFalse(s2.isIncludedWithin(s3));
    	assertFalse(s1.isIncludedWithin(s3));
    	assertFalse(s1.isIncludedWithin(s4));
    }
    
    @Test
    public void testIsIncludedWithin() throws Exception {
    	ArraySpecification s1 = new ArraySpecification(":,:");
    	ArraySpecification s2 = new ArraySpecification("1:3,:6");
    	ArraySpecification s3 = new ArraySpecification("1:3");
    	ArraySpecification s4 = new ArraySpecification(":");
    	
    	assertTrue(s3.isIncludedWithin(s2));
    	assertTrue(s4.isIncludedWithin(s1));
    	assertTrue(s3.isIncludedWithin(s2));
    }
}

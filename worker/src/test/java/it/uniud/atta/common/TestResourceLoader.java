/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common;

import it.uniud.atta.common.lang.ResourceLoader;

import java.io.FileNotFoundException;

import org.junit.Test;

public class TestResourceLoader {
    
    @Test
    public void testCorrect() throws Exception {
        
        ResourceLoader loader = new ResourceLoader("yaml/repo/git/default.yaml");
        loader.getFile();
    }
    
    @Test(expected = FileNotFoundException.class)
    public void testIncorrect() throws Exception {
        
        ResourceLoader loader = new ResourceLoader("yaml/repo/git");
        loader.getFile();
    }
    
    @Test(expected = FileNotFoundException.class)
    public void testMissingFile() throws Exception {
        
        ResourceLoader loader = new ResourceLoader("yaml/repo/git/missing.yaml");
        loader.getFile();
    }
    
    @Test
    public void testChainedLoader() throws Exception {
        
        ResourceLoader another = new ResourceLoader("yaml/repo/git");
        ResourceLoader loader = new ResourceLoader(another,"default.yaml");
        loader.getFile();
    }
}

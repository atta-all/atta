/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common;

import static org.junit.Assert.*;

import it.uniud.atta.common.lang.PermissiveNameChecker;

import org.junit.Test;

public class TestPermissiveNameChecker {
    
    @Test
    public void testTextual() throws Exception {
        assertTrue(PermissiveNameChecker.validate("Test"));
    }
    
    @Test
    public void testEmpty() throws Exception {   
        assertFalse(PermissiveNameChecker.validate(""));
    }
    
    @Test
    public void testInvalidStartingChar() throws Exception {
        assertFalse(PermissiveNameChecker.validate("1est"));
    }
        
    @Test
    public void testValidYetWeird() throws Exception {
        assertTrue(PermissiveNameChecker.validate("_1.:_"));
    }
    
    @Test
    public void testInvalidDotUse() throws Exception {
        assertFalse(PermissiveNameChecker.validate("a_."));
    }
    
    @Test
    public void testInvalidColonUse() throws Exception {
        assertFalse(PermissiveNameChecker.validate("a_.:"));
    }    
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common;

import static org.junit.Assert.*;
import it.uniud.atta.common.lang.StrictNameChecker;

import org.junit.Test;

public class TestStrictNameChecker {
    
    @Test
    public void testTextual() throws Exception {      
        assertTrue(StrictNameChecker.validate("Test"));
    }
    
    @Test
    public void testInvalidStartingChar() throws Exception {      
        assertFalse(StrictNameChecker.validate("1est"));
    }
    
    @Test
    public void testInvalidChar() throws Exception {
        assertFalse(StrictNameChecker.validate("Te$t"));
    }
        
    @Test
    public void testValidYetWeird() throws Exception {
        assertTrue(StrictNameChecker.validate("_1_"));
    }
}

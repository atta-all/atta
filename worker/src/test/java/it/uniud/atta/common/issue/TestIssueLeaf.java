/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.common.issue;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import it.uniud.atta.common.issue.IssueLeaf;
import it.uniud.atta.metamodel.PublishedInternalResolutionIssue;

import org.junit.Test;

public class TestIssueLeaf {
    
    @Test
    public void testEqual() {
    	IssueLeaf node1 = new IssueLeaf(PublishedInternalResolutionIssue.UNUSED_LABEL);
    	IssueLeaf node2 = new IssueLeaf(PublishedInternalResolutionIssue.UNUSED_LABEL);
        assertEquals(node1,node2);
    }
    
    @Test
    public void testUnequalIssue() {
    	IssueLeaf node1 = new IssueLeaf(PublishedInternalResolutionIssue.UNUSED_LABEL);
    	IssueLeaf node2 = new IssueLeaf(PublishedInternalResolutionIssue.UNBOUND_REFERENCE);
        assertThat(node1,not(equalTo(node2)));
    }
}

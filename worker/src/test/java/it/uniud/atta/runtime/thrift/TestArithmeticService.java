/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.runtime.thrift;

import static org.junit.Assert.*;

import java.io.Closeable;

import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;
import org.apache.thrift.async.TAsyncClientManager;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.server.TNonblockingServer;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadPoolServer;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TNonblockingServerTransport;
import org.apache.thrift.transport.TNonblockingSocket;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.junit.*;

public class TestArithmeticService {
    
    public static class Server implements Runnable, Closeable {
        
        private TServer tserver = null;
		private TServerSocket serverTransport = null;
		
        @Override
        public void run() {
            ArithmeticService.Processor<ArithmeticServiceImpl> processor = new ArithmeticService.Processor<>(new ArithmeticServiceImpl());
            
            tserver = new TThreadPoolServer(new TThreadPoolServer.Args(serverTransport).processor(processor));
            tserver.serve();
        }

        public void start() throws TTransportException {
        	
        	serverTransport = new TServerSocket(7911);
        	
            new Thread(this).start();
        }

        @Override
        public void close() {
        	if (serverTransport != null)
        		serverTransport.close();
        	if (tserver != null)
        		tserver.stop();
        }
    }
    
    public static class NonblockingTransportServer implements Runnable, Closeable {
        
        private TNonblockingServer tserver = null;
        private TNonblockingServerTransport serverTransport = null;
        
        @Override
        public void run() {
            ArithmeticService.Processor<ArithmeticServiceImpl> processor = new ArithmeticService.Processor<>(new ArithmeticServiceImpl());
                
            tserver = new TNonblockingServer(new TNonblockingServer.Args(serverTransport).processor(processor));
            tserver.serve();
        }

        public void start() throws TTransportException {
        	
        	serverTransport = new TNonblockingServerSocket(7911);
        	
            new Thread(this).start();
        }

        @Override
        public void close() {
        	if (serverTransport != null)
        		serverTransport.close();
        	if (tserver != null)
        		tserver.stop();
        }
    }
    
    public static class AsyncServer implements Runnable, Closeable {
        
        private TNonblockingServer tserver = null;
        private TNonblockingServerTransport serverTransport = null;
        
        @Override
        public void run() {
            ArithmeticService.AsyncProcessor<AsyncArithmeticServiceImpl> processor = new ArithmeticService.AsyncProcessor<>(new AsyncArithmeticServiceImpl());
                
            tserver = new TNonblockingServer(new TNonblockingServer.Args(serverTransport).processor(processor));
            tserver.serve();
        }

        public void start() throws TTransportException {
        	serverTransport = new TNonblockingServerSocket(7911);
        	
            new Thread(this).start();
        }

        @Override
        public void close() {
        	if (serverTransport != null)
        		serverTransport.close();        	
        	if (tserver != null)
        		tserver.stop();
        }
    }

    @Test
    public void testBlocking() throws Exception {

        Server server = new Server();
        TTransport transport = null;
        try {
            server.start();

            Thread.sleep(100);
        		
            transport = new TSocket("localhost", 7911);

            TProtocol protocol = new TBinaryProtocol(transport);

            ArithmeticService.Client client = new ArithmeticService.Client(protocol);
            transport.open();

            long addResult = client.add(38, 4);
            assertEquals(42,addResult);
            
            long multiplyResult = client.multiply(7, 12);
            assertEquals(84,multiplyResult);        	
        } finally {
        	if (transport != null)
        		transport.close();
	        server.close();
        }
    }

    @Test
    public void testAsyncClientWithNonblockingTransportServer() throws Exception {

        NonblockingTransportServer server = new NonblockingTransportServer();
        
        try {
	        server.start();
	
	        ArithmeticService.AsyncClient client = null;
	
	        Thread.sleep(100);
	       
	        TAsyncClientManager clientManager = new TAsyncClientManager();
	        TProtocolFactory protocolFactory = new TBinaryProtocol.Factory();
	
	        client = new ArithmeticService.AsyncClient(protocolFactory,clientManager,new TNonblockingSocket("localhost", 7911));
	        
	        AddMethodCallback addCb = new AddMethodCallback();
	        client.add(38, 4, addCb);
	        
	        client = new ArithmeticService.AsyncClient(protocolFactory,clientManager,new TNonblockingSocket("localhost", 7911));
	        MultiplyMethodCallback multiplyCb = new MultiplyMethodCallback();
	        client.multiply(7, 12, multiplyCb);
	
	        while(!(addCb.hasCompleted() && multiplyCb.hasCompleted())) {
	            Thread.sleep(50);
	        }
	            
	        assertEquals(42,addCb.getValue());
	        assertEquals(84,multiplyCb.getValue());
        } finally {
        	server.close();
        }
    }
    
    @Test
    public void testAsync() throws Exception {

    	AsyncServer server = new AsyncServer();
    	try {
	        server.start();
	
	        ArithmeticService.AsyncClient client = null;
	
	        Thread.sleep(100);
	       
	        TAsyncClientManager clientManager = new TAsyncClientManager();
	        TProtocolFactory protocolFactory = new TBinaryProtocol.Factory();
	
	        client = new ArithmeticService.AsyncClient(protocolFactory,clientManager,new TNonblockingSocket("localhost", 7911));
	        
	        AddMethodCallback addCb = new AddMethodCallback();
	        client.add(38, 4, addCb);
	        
	        client = new ArithmeticService.AsyncClient(protocolFactory,clientManager,new TNonblockingSocket("localhost", 7911));
	        MultiplyMethodCallback multiplyCb = new MultiplyMethodCallback();
	        client.multiply(7, 12, multiplyCb);
	
	        while(!(addCb.hasCompleted() && multiplyCb.hasCompleted())) {           
	            Thread.sleep(50);
	        }
	            
	        assertEquals(42,addCb.getValue());
	        assertEquals(84,multiplyCb.getValue());

    	} finally { 
    		server.close();
    	}
    }
    
    class AddMethodCallback implements AsyncMethodCallback<ArithmeticService.AsyncClient.add_call> {

        private long valueObtained;
        private volatile boolean hasCompleted;
        
        public void onComplete(ArithmeticService.AsyncClient.add_call add_call) {
            try {
                valueObtained = add_call.getResult();
                hasCompleted = true;
            } catch (TException e) {
                e.printStackTrace();
            }
        }
        
        public void onError(Exception e) {
            e.printStackTrace();
        }
        
        public long getValue() {
            return valueObtained;
        }

        public boolean hasCompleted() {
            return hasCompleted;
        }
    }
    
    class MultiplyMethodCallback implements AsyncMethodCallback<ArithmeticService.AsyncClient.multiply_call> {

        private long valueObtained;
        private volatile boolean hasCompleted;
        
        public void onComplete(ArithmeticService.AsyncClient.multiply_call multiply_call) {
            try {
                valueObtained = multiply_call.getResult();
                hasCompleted = true;
            } catch (TException e) {
                e.printStackTrace();
            }
        }
        
        public void onError(Exception e) {
            e.printStackTrace();
        }
        
        public long getValue() {
            return valueObtained;
        }

        public boolean hasCompleted() {
            return hasCompleted;
        }
    }
}

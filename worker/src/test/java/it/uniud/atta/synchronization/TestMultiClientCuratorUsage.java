/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.synchronization;

import static org.junit.Assert.*;

import org.junit.*;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.api.CuratorWatcher;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.test.TestingServer;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher.Event.EventType;

@Ignore
public class TestMultiClientCuratorUsage {

    private static final String connectionString = "localhost";
    private static final String PATH = "/etest";

    private static TestingServer server;

    @BeforeClass
    public static void startServer() throws Exception {
        
        server = new TestingServer(2181);
    }
    
    @AfterClass
    public static void stopServer() throws IOException {
        server.stop();
    }
    
    @Test
    public void testEphemeral() throws Exception {
        
        RetryPolicy policy = new ExponentialBackoffRetry(1000, 3);
        CuratorFramework client1 = CuratorFrameworkFactory.newClient(connectionString, policy);
        client1.start();
        CuratorFramework client2 = CuratorFrameworkFactory.newClient(connectionString, policy);
        client2.start(); 
        
        client1.create().withMode(CreateMode.EPHEMERAL).forPath(PATH);
        assertNotNull(client2.checkExists().forPath(PATH));
        
        client1.close();
        assertNull(client2.checkExists().forPath(PATH));
        
        client2.close();
    }
    
    @Test
    public void testWatcher() throws Exception {
        
        final CountDownLatch deletedSignal = new CountDownLatch(1);
        
        RetryPolicy policy = new ExponentialBackoffRetry(1000, 3);
        CuratorFramework client1 = CuratorFrameworkFactory.newClient(connectionString, policy);
        client1.start();
        CuratorFramework client2 = CuratorFrameworkFactory.newClient(connectionString, policy);
        client2.start(); 
        
        client1.create().withMode(CreateMode.EPHEMERAL).forPath(PATH);
        assertNotNull(client2.checkExists().forPath(PATH));
        
        client2.checkExists().usingWatcher(new CuratorWatcher() {

                @Override
                public void process(WatchedEvent event) throws Exception {
                    if (event.getType() == EventType.NodeDeleted)
                        deletedSignal.countDown();
                }
            }
        ).forPath(PATH);
        
        client1.close();
        
        deletedSignal.await();
        
        assertNull(client2.checkExists().forPath(PATH));
        
        client2.close();
    }  
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.synchronization;

import static org.junit.Assert.*;

import org.junit.*;
import static org.hamcrest.CoreMatchers.*;

import java.io.IOException;
import java.util.Random;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.test.TestingServer;
import org.apache.zookeeper.CreateMode;

@Ignore
public class TestSingleClientCuratorUsage {

    private static final String connectionString = "localhost";
    private static final int NUMCHARS = 10;
    private static final String PATH = "/atta";

    private static CuratorFramework client;
    private static TestingServer server;

    @BeforeClass
    public static void startServer() throws Exception {
        
        server = new TestingServer(2181);
    }
    
    @AfterClass
    public static void stopServer() throws IOException {
        server.stop();
    }
    
    @Before
    public void connect() throws IOException, InterruptedException {
        
        ExponentialBackoffRetry retryPolicy = new ExponentialBackoffRetry(1000, 3);
        
        client = CuratorFrameworkFactory.newClient(connectionString, retryPolicy);
        client.start();
    }
    
    @After
    public void deleteAndDisconnect() throws Exception {
        if (client.checkExists().forPath(PATH) != null)
            client.delete().deletingChildrenIfNeeded().forPath(PATH);
        
        client.close();
    }

    @Test
    public void testCreateNodeWithData() throws Exception {
        
        Random rnd = new Random();
        
        byte[] randomBytes = new byte[NUMCHARS];
        rnd.nextBytes(randomBytes);
        
        client.create().withMode(CreateMode.EPHEMERAL).forPath(PATH, randomBytes);
        
        byte[] readBytes = client.getData().forPath(PATH);
        
        assertThat(readBytes,equalTo(randomBytes));
    }
    
    @Test
    public void testCreateAndDeleteNode() throws Exception {
        
        client.create().withMode(CreateMode.EPHEMERAL).forPath(PATH);
        
        assertNotNull(client.checkExists().forPath(PATH));
        
        client.delete().forPath(PATH);
        
        assertNull(client.checkExists().forPath(PATH));
    }

    @Test
    public void testCreateSubpath() throws Exception {
        
        final String fullPath = PATH + "/subpath"; 
        
        client.create().forPath(PATH);
        client.create().forPath(fullPath);
        assertNotNull(client.checkExists().forPath(PATH));
        assertNotNull(client.checkExists().forPath(fullPath));
    }
    
    @Test
    public void testSequentialNodes() throws Exception {
        
        String first = client.create().withMode(CreateMode.PERSISTENT_SEQUENTIAL).forPath(PATH);
        assertNotNull(client.checkExists().forPath(first));
        
        String second = client.create().withMode(CreateMode.PERSISTENT_SEQUENTIAL).forPath(PATH);
        assertNotNull(client.checkExists().forPath(second));
        
        int firstSeqNum = Integer.valueOf(first.substring(PATH.length()));
        int secondSeqNum = Integer.valueOf(second.substring(PATH.length()));
        
        assertTrue(firstSeqNum < secondSeqNum);
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.synchronization;

import static org.junit.Assert.*;

import org.junit.*;
import static org.hamcrest.CoreMatchers.*;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.CountDownLatch;

import org.apache.curator.test.TestingServer;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.KeeperState;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;

@Ignore
public class TestZookeeper {

    private static final int SESSION_TIMEOUT = 10000;
    private static final String HOSTS = "localhost";
    private static final int NUMCHARS = 10;

    private ZooKeeper zkClient;
    private static TestingServer zkServer;
    private CountDownLatch connectedSignal = new CountDownLatch(1);

    @BeforeClass
    public static void startServer() throws Exception {
        
        zkServer = new TestingServer(2181);
    }
    
    @AfterClass
    public static void stopServer() throws IOException {
        zkServer.stop();
    }
    
    @Before
    public void connect() throws IOException, InterruptedException {
        zkClient = new ZooKeeper(HOSTS, SESSION_TIMEOUT, new Watcher() {
            @Override
            public void process(WatchedEvent event) {
                if (event.getState() == KeeperState.SyncConnected) {
                    connectedSignal.countDown();
                }
            }            
        });
        connectedSignal.await();
    }
    
    @After
    public void disconnect() throws InterruptedException {
        zkClient.close();
    }

    @Test
    public void testRandomBytesStoreAndRetrieve() throws Exception {
        
        Random rnd = new Random();
        
        byte[] randomBytes = new byte[NUMCHARS];
        rnd.nextBytes(randomBytes);
        
        zkClient.create("/etest", randomBytes, Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL);
        
        byte[] readBytes = zkClient.getData("/etest",false,null);
        
        assertThat(readBytes,equalTo(randomBytes));
    }  
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.locator;

import static org.junit.Assert.*;
import it.uniud.atta.common.lang.ResourceLoader;
import it.uniud.atta.common.yaml.YamlLoader;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.locator.Locator;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestLocator {
    
    public static final ResourceLoader DESCRIPTORS_BASE_DIR = new ResourceLoader("yaml/locator");
    
    @Test
    public void testCorrectInline() throws Exception {
        Locator remote = new LocatorBuilder(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_BASE_DIR,"inline.yaml").getFile())).build(); 
        assertThat(remote.getPath(),is("types/abstract/composite/2dcoord"));
        assertThat(remote.getRevision(),is("25fd27b30b960124ccfecf7b476e14c2e5bfe6e6"));
        assertThat(remote.getRepository().getAddress().getName(),is("src/test/resources/repos/hg-artifacts"));
    }
    
    @Test
    public void testEquality() throws Exception {
        Locator artifact1 = new LocatorBuilder(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_BASE_DIR,"inline.yaml").getFile())).build();
        Locator artifact2 = new LocatorBuilder(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_BASE_DIR,"duplicate-of-inline.yaml").getFile())).build();
        assertThat(artifact1,equalTo(artifact2));
    }    
}

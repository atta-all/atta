/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.map;

import static org.junit.Assert.*;
import it.uniud.atta.metamodel.PublishedFactory;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicAbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.MapType;
import it.uniud.atta.metamodel.edgetype.api.EdgeDataConvertibility;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestMapTypeConvertibility {
    
    private static final String DESCRIPTORS_REMOTE_DIR = "types/abstract/map/";
    
    @Test
    public void testIdentity() throws Exception {
        MapType primitive = getMap("primitive");
        MapType primitiveCopy = getMap("primitive");
        assertThat(primitive.convertibilityTo(primitiveCopy),is(EdgeDataConvertibility.EQUAL));
    }
    
    @Test
    public void testDifferentType() throws Exception {
        MapType primitive = getMap("primitive");
        assertThat(primitive.convertibilityTo(AtomicAbstractType.DOUBLE),is(EdgeDataConvertibility.NONE));
    }

    @Test
    public void testCompatible() throws Exception {
        MapType xyzDouble = getMap("xyz-double");
        MapType internalEnumKey = getMap("internal-enumkey");
        assertThat(xyzDouble.convertibilityTo(internalEnumKey),is(EdgeDataConvertibility.EQUAL));
        assertThat(internalEnumKey.convertibilityTo(xyzDouble),is(EdgeDataConvertibility.EQUAL));
    }
    
    private static MapType getMap(String fileName) throws Exception {
        LocatorBuilder locatorBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_REMOTE_DIR + fileName);
        return (MapType) PublishedFactory.makeFrom(locatorBuilder);
    }
}

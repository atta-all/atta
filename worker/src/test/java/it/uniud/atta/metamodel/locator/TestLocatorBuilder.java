/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.locator;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.common.lang.ResourceLoader;
import it.uniud.atta.common.yaml.YamlLoader;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.locator.LocatorIssue;

import org.junit.*;

public class TestLocatorBuilder {
    
    public static final ResourceLoader DESCRIPTORS_BASE_DIR = new ResourceLoader("yaml/locator");

    @Test
    public void testMissingPath() throws Exception {
        checkIssue("missing-path.yaml",LocatorIssue.MISSING_PATH);
    }
    
    @Test
    public void testMissingRepository() throws Exception {
        checkIssue("missing-repo.yaml",LocatorIssue.MISSING_REPOSITORY);
    }
    
    @Test
    public void testRepoHasErrors() throws Exception {
    	LocatorBuilder builder = new LocatorBuilder(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_BASE_DIR,"repo-has-errors.yaml").getFile()));
    	builder.checkCorrectness();
    	assertTrue(builder.getIssues().hasIssuesFor(builder.getRepositoryBuilder()));
    	assertThat(builder.getRepositoryBuilder().getIssues().getSummary(),is(IssueLevel.ERROR));
    }
    
    private void checkIssue(String fileName, LocatorIssue issue) throws Exception {
        LocatorBuilder builder = new LocatorBuilder(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_BASE_DIR,fileName).getFile()));
        builder.checkCorrectness();
        assertTrue(builder.getIssues().contains(issue));
    }
}

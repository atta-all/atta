/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.locator;

import java.util.HashSet;

import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.repository.RepositoryBuilder;
import it.uniud.atta.repository.RepositoryBuilderImpl;
import it.uniud.atta.repository.api.RepositoryKind;
import it.uniud.atta.repository.git.GitRepositoryAddress;

public final class LocatorBuilderUtil {
    
    private static final String address = "src/test/resources/repos/git-artifacts";
    
    public static LocatorBuilder makeFrom(String path) {
        return makeFrom(path,address,"");
    }
    
    public static LocatorBuilder makeFrom(String path, String repoAddress) {
        return makeFrom(path,repoAddress,"");
    }
    
    public static LocatorBuilder makeFrom(String path, String repoAddress, String revision) {
        RepositoryBuilder repoBuilder = new RepositoryBuilderImpl(RepositoryKind.GIT,new GitRepositoryAddress(repoAddress),new HashSet<>());
        return new LocatorBuilder(repoBuilder,path,revision);
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior.property;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.common.lang.ResourceLoader;
import it.uniud.atta.common.yaml.YamlLoader;

import org.junit.*;

public class TestWorkerProperty {
	
	static final ResourceLoader DESCRIPTORS_BASE_DIR = new ResourceLoader("yaml/property");

    @Test
    public void testCorrect() throws Exception {
    	IssueCollection issues = getPropertyBuilderIssues("correct.yaml");
    	assertThat(issues.getSummary(),is(IssueLevel.OK));
    }
    
    private IssueCollection getPropertyBuilderIssues(String fileName) throws Exception {
    	CustomWorkerPropertyBuilder builder = new CustomWorkerPropertyBuilder(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_BASE_DIR,fileName).getFile()));
    	builder.checkCorrectness();
    	return builder.getIssues();
    }
}
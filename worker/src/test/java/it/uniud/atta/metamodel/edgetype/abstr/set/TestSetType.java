/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.set;

import static org.junit.Assert.*;
import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.metamodel.PublishableBuilder;
import it.uniud.atta.metamodel.PublishedBuilderFactory;
import it.uniud.atta.metamodel.PublishedFactory;
import it.uniud.atta.metamodel.edgetype.abstr.api.SetType;
import it.uniud.atta.metamodel.edgetype.abstr.set.SetTypeIssue;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.*;

public class TestSetType {
    
    private static final String DESCRIPTORS_REMOTE_DIR = "types/abstract/set/";
    
    @Test
    public void testDoubleSet() throws Exception {
        
        SetType s = getPublished("double-example");
        Set<Double> setValues = new HashSet<>();
        setValues.add(1.3);
        setValues.add(5.1);
        setValues.add(-0.4);
        assertEquals(setValues,s.getValues());
    }
    
    @Test
    public void testInt32Set() throws Exception {
        
        SetType s = getPublished("int32-example");
        Set<Integer> setValues = new HashSet<>();
        setValues.add(922375803);
        setValues.add(-344775600);
        assertEquals(setValues,s.getValues());
    }
    
    @Test
    public void testLongSet() throws Exception {
        
        SetType s = getPublished("long-example");
        Set<Long> setValues = new HashSet<>();
        setValues.add(9223372036854775803L);
        setValues.add(-9223372036854775600L);
        assertEquals(setValues,s.getValues());
    }
    
    @Test
    public void testIntegerSet() throws Exception {
        
        SetType s = getPublished("integer-example");
        Set<Integer> setValues = new HashSet<>();
        setValues.add(922375803);
        setValues.add(-344775600);
        assertEquals(setValues,s.getValues());
    }
    
    @Test
    public void testCompositeSet() throws Exception {
    	
        SetType s = getPublished("composite-example");
        Set<Map<String,String>> people = new HashSet<>();
        Map<String,String> lucaGeretti = new HashMap<>();
        lucaGeretti.put("name", "Luca");
        lucaGeretti.put("surname", "Geretti");
        Map<String,String> antonioAbramo = new HashMap<>();
        antonioAbramo.put("name", "Antonio");
        antonioAbramo.put("surname", "Abramo");
        people.add(lucaGeretti);
        people.add(antonioAbramo);
        assertEquals(people,s.getValues());
    }
    
    @Test
    public void testInvalidValues() throws Exception {
    	
        IssueCollection issues = getIssues("invalid-values");
        assertTrue(issues.contains(SetTypeIssue.INVALID_VALUES));
    }
    
    @Test
    public void testInt32DuplicateValues() throws Exception {
    	
        IssueCollection issues = getIssues("int32-duplicate-values");
        assertTrue(issues.contains(SetTypeIssue.DUPLICATE_VALUES));
    }
    
    @Test
    public void testCompositeDuplicateValues() throws Exception {
    	
        IssueCollection issues = getIssues("composite-duplicate-values");
        assertTrue(issues.contains(SetTypeIssue.DUPLICATE_VALUES));
    }
    
    private static SetType getPublished(String fileName) throws Exception {
        LocatorBuilder locatorBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_REMOTE_DIR + fileName);
        return (SetType) PublishedFactory.makeFrom(locatorBuilder);
    }
    
    private static IssueCollection getIssues(String fileName) throws Exception {
        PublishableBuilder<?> builder = PublishedBuilderFactory.makeFrom(LocatorBuilderUtil.makeFrom(DESCRIPTORS_REMOTE_DIR + fileName));
        builder.checkCorrectness();
        return builder.getIssues();
    }
}

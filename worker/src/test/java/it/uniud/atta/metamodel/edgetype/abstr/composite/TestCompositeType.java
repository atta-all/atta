/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.composite;

import static org.junit.Assert.*;
import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.metamodel.Publishable;
import it.uniud.atta.metamodel.PublishableBuilder;
import it.uniud.atta.metamodel.PublishedBuilderFactory;
import it.uniud.atta.metamodel.PublishedFactory;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicAbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.CompositeType;
import it.uniud.atta.metamodel.edgetype.abstr.api.Component;
import it.uniud.atta.metamodel.edgetype.abstr.api.MapType;
import it.uniud.atta.metamodel.edgetype.abstr.composite.ComponentIssue;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestCompositeType {
    
    private static final String DESCRIPTORS_REMOTE_DIR = "types/abstract/composite/";
    
    private Publishable publishableEntity = null;
    
    @Test
    public void testMissingComponentName() throws Exception {
        
        IssueCollection issues = getIssues("missing-component-name");
        assetFieldIssue(issues,ComponentIssue.MISSING_NAME);
    }
    
    @Test
    public void testMissingComponentType() throws Exception {
        
        IssueCollection issues = getIssues("missing-component-type");
        assetFieldIssue(issues,ComponentIssue.MISSING_TYPE);
    }
    
    @Test
    public void testInvalidFieldArraySpecification() throws Exception {
        
        IssueCollection issues = getIssues("invalid-array-specification");
        assetFieldIssue(issues,ComponentIssue.INVALID_ARRAY_SPECIFICATION);
    }
    
    @Test
    public void testRemotePrimitive() throws Exception {
        publishableEntity = getPublishedEntity("primitive");
        assertThat(publishableEntity,instanceOf(CompositeType.class));
        CompositeType c = (CompositeType)publishableEntity;
        Component nameField = c.getField("name");
        assertThat(nameField.getType(),instanceOf(AtomicAbstractType.class));
        Component surnameField = c.getField("surname");
        assertThat(surnameField.getType(),instanceOf(AtomicAbstractType.class));
        Component ageField = c.getField("age");
        assertThat(ageField.getType(),instanceOf(AtomicAbstractType.class));
    }
    
    @Test
    public void testRemoteInternal() throws Exception {
        publishableEntity = getPublishedEntity("internal");
        assertComplexCompositeType(publishableEntity);
    }

    @Test
    public void testRemoteInternalExternalDepth1() throws Exception {
        publishableEntity = getPublishedEntity("internal-external-depth1");
        assertComplexCompositeType(publishableEntity);
    }
    
    @Test
    public void testRemoteInternalExternalDepth2() throws Exception {
        publishableEntity = getPublishedEntity("internal-external-depth2");
        assertComplexCompositeType(publishableEntity);
    }
    
    @Test
    public void testRemoteExternal() throws Exception {
        publishableEntity = getPublishedEntity("external");
        assertComplexCompositeType(publishableEntity);
    }
    
    private static void assertComplexCompositeType(Publishable entity) throws Exception {
        assertThat(entity,instanceOf(CompositeType.class));
        CompositeType c = (CompositeType)entity;
        Component nameField = c.getField("name");
        assertThat(nameField.getType(),instanceOf(AtomicAbstractType.class));
        Component propsField = c.getField("props");
        assertThat(propsField.getType(),instanceOf(MapType.class));
        Component locationField = c.getField("location");
        assertThat(locationField.getType(),instanceOf(CompositeType.class));
        Component locationCoordsField = ((CompositeType)locationField.getType()).getField("coords");
        assertThat(locationCoordsField.getType(),instanceOf(MapType.class));
    }
    
    private static Publishable getPublishedEntity(String fileName) throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_REMOTE_DIR + fileName);       
        return PublishedFactory.makeFrom(remoteBuilder);
    }
    
    private static IssueCollection getIssues(String fileName) throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_REMOTE_DIR + fileName);     
        PublishableBuilder<?> builder = PublishedBuilderFactory.makeFrom(remoteBuilder);
        builder.checkCorrectness();
        return builder.getIssues();
    }
    
    private static void assetFieldIssue(IssueCollection typeIssues, ComponentIssue fieldIssue) {
        assertThat(typeIssues.getSummary(),is(IssueLevel.ERROR));
        assertThat(typeIssues.size(),is(1));
        IssueCollection fieldIssues = (IssueCollection)typeIssues.getAll().iterator().next();
        assertThat(fieldIssues.size(),is(1));
        assertTrue(fieldIssues.contains(fieldIssue));
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.enumeration;

import static org.junit.Assert.*;
import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.metamodel.PublishableBuilder;
import it.uniud.atta.metamodel.PublishedBuilderFactory;
import it.uniud.atta.metamodel.edgetype.abstr.api.EnumType;
import it.uniud.atta.metamodel.edgetype.abstr.enumeration.EnumTypeIssue;
import it.uniud.atta.metamodel.entity.Entity;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestEnumType {
    
    private static final String DESCRIPTORS_REMOTE_DIR = "types/abstract/enum/";
    
    @Test
    public void testResolved() throws Exception {
        
        Entity entity = getPublishedEntity("3d");
        assertThat(entity,instanceOf(EnumType.class));
    }
    
    @Test
    public void testInvalidValue() throws Exception {
        
        IssueCollection issues = getIssues("invalid-value");
        assertTrue(issues.contains(EnumTypeIssue.INVALID_VALUE));
    }
    
    @Test
    public void testDuplicateValue() throws Exception {
        
        IssueCollection issues = getIssues("duplicate-value");
        assertTrue(issues.contains(EnumTypeIssue.DUPLICATE_VALUE));
    }
    
    private static PublishableBuilder<?> getPublishedBuilder(String fileName) throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_REMOTE_DIR + fileName);
        PublishableBuilder<?> result = PublishedBuilderFactory.makeFrom(remoteBuilder);
        result.checkCorrectness();
        return result;
    }
    
    private static Entity getPublishedEntity(String fileName) throws Exception {
        return getPublishedBuilder(fileName).build();       
    }
    
    private static IssueCollection getIssues(String fileName) throws Exception {
    	return getPublishedBuilder(fileName).getIssues();
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.io.FileNotFoundException;

import it.uniud.atta.metamodel.PublishableBuilder;
import it.uniud.atta.metamodel.PublishedBuilderFactory;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;
import it.uniud.atta.metamodel.verteximplementation.VertexSecurity;
import it.uniud.atta.metamodel.verteximplementation.api.Behavior;
import it.uniud.atta.metamodel.verteximplementation.behavior.BehaviorLanguage;
import it.uniud.atta.metamodel.verteximplementation.behavior.build.MavenSpecification;
import it.uniud.atta.metamodel.verteximplementation.behavior.property.WorkerProperty;
import it.uniud.atta.metamodel.verteximplementation.behavior.property.builtin.BuiltinProperty;

import org.junit.*;

public class TestBehavior {
	
    public static final String DESCRIPTORS_BASE_DIR = "implementations/behavioral/";
    
    @Test
    public void testMinimal() throws Exception {
        Behavior behavior = getBehavior("minimal");
        assertThat(behavior.getLanguage(),is(BehaviorLanguage.JAVA));
        assertThat(behavior.getSecurity(),is(VertexSecurity.UNFORCED));
    }
    
    @Test
    public void testFull() throws Exception {
        Behavior behavior = getBehavior("full");
        assertTrue(behavior.getBuildSpecification() instanceof MavenSpecification);
        WorkerProperty property = behavior.getBuildSpecification().getRunners().get(0).getConstraints().get(0).getProperty();
        assertThat(property,is(BuiltinProperty.OS_KIND));
    }
    
    private Behavior getBehavior(String fileName) throws FileNotFoundException {
        PublishableBuilder<?> result = PublishedBuilderFactory.makeFrom(LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + fileName));
    	return (Behavior)result.build();
    }
}

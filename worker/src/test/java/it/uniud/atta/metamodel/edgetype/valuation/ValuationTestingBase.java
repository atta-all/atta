/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.valuation;

import it.uniud.atta.common.lang.ResourceLoader;
import it.uniud.atta.common.yaml.YamlLoader;
import it.uniud.atta.metamodel.PublishedBuilderFactory;
import it.uniud.atta.metamodel.edgetype.abstr.api.AbstractType;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;

public abstract class ValuationTestingBase {
   
    protected static final ResourceLoader VALUATIONS_DIR = new ResourceLoader("yaml/valuation");
    protected static final String TYPES_DIR = "types/abstract/";
    
    protected static Object getValuation(String fileName) throws Exception {
    	return YamlLoader.getInstance().load(new ResourceLoader(VALUATIONS_DIR,fileName).getFile());
    }
    
    protected static AbstractType getType(String fileName) throws Exception {
    	return (AbstractType)PublishedBuilderFactory.makeFrom(LocatorBuilderUtil.makeFrom(TYPES_DIR + fileName)).build();  
    }
}
/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.time;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;
import static it.uniud.atta.metamodel.time.SimulationTimeUnit.*;

public class TestSimulationTimeUnit {
    
    @Test
    public void testRatios() throws Exception {
        assertThat(DAY.dividedBy(SEC),is(86400d));
        assertThat(DAY.dividedBy(FEMTOS),is(86400000000000000000D));
        assertEquals(1.0/1440,MIN.dividedBy(DAY),0.000001);
    }
    
    @Test
    public void testApproximateTime() throws Exception {
        assertThat(getNormalizedApproximateTimeFormatFor(new BigInteger("0")),is("0fs"));
        assertThat(getNormalizedApproximateTimeFormatFor(new BigInteger("1")),is("1fs"));
        assertThat(getNormalizedApproximateTimeFormatFor(new BigInteger("2")),is("2fs"));
        assertThat(getNormalizedApproximateTimeFormatFor(new BigInteger("999")),is("999fs"));
        assertThat(getNormalizedApproximateTimeFormatFor(new BigInteger("1000")),is("1ps"));
        assertThat(getNormalizedApproximateTimeFormatFor(new BigInteger("1001")),is("1ps"));
        assertThat(getNormalizedApproximateTimeFormatFor(new BigInteger("2001")),is("2ps"));
        assertThat(getNormalizedApproximateTimeFormatFor(new BigInteger("999999")),is("999ps"));
        assertThat(getNormalizedApproximateTimeFormatFor(new BigInteger("1000000")),is("1ns"));
    }
    
    @Test
    public void testAccurateTime() throws Exception {
        assertThat(getNormalizedAccurateTimeFormatFor(new BigInteger("0")),is("0fs"));
        assertThat(getNormalizedAccurateTimeFormatFor(new BigInteger("1")),is("1fs"));
        assertThat(getNormalizedAccurateTimeFormatFor(new BigInteger("2")),is("2fs"));
        assertThat(getNormalizedAccurateTimeFormatFor(new BigInteger("999")),is("999fs"));
        assertThat(getNormalizedAccurateTimeFormatFor(new BigInteger("1000")),is("1ps"));
        assertThat(getNormalizedAccurateTimeFormatFor(new BigInteger("1001")),is("1001fs"));
        assertThat(getNormalizedAccurateTimeFormatFor(new BigInteger("2001")),is("2001fs"));
        assertThat(getNormalizedAccurateTimeFormatFor(new BigInteger("999999")),is("999999fs"));
        assertThat(getNormalizedAccurateTimeFormatFor(new BigInteger("1000000")),is("1ns"));
        assertThat(getNormalizedAccurateTimeFormatFor(new BigInteger("1001000")),is("1001ps"));
        assertThat(getNormalizedAccurateTimeFormatFor(new BigInteger("1010000")),is("1010ps"));
    }
}
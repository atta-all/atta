/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.io.FileNotFoundException;

import it.uniud.atta.common.issue.*;
import it.uniud.atta.metamodel.PublishableBuilder;
import it.uniud.atta.metamodel.PublishedBuilderFactory;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;

import org.junit.*;

public class TestBehaviorBuilderAtInternalResolution {
	
    public static final String DESCRIPTORS_BASE_DIR = "implementations/behavioral/";
    
    @Test
    public void testMinimal() throws Exception {
        PublishableBuilder<?> publishedBuilder = getBuilder("minimal");
        assertThat(publishedBuilder.getIssues().getSummary(),is(IssueLevel.OK));
    }
    
    @Test
    public void testFull() throws Exception {
        PublishableBuilder<?> publishedBuilder = getBuilder("full");
        assertThat(publishedBuilder.getIssues().getSummary(),is(IssueLevel.OK));
    }
    
    private PublishableBuilder<?> getBuilder(String fileName) throws FileNotFoundException {
        PublishableBuilder<?> result = PublishedBuilderFactory.makeFrom(LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + fileName));
    	result.resolveInternalReferences();
    	return result;
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.behavior.build;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import it.uniud.atta.common.issue.*;
import it.uniud.atta.common.lang.ResourceLoader;
import it.uniud.atta.common.yaml.YamlLoader;
import it.uniud.atta.metamodel.verteximplementation.behavior.build.BuildSpecificationBuilder;
import it.uniud.atta.metamodel.verteximplementation.behavior.build.BuildSpecificationIssue;

import org.junit.*;

public class TestBuildSpecificationBuilder {
	
    public static final ResourceLoader DESCRIPTORS_BASE_DIR = new ResourceLoader("yaml/build");

    @Test
    public void testInvalidKind() throws Exception {
    	IssueCollection issues = getBuilderIssues("invalid-kind.yaml");
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertTrue(issues.contains(BuildSpecificationIssue.UNRECOGNIZED_KIND));
    }
    
    private BuildSpecificationBuilder getBuilder(String fileName) throws Exception {
    	return new BuildSpecificationBuilder(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_BASE_DIR,fileName).getFile()));
    }
    
    private IssueCollection getBuilderIssues(String fileName) throws Exception {
    	BuildSpecificationBuilder builder = getBuilder(fileName);
    	builder.checkCorrectness();
        return builder.getIssues();
    }
}

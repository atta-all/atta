/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.metamodel.PublishableBuilder;
import it.uniud.atta.metamodel.PublishedInternalResolutionIssue;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;

import org.junit.*;

public class TestPublishedBuilderAtInternalResolution {
	
    public static final String DESCRIPTORS_BASE_DIR = "types/abstract/composite/";
    
    @Test
    public void testCorrect() throws Exception {
        IssueCollection issues = getBuilderIssues("external");
        assertThat(issues.getSummary(),is(IssueLevel.OK));
    }
    
    @Test
    public void testInternalTypeNameNotCompliant() throws Exception {
        IssueCollection issues = getBuilderIssues("invalid-internaltype-name");
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertThat(issues.size(),is(1));
        assertTrue(issues.contains(PublishedInternalResolutionIssue.TYPE_NAME_NOT_COMPLIANT));
    }
    
    @Test
    public void testInternalNameIsAtomicTypeName() throws Exception {
        IssueCollection issues = getBuilderIssues("internaltype-name-is-atomic");
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertThat(issues.size(),is(2));
        assertTrue(issues.contains(PublishedInternalResolutionIssue.RESERVED_ATOMIC_TYPE_NAME));
        assertTrue(issues.contains(PublishedInternalResolutionIssue.UNUSED_LABEL));
    }
    
    @Test
    public void testReferenceNameIsAtomicTypeName() throws Exception {
        IssueCollection issues = getBuilderIssues("locator-name-is-atomic");
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertThat(issues.size(),is(2));
        assertTrue(issues.contains(PublishedInternalResolutionIssue.RESERVED_ATOMIC_TYPE_NAME));
        assertTrue(issues.contains(PublishedInternalResolutionIssue.UNUSED_LABEL));
    }
    
    @Test
    public void testIncompatibleLabelForReference() throws Exception {
        IssueCollection issues = getBuilderIssues("incompatible-label-for-ref");
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertThat(issues.size(),is(1));
        assertTrue(issues.contains(PublishedInternalResolutionIssue.MISMATCHING_LABEL_CONTENT));
    }
    
    @Test
    public void testReferenceNameIsBuiltinPropertyName() throws Exception {
        PublishableBuilder<?> builder = PublishedBuilderFactory.makeFrom(LocatorBuilderUtil.makeFrom("implementations/behavioral/locator-name-is-builtin-property"));
        builder.resolveInternalReferences();
        IssueCollection issues =  builder.getIssues();
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertThat(issues.size(),is(2));
        assertTrue(issues.contains(PublishedInternalResolutionIssue.RESERVED_BUILTIN_PROPERTY_NAME));
        assertTrue(issues.contains(PublishedInternalResolutionIssue.UNUSED_LABEL));
    }    
    
    @Test
    public void testUnboundReference() throws Exception {
        IssueCollection issues = getBuilderIssues("unbound-reference");
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertThat(issues.size(),is(1));
        assertTrue(issues.contains(PublishedInternalResolutionIssue.UNBOUND_REFERENCE));
    }
    
    @Test
    public void testUnusedInternal() throws Exception {
        IssueCollection issues = getBuilderIssues("unused-internal");
        assertThat(issues.getSummary(),is(IssueLevel.WARNING));
        assertThat(issues.size(),is(1));
        assertTrue(issues.contains(PublishedInternalResolutionIssue.UNUSED_LABEL));
    }
    
    @Test
    public void testUnusedRemote() throws Exception {
        IssueCollection issues = getBuilderIssues("unused-remote");
        assertThat(issues.getSummary(),is(IssueLevel.WARNING));
        assertThat(issues.size(),is(1));
        assertTrue(issues.contains(PublishedInternalResolutionIssue.UNUSED_LABEL));
    }
    
    @Test
    public void testUnusedInternalAndRemote() throws Exception {
        IssueCollection issues = getBuilderIssues("unused-internal-and-remote");
        assertThat(issues.getSummary(),is(IssueLevel.WARNING));
        assertThat(issues.size(),is(2));
        assertTrue(issues.contains(PublishedInternalResolutionIssue.UNUSED_LABEL));
    }
    
    @Test
    public void testInternalCycle() throws Exception {
        IssueCollection issues = getBuilderIssues("cycle-internal");
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertThat(issues.size(),is(1));
        assertTrue(issues.contains(PublishedInternalResolutionIssue.CYCLIC_DEPENDENCY));
    }
    
    @Test
    public void testMultipleIssues() throws Exception {
        IssueCollection issues = getBuilderIssues("multiple-issues");
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertThat(issues.size(),is(3));
        
        IssueCollection expected = new IssueCollection(issues.getResponsible());
        expected.append(PublishedInternalResolutionIssue.UNBOUND_REFERENCE,"properties")
        		.append(PublishedInternalResolutionIssue.UNUSED_LABEL,"props")
        		.append(PublishedInternalResolutionIssue.UNUSED_LABEL,"3dcoord");
        
        assertThat(issues,equalTo(expected));
    }
    
    private IssueCollection getBuilderIssues(String fileName) throws Exception {
        PublishableBuilder<?> builder = PublishedBuilderFactory.makeFrom(LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + fileName));
        builder.resolveInternalReferences();
        return builder.getIssues();
    }
}

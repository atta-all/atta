/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.enumeration;

import static org.junit.Assert.*;
import it.uniud.atta.metamodel.PublishedFactory;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicAbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.EnumType;
import it.uniud.atta.metamodel.edgetype.api.EdgeDataConvertibility;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestEnumTypeConvertibility {
    
    private static final String DESCRIPTORS_REMOTE_DIR = "types/abstract/enum/";
    
    @Test
    public void testIdentity() throws Exception {
        EnumType enum3d = getEnum("3d");
        EnumType enum3d_other = getEnum("3d");
        assertThat(enum3d.convertibilityTo(enum3d_other),is(EdgeDataConvertibility.EQUAL));
    }
    
    @Test
    public void testInclusion() throws Exception {
        EnumType enum3d = getEnum("3d");
        EnumType enum2d = getEnum("2d");
        assertThat(enum2d.convertibilityTo(enum3d),is(EdgeDataConvertibility.EQUAL));
    }
    
    @Test
    public void testIncludes() throws Exception {
        EnumType enum3d = getEnum("3d");
        EnumType enum2d = getEnum("2d");
        assertThat(enum3d.convertibilityTo(enum2d),is(EdgeDataConvertibility.NONE));
    }
    
    @Test
    public void testDifferent() throws Exception {
        EnumType enum3d = getEnum("3d");
        assertThat(enum3d.convertibilityTo(AtomicAbstractType.TEXT),is(EdgeDataConvertibility.NONE));
    }
    
    private static EnumType getEnum(String fileName) throws Exception {
    	LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_REMOTE_DIR + fileName);
        return (EnumType)PublishedFactory.makeFrom(remoteBuilder);       
    }
}

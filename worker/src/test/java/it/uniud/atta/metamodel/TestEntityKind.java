/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel;

import static org.junit.Assert.*;
import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.metamodel.entity.EntityIssue;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestEntityKind {
	
    public static final String DESCRIPTORS_BASE_DIR = "types/abstract/";
    
    @Test
    public void testUnrecognizedPublishedKind() throws Exception {
    	
    	IssueCollection issues = getIssues("composite/undefined-kind");
    	
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertThat(issues.size(),is(1));
        assertTrue(issues.contains(EntityIssue.UNDEFINED_KIND));  
    }
    
    @Test
    public void testAmbiguousPublishedKind() throws Exception {
        
        IssueCollection issues = getIssues("composite/ambiguous-kind");
        
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertThat(issues.size(),is(1));
        assertTrue(issues.contains(EntityIssue.AMBIGUOUS_KIND));  
    }  
    
    @Test
    public void testUnrecognizedField() throws Exception {
        
        IssueCollection issues = getIssues("composite/unrecognized-field");
        
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertThat(issues.size(),is(1));
        assertTrue(issues.contains(EntityIssue.UNRECOGNIZED_FIELD));        
    }
    
    @Test
    public void testUnrecognizedFieldInLabel() throws Exception {
        
        IssueCollection issues = getIssues("composite/unrecognized-field-in-label");
        
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertThat(issues.size(),is(1));
        assertTrue(issues.contains(EntityIssue.UNRECOGNIZED_FIELD));        
    }
    
    @Test
    public void testMissingField() throws Exception {
        
        IssueCollection issues = getIssues("map/missing-values");
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertTrue(issues.contains(EntityIssue.MISSING_FIELD));
    }
    
    @Test
    public void testMissingFieldInLabel() throws Exception {
        
        IssueCollection issues = getIssues("composite/external-with-incomplete-reference");
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertTrue(issues.contains(EntityIssue.MISSING_FIELD));
    }

    private static IssueCollection getIssues(String fileName) throws Exception {
        return PublishedBuilderFactory.makeFrom(LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + fileName)).getIssues();
    }
}

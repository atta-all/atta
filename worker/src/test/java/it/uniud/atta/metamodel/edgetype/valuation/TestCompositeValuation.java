/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.valuation;

import static org.junit.Assert.*;
import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.metamodel.edgetype.abstr.api.AbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.ValuationIssue;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestCompositeValuation extends ValuationTestingBase {
   
    @Test
    public void testCorrect() throws Exception {
    	Object valuation = getValuation("composite_primitive.yaml");
        AbstractType type = getType("composite/primitive");

        IssueCollection issues = type.validate(valuation);
        
        assertTrue(issues.isEmpty());
    }
    
    @Test
    public void testMissingField() throws Exception {
    	Object valuation = getValuation("composite_primitive_missing_field.yaml");
        AbstractType type = getType("composite/primitive");

        IssueCollection issues = type.validate(valuation);
        
        assertTrue(issues.contains(ValuationIssue.MISSING_FIELD));
    }
    
    @Test
    public void testUnrecognizedField() throws Exception {
    	Object valuation = getValuation("composite_primitive_unrecognized_field.yaml");
        AbstractType type = getType("composite/primitive");

        IssueCollection issues = type.validate(valuation);
        
        assertTrue(issues.contains(ValuationIssue.UNRECOGNIZED_FIELD));
    }
    
    @Test
    public void testValueNotSingleton() throws Exception {
    	Object valuation = getValuation("composite_primitive_not_singleton.yaml");
        AbstractType type = getType("composite/primitive");

        IssueCollection issues = type.validate(valuation);
        
        assertTrue(issues.contains(ValuationIssue.VALUE_NOT_SINGLETON));
    }
	
    @Test
    public void testAtomicValuation() throws Exception {
    	Object valuation = getValuation("atomic_integer.yaml");
        AbstractType type = getType("composite/primitive");

        IssueCollection issues = type.validate(valuation);
        
        assertTrue(issues.contains(ValuationIssue.IS_ATOMIC));
    }
    
    @Test
    public void testListValuation() throws Exception {
    	Object valuation = getValuation("list_integer.yaml");
        AbstractType type = getType("composite/primitive");

        IssueCollection issues = type.validate(valuation);
        
        assertTrue(issues.contains(ValuationIssue.IS_LIST));
    }
    
    @Test
    public void testCorrectArray() throws Exception {
    	Object valuation = getValuation("composite_primitive_array_valid.yaml");
        AbstractType type = getType("composite/primitive-1d-array");

        IssueCollection issues = type.validate(valuation);
        
        assertTrue(issues.isEmpty());
    }
    
    @Test
    public void testInvalidArraySize() throws Exception {
    	Object valuation = getValuation("composite_primitive_array_invalid_size.yaml");
        AbstractType type = getType("composite/primitive-1d-array");

        IssueCollection issues = type.validate(valuation);
        
        assertTrue(issues.contains(ValuationIssue.INVALID_ARRAY_SIZES));
    }
    
    @Test
    public void testInvalidArrayDims() throws Exception {
    	Object valuation = getValuation("composite_primitive_array_invalid_dims.yaml");
        AbstractType type = getType("composite/primitive-1d-array");

        IssueCollection issues = type.validate(valuation);
        
        assertTrue(issues.contains(ValuationIssue.INVALID_ARRAY_SIZES));
    }
    
    @Test
    public void testJaggedArray() throws Exception {
    	Object valuation = getValuation("composite_primitive_array_jagged.yaml");
        AbstractType type = getType("composite/primitive-2d-array");

        IssueCollection issues = type.validate(valuation);
        
        assertTrue(issues.contains(ValuationIssue.ARRAY_IS_JAGGED));
    }
    
    @Test
    public void testArrayInvalidValues() throws Exception {
    	Object valuation = getValuation("composite_primitive_array_invalid_values.yaml");
        AbstractType type = getType("composite/primitive-1d-array");

        IssueCollection issues = type.validate(valuation);
        
        assertTrue(issues.contains(ValuationIssue.INVALID_VALUE));
        assertThat(issues.size(),is(2));
    }
}

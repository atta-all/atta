/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.composite;

import static org.junit.Assert.*;
import it.uniud.atta.metamodel.PublishedFactory;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicAbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.CompositeType;
import it.uniud.atta.metamodel.edgetype.api.EdgeDataConvertibility;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestCompositeTypeConvertibility {
    
    private static final String DESCRIPTORS_REMOTE_DIR = "types/abstract/composite/";
    
    @Test
    public void testIdentity() throws Exception {
        CompositeType primitive = getComposite("primitive");
        CompositeType primitiveCopy = getComposite("primitive");
        assertThat(primitive.convertibilityTo(primitiveCopy),is(EdgeDataConvertibility.EQUAL));
    }
    
    @Test
    public void testSuperset() throws Exception {
        CompositeType primitiveSubset = getComposite("primitive-subset");
        CompositeType primitive = getComposite("primitive");
        assertThat(primitiveSubset.convertibilityTo(primitive),is(EdgeDataConvertibility.NONE));
    }
    
    @Test
    public void testSubset() throws Exception {
        CompositeType primitiveSubset = getComposite("primitive-subset");
        CompositeType primitive = getComposite("primitive");
        assertThat(primitive.convertibilityTo(primitiveSubset),is(EdgeDataConvertibility.SUBSET_AMBIGUOUS));
    }
    
    @Test
    public void testSubsetUnsafe() throws Exception {
        CompositeType primitiveSubset = getComposite("primitive-subset-unsafe");
        CompositeType primitive = getComposite("primitive");
        assertThat(primitive.convertibilityTo(primitiveSubset),is(EdgeDataConvertibility.SUBSET_AMBIGUOUS_UNSAFE));
    }
    
    @Test
    public void testDifferentType() throws Exception {
    	CompositeType primitive = getComposite("primitive");
        assertTrue(primitive.findSubsetsEqualTo(AtomicAbstractType.DOUBLE).isEmpty());
    }
    
    private static CompositeType getComposite(String fileName) throws Exception {
        LocatorBuilder locatorBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_REMOTE_DIR + fileName);
        return (CompositeType) PublishedFactory.makeFrom(locatorBuilder);
    }
}

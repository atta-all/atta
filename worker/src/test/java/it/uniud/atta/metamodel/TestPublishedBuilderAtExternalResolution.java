/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import java.io.FileNotFoundException;

import it.uniud.atta.common.issue.Issue;
import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.metamodel.PublishableBuilder;
import it.uniud.atta.metamodel.PublishedBuilderFactory;
import it.uniud.atta.metamodel.PublishedExternalResolutionIssue;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;
import it.uniud.atta.metamodel.locator.PublishedIssue;
import it.uniud.atta.repository.exception.RepositoryOperationException;

import org.junit.*;

public class TestPublishedBuilderAtExternalResolution {
	
    public static final String DESCRIPTORS_BASE_DIR = "types/abstract/";

    @Test
    public void testUnreachableAddress() throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + "composite/2dcoord", "src/test/resources/repos/git-artfacts");
        checkIssue(remoteBuilder,PublishedIssue.UNREACHABLE_SOURCES);
    } 
    
    @Test
    public void testRepositoryPathNotFound() throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + "composite/2dcoor");
        checkIssue(remoteBuilder,PublishedIssue.PATH_NOT_FOUND);
    }
    
    @Test
    public void testRevisionNotFound() throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + "composite/2dcoord", "src/test/resources/repos/git-artifacts", "25fd27b30b960124ccfecf7b476e14c2e5bfe6e5");
        checkIssue(remoteBuilder,PublishedIssue.REVISION_NOT_FOUND);
    }
    
    @Test
    public void testDescriptorFileNotFound() throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + "composite");
        checkIssue(remoteBuilder,PublishedIssue.DESCRIPTOR_FILE_NOT_FOUND);
    } 
    
    @Test
    public void testAmbiguousDescriptorFile() throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + "composite/ambiguous-descriptor-file");
        checkIssue(remoteBuilder,PublishedIssue.AMBIGUOUS_DESCRIPTOR_FILE);
    } 
    
    @Test
    public void testMalformedDescriptorFileDueToParenthesis() throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + "composite/malformed-descriptor-file-parenthesis");
        checkIssue(remoteBuilder,PublishedIssue.MALFORMED_DESCRIPTOR_FILE);
    } 

    @Test
    public void testMalformedDescriptorFileDueToDuplicateMapKeys() throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + "composite/malformed-descriptor-file-duplicates");
        checkIssue(remoteBuilder,PublishedIssue.MALFORMED_DESCRIPTOR_FILE);
    } 
    
    @Test
    public void testDescriptorFileNotFoundOnDependency() throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + "map/external-with-error");
        checkIssue(remoteBuilder,PublishedIssue.DESCRIPTOR_FILE_NOT_FOUND);
    }
    
    @Test
    public void testCyclicDependency() throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + "map/cross-dependent-A");
        checkIssue(remoteBuilder,PublishedExternalResolutionIssue.CYCLIC_DEPENDENCY);
    }  
    
    @Test
    public void testMismatchingLocatorLabelContent() throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + "composite/mismatching-locator-label-content");
        checkIssue(remoteBuilder,PublishedExternalResolutionIssue.MISMATCHING_LOCATOR_LABEL_CONTENT);
    }
    
    @Test
    public void testExternalHavingMissingRef() throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + "composite/external-having-missing-ref");
        IssueCollection issues = getBuilderIssues(remoteBuilder);
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertTrue(issues.contains(PublishedInternalResolutionIssue.UNBOUND_REFERENCE));
    }
    
    private IssueCollection getBuilderIssues(LocatorBuilder remoteBuilder) throws RepositoryOperationException, FileNotFoundException {
        PublishableBuilder<?> artifactBuilder = PublishedBuilderFactory.makeFrom(remoteBuilder);
        artifactBuilder.resolveExternalReferences();
        return artifactBuilder.getIssues();
    }
    
    private void checkIssue(LocatorBuilder remoteBuilder, Issue issue) throws Exception {
    	IssueCollection issues = getBuilderIssues(remoteBuilder);
        assertThat(issues.getSummary(),is(IssueLevel.ERROR));
        assertThat(issues.size(),is(1));
        assertTrue(issues.contains(issue));
    }
}

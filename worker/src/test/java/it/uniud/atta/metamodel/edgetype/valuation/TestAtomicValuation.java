/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.valuation;

import static org.junit.Assert.*;
import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicAbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.ValuationIssue;

import org.junit.*;

public class TestAtomicValuation extends ValuationTestingBase {
    
    @Test
    public void testCorrect() throws Exception {
        Object valuation = getValuation("atomic_integer.yaml");

        IssueCollection issues = AtomicAbstractType.INTEGER.validate(valuation);
        		
        assertTrue(issues.isEmpty());
    }
    
    @Test
    public void testCompositeValuation() throws Exception {
        Object valuation = getValuation("composite_primitive.yaml");
        
        IssueCollection issues = AtomicAbstractType.INT32.validate(valuation);
        
        assertTrue(issues.contains(ValuationIssue.NOT_ATOMIC));
    }
    
    @Test
    public void testListValuation() throws Exception {
        Object valuation = getValuation("list_integer.yaml");
        
        IssueCollection issues = AtomicAbstractType.FLOAT.validate(valuation);
        
        assertTrue(issues.contains(ValuationIssue.NOT_ATOMIC));
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.set;

import static org.junit.Assert.*;
import it.uniud.atta.metamodel.PublishedFactory;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicAbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.SetType;
import it.uniud.atta.metamodel.edgetype.api.EdgeDataConvertibility;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestSetTypeConvertibility {
    
    private static final String DESCRIPTORS_REMOTE_DIR = "types/abstract/set/";
    
    @Test
    public void testIdentity() throws Exception {
        SetType doubleSet = getSet("double-example");
        SetType doubleSetCopy = getSet("double-example");
        assertThat(doubleSet.convertibilityTo(doubleSetCopy),is(EdgeDataConvertibility.EQUAL));
    }
    
    @Test
    public void testToEnlargedSet() throws Exception {
        SetType doubleSet = getSet("double-example");
        SetType doubleExtendedSet = getSet("double-extended-example");
        assertThat(doubleSet.convertibilityTo(doubleExtendedSet),is(EdgeDataConvertibility.EQUAL));
    }
    
    @Test
    public void testToSubset() throws Exception {
        SetType doubleSet = getSet("double-example");
        SetType doubleExtendedSet = getSet("double-extended-example");
        assertThat(doubleExtendedSet.convertibilityTo(doubleSet),is(EdgeDataConvertibility.NONE));
    }
    
    @Test
    public void testToSafeValueSet() throws Exception {
        SetType doubleSet = getSet("double-example");
        SetType decimalSet = getSet("decimal-example");
        assertThat(doubleSet.convertibilityTo(decimalSet),is(EdgeDataConvertibility.EQUAL));
    }
    
    @Test
    public void testToUnsafeValueSet() throws Exception {
        SetType doubleSet = getSet("double-example");
        SetType int32Set = getSet("int32-example");
        assertThat(doubleSet.convertibilityTo(int32Set),is(EdgeDataConvertibility.NONE));
    }
    
    @Test
    public void testToDifferentSet() throws Exception {
        SetType doubleSet = getSet("double-example");
        SetType compositeSet = getSet("composite-example");
        assertThat(doubleSet.convertibilityTo(compositeSet),is(EdgeDataConvertibility.NONE));
    }
    
    @Test	
    public void testSameType() throws Exception {
        SetType doubleSet = getSet("double-example");
        assertThat(doubleSet.convertibilityTo(AtomicAbstractType.DOUBLE),is(EdgeDataConvertibility.EQUAL));
    }
    
    @Test	
    public void testUnsafeType() throws Exception {
        SetType doubleSet = getSet("double-example");
        assertThat(doubleSet.convertibilityTo(AtomicAbstractType.FLOAT),is(EdgeDataConvertibility.EQUAL_UNSAFE));
    }
    
    private static SetType getSet(String fileName) throws Exception {
        LocatorBuilder locatorBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_REMOTE_DIR + fileName);
        return (SetType) PublishedFactory.makeFrom(locatorBuilder);
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation.port;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.common.lang.ResourceLoader;
import it.uniud.atta.common.yaml.YamlLoader;
import it.uniud.atta.metamodel.verteximplementation.behavior.BehaviorLanguage;
import it.uniud.atta.metamodel.verteximplementation.port.AbstractPortBuilder;
import it.uniud.atta.metamodel.verteximplementation.port.ConcretePortBuilder;
import it.uniud.atta.metamodel.verteximplementation.port.PortIssue;
import it.uniud.atta.metamodel.verteximplementation.port.api.PortSensitivity;

import org.junit.Test;

public class TestPort {
	
	static final ResourceLoader DESCRIPTORS_BASE_DIR = new ResourceLoader("yaml/port");

    @Test
    public void testCorrect() throws Exception {
    	IssueCollection issues = getPortBuilderIssues("correct.yaml");
    	assertThat(issues.getSummary(),is(IssueLevel.OK));
    }
    
    @Test
    public void testDefaultSensitivityForAbstract() throws Exception {
    	AbstractPortBuilder builder = new AbstractPortBuilder(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_BASE_DIR,"correct.yaml").getFile()));
    	assertThat(builder.getSensitivity(),is(PortSensitivity.UNDEFINED));
    }
    
    @Test
    public void testDefaultSensitivityForConcrete() throws Exception {
    	ConcretePortBuilder builder = new ConcretePortBuilder(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_BASE_DIR,"correct.yaml").getFile()),BehaviorLanguage.JAVA);
    	assertThat(builder.getSensitivity(),is(PortSensitivity.SENSITIVE));
    }
    
    @Test
    public void testNameNotCompliant() throws Exception {
    	IssueCollection issues = getPortBuilderIssues("name-not-compliant.yaml");
    	assertThat(issues.getSummary(),is(IssueLevel.ERROR));
    	assertTrue(issues.contains(PortIssue.NAME_NOT_COMPLIANT));
    }
    
    @Test
    public void testUnrecognizedDirection() throws Exception {
    	IssueCollection issues = getPortBuilderIssues("unrecognized-direction.yaml");
    	assertThat(issues.getSummary(),is(IssueLevel.ERROR));
    	assertTrue(issues.contains(PortIssue.UNRECOGNIZED_KIND));
    }
    
    @Test
    public void testImproperSensitiveSpec() throws Exception {
    	IssueCollection issues = getPortBuilderIssues("improper-sensitive-spec.yaml");
    	assertThat(issues.getSummary(),is(IssueLevel.WARNING));
    	assertTrue(issues.contains(PortIssue.IMPROPER_SENSITIVITY));
    }
    
    private IssueCollection getPortBuilderIssues(String fileName) throws Exception {
    	AbstractPortBuilder builder = new AbstractPortBuilder(YamlLoader.getInstance().loadAsMap(new ResourceLoader(DESCRIPTORS_BASE_DIR,fileName).getFile()));
    	builder.checkCorrectness();
    	return builder.getIssues();
    }
}

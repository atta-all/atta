/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import it.uniud.atta.metamodel.PublishedBuilderFactory;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;
import it.uniud.atta.metamodel.verteximplementation.VertexSecurity;
import it.uniud.atta.metamodel.verteximplementation.api.VertexImplementation;

import org.junit.*;

public class TestVertexImplementation {
	
    public static final String DESCRIPTORS_BASE_DIR = "implementations/behavioral/";
    
    @Test
    public void testSecurityForcedOn() throws Exception {
        VertexImplementation implementation = getVertexImplementation("security-forced-on");
        assertThat(implementation.getSecurity(),is(VertexSecurity.FORCED_ON));
    }
    
    @Test
    public void testSecurityForcedYes() throws Exception {
        VertexImplementation implementation = getVertexImplementation("security-forced-yes");
        assertThat(implementation.getSecurity(),is(VertexSecurity.FORCED_ON));
    }
    
    @Test
    public void testSecurityForcedTrue() throws Exception {
        VertexImplementation implementation = getVertexImplementation("security-forced-true");
        assertThat(implementation.getSecurity(),is(VertexSecurity.FORCED_ON));
    }
    
    private VertexImplementation getVertexImplementation(String fileName) throws Exception {
    	return (VertexImplementation)PublishedBuilderFactory.makeFrom(LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + fileName)).build();
    }
}

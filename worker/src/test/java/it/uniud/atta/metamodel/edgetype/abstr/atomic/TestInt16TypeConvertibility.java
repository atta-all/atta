/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.atomic;

import java.util.HashMap;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicAbstractType;
import it.uniud.atta.metamodel.edgetype.api.EdgeDataConvertibility;

import org.junit.*;

public class TestInt16TypeConvertibility extends AtomicTypeConvertibilityTestingBase {
    
    @BeforeClass
    public static void init() {
    	type = AtomicAbstractType.INT16;
    	targets = new HashMap<>();
    	targets.put(AtomicAbstractType.BOOL,EdgeDataConvertibility.NONE);
    	targets.put(AtomicAbstractType.TEXT,EdgeDataConvertibility.NONE);
    	targets.put(AtomicAbstractType.UINT8,EdgeDataConvertibility.EQUAL_UNSAFE);
    	targets.put(AtomicAbstractType.UINT16,EdgeDataConvertibility.EQUAL_UNSAFE);
    	targets.put(AtomicAbstractType.UINT32,EdgeDataConvertibility.EQUAL_UNSAFE);
    	targets.put(AtomicAbstractType.UINT64,EdgeDataConvertibility.EQUAL_UNSAFE);
    	targets.put(AtomicAbstractType.INT8,EdgeDataConvertibility.EQUAL_UNSAFE);
    	targets.put(AtomicAbstractType.INT16,EdgeDataConvertibility.EQUAL);
    	targets.put(AtomicAbstractType.INT32,EdgeDataConvertibility.EQUAL);
    	targets.put(AtomicAbstractType.INT64,EdgeDataConvertibility.EQUAL);
    	targets.put(AtomicAbstractType.INTEGER,EdgeDataConvertibility.EQUAL);
    	targets.put(AtomicAbstractType.FLOAT,EdgeDataConvertibility.EQUAL_UNSAFE);
    	targets.put(AtomicAbstractType.DOUBLE,EdgeDataConvertibility.EQUAL_UNSAFE);
    	targets.put(AtomicAbstractType.DECIMAL,EdgeDataConvertibility.EQUAL);
    }
    
    @Test
    public void testBool() throws Exception {
    	check(AtomicAbstractType.BOOL);
    }    
    
    @Test
    public void testText() throws Exception {
    	check(AtomicAbstractType.TEXT);
    }    
    
    @Test
    public void testUint8() throws Exception {
    	check(AtomicAbstractType.UINT8);
    }    
    
    @Test
    public void testUint16() throws Exception {
    	check(AtomicAbstractType.UINT16);
    }
    
    @Test
    public void testUint32() throws Exception {
    	check(AtomicAbstractType.UINT32);
    }
    
    @Test
    public void testUint64() throws Exception {
    	check(AtomicAbstractType.UINT64);
    }
    
    @Test
    public void testInt8() throws Exception {
    	check(AtomicAbstractType.INT8);
    }    
    
    @Test
    public void testInt16() throws Exception {
    	check(AtomicAbstractType.INT16);
    }
    
    @Test
    public void testInt32() throws Exception {
    	check(AtomicAbstractType.INT32);
    }
    
    @Test
    public void testInt64() throws Exception {
    	check(AtomicAbstractType.INT64);
    }
    
    @Test
    public void testInteger() throws Exception {
    	check(AtomicAbstractType.INTEGER);
    }
    
    @Test
    public void testFloat() throws Exception {
    	check(AtomicAbstractType.FLOAT);
    }
    
    @Test
    public void testDouble() throws Exception {
    	check(AtomicAbstractType.DOUBLE);
    }
    
    @Test
    public void testDecimal() throws Exception {
    	check(AtomicAbstractType.DECIMAL);
    }
    
    @Test
    public void testNonAtomic() throws Exception {
    	innerTestNonAtomic();
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.verteximplementation;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.common.issue.IssueLevel;
import it.uniud.atta.metamodel.PublishableBuilder;
import it.uniud.atta.metamodel.PublishedBuilderFactory;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;
import it.uniud.atta.metamodel.verteximplementation.VertexImplementationIssue;

import org.junit.*;

public class TestVertexImplementationAtChecking {
	
    public static final String DESCRIPTORS_BASE_DIR = "implementations/behavioral/";
    
    @Test
    public void testSecurityUnknown() throws Exception {
    	IssueCollection issues = getBuilderIssues("security-unknown");
    	assertThat(issues.getSummary(),is(IssueLevel.ERROR));
    	assertTrue(issues.contains(VertexImplementationIssue.UNRECOGNIZED_SECURITY_VALUE));
    }
    
    private PublishableBuilder<?> getBuilder(String fileName) throws Exception {
    	return PublishedBuilderFactory.makeFrom(LocatorBuilderUtil.makeFrom(DESCRIPTORS_BASE_DIR + fileName));
    }
    
    private IssueCollection getBuilderIssues(String fileName) throws Exception {
    	PublishableBuilder<?> builder = getBuilder(fileName);
    	builder.checkCorrectness();
        return builder.getIssues();
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.map;

import static org.junit.Assert.*;
import it.uniud.atta.metamodel.Publishable;
import it.uniud.atta.metamodel.PublishedFactory;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicAbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.EnumType;
import it.uniud.atta.metamodel.edgetype.abstr.api.MapType;
import it.uniud.atta.metamodel.entity.Entity;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;

import org.junit.*;

import static org.hamcrest.CoreMatchers.*;

public class TestMapType {
    
    private static final String DESCRIPTORS_REMOTE_DIR = "types/abstract/map/";
	
    private Entity entity;

    @Test
    public void testRemotePrimitive() throws Exception {
        
        entity = getPublishedEntity("primitive");
        assertThat(entity,instanceOf(MapType.class));
        MapType m = (MapType)entity;
        assertThat((AtomicAbstractType)m.getKeyType(),is(AtomicAbstractType.TEXT));
        assertThat((AtomicAbstractType)m.getValueType(),is(AtomicAbstractType.TEXT));
    }
    
    @Test
    public void testRemoteInternalEnumKey() throws Exception {
        
        entity = getPublishedEntity("internal-enumkey");
        assertThat(entity,instanceOf(MapType.class));
    }
    
    @Test
    public void testRemoteInternalDepth2() throws Exception {
        
        entity = getPublishedEntity("internal-depth2");
        assertMapMapEnum(entity);
    }
    
    @Test
    public void testRemoteInternalToExternalDepth2() throws Exception {
        
        entity = getPublishedEntity("internal-to-external");
        assertMapMapEnum(entity);
    }    
    
    private static Publishable getPublishedEntity(String fileName) throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_REMOTE_DIR + fileName);
        return PublishedFactory.makeFrom(remoteBuilder);      
    }
    
    private static void assertMapMapEnum(Entity entity) throws Exception {
        assertThat(entity,instanceOf(MapType.class));
        MapType map = (MapType)entity;
        assertThat(map.getValueType(),instanceOf(MapType.class));
        MapType valuesMap = (MapType)map.getValueType();
        assertThat(valuesMap.getValueType(),instanceOf(EnumType.class));        
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.entity;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.*;

public class TestRecognizedEntityKind {
    
    @Test
    public void testAllHaveEntityFieldSubclass() throws Exception {
        for (RecognizedEntityKind kind : RecognizedEntityKind.values())
            assertFalse(kind.getAllFields().isEmpty());
    }
    
    @Test
    public void testNoDuplicateDefiningFields() throws Exception {
        Set<String> encounteredFieldNames = new HashSet<>();
        for (RecognizedEntityKind kind : RecognizedEntityKind.values()) {
            Set<EntityField> definingFields = kind.getFields(EntityFieldKind.IDENTIFYING);
            int originalSize = encounteredFieldNames.size();
            for (EntityField field : definingFields)
                encounteredFieldNames.add(field.toString());
            
            if (encounteredFieldNames.size() - definingFields.size() < originalSize)
                fail("Duplicate defining fields within " + definingFields);
        }
    }
}

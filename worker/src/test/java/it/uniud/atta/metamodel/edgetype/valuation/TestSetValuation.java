/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.valuation;

import static org.junit.Assert.*;
import it.uniud.atta.common.issue.IssueCollection;
import it.uniud.atta.metamodel.edgetype.abstr.api.AbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.ValuationIssue;

import org.junit.*;

public class TestSetValuation extends ValuationTestingBase {
    
    @Test
    public void testCorrect() throws Exception {
        Object valuation = getValuation("atomic_integer.yaml");
        AbstractType type = getType("set/integer-example");
        
        IssueCollection issues = type.validate(valuation);
		
        assertTrue(issues.isEmpty());
    }
    
    @Test
    public void testValueNotIncluded() throws Exception {
        Object valuation = getValuation("atomic_integer_big.yaml");
        AbstractType type = getType("set/integer-example");
        
        IssueCollection issues = type.validate(valuation);
		
        assertTrue(issues.contains(ValuationIssue.VALUE_NOT_INCLUDED));
    }
}

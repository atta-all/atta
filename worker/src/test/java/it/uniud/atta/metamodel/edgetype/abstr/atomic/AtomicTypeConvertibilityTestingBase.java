/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.metamodel.edgetype.abstr.atomic;

import static org.junit.Assert.*;

import java.util.Map;

import it.uniud.atta.metamodel.Publishable;
import it.uniud.atta.metamodel.PublishedFactory;
import it.uniud.atta.metamodel.edgetype.abstr.api.AbstractType;
import it.uniud.atta.metamodel.edgetype.abstr.api.AtomicAbstractType;
import it.uniud.atta.metamodel.edgetype.api.EdgeDataConvertibility;
import it.uniud.atta.metamodel.locator.LocatorBuilder;
import it.uniud.atta.metamodel.locator.LocatorBuilderUtil;
import static org.hamcrest.CoreMatchers.*;

public abstract class AtomicTypeConvertibilityTestingBase {
    
    protected static final String DESCRIPTORS_REMOTE_DIR = "types/abstract/composite/";
    
    protected static AtomicAbstractType type;
    protected static Map<AtomicAbstractType,EdgeDataConvertibility> targets;

    protected static Publishable getPublishedEntity(String fileName) throws Exception {
        LocatorBuilder remoteBuilder = LocatorBuilderUtil.makeFrom(DESCRIPTORS_REMOTE_DIR + fileName);        
        return PublishedFactory.makeFrom(remoteBuilder);
    }
    
    protected static void check(AtomicAbstractType other) {
    	assertThat(type.convertibilityTo(other),is(targets.get(other)));
    }
    
    protected static void innerTestNonAtomic() throws Exception {
    	AbstractType other = (AbstractType)getPublishedEntity("primitive");
    	assertThat(type.convertibilityTo(other),is(EdgeDataConvertibility.NONE));
    }
}

/**
 * Copyright (C) 2014 Luca Geretti (luca.geretti@uniud.it)
 *
 * This file is part of Atta.
 *
 *     Atta is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Atta is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Atta.  If not, see <http://www.gnu.org/licenses/>.
 */
package it.uniud.atta.serialization;

import static org.junit.Assert.*;

import java.io.*;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;

public class TestObjectSerialization {
    
    @Test
    public void testObjectSerializeDeserialize() throws Exception {
        
        SampleObject original = new SampleObject();
        
        original.text = "Test";
        int[] vals = new int[2];
        vals[0] = 2;
        vals[1] = 15;
        original.val = vals;
        
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(original);
        
        
        ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(bos.toByteArray()));
        SampleObject retrieved = (SampleObject)ois.readObject();
        
        assertThat(retrieved.text,equalTo(original.text));
        assertThat(retrieved.val,equalTo(original.val));
    }    
}

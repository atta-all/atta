\section{Time and guards}
\label{sec:time-guards}

The execution of a distributed data flow application necessarily needs some ordering for successive data values flowing through a specific edge. This is particularly true when dealing with cyclic graphs, since different paths may generate data at different rates. Consequently, we need to enrich data flowing through an edge with a {\em flow time} $\phi$ that is guaranteed to be a strictly monotonic quantity: successive data produced for the edge must have a higher value of the flow time. The flow time follows these simple rules for an arbitrary open cyclic graph, where vertices represent behavioral implementations and edges represent data transfers between them:
\begin{enumerate}
\item The flow time provided for an input edge of the graph is strictly monotonic;
\item The flow time for all output edges of a vertex is updated after the execution of the corresponding behavior;
\item The flow time for all output edges of a vertex is one plus the maximum over the flow times of its inputs edges.
\end{enumerate}
The first rule forces the integrity of strict monotonicity over the boundaries of the graph in order to allow the execution of open graphs. The second rule is essentially a consequence of the application of the data flow paradigm. 
The third rule guarantees the monotonicity within the graph when cycles are present: even if one output edge is fed as input to the same vertex, the flow time of its data will be greater that the flow time of the previous data. The third rule also suggests that the second rule is a simplification that allows to ignore the destination of edges when choosing the flow time of outputs.

The flow time allows us to order data for a specific edge. The notion of a flow time may be sufficient for a dataflow application modeled as a simple Petri Net, where the consistency of the flow is enforced at graph level. This enforcement may be too limiting, however: we may want to enrich data with time, to be able to gain information on the "arrival time" of data among inputs of a vertex. 

We therefore introduce a system-wide reference time to which data can be marked with. Beware that this {\em system time} has no relation whatsoever with the real time of the worker nodes or the client nodes. System time is provided as an arbitrary-precision time type, expressed in seconds. Time progresses through a behavioral implementation as a result of an execution: the execution of a behavior $B$ is performed at a certain execution system time $T_x(B)$ and each output port $p$ has an output system time $T_o(B,p)$ equal to $T_x(B)$ plus a  {\em propagation delay}. The propagation delay is specified as a fixed value for a behavior. Since all outputs are produced as the output system time, an edge from a behavioral implementation has a common system time. On the contrary, edges in a structural implementation may have different times due to the fact that we allow the joining of edges.

In order to decide when execution must be performed, we discriminate between {\em sensitive} and {\em insensitive} input ports of a behavior. A sensitive input port is such that each new value for that port triggers the execution of the behavior, irrespective of the other ports; after a value for a sensitive input port is used, it is discarded. An insensitive input port instead is allowed to offer its latest value indefinitely. Consequently, when execution is triggered by a sensitive port at a given system time $T_x$, all the insensitive ports use the most recent value up to $T_x$ included. If for example a sequence of data values are received all at a given $T_i$ on a sensitive port, there is a sequence of executions with increasing flow time but constant system time $T_x = T_i$; if the port is insensitive and an execution occurs at $T_x \geq T_i$, the latest value of the sequence will be used; also, if a new value is received between $T_x$ and $T_i$, that value is used thus discarding all the values of the sequence.

We call {\em sensitivitivy set} the subset of input ports ${\mathbb P}_S(B) \subseteq {\mathbb P}_I(B)$.

The canonical use case of an insensitive input port is a behavior having ports whose inputs arrive at different frequencies in terms of system time: if we required each input port to be a sensitive one, some data sources would be compelled to produce data at a higher rate than necessary. On the contrary, we may be interested to have a behavior that "samples" an input instead of reacting to each and every value change. Insensitive ports therefore help to decouple behaviors.

\subsection{Guards}

Guards are introduced to control when execution of a behavioral implementation is possible as a function of edge or input port data {\em values}; as such, it is an additional and independent way to restrict execution of a behavior, compared to system time paired with sensitivity.

Guards in Atta can be of three different classes: edge, vertex and switch.

An edge guard means that the data from the given edge is ignored as long as it does not satisfy such guard. A vertex guard means that all the data from the input ports is ignored as long as the guard is not satisfied. By ignored we mean that the observable data held by an edge/port is the latest one that satisfies the guard. This is different from saying that no data is actually available. Ignoring new data for a sensitive port may prevent execution of a behavior: for this reason, guards can ultimately be considered an additional way to restrict the execution flow. In particular, using guards we can model conditional behavior, both for conditional execution but also when switching between alternative implementations. Let us consider, for example, digital synchronous circuits where the rising/falling edge of one signal triggers the update of the output: we provide the signal as the sensitive input, and introduce a guard for which the value must be high.

Please note that vertex guards are more expressive than edge guards, since we can use all the fields from all the edges.

For switches, we provide the following example: different behaviors, each one computing the same outputs but with different accuracies. We can pass a measure of desired accuracy as an input, with disjoint guards over such input, to allow one and only one implementation to be used at a time.

Switch and vertex guards can refer to any leaf field of any port of a vertex, while edge guards are limited to the fields of the edge type. A {\em leaf field guard} (or simply field guard) $g_f$ usually refers to a leaf field as being included into an interval. Such intervals must be singletons (specific values) for text, otherwise they can be either singleton or with non-empty interior, bounded or unbounded, open or closed on the lower/upper bound. Also, for simplicity, inequality can be used. In addition, we can also refer to the length of a matrix, with the syntax \verb+L(matrix,dim)+, where \verb+matrix+ is the field referring to the matrix and \verb+dim+ is the interested dimension; if one dimension is present, the \verb+L(matrix)+ short notation is allowed. Some examples:

\begin{itemize}
\item \verb+x < 0+ : the scalar numeric field \verb+x+ must be lesser or equal to a real value;
\item \verb+1.5 <= y[1] <= 3+ : the scalar numeric field corresponding to the second element of the monodimensional array \verb+y+ must be between some real bounds;
\item \verb+a.u = 'Hello'+ : the text field \verb+u+ of \verb+a+ must be equal to a string;
\item \verb+L(b.q,1) != 3+ : the length of the second dimension of the matrix field \verb+q+ of \verb+b+ must be different from a positive integer value;
\item \verb+L(z.j) >= 1+ : the length of the dimension of the array field \verb+j+ of \verb+z+ must be greater or equal to a positive integer value;
\end{itemize}

Please note that by leaf field we mean that for composite ports used in binary clauses, $I$ must necessarily refer to an atomic type through subaddressing (e.g., \verb+x.y.z < 3.0+ with \verb+z+ being a floating point field).

A guard is expressed in the form $g = \bigvee_i g_i$, where each $g_i = \bigwedge_f g_f$ is a conjunctive clause over leaf fields, and the set is composed through disjunction; an alternative set representation is ${\mathbb G} = \{g_i\}$. The satisfaction of a guard $g$ at a certain time $t$ is expressed by making the dependency on time explicit, i.e., $g(t)$. In order to point out the dependency of a guard on an edge or a vertex or even a behavior, we can add one or more arguments to $g$.

For the particular case of switches, each vertex defines the guards that regulate the transitions from one implementation instance to another, thus identifying a switched system; if a transition does not exist, it is equivalent to a guard being empty. The guards of implementations are checked for intersection, since it is not allowed to perform multiple transitions at the same time. They are also checked to identify whether cycles exist that are satisfied for a fixed point trajectory: that would imply infinite transitions in a finite time. Since switched systems are ultimately converted to an internal representation that makes use of vertex guards only, in the end only edge and vertex guards are used.

Guards at different levels of abstraction are "flattened" onto behaviors, by taking the conjunction of all the guards (edge or vertex) that are crossed from all the sources of the behavior input ports. This implies that a behavior may have to satisfy guards related to data it does not read otherwise. 

At design time, it is properly checked that no empty guard sets are produced for any implementation instance. Consequently, we have that the execution of a behavior is triggered by the cartesian product ${\mathbb G} \times {\mathbb P}_S$ of the guard conjunctions with the sensitivity conjunction. This also means that in the presence of vertex/switch guards, the guards that influence execution of a behavioral implementation may include edge fields that do not reach the sensitive inputs of the behavior.

%TODO: provide/find an algorithm to check for fixpoints and to rewrite the guards

\subsection{Data availability}

Due to the presence of both guards and sensitivity predicates, data can be prevented from being used. We denote with $\alpha$ the availability function of a vertex/implementation port. The availability represents under which conditions data on the "right side" of the port is available: for this reason, $\alpha(I.ip)$ of a behavioral implementation is undefined, since there is no right side of such a port. Summarizing, we can have the following four cases:

\begin{itemize}
\item $\alpha(I.op)$: the availability of data on the right side of an output port $op$ of an implementation $I$;
\item $\alpha(I.ip)$: the availability of data on the right side of an input port $ip$ of a structural implementation $I$;
\item $\alpha(V.op)$: the availability of data on the right side of an output port $op$ of a vertex $V$;
\item $\alpha(V.ip)$: the availability of data on the right side of an input port $ip$ of a vertex $V$.
\end{itemize} 

Such boolean functions depend on time, but right now we ignore this fact and only analyze the system graph in a static way. Following the wiring and binding rules, we obtain these expressions:

\begin{align}
\label{eq:Iop}
\alpha(I.op) & = \left\{
\begin{array}{ll}
g(V(I)) \land \left( \bigvee_s \alpha(V(I).ip_s) \right) & , \,\textrm{if $I$ behavioral} \\
\left\{ \bigvee_{int} \left[ g(V_{int}.op_{int},I.op) \land \alpha(V_{int}.op_{int}) \right]\right\} \vee g(I.ip_{ext},I.op) \land \alpha(I.ip_{ext}) & , \,\textrm{if $I$ structural} 
\end{array}
\right. \\
\label{eq:Iip}
\alpha(I.ip) & = \left\{
\begin{array}{ll}
\textrm{undefined} & , \,\textrm{if $I$ behavioral} \\
g(V(I)) \land \alpha(V(I).ip) & , \,\textrm{if $I$ structural}
\end{array}
\right. \\
\label{eq:Vop}
\alpha(V.op) & = \alpha(I(V).op) \\
\label{eq:Vip}
\alpha(V.ip) & = \left[g(I_V.ip_{ext},V.ip) \land \alpha(I_V.ip_{ext})\right] \vee \left[g(V_{int}.op_{int},V.ip) \land \alpha(V_{int}.op_{int})\right] 
\end{align}
where $I(V)$ is the implementation instance for a given $V$, $V(I)$ is the vertex for the implementation instance $I$, and $I_V$ is the structural implementation instance that contains $V$. 

Data from a behavioral implementation output is available as long as both the guard and the sensitivity predicate are satisfied in respect to the availability of the vertex input ports; please note how the outcome does not actually depend on the output port of the implementation; in other terms, it is available if the execution of the behavior is allowed. This expression is chosen by construction rather than derived; all the other expressions instead are a result of the binding/wiring rules.
For a structural implementation, its availability depends on the availability of either a set of vertices internal to the structure (e.g., the if-else template), or an external source. The availability from an implementation input port applies only to structural implementations: it implies that the vertex guard is satisfied, and the source for the vertex is available. A vertex output port follows the same rules of its implementation at the bound port, since no guard or sensitivity predicate is introduced in an output binding. Finally, a vertex input port availability comes either from an external source (thus passing through the enclosing implementation) or from an internal source (i.e., another vertex): in the case of the loop template, we have both sources and the internal one is the vertex itself; otherwise only one exists. 

In practice, these expressions allow us to evaluate the possibility of executing a behavioral implementation $B$, by evaluating $\alpha(B.op)$, which is actually independent of the output port.

We now introduce an example subsystem as shown in Fig.~\ref{fig:complex-dependencies}, where multiple vertices are present, whose implementations for simplicity perfectly match the interface. Behavioral implementations are shown in blue, while structural ones in yellow/orange. For simplicity we call $x$ or $y$ a generic input port, and $u$ or $v$ a generic output port. We recognize that $I_4$ is a loop template, while $I_6$ is an if-else template; the actual function of a behavioral implementation is of no consequence here. We provide the sensitivity predicate $s(I_1) = x \vee y$ (which is the default one) for the implementation of $V1$. Guards are assumed to be present on every vertex and edge, but not specified for simplicity. 

\begin{figure}
\centering
\includegraphics[width=0.6\textwidth]{img/complex-dependencies.png}
\caption{Example of complex dependencies.}
\label{fig:complex-dependencies}
\end{figure}

If we want to compute the executability of $I_1$, we compute when any of its outputs are available, i.e., $\alpha(I_1.u)$ or $\alpha(I_1.v)$ equivalently:

\begin{eqnarray*}
\alpha(I_1.u) & = & g(V_1) \land \left(\alpha(V_1.x) \vee \alpha(V_1.y)\right) \\
 & = & g(V_1) \land \left[\alpha(I_4.x) \land \left(g(I_4.x,V_1.x)\right) \vee \left(\alpha(I_1.u) \land g(V_1.u,V_1.x)\right) \vee \left(\alpha(I_4.y) \land g(I_4.y,V_1.y)\right) \right] 
\end{eqnarray*}

We pause at this point to underline that $\alpha(I_1.u)$ depends on the production of inputs from a behavior, consequently we do not need to further expand a path whose source is an output port of a behavior. We remind here that, since edge sharing is allowed (even if only in specific cases), multiple paths can reach a given destination input port $D.ip$.

Therefore it is convenient to explicitly refer to paths: a path $\pi$ is a sequence of vertices and implementations (not necessarily alternated), along with their ports. The length of a path is referred to as $|\pi|$; a path can be addressed with an index, where $\pi[1]$ is the start and $\pi[|\pi|]$ is the end. The guard for the $i$-th path position, $g(\pi,i)$ is given by:

\begin{itemize}
\item $g(V(\pi[i]))$, if the $i$-th element is an input port of a structural implementation, i.e., this is the guard of the enclosing vertex;
\item $g(\pi[i-1],\pi[i])$, if the $i$-th element is an input port of a vertex or an output port of a structural implementation; i.e., this is the guard from the previous element (either an input port of a structural implementation or an output port of a vertex) to the current;
\item {\em true} otherwise: input or output ports of behaviors, or output ports of implementations, do not have guards associated.
\end{itemize}

In order to compact the expressions used before, we define as {\em path guard} $g(\pi)$ the conjunction of all the guards on a specific path $\pi$, i.e. $g(\pi) = \bigwedge_i g(\pi,i)$.

We define as $\Pi(S.op,D.ip)$ the set of paths from the output port of a source to the input port of a destination. In particular, an availability path $\pi_a(D.ip)$ is a path that ends at $D.ip$ and starts from an output port of a behavioral implementation. We then define $\Pi_a(D.ip) = \{\pi_a(D.ip)\}$ as the set of all availability paths.

Let us continue by simplifying the previous result and by putting the ``completed'' terms aside at the end:

\begin{eqnarray*}
\alpha(I_1.u) & = &  \left[ \alpha(I_4.x) \land g(\pi_1) \right] \vee \left[ \alpha(I_4.y) \land g(\pi_2) \right] \vee \left[ \alpha(I_1.u) \land g(\pi_3) \right] \\
& = & \left[ \alpha(V_4.x) \land g(V_4)  \land g(\pi_1) \right] \vee \left[ \alpha(V_4.y) \land g(V_4) \land g(\pi_2) \right] \vee \left[ \alpha(I_1.u) \land g(\pi_3) \right] \\
\pi_1 & = & \{I_4.x, V_1.x, I_1.x\} \\
\pi_2 & = & \{I_4.y, V_1.y, I_1.y\} \\
\pi_3 & = & \{I_1.u, V_1.u, V_1.x, I_1.x\}
\end{eqnarray*}

where the last disjunctive term cannot be backtracked further, since a behavioral implementation output port has been reached. Continuing, we have

\begin{eqnarray*}
\alpha(I_1.u) & = & \left[ \alpha(V_4.x) \land g(\pi_4) \right] \vee \left[ \alpha(V_4.y) \land g(\pi_5) \right] \vee \left[ \alpha(I_1.u) \land g(\pi_3) \right] \\
& = & \left[ \alpha(I_5.x) \land g(I_5.x,V_4.x) \land g(\pi_4) \right] \vee \left[ \alpha(I_1.u) \land g(\pi_3) \right] \\
& & \left[ \alpha(V_2.u) \land g(V_2.u,V_4.y) \land g(\pi_5) \right] \vee \left[ \alpha(I_1.u) \land g(\pi_3) \right] \\
& = & \left[ \alpha(I_5.x) \land g(\pi_6) \right] \vee \left[ \alpha(I_2.u) \land g(\pi_7) \right] \vee \left[ \alpha(I_1.u) \land g(\pi_3) \right] \\
\pi_4 & = & \{V_4.x, I_4.x, V_1.x, I_1.x\} \\
\pi_5 & = & \{V_4.y, I_4.y, V_1.y, I_1.y\} \\
\pi_6 & = & \{I_5.x, V_4.x, I_4.x, V_1.x, I_1.x\} \\
\pi_7 & = & \{V_2.u, V_4.y, I_4.y, V_1.y, I_1.y\}
\end{eqnarray*}

We start seeing the pattern here: for each source behavioral implementation, we must compute the guard from the source port of the availability clause down to the input port of the behavior. Finally:

\begin{eqnarray*}
\alpha(I_1.u) & = & \left[ \alpha(V_5.x) \land g(V_5) \land g(\pi_6) \right] \vee \left[ \alpha(I_2.u) \land g(\pi_7) \right] \vee \left[ \alpha(I_1.u) \land g(\pi_3) \right] \\
& = & \left[ \alpha(V_3.u) \land g(V_3.u,V_5.x) \land g(\pi_8) \right] \vee \left[ \alpha(I_2.u) \land g(\pi_7) \right] \vee \left[ \alpha(I_1.u) \land g(\pi_3) \right] \\
& = & \left[ \alpha(I_3.u) \land g(\pi_9) \right] \vee \left[ \alpha(I_2.u) \land g(\pi_7) \right] \vee \left[ \alpha(I_1.u) \land g(\pi_3) \right] \\
\pi_8 & = & \{V_5.x, I_5.x, V_4.x, I_4.x, V_1.x, I_1.x\} \\
\pi_9 & = & \{I_3.u, V_3.u, V_5.x, I_5.x, V_4.x, I_4.x, V_1.x, I_1.x\}
\end{eqnarray*}

Here we must remark that $V_6$ is irrelevant, since it does not reach $I_1$.

Consequently, we can generalize the availability for a behavioral implementation $B$ (equivalently, for $B.op$ for any $op$). We define as a data source $C_{\pi}$ for a path $\pi$ either a behavioral implementation output port (the specific port is indifferent, since all outputs are produced together) or a graph input port (which would be a structural implementation input port); only one data source exists for a given path. We have:

\begin{equation}
\label{eq:alpha_B}
\alpha(B) = \bigvee_{ip \in {\mathbb P}_S(B)} \bigvee_{\pi \in \Pi_a(B.ip)} \left[\alpha(\pi[1]) \land g(\pi) \right] = \bigvee_{ip \in {\mathbb P}_S(B)} \bigvee_{\pi \in \Pi_a(B.ip)} \left[\alpha(C_{\pi}) \land g(\pi) \right] 
\end{equation}
where the first disjunction accounts for multiple input ports of $B$, and the second disjunction accounts for multiple paths to that port due to edge sharing. In other terms, we consider all the paths from each sensitive input port, computing the path guard until we reach the actual data source. The availabilities of the data sources correspond to the actual presence of new data.

\subsection{How to evaluate times}

In order to evaluate how the guards evolve over time, in general we need to update the {\em satisfaction} of an edge guard $g(S.op,D.ip)$ or a vertex guard $g(V)$, hereby denoted with the $\sigma_g$ symbol:
$$\sigma_g(S.op,D.ip,t)$$
and
$$\sigma_g(V,t)$$
where $S$ is a source implementation/vertex, and $D$ is a destination implementation/vertex.

Let us first introduce some additional definitions:
\begin{itemize}
\item ${\mathbb C}_g(S.op,D.ip)$: the set of data sources for the satisfaction of the specific edge guard $g(S.op,D.ip)$; multiple sources may exist due to edge sharing (e.g., if-else template);
\item ${\mathbb C}_g(V)$: the set of data sources for the satisfaction of the specific vertex guard $g(V)$; multiple sources may exist since vertex guards involve multiple ports;
\item ${\mathbb C}_{gs}(B_s.op,B.ip,\pi)$: the set of data sources for the satisfaction of the path guard $g(\pi)$ (i.e., the conjunction of all the guards for a path $\pi$ from $B_s.op$ to $B.ip$ itself);
\item ${\mathbb C}_{gs}(B.ip)$: the set of data sources for the satisfaction of all the path guards to $B.ip$;
\item ${\mathbb C}_{gs}(B)$: the set of data sources for the satisfaction of the guards on all the paths of all the input ports of $B$;
\item ${\mathbb C}_a(B.ip)$: the set of data sources whose availability is required by $\alpha(B)$ at the input port $ip$, for all paths;
\item ${\mathbb C}_a(B)$: the set of data sources whose availability is required by $\alpha(B)$ for all ports and all paths, i.e., $\bigcup_{ip \in {\mathbb P}_S(B)} {\mathbb C}_a(B.ip)$;
\item ${\mathbb C}_{a,gs}(B)$: this is ${\mathbb C}_a(B) \cup {\mathbb C}_{gs}(B)$, hence the set of all the data sources that influence $\alpha(B)$ either in terms of availability or guard satisfaction.
\end{itemize}

Please note the difference between ${\mathbb C}_{gs}$ and ${\mathbb C}_a$: the former accounts for those data sources that influence the guard, while the latter accounts for those data sources that appear in the availability expression.

The update of a specific guard is performed as values from the ports in ${\mathbb C}_g(S.op,D.ip)$, or ${\mathbb C}_g(V)$, become available; in practice, the satisfaction of the guard is expressed as a sequence of timed values. The function has the {\em triboolean} codomain, meaning that it can also take the value of {\em indeterminate}. The time range of definition (which we call the {\em support}) 
$$<\sigma_g(S.op,D.ip,t)>$$ 
or
$$<\sigma_g(V,t)>$$ 
starts from $T_{px}(B)$, since there is no need to keep previous values; its ending time actually depends on the latest update to ${\mathbb C}_g(S.op,D.ip)$ or ${\mathbb C}_g(V)$; the initial lower bound of the support is strict, since it may be the case that no system time passes between two executions of a behavior.

For path guards $g(\pi)$ the definition is similar:

$$\sigma_g(\pi,t)$$

where $\pi[1] = S.op$ and $\pi[|pi|] = D.ip$.

Due to the fact that a path guard in general is a disjunction of conjunctions, and since $T_{px}(B)$ may be undefined, we will need to disjunct and conjunct guards with different upper and lower bounds on their support. The support of a conjunction is the intersection of the supports, while the support of a disjunction is the {\em convex hull} of the supports; in the second case, we extend the narrower function with the indeterminate value.

Having a path guard satisfied is not sufficient: we also need new data from the data sources themselves. To know when data is new, we use the flow time $\phi$. We can actually associated a flow time with any new data produced by a source (either an external one or the execution of a behavior), instead of associating it with an edge. The flow time of an edge will be the flow time of the latest data produced for that edge.

In particular, we introduce the flow time of the previous execution of a given source $C$ as $\phi_{px}(C)$; we have that data associated with a subsequent data source execution always respects the $\phi(C) > \phi_{px}(C)$ relation.

We remind here that ${\mathbb C}_a(B)$ and ${\mathbb C}_{gs}(B)$ may be different or even disjoint. We consequently introduce a {\em guarded availability} function
$$\alpha_{gs}(\pi,t)$$
which is the projection of $\sigma_{gs}(\pi,t)$ on the support obtained from those outputs of $\pi[1]$ having $\phi_x(\pi[1]) > \phi_{px}(\pi[1])$ and $T_o(\pi[1]) \geq T_{px}(B)$; again, this is a sequence of timed triboolean values. In practice, when new data from $\pi[1]$ is available,
\begin{eqnarray}
\alpha_{gs}(\pi,T_o(\pi[1])) & = & \sigma_{gs}(\pi,T_o(\pi[1])) \\ 
& \Leftrightarrow & \sigma_{gs}(\pi,T_o(\pi[1])) \textrm{ is defined} \\
& \Leftrightarrow & T_o(\pi[1]) \in <\sigma_{gs}(\pi,t)>.
\end{eqnarray}
Again, this is because in general the availability set and the guard set may be disjoint, hence their supports may be disjoint too. As soon as at least one data value for each source in ${\mathbb C}_{gs}(B)$ is produced, however, the support is defined back to $T_{px}(B)$ and therefore $T_o(\pi[1])$ always falls within $<\sigma_{gs}(\pi,t)>$.

We have that for a given path $\pi$ from a data source to an input port $ip$ of $B$,
\begin{equation}
T_x(B,ip,\pi) = \argmin_t\, \alpha_{gs}(\pi,t)
\end{equation}
is the minimum time that satisfies the guarded availability function; consequently, it could be considered the execution time as far as that path for that input port is concerned. If we consider all the paths for the port, we have:
\begin{equation}
T_x(B,ip) = \min_{\pi \in \Pi_a(B.ip)}\, T_x(B,ip,\pi)
\end{equation}
with the execution time being expressed as:
\begin{equation}
T_x(B) = \min_{ip \in {\mathbb P}_S(B)} T_x(B,ip).
\end{equation}
$T_x(B)$ is defined if at least one $T_{x}(B,ip,\pi)$ is defined. Please note how the priority of the different paths (which is relevant only when loop templates are present) does not influence the time: it is relevant only when choosing the actual input value to load for execution.

Since it is normally the case that no $T_x(B,ip,\pi)$ is defined immediately after $T_{px}(B)$, it is useful to introduce the concept of {\em next execution time}
\begin{equation}
\nonumber T_{nx}(B)
\end{equation}
which is a variable quantity that represents an interval where the time for the next execution is guaranteed to lie into; as with the guarded satisfaction function, we will progressively refine $T_{nx}(B)$ as inputs arrive. In fact, we are only interested in the lower bound $\underline{\,T}_{nx}(B)$, while $\overline{\,T}_{nx}(B) = +\infty$ holds. The lower bound may be strict or regular: in the first case, we consider the value incremented by an $\epsilon$ infinitesimal quantity which, while non-additive, determines whether the bound is strict; consequently, $T_{nx}(B) > t$ and $\underline{\,T}_{nx}(B) = t+\epsilon$ provide the same information. We also guarantee that $\underline{\,T}_{nx}(B)$ is a monotonically increasing quantity, where $T_{nx}(B) \geq T_{px}(B)$ initially holds.

First, we identify 
\begin{equation}
t^*_{nx}(\pi) = \argmin_{t \in <\alpha_{gs}(\pi,t)>} \left[\alpha_{gs}(\pi,t) = indeterminate\right]
\end{equation}
as the highest value of $t$ for which it is certain that no execution time is defined. It follows that

\begin{eqnarray}
\nonumber \overline{\,T}_{nx}(B,\pi) & = & +\infty \\
\nonumber \underline{\,T}_{nx}(B,\pi) & = & \left\{
\begin{array}{ll}
t^*_{nx}(\pi)  & \textrm{, if $\alpha_{gs}(\pi,t^*_{nx}(\pi)) = indeterminate$} \\
t^*_{nx}(\pi) + \epsilon & \textrm{, otherwise}
\end{array}
\right.
\end{eqnarray}
which means that if no indeterminate value has been found in $\alpha_{gs}(\pi,t)$, then we can rule $t^*(\pi)$ out.

The next execution time consequently is such that
\begin{equation}
\underline{\,T}_{nx}(B) = \min_{ip \in {\mathbb P}_S(B)} \min_{\pi \in \Pi_a(B.ip)}\, \underline{\,T}_{nx}(B,\pi)
\end{equation}

\subsubsection{Input arrival estimate}

As said before, an execution is possible as soon as one input from a sensitive input port arrives. When such an event occurs, we use the values for the other ports and process the behavior. However, we have absolutely no guarantee that the currently available inputs for the other ports are the correct ones: some source may produce a new input with system time lesser than $T_x$ after we executed the behavior. We consequently should wait for all sources to produce the most recent values in respect to $T_x$. This approach unfortunately has a drawback: it could be the case that no additional input for a source is produced anymore, or that it can be produced only as a consequence of the behavior itself being executed. The described approach would therefore block execution.

While the maximum distance in time between two consecutive values of an input may be undefined, the minimum distance is necessarily greater or equal to zero. We can therefore try to estimate the arrival of data based on their actual lower bound on the arrival time, then decide if it is necessary to wait for a new input or if all inputs have been received for this pending execution. The approach also applies to the data sources that influence the satisfaction of guards, since we can extend their support accordingly. Consequently, we want to estimate the arrival from all sources in ${\mathbb C}_{a,gs}(B)$.

It is important to underline how we deal with sources that produce a sequence of data with zero system time: if the execution time of a behavior equals the system time of such a sequence and the destination input port is sensitive, then we pick the earliest data that satisfies the guard: this is because all values for a sensitive port must be used. If on the other hand, the input port is insensitive, then we pick the latest data that satisfies the guard. This mechanism is a direct consequence of how sensitive ports operate: each valid input has to be processed along with the other existing inputs; if insensitive, we collect the latest value at the execution time. An alternative approach compared to the use of an insensitive port would be the use of an edge guard: such guard would be satisfied only the latest data value from the sequence.

The lower bound on the estimated arrival time of the next input for behavior $B$ at port $ip$ for the path $\pi$ is referred to as $T_{ei}(B,ip,\pi)$; again, we only consider the lower bound, while the upper bound is infinity. The actual expected time becomes 

\begin{equation}
\underline{\,T}_{ei}(B,ip) = \min_{\pi \in \Pi_a(B.ip)}\, \underline{\,T}_{ei}(B,ip,\pi)
\end{equation}
since we can't rely on guard satisfaction, which is unknown at $T_o(\pi[1])$, and must necessarily settle for the worst case scenario.

Let us now consider a behavioral implementation $B$ where an execution time $T_x(B)$ has been identified, but where at least one sensitive input port has its most recent input at $t < T_x(B)$, or an insensitive input port has its most recent input at $t \leq T_x(B)$; we call such behavior a {\em waiting} behavior, since it has to wait for possible new values for all of these particular input ports. In order to lift the waiting condition and enable execution, we must have
\begin{equation}
\label{eq:resolution}
\forall\,ip\in {\mathbb P}_I(B), \left\{
\begin{array}{ll}
\underline{\,T}_{ei}(B,ip) \geq T_x(B) & \textrm{, if {\em ip} is sensitive} \\
\underline{\,T}_{ei}(B,ip) > T_x(B) & \textrm{, if {\em ip} is insensitive}
\end{array}
\right.
\end{equation}
i.e., all input ports must be {\em resolved}; conversely, we call {\em unresolved} a port for which the condition of Eq.~(\ref{eq:resolution}) does not hold. This means that now we need to evaluate $T_{ei}(B,ip)$ for all input ports, insensitive included.

Operatively, for each $\pi \in \Pi_a(B.ip)$, we want to consider any new information useful to update the $T_{ei}(B,ip,\pi)$ values and consequently $T_{ei}(B,ip)$ values. First, we define $T_{eo}(\pi[1])$ as the estimated output time of the first element of the path (i.e., an output port of a behavior), and $\pi[1].b$ as the corresponding behavior. The estimated output time is updated as follows:
\begin{equation}
\underline{\,T}_{eo}(\pi[1]) = \left\{
\begin{array}{ll}
\underline{\,T}_{nx}(\pi[1],b) + \underline{\,pd}(\pi[1]) & \textrm{, if $T_{nx}(\pi[1],b)$ is updated} \\
\underline{\,T}_x(\pi[1],b) + \underline{\,pd}(\pi[1]) & \textrm{, if $T_x(\pi[1],b)$ is identified} 
\end{array}
\right.
\end{equation}
where we remind that since $pd$ and $T_{nx}$ can have strict lower bounds, then also $T_{eo}(\pi[1])$ can be strict. In the particular case of $pd(\pi[1])$ being exact (as our current timing specification imposes), the identification of $T_x(\pi[1],b)$ yields the exact output time; however, since we do not have any use for such known upper bound, $\overline{T}_{eo}(\pi[1])$ is not updated for simplicity. Why use lower bounds only if the delays are exact? This is because we may not be able to know if guards will be activated until we receive the value, hence in general no upper bound is computable when estimating the arrival time.

We have that
\begin{equation}
\forall ip \in {\mathbb P}_I(B),\, \pi \in \Pi_a(B.ip): \underline{\,T}_{ei}(B,ip,\pi) = \underline{\,T}_{eo}(\pi[1]).
\end{equation}

Interestingly, the fact that $\underline{\,T}_{eo}$ is monotonically increasing allows us to use the same approach used for ${\mathbb C}_a(B)$ also for ${\mathbb C}_{gs}(B)$, thus extending the guards and updating the availability functions, from which a better next execution time for $B$ can be obtained. More precisely:

Given a $C.op \in {\mathbb C}_{gs}(B)$ such that $\underline{\,T}_{eo}(C,op)$ is updated, for all edge/vertex guards in the source path guards where $C.op$ appears, extend the guard up to $\underline{\,T}_{eo}(C,op)$. Please note that if $T_{eo}(C,op)$ has a regular lower bound, then the upper bound on the guard is strict, and viceversa. The updates on the guards induce updates on any $\sigma_{gs}(\pi,t)$ and $\alpha_{gs}(\pi,t)$ functions that depend on $C.op$.

This result in turn allows to refine $T_{nx}(B)$, and even possibly predict $T_x(B)$ before the required inputs are actually received. These improvements then translate into improved input arrival estimates for the dependent behaviors, and so on. The termination criterion for this circular update is that when one $T_x(B)$ is identified, which happens before updating $T_{nx}(B)$, no further update to $T_{nx}(B)$ is performed; instead, the $T_{ei}(B,p)$ values are updated until we resolve all input ports.
	
\begin{figure}
\centering
\includegraphics[width=0.6\textwidth]{img/input-resolution1.png}
\caption{Example of exclusive branches wrapped in a loop.}
\label{fig:input-resolution1}
\end{figure}

An example is shown in Fig.~\ref{fig:input-resolution1}, where four behaviors $A$, $B$, $C$ and $D$ are present; all ports are sensitive. Here $B$ and $C$ have different propagation delays, but they both provide inputs to $D$. The guards on the input edges to $B$ and $C$ are mutually exclusive, while on the feedback edge between $D$ and $A$ the guard is provided in order to guarantee that data from the entry path to $A$ can be used sometime. The sensitivity clause of $D$ is such that a new output from either $B$ or $C$ can be used to trigger the execution.

Let's start from an input to $A$ that identifies $T_x(A)$, namely $T_x(A) = 0$ for convenience. The chain of updates is the following:
\begin{enumerate}
\item $T_x(A) = 0$
\item 1 causes 
\begin{enumerate}
\item $T_{nx}(B) \geq 1$
\item $T_{nx}(C) \geq 1$
\end{enumerate}
\item 2a + 2b cause $T_{nx}(D) \geq 2$
\end{enumerate}
This chain ends here because $A$ is not in the idle state, therefore its next execution time is locked to $T_x$ until it completes the current execution.

At a certain point, it must be the case that $A$ is executed. We denote this event with $d(A)@1$ to signify that data is available and the output time is provided. The chain is:
\begin{enumerate}
\item $d(A)@1$
\item 1 causes
\begin{enumerate}
\item $T_{nx}(A) \geq 0$
\item $T_x(B) = 1$
\item $T_{nx}(C) > 1$
\end{enumerate}
\item 2a causes $T_{nx}(A) \geq 2$
\item 2b + 2c cause $T_{nx}(D) \geq 2$
\item 4 causes $T_{nx}(A) \geq 2$ (no effect)
\item 5 causes $T_{nx}(C) \geq 3$
\end{enumerate}
Several comments are due here. First, the output from $A$ increases its next execution time, and defines $T_x(B)$ under the assumption that the guard towards $B$ is satisfied; if that is the case, the guard towards $C$ is not satisfied, then we must wait for a new input, consequently increasing the next execution time of $C$ of an $\epsilon$. When such next execution time changes, also the minimum of the inputs of $D$ changes, therefore we can update $D$. That, in turn, changes $T_{nx}(A)$ again, even if with no effect since the propagation time for $D$ is zero. Finally, $C$ is updated again to reflect the input from $A$. No additional change for $D$ is possible since $C$ does not provide the earliest arriving input to $D$ anymore: this is the worst-case situation where the inactive path is the shortest one; our iterative local approach does not allow further improvements on the next execution time estimate.

When $B$ is executed at $T_o(B) = 3$, we have
\begin{enumerate}
\item $d(B)@3$ 
\item 1 causes 
\begin{enumerate}
\item $T_{nx}(B) \geq 1$
\item $T_x(D) = 3$
\end{enumerate}
\item 2b causes $T_{nx}(A) \geq 3$
\item 2a + 3 cause 
\begin{enumerate}
\item $T_{nx}(B) \geq 4$
\item $T_{nx}(C) \geq 4$
\end{enumerate}
\end{enumerate}
Now we are able to resolve all inputs in respect to the clauses of $D$: it was necessary to know $T_o(B)$, since it may have been greater than $T_{nx}(C)+pd(C)$, thus requiring further resolution. 

When $D$ is executed and supposing that the data satisfies the guard towards $A$, we restart from $A$, otherwise we move along. If it happens that $C$ is satisfied, this time $T_x(D)$ is identified as soon as $C$ is executed.

It is interesting to note that if the entry path has an estimated arrival time of infinity, the update grows indefinitely. That is a problem if we want to identify whether a behavior is dead. For that reason, we supply a separate mechanism to solve this situation, by injecting a {\em death probe} (see Sec.~\ref{sec:death-probing}).

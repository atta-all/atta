\section{Repositories}
\label{sec:repositories}

The repository {\em descriptor} is essential for identifying and downloading artifacts (like implementations and type definitions). This descriptor essentially associates the {\em locator} of one artifact, i.e. a set of coordinates that uniquely identify the artifact in a domain and also allows to check out the artifact descriptor along with any other relevant files. The preferred domain is global, i.e. the Internet, in order to be able to produce results that may be reused by other applications without concern for name clashes on artifacts. Still, a restriction to a local (machine or network) domain is allowed for convenience.

This is an example of the syntax to specify a repository within an \verb+artifact.yaml+ file:

\begin{verbatim}
repositories:
- name: myrepo
  kind: git
  address: 'git@mysite.com:myself/myproject.git'
  mirrors:
  - 'git@thirdsite.com:myname/myproject.git'
  - 'git@fourthsite.com:othername/someproject.git'
\end{verbatim}

The reference, i.e, the locator of the required artifact, consists of {\em control}, {\em address}, {\em mirrors} and {\em origin} elements.

The \verb+name+ is a name given to the repository in the domain of this descriptor.

The \verb+kind+ represents the version control system used. We support the following kinds:

\begin{itemize}
\item \verb+git+: the Git version control system;
\item \verb+svn+: the Subversion version control system;
\item \verb+hg+: the Mercurial version control system;
\end{itemize}

The \verb+address+ represents the main coordinates for downloading artifacts, hence it may include any user identifier required for SSH access. 

Mirrors represent equivalent implementations that are alive, i.e., exist simultaneously. We provide the ability to specify mirrors in order to avoid a single-point-of-failure situation where no worker node is able to collect a required artifact. \footnote{Mirrors represent equivalent locators as far as reusal of results is concerned.}

As already said before, local network addresses can be used, for convenience. However, necessarily the worker nodes will need to reside within the network. While it is always possible to ``upgrade'' a local repository to become global, it is not possible to reuse a cached result as they are. To do that, the reference to the implementation in the result entry must be rewritten to account for the new global repository. In particular this means having write access to the persistence layer.

To reference an artifact within a repository, we introduce the concept of a {\em reference}. References have their own list in an artifact descriptor:

\begin{verbatim}
references:
- name: vector
  repo: myrepo
  path: myproject/physics/vector
  revision: 477125dbe78fe0a51be2486d8902b49ec2161450
\end{verbatim}

The \verb+name+ is a name given to the artifact in the domain of this descriptor, while \verb+repo+ references the declared repository in this descriptor.

The \verb+path+ provides the actual path to the artifact: following this path, a \verb+artifact.yaml+ file describing the artifact is expected to be found. Omitting the path implies that we refer to the root of the chosen repository. 

The \verb|revision| can be either a valid commit identifier (a progressive number, in Subversion, or a hash in Mercurial/Git), a tag or a branch\footnote{In Subversion tags and branches are not handled separately by the version control system: instead, a proper path should be used.}. While the commit identifier guarantees immutable references to artifacts, the tag is certainly useful to identify a particular release quickly; it must be reminded that a tag may be dropped and re-created for a different commit, hence using a tag is not a strong guarantee of immutability. On the opposite side of the spectrum lies the branch: using a branch allows to update an application without changing the application model itself; in practice, we actually point to the \verb|tip| of such branch. This can be a problem in terms of comparing two runs of the same application, since it is more difficult to identify which revision of each part has been executed. When comparison is less important, though, this approach simplifies the simulation of new revisions produced in a short time (where the update of the application model would be a considerable burden). The revision can even be omitted: in that case, it is assumed that the primary branch \footnote{The primary branch is \em{master} for Git, \em{default} for Mercurial: clearly, it is necessary that the repositories adhere to these defaults; otherwise the branch name should be provided.} is used; omitting is allowed for Subversion too, implying that we can use the latest commit available. It must be noted that local revision identifiers in Mercurial and hash prefixes in Git are not allowed, since they are not global: using them issues an error. In particular, an invalid revision identifier in Git is any string of less than 40 characters that can be parsed as hexadecimal: this means that tags/branches with such format must not be used; it is a design choice that addresses ambiguity in how tags and branches can be named in Git.

In this way we are able to identify an artifact, whatever that artifact may be, as long as it resides at the given checkout address with no extra-linguistic external references. It is useful to be able to find the list of available artifacts from a partial descriptor, in particular when we are not the owners of the repository and consequently the available artifacts must still be discovered. We decided to provide this functionality within client nodes: after checkout, the repository is traversed to get the artifacts. This solution, while not the most efficient, is considered the most agile for developers, who do not need to track the presence of \verb+artifact.yaml+ files around the repository or, for example, to maintain a list file at the root of the repository with all the artifacts contained.

We distinguish between repositories and references for the following reason: it is common to have one repository holding several artifacts. To link one artifact to its location directly would imply that if, e.g, ten artifacts are in a repository, then I need to write the repository control/address/mirrors text for ten times. On the other hand, however, it may be the case that each repository is referenced once only; for that particular case, we also offer an {\em inline} declaration of the repository within the reference itself, as this example demonstrates:

\begin{verbatim}
references:
- name: vector
  repo:
    kind: git
    address: 'git@mysite.com:myself/myproject.git'
    mirrors:
    - 'git@thirdsite.com:myname/myproject.git'
    - 'git@fourthsite.com:othername/someproject.git'
  path: myproject/physics/vector
  revision: 477125dbe78fe0a51be2486d8902b49ec2161450
\end{verbatim}

Here the repository name is not required since it would not be used anyway.

It must be noted that references are mainly used for compositional purposes: to compose the structure of the application graph or to compose a hierarchical data type. Cyclic dependencies between artifacts are clearly not allowed in such a context. Fortunately, this error is quite difficult to reproduce; in particular, if a reference uses a previously created revision commit, it is impossible to create a cycle: this is due to the fact that to change an artifact reference we must commit the change, hence creating a new revision and practically a new artifact. This behavior however can be overridden by creating a commit with the cyclic dependent artifacts referencing each other using the revision of the next commit. This can be done easily in Subversion, since the next commit has a well defined value. It also works with tag revisions, if we decide beforehand which tag name to associate to the next commit: we use such tag for the cyclic references, commit and then actually create the tag for the commit. Finally, tip revisions also suffer from this problem as tag revisions do, even if that may be temporary due to the mutability of the pointed commit. For these reasons, we check for cyclic dependencies when resolving the references of an artifact.

On a final note, we recognize that third-party libraries are often made available as packaged files downloadable from a non-versioned repository such as an HTTP resource. This is a serious issue when libraries are moved, or changed with no corresponding change to the file name. Therefore we impose that in such cases libraries must be copied within a repository with an artifact descriptor that will provide the building and linking specification. While this approach represents a slight burden on the designer, it also enables automated reproducible access to the library, and also allows safe reusal if the repository is versioned.
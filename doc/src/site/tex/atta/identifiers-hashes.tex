\section{Identifiers and hashes}
\label{sec:identifiers-hashes}

The identifiers used in Atta are meant to provide a unique representation. In some cases, however, a more compact representation is desirable for indexing by the runtime. In that case, hashes are provided.

We consider different classes of resources whose ID is required, namely:

\begin{itemize}
\item Implementation model
\item Implementation instance
\item Worker node
\item Execution
\end{itemize}

For each of these classes, we must guarantee that no two different resources of the same class share the same hash code. While very unlikely, this situation is avoided by "registering" the resource once and for all, so that no additional checks will be required.

Registration is performed in respect to the persistence layer, which is the reference for producing hashes that the runtime must use. In fact, if another resource of the same class is found with the hash of the ID of the resource, the ID must be internally modified to obtain a different hash. To do that, a dollar character followed by a random character is added to the previous ID and the result re-hashed, adding new random characters iteratively until no collision exists.

In the following we provide the details on the identifier structure and the hash registration process for each class.

\subsection{Implementation model}

An implementation model (or simply implementation) is represented by the coordinates to check out the content via a VCS.

\begin{verbatim}
<protocol> <checkout URI> <directory path> <version identifier>
\end{verbatim}

\subsection{Implementation instance}

The implementation instance ID is given by the identifier of the implementation model along (if the implementation is structural) with the sequence of implementation instance hashes of its vertices.

\begin{verbatim}
<impl. ID> <vertex 1 impl. instance hash> ... <vertex N impl. instance hash>
\end{verbatim}

If the implementation is behavioral, then only the implementation model ID is present.

The instance hash is the hash of the ID itself. Working with hashes in this case is particularly sensible, since otherwise the instance identifier grows indefinitely as the hierarchy depth increases; in this case, instead, the identifier size grows with the number of vertices, which is fixed.

Since implementation instance hashes have validity across application instances, we must be able to reuse them but at the same time we must guarantee proper creation the first time. Hence an implementation instance hash is recovered as soon as a switch in any implementation instance is performed (this includes the application instance initialization). The first step of the process is for each implementation, top down starting from the system implementation, to recover the hash of its vertices; as soon as the hash of the vertices are recovered, the hash of the interested implementation can be requested. The request implies the creation of the candidate hash and the verification of the existence of such hash: if absent, then the hash is accepted, a new entry for the implementation instance is created; if present, the existing entry is checked for its stored identifier field: if the identifiers match, then the implementations are the same and no new implementation instance entry is due; otherwise a new unique hash is constructed and the related entry is added, along with a field holding the related identifier.

\subsection{Worker node}

A worker node identifier is given by its IP address, the {\em lshw} hash (as from the Linux command) and a supplied public key:

\begin{verbatim}
<ip> <lshw> <keytype> <pubkey>
\end{verbatim}
where spaces are used to divide the fields; the keytype can be \verb+rsa+ or \verb+dsa+.

Each time a worker node joins the runtime, it is given its node hash to be used for identification.

\subsection{Execution}

An execution is identified by the identifier of the (behavioral) implementation, along with a JSON serialization of the inputs for each input port of the implementation.

\begin{verbatim}
<impl. id> <inputs serialization>
\end{verbatim}

The inputs therefore include parameters, that would depend on binding when the implementation is used for a vertex. 

The motivation for supplying such an identifier is to identify whether an execution of the same behavior has already been performed and possibly load the result from the persistence.

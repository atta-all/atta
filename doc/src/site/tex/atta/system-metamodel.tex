\chapter{System metamodel}
\label{ch:system-metamodel}

The systems to be simulated are expressed through a metamodel. We use the term metamodel since it represents a model of the model of a system. In other terms, an instance of a system model is the actual application run in Atta, while the metamodel is simply the set of rules under which such model can be specified. The user will ultimately construct the system model using a graphical toolset. The resulting model will then be persisted as an XML file for consistent manual and programmatic editing.

Please note that this metamodel does not aim to guarantee decidability of every sanity check on the system definition. Its main objective is to allow the greatest degree of generality with a scalable complexity perceived by the user. Since the semantics of the implementation of any block of the system is free to the implementer, we consider external correctness to be of marginal relevance anyway.

At a first glance, the metamodel is a (cyclic) directed graph; such graph can be open, input-open, output-open or closed: no restrictions are introduced. A vertex of the graph is a functional block, which exposes an I/O interface. An edge is a medium for data exchanged between vertices. This metamodel consequently is to be classified within the dataflow category. Completion of the execution of a system model happens as soon as every vertex is dead, i.e. it cannot be executed anymore. However, termination is not by itself a requirement of the metamodel: the modeled system may have an unlimited evolution, with real-time application of inputs.

Since the system as a whole can also be an input-closed graph, input data can be specified by internal constants. The input-open version instead implies the presence of a stream of inputs fed to the system. Outputs are also optional, but when defined they have a special role for clients. In fact it must be noted that, by design, any edge data is actually observable by the user: output edges are consequently defined for programmatic purposes, among which dynamic composition. In general, representing a system with an open graph is usually preferable in terms of extensibility.

Vertices are black-box containers for implementations. Implementations can be behavioral (i.e. a routine written using some programming language, using a functional I/O interface) or structural (i.e. another input/output open graph). The hierarchical structure stemming from structural implementations allows to incapsulate information and simplify the observation and manipulation of the system by the user/designer. It also facilitates "code" reusal.

This description only scratches the surface, since the described metamodel still works for static models. In order to allow different implementations for a single vertex, the metamodel can be enriched by providing "alternatives". Each alternative must comply with the interface of the vertex that it will implement. Multiple behavioral and structural alternatives for a vertex are defined as a switched system, configured on one specific implementation at a given time. Truth be told, the model remains static, since the runtime ``flattens'' the model to treat the alternatives as separate branches of a conditional subgraph: the "alternatives" therefore are only an aid for the definition and observation of the system. The runtime is designed to operate on a planar graph of behavioral implementations, where the specific one $B$ interacts with its {\em source} behaviors ${\mathbb S}(B)$ (i.e., the behaviors whose outputs ultimately provide data to the behavior) and {\em destination} behaviors ${\mathbb D}(B)$ (i.e., the behaviors that receive data produced by the behavior).

We will also see that the state model associated with the system model will allow to rewind and resume the execution: this feature is considered important for simulation, since it also enables the static substitution of an implementation with a different one within the evolution of the system.

In the following we discuss about all the aspects that relate to the syntax and semantics of the metamodel, while the implementation aspects are found in Chapter~\ref{ch:runtime}. The XML syntax is provided everywhere relevant to the discussion.

\input{repositories}
\input{types}
\input{vertex-impl}
\input{time-guards}
\input{state}
\input{model-rewriting}
\input{composition}
\input{static-analysis}
\section{Type System}
\label{sec:typesystem}

Data edges in the model are represented using names with language-agnostic {\em abstract} (data) types. In addition, implementation ports explicitly refer to types. On the other hand, data is processed using {\em concrete} (data) types.

The primitive abstract types are the following:

\begin{itemize}
\item Boolean \verb+bool+: either a true or false value; used for binary flags;
\item Byte \verb+int8+: a signed 8 bit integer value, used for bit arrays, or raw data, especially file streams;
\item Byte \verb+int16+: a signed 16 bit integer value;
\item Byte \verb+int32+: a signed 32 bit integer value;
\item Byte \verb+int64+: a signed 64 bit integer value;
\item Real \verb+double+: a signed 64 bit floating point value;
\item Text \verb+text+: a sequence of characters in UTF8 format; used for human-readable data representations, including small documents;
\end{itemize}

These data types are also called {\em atomic}, since they cannot be subdivided further.

For each of these types, multi-dimensional homogeneous arrays are supported, thus limiting to a regular (i.e., rectangular) matrix; hence we say that a type is either a {\em scalar} (if no array specification exists) or an {\em array}. 

Array types default as having one dimension and unbounded cardinality, meaning that an array can contain from zero to a very large number of homogeneous elements\footnote{A hard limit still has to be defined, most likely having each dimension within 16 bits, i.e. a Java short.} The number of dimensions is not explicitly provided, but it is implicit from the definition of the bounds. Bounds (lower and upper) are provided by a simple expression, e.g.:

\begin{itemize}
\item[-] \verb+0:4+ : an array of none to 4 elements
\item[-] \verb+2+ : an array of exactly 2 elements (shorthand for \verb+2:2+)
\item[-] \verb+1:+ : an array of at least 1 element
\item[-] \verb+:3+ : an array of at most 3 elements (shorthand for \verb+0:3+)
\item[-] \verb+:+ : an unlimited array (shorthand for \verb+0:+)
\item[-] \verb+1:2,3:5+ : a rectangular matrix of 1 to 2 rows, where all the rows have the same number (3, 4 or 5) of columns 
\item[-] \verb+1:,5+ : a rectangular matrix of at least 1 row and exactly 5 columns
\item[-] \verb+3,3+ : a square matrix of 3 rows and 3 columns
\item[-] \verb+:,:+ : unbounded bidimensional rectangular matrix
\end{itemize}

Summarizing, bounds are separated by colons, while dimensions are separated by commas. It must be stressed that the lower bound means that we guarantee/force the array to always be populated with at least a given number of elements. This is important when evaluating the compatibility between types, as described in the following. To provide reasonable bounds is always preferable, especially for large data sets, since it allows to preallocate data structures and possibly to evaluate the bandwidth required to exchange them. For dimensions greater than the first, the implicit lower bound is one, while an explicit lower bound of zero is invalid.

If we composite types together, we have a {\em composite} type. Composites can be made of any types, independently of them being scalars or arrays. Moreover, we are not limited to compose atomic types but we can compose composites too. Arrays of composites are also allowed, thus making composites as usable as atomic types are.

Composites may be defined either on the same descriptor file or as an external reference. Please note that a separate definition of composite types promotes reusal of such types within a descriptor file, while an external reference extends such reusal to other descriptor files. If we want to provide additional types within the same abstract type descriptor file, those must be listed within a \verb|types| element with the same name as the type name used. If on the other hand we want to use an external reference, the reference name must match the type name used; it is important to note that the name of the referenced abstract type found within its artifact descriptor file is irrelevant.

In addition, we are able to define enumerations, sets and maps. For simplicity, the numeral associated to an enumeration starts from 0 and goes up to 127: we do not support custom numerals in order to keep the syntax as terse as possible. Enumerations therefore only have one \verb|enum| field other than the name. A set is a collection of values of a given \verb|type|, specified in the \verb|set| element; a set may be useful to define the range of values for some inputs. A map is an unbounded array of key-value pairs; we support only primitive types as \verb|keys| (in order to support some programming languages for which keys must have a primitive type), while the \verb|values| can be of any type, even another map.

Summarizing, the definition of a type has always a \verb|name|, plus one of the following elements based on the class of abstract type:
\begin{enumerate}
\item Composite: \verb|fields|
\item Enumeration: \verb|enum|
\item Set: \verb|type| and \verb|set|
\item Map: \verb|keys| and \verb|values|
\end{enumerate}

A descriptor for an abstract type is like the following:

\begin{verbatim}
name: town
fields: 
- name: name
  type: text
- name: location
  type: text
\end{verbatim}

This type identifies a town, with a name and a textual location. This is quite a simple composite type, where we used only primitive types for the fields. We could enrich this type by creating a location composite type with a province, region, state and 2 or 3 coordinates, and finally a key-value map of other textual properties. 

For example, we can have the following:

\begin{verbatim}
name: town
fields: 
- name: name
  type: text
- name: location
  type: location
- name: props
  type: txttxtmap
types:
- name: xyz
  enum: [x, y, z]
- name: coordmap
  keys: xyz
  values: double
- name: location
  fields:
  - {name: province, type: text}
  - {name: region, type: text}
  - {name: state, type: text}
  - {name: coords, type: coordmap, array: '2:3'}
- name: txttxtmap
  keys: text
  values: text
\end{verbatim}

As we can see, we can use either YAML syntax for map elements (on one line or on separate lines). 

When we need to address specific fields of a composite within other artifact descriptor files, we use the dot notation; for example, a \verb|town| type with a \verb|location| subtype having a \verb|road| field with text values, would be addressed with \verb|town.location.road|.

Finally, it is important to remark that the order between fields on the same level of the type hierarchy is not defined. In practice, we do not rely on either order or names of the fields; the latter in particular simply represent aids for the designer. This aspect is relevant when assessing type compatibility.

\subsection{Type compatibility and mapping}

Our metamodel allows to map a data source to a data destination quite freely, practically meaning that in some cases a subset of the source can be mapped onto a destination, or that a subset of the destination can be mapped to the source. In order to provide a general way to associate sources and destinations, we must identify whether a (sub)type is at least compatible with another type. From weaker to stronger relation, we distinguish between {\em compatibility}, {\em matching} and {\em equivalence}. Compatibility means that there is at least one ordering of field types in the source (sub)type that returns the same type tree as that of the destination type; in that case we say that the destination is compatible with the source. Matching means that only one such ordering exists (regardless of it being the order defined in the type descriptor or not). Equivalence means that the types are explicitly defined as being the same, and this is clearly a symmetric property.

A compatibility check for atomic elements is obviously trivial: as long as the types are the same, they are equivalent. For composite types, a type $D$ is compatible with another type $S$ if the type tree is the same (for at least one ordering). When arrays are not involved, compatibility checking is quite straightforward. When instead a (multi-dimensional) array is present, the lower bound of the destination array must not be higher than the lower bound of the source array, for each dimension involved. If that were the case, the source would not guarantee to supply the required elements to the destination; upper bounds are not a problem, but may require a matching specification, as discussed in the following. Note that field names are never considered for checking compatibility: only atomic types count; this also means that composites (implicit or explicit) are transparent to this check. Let us consider the following types:

\begin{verbatim}
types:
- name: 3Dcoordinates
  fields:
  - {name: x, type: double}
  - {name: y, type: double}
  - {name: z, type: double}
- name: 2Dcoordinates
  fields:
  - {name: x, type: double}
  - {name: y, type: double}
\end{verbatim}

This example is deceivingly simple: we would be tempted to say that the first two fields of \verb+3Dcoordinates+ match \verb+2Dcoordinates+, but we cannot rely on names for semantics. What if the bidimensional 2D coordinates actually refer to \verb+y+ and \verb+z+ in the other reference system? This is a clear case where there is only compatibility. As a consequence, we need to define a {\em mapping} between the two types, as discussed in Subsec.~\ref{subsec:binding}. The problem stems from the fact that types may come from different repositories and consequently their semantics could be similar, but not equal. To provide automatic mapping, the types must match or, better, be strictly the same. While matching is not achievable when a composite type has multiple fields with the same type, if we operate on the same domain we can properly design the types to guarantee equivalence. More specifically, we design for {\em composition}: for example, in our coordinates case, we could rewrite \verb+3Dcoordinates+ as follows:

\begin{verbatim}
name: 3Dcoordinates
fields:
- {name: planar, type: 2Dcoordinates}
- {name: z, type: double}
\end{verbatim}
which lifts the ambiguity at the cost of some additional structuring. Consequently, defined composites also have an important role for establishing equivalence and for allowing automatic mapping, which cannot rely on compatibility. Another abstract type containing a \verb+2Dcoordinates+ and a \verb+double+ (regardless of their order) then would {\em match} the \verb+3Dcoordinates+ type.

Let's now consider a different (more abstract) example:

\begin{verbatim}
types:
- name: srctype
  fields:
  - {name: sa, type: byte, array: '4:'}
  - {name: sb, type: byte, array: '3:9'}
  - {name: sc, type: byte, array: '2'}
- name: dsttype
  fields:
  - {name: da, type: byte, array: '2:7'}
  - {name: db, type: byte, array: '2'}
  - {name: dc, type: byte, array: ':5'}
\end{verbatim}

Here it is clear that \verb+da+ is compatible with \verb+sa+, \verb+db+ is compatible with \verb+sb+ and \verb+dc+ is compatible with \verb+sc+. However, there are two aspects to consider: first, since the fields have the same types, again we can also map the fields using a different ordering. The second aspect is the actual bounds of the arrays: even if \verb+db+ is a destination for \verb+sb+, which of the three guaranteed bytes from \verb+sb+ go to the two bytes of \verb+db+? A reasonable strategy would take the first elements of the array, and try to map fields in their natural order, but the intent of the designer may be different. This is another case that explains why explicit matching is required for composite types (and for some arrays).

As we explained earlier, if types pertain to different domains (i.e., different descriptors, such as those coming from different implementations), then types with the exact same definition cannot be considered equivalent and thus may not even be matching but only be compatible. In that case we can rely on a type descriptor on a repository and use for any other artifact we need. We remind that only one artifact descriptor (abstract type, in this cases) is allowed within the repository file. For example:

\begin{verbatim}
name: measures
fields:
- {name: height, type: double}
- {name: width, type: double}
\end{verbatim}

Please note that, while a \verb+name+ is supplied, it only has use for documentation purposes, since two abstract types with the same name and structure, but coming from two different domains, may still have different semantics.

To reference a type towards a repository, we only need to use the name of an artifact reference within the description of a type:

\begin{verbatim}
name: 3Dcoordinates

fields: 
- {name: planar, type: 2Dcoordinates}
- {name: z, type: double}

references:
- name: 2Dcoordinates
  repo: myrepo
  path: myproject/physics/2dcoord
  revision: 477125dbe78fe0a51be2486d8902b49ec2161450
  
repositories:
- name: myrepo
  control: git
  address: 'git@mysite.com:myself/myproject.git'
\end{verbatim}

This example shows how the artifact descriptor of an abstract type with external dependencies looks like. Here \verb|2Dcoordinates| is a label for an artifact that will be looked for within either the internal types or the references; in this case, a match in the \verb|2Dcoordinates| reference is found.

In some situations, we may be stuck with some implementations for which types cannot be changed, either due to the burden of changes involved, or simply because the artifacts are not under our control; in other situations we may just want to have types with different names inside our domain. In either case we need to supply an {\em equivalence relation} using the \verb+equivalences+ field. The simplest equivalence relation may read as follows:

\begin{verbatim}
equivalences:
- between: mytype1
  and: mytype2
\end{verbatim}

This equivalence definition works if the two types are matching: if they are only compatible, this is considered an error. Only one equivalence relation is necessary: you do not need to create another relation with the two types switched. When the types are only compatible, we also need to supply the matching ourselves, for example:

\begin{verbatim}
equivalences:
- between: mytype1
  and: mytype2
  matching:
  - field: x
    with: height
  - field: y
    with: width
  - field: z
    with: depth
\end{verbatim}

Here, for each field of the \verb+between+ to match, we provide a corresponding field of the \verb+and+ type; the \verb+field+ represents the field name of the first, while the \verb+with+ represents the field of the second. Summarizing, when types are not equivalent (either by symbolic reference or by explicit equivalence specification), we are not allowed to associate two (sub)types.
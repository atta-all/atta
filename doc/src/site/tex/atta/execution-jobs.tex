\section{Execution jobs}
\label{sec:execution-jobs}

This section covers how an {\em execution job}, which relates to the evolution of one behavior instance, is handled by the runtime.

First, each application instance can be found under the \verb+/ai/+ node, created as a sequential znode with the generic name \verb+aiid+. For each instance, we always have the current system implementation \verb+/ai/$(aiid)/curr+ and possibly the previous system implementation \verb+/ai/$(aiid)/prev+. This is in order to discriminate between multiple systems in a substitution: until the transient is completed, we need to have jobs on both system implementations. Each system implementation \verb+si+ a field \verb+/ai/$(si)/_impl+ that contains the identifier of the system implementation, in order for jobs to understand which implementation(s) are present.

Then, we define as \verb+bip+ a behavior instance path, i.e. the path through the model hierarchy that starts from the system implementation through vertices until the vertex for a given behavior is reached; since the execution model has only one implementation per vertex, this information is sufficient to identify one implementation. Therefore \verb+bip+ is a concatenation of vertices named after their full name, e.g., \verb+foo/bar/baz+.

An execution job (xjob) exists for each behavior of the current application instance. An xjob can be of four classes, depending on it being {\em master/slave}, on one hand, and {\em permanent/transient}, on the other. A master job is a job that has most of the responsibilities of a meshed implementation, while a slave one has limited activities to perform; obviously, a behavior of a non-meshed implementation is handled by a master xjob. A transient xjob is related to a behavior that is part of the source of an implementation substitution: as such, it will be dropped as soon as the source implementation is not required anymore; a permanent xjob, which is the default case, pertains to the destination system of the substitution.

All xjobs are initially unassigned and can be found under the \verb+$(aiid)/xjobs/u/+ znode. Consequently, the xjob identifier \verb+xjid+ is created when creating sequential znodes in \verb+$(aiid)/xjobs/u/+; the data for such znode is given by the behavior instance path \verb+bip+ concatenated with the {\em coordinates} \verb+coord+ of the behavior: such coordinates are present in the case of meshed implementations, and also represent the values for each index input; for example, a concatenation could be \verb+this/behavior/is/meshed[1][0]+. Worker nodes listen for changes to the xjobs list and perform the appropriate distributed load balancing and scheduling to be assigned to them.

Xjobs are assigned to wnodes by having a znode under \verb+$(aiid)/xjobs/a/$(wnid)/+ with a name and data equal to the job identifier \verb+xjid+; \verb+aiid+ is the application instance identifier, while \verb+wnid+ is the identifier of the wnode as present under \verb+/wnodes+. Please note that at any point of time, assigned xjobs can return to the unassigned state.

A behavior state is kept at the \verb+$(aiid)/$(si)/$(bip)+ znode, under which we identify \verb+_nxtime+ (i.e., the next execution time $T_{nx}$); it must be remarked how meshed implementations share the same znode. Information \verb+INFO+ on a certain aspect of an execution is stored under \verb+$(bip)/$(INFO)/$(xid)/+, where \verb+xid+ is the execution id, created in the \verb+x-COUNTER+ format; the execution id is global for the instance, therefore its value is held in a sequential znode created under \verb+$(aiid)/xids/+; the znode will be initially created with only the \verb+(bip)+ as data; when the execution time is found, it is appended with the \verb+;t=+ prefix, followed by the \verb+;d=[+ prefix; as soon as one input is resolved, the corresponding execution of the source behavior is attached with \verb+$(xid);+; finally when the execution is ready, all the depending executions are present and the \verb+]+ list termination replaces the trailing semicolon. In this way, the state of the corresponding xjob can be quickly recovered by reading one znode only. However, the data on an xjob znode is assumed to be possibly delayed in respect to the actual execution of the xjob by the assigned wnode; since the data is incrementally added, the conveyed information is never outdated.

\verb+INFO+ takes the following values:

\begin{itemize}
\item \verb+ds+: the data set, under which all the results of the execution are created;
\item \verb+xt+: the execution time obtained when the execution guard becomes enabled;
\item \verb+r+: the ready flag which signals that all inputs are resolved;
\item \verb+mc+: the meshed completion pool where each instance notifies the completion of its specific data storage;
\item \verb+c+: the completed flag which signals that the data for execution is fully stored.
\end{itemize}

Therefore all the entries have the same name, but are present under different paths; in the case of the execution time, the data is the time itself; in the case of the data set, children znodes are created hierarchically to match the output ports type structures; also, based on the behavior declaration, we can have a \verb+ds/$(xid)/_t+ field for the output time of all the data, or alternatively specific \verb+ds/$(xid)/$(outputport)/_t+ for the output time of each output port. For the \verb+id+, \verb+ds+ and \verb+c+ znodes, we will see that only one thread is responsible for their creation, therefore they can be creates as sequential znodes; for \verb+xt+ and \verb+r+ instead we will have to directly use \verb+xid+ and handle conflicts. The \verb+mc/+ children are created only in the case of meshed implementations, where each xjob creates a sequential znode starting with \verb+$(coord)+; when a number of entries equal to the size of the mesh is received, then the mesh is completed and the \verb+c+ znode can be created.

We call \verb+cxid+ the current (latest) execution id, whose location is found by reading the children of \verb+$(aiid)/xids+ and picking the highest counter value for \verb+bip+; similarly, \verb+pxid+ is the previous execution id. Therefore, a behavior state is captured by the following information:

\begin{itemize}
\item Dead: if \verb+_nxtime+ equals infinity;
\item Idle: if \verb+_nxtime+ is finite, and \verb+c/$(cxid)+ exists or \verb+xt/$(cxid)+ does not exist;
\item Waiting: if \verb+xt/$(cxid)+ exists, but \verb+r/$(cxid)+ does not;
\item Ready: if \verb+r/$(cxid)+ exists, but \verb+c/$(cxid)+ does not.
\end{itemize}

The initial state of an application instance involves the creation of special input znodes, at

\noindent
\verb+$(aiid)/_inputs/$(input)+. Each input znode is fed with new inputs from clients. For all the other aspects, it looks like any other behavior instance znode, including the presence of a \verb+_nxtime+ that is monotonically updated. All the behaviors initially have a negative next execution time, signifying that the value is invalid.

An xjob has several {\em phases} of operation; some of them are related to the {\em lifecycle}, i.e. from its creation to its removal, through its assignment to a wnode; when assigned, an xjob has {\em processing} phases that evolve the corresponding behavior state. Each phase of an xjob must be safe in the presence of eventual consistency in the read data and must be properly recoverable if any involved wnode suddenly fails. In particular, we identify four mandatory properties:

\begin{itemize}
\item Atomicity: any wnode listening for the creation of data must also be able to detect when such creation is completed;
\item Isolation: it is not possible that the effect of multiple concurrent writers produce an incorrect state;
\item Lazyness: reading arbitrarily outdated data does not produce an incorrect strategy for the reader;
\item Robustness: if a wnode fails, we can restore the runtime to a correct state.
\end{itemize}

Atomicity must be provided since we need composite data to be read consistently, while it cannot be written in one go. Isolation is required since write conflicts are possible due to the asynchronous nature of the runtime. Lazyness is necessary due to the eventual consistency offered by the synchronization and persistence layers, meaning that updates between nodes in an ensemble/cluster are delayed; updates are guaranteed to be issued in the order of creation. And finally, robustness is strongly suggested in a context where nodes can fail for a variety of reasons, including loss of connectivity, power failures or just plain software crashes; we do not aims for a fast recovery, since failure detection in a TCP/IP environment requires an interval of seconds, but we must guarantee that the runtime is always recovered to a safe state.

Let's now analyze the phases in more detail, starting from the lifecycle phases followed by the processing phases.

\subsection{Job lifecycle}

The lifecycle of an xjob includes the following phases:

\begin{enumerate}
\item Create
\item Assign
\item Recover
\item Remove
\item Unassign
\end{enumerate}

These phases must be carefully designed so to guarantee that every wnode failure results in the creation of an unassigned job.

\subsubsection{Create}

Creation of a new unassigned job happens only in the initialization of the instance. A new job is created as a sequential znode under \verb+$(aiid)/xjobs/u+, with data equal to the behavior instance path.

\subsubsection{Recover}

This phase is concerned with the transfer of jobs assigned to a failed wnode into the list of unassigned jobs. As soon as a wnode fails, its ephemeral node in \verb+/wnodes+ is removed: if at least another wnode exists, it will be notified of changed children in \verb+/wnodes+; if no other wnode exists, it is obviously the case that no processing is possible. When a wnode receives such watch event, it may try to get a lock for recovery for each instance, under \verb+$(aiid)/locks/r/+ using the distributed load balancing; consequently, recovery has an inter-application character. If the lock is gained, the wnode compares the \verb+$(aiid)/xjobs/a+ children (which are wnode ids) with the children in \verb+/wnodes+ for wnodes absent in the latter. Then, for each failed \verb+wnid+ with existing assigned jobs, it picks each job \verb+$(aiid)/xjobs/a/$(wnid)/$(ajid)+ and creates the unassigned job under \verb+$(aiid)/xjobs/u/+ with the same name and data, if it does not exist, then removes \verb+$(aiid)/xjobs/a/$(wnid)/$(ajid)+; when no assigned job remains, it removes \verb+$(aiid)/xjobs/a/$(wnid)+. When no remaining \verb+wnid+ are present, it removes its own lock, terminating the recovery. We check for pre-existing unassigned jobs with the same name because these two situations may occur: either the failed wnode stopped operating during a Recovery phase, just before removing the assigned job znode, or the failed wnode stopped operating during an Assignment phase, just before removing the unassigned job znode; in both cases, the unassigned job znode must remain, while the assigned one must be removed. If a wnode simply fails after getting the lock, the next wnode that requested the lock will get it and proceed in its place. When the lock is finally removed, a watching wnode that gets the lock next will identify no \verb+wnid+ and will directly remove its own lock, cascading.

\subsubsection{Assign}

All wnodes listen for new entries in \verb+$(aiid)/xjobs/u/+. As soon as a new unassigned job is seen, the wnode competes for assignment by means of the distributed load balancing. The wnode tries to acquire a lock on the job under the path \verb+$(aiid)/locks/a/$(ujid)/+, where \verb+ujid+ is the identifier of the unassigned job. The "winning" wnode copies the znode under \verb+$(aiid)/xjobs/a/$(wnid)/+ then removes the unassigned job znode followed by the lock. From now on, the job is assigned to this wnode and the processing phases can be performed. A failure before copying the job is safe, because no changes have been performed apart from creating the lock znode, which being ephemeral is removed: this triggers a lock acquirement by the next competing wnode. A failure before or after removing the unassigned job is handled by the recovery phase, as previously explained. The removal of the lock triggers the watch event for the next wnode, which sees that the unassigned job does not exist anymore and removes its own lock, cascading.

\subsubsection{Remove}

The removal of an xjob is a result of either the death of the behavior or, in the case of a transient xjob, the completion of a substitution. Such removal is handled by the wnode that is assigned to the job, and from the synchronization viewpoint it involves only the deletion of the \verb+$(aiid)/xjobs/a/$(wnid)/$(ajid)+ znode.

In order to identify the completion of a substitution, we keep a watch for new children of each

\noindent
\verb+$(aiid)/curr/$(dobip)/c/+, where \verb+dobip+ represents a current behavior having a vertex in common with \verb+bip+ and whose outputs are outputs for the vertex. Since a substitution can involve multiple vertices, it is possible for two transient xjobs to watch different sets of \verb+dobip+. Only when all the \verb+dobip+ of one transient xjob have provided one output whose time guard is satisfied, we can remove the xjob itself. Before doing that, however, we must set \verb+$(bip)/_nxtime+ to infinity to simplify the execution of the following behaviors.

\subsubsection{Unassign}

If a wnode wants to relinquish its hold on an xjob, it can unassign it by moving the assigned xjob to \verb+$(aiid)/xjobs/u/+. To do that, it must first create the unassigned job and then remove its assignment. If the opposite were enforced, a failure of the wnode between the two operations would essentially be undistinguishable from removing the xjob. Instead, using this order, a failure in-between would trigger a recover phase, where the assignment znode would be removed by the recovering wnode.

\subsection{Job processing}

The job phases that relate to processing are:

\begin{enumerate}
\item Initialization
\item Loading
\item Execution
\item Storage
\end{enumerate}

Such phases must guarantee to move the behavior state monotonically between its idle, waiting and ready states, as previously discussed; a wnode that has just been assigned the job starts from the Initialization phase.

We will discriminate between two classes of xjobs: master and slave. A master xjob is a job associated with a non-meshed behavior or a job associated with the zero-th instance of a meshed behavior: it is responsible for identifying the execution time, the ready condition, to execute and to store the results. A slave job instead does not need to identify the execution time or the ready conditions, since those can be computed by only one xjob (the master xjob, that is); consequently, the Loading phase of a slave job is considerably simplified.

\subsubsection{Initialization}

This phase creates the necessary znodes while possibly cleaning up after the failure of a previous wnode assigned to this same job.

\begin{figure}
\centering
\begin{tikzpicture}[auto]
\tikzstyle{decision} = [diamond, draw, fill=white!20, text badly centered, inner sep=0pt]
\tikzstyle{block} = [rectangle, draw, fill=white!20, text centered, rounded corners, minimum height=4em]
\tikzstyle{line} = [draw, -latex']

\node [decision, node distance=auto, text width=7em] (cxid) {\verb+cxid+ defined $\land \,\nexists$ \verb+c/$(cxid)+};
\node [decision, right of=cxid, node distance=4cm, text width=7em] (meshed) {is a meshed implementation};
\node [decision, below of=meshed, text width=6em, node distance=4cm] (mesh-completed) {is mesh completed};
\node [block, below of=mesh-completed, node distance=3cm] (create-completion) {create \verb+c/$(cxid)+};
\node [decision, right of=mesh-completed, node distance=4cm] (exists-dataset) {$\exists$ \verb+ds/$(cxid)+};
\node [block, below of=exists-dataset, text width=7.5em, node distance=3cm] (create-dataset) {create \verb+ds/$(cxid)+};
\node [block, right of=create-dataset, text width=9.5em, node distance=4cm] (remove-dataset) {remove \verb+ds/$(cxid)/$(data)+ for the instance};
\node [decision, below of=remove-dataset, text width=7.5em, node distance=3cm] (master-xjob-nocreate) {is a master xjob};
\node [decision, below of=create-completion, text width=7.5em, node distance=3cm] (master-xjob-create) {is a master xjob};
\node [block, below of=create-dataset, text width=7.5em, node distance=6cm] (watch-new-xid) {watch: new \verb+xid+ $\rightarrow$ go to Loading};
\node [block, below of=watch-new-xid, text width=7.5em, node distance=2cm] (goto-loading) {go to Loading};
\node [block, below of=master-xjob-create, text width=7.5em, node distance=3cm] (create-xid) {create new \verb+xid+};

\path [line] (cxid) -- node [near start] {yes} (meshed);
\path [line] (cxid) |- node [near start] {no} (master-xjob-create);
\path [line] (meshed) -- node [near start] {yes} (mesh-completed);
\path [line] (meshed) -| node [near start] {no} (exists-dataset);
\path [line] (mesh-completed) -- node [near start] {yes} (create-completion);
\path [line] (mesh-completed) -- node [near start] {no} (exists-dataset);
\path [line] (exists-dataset) -- node [near start] {no} (create-dataset);
\path [line] (exists-dataset) -| node [near start] {yes} (remove-dataset);
\path [line] (create-completion) -- node [near start] {} (master-xjob-create);
\path [line] (create-dataset) |- node [near start] {} (master-xjob-nocreate);
\path [line] (remove-dataset) -- node [near start] {} (master-xjob-nocreate);
\path [line] (master-xjob-nocreate) -- node [near start] {no} (watch-new-xid);
\path [line] (master-xjob-create) -- node [near start] {no} (watch-new-xid);
\path [line] (master-xjob-create) -- node [near start] {yes} (create-xid);
\path [line] (master-xjob-nocreate) |- node [near start] {yes} (goto-loading);
\path [line] (create-xid) |- node [near start] {} (goto-loading);
\end{tikzpicture}
\caption{Flow for the initialization phase.}
\label{fig:initialization-flow}
\end{figure}

As a first operation, if this is the first time the wnode enters the Initialization phase, it does not know about \verb+cxid+. To recover it, we read the children of \verb+$(aiid)/xids/+ and find the latest value with data prefix equal to \verb+bip+. Then we have the flow of Fig.~\ref{fig:initialization-flow}. We note that we need to clean up data in \verb+ds/$(cxid)/+ if no completion has been assessed, since it means that the previous wnode failed before completing writing. In the case of a meshed implementation, we must clean up only the data pertaining to this xjob.

If the wnode fails before creating the new execution id, then it simply updates the next execution time again with the same value, and proceeds.

\subsubsection{Loading}

Let us now describe the activity of the involved threads, in the case of a master xjob; for a slave xjob we will introduce some simplifications due to the limited concerns of the xjob.

First we get \verb+$(bip)/xt/$(pxid)+ as $T_{px}$, if a previous execution exists, otherwise a negative value is used. Then, we check for children of \verb+$(bip)/r/+ while setting a watch with a \verb+NOWREADY+ callback. If a \verb+r/$(cxid)+ znode is already present, we create a \verb+WASREADY+ thread for loading previous data. Otherwise, for each depending behavior \verb+dbip+ with \verb+$(dbip)/_nxtime+ lesser or equal than \verb+xt/$(cxid)+ (which, if \verb+xt/+ is empty, is considered infinity), we get \verb+$(dbip)/_nxtime+ with a watch named \verb+NXTIME(dbip)+; we set a watch for children of \verb+$(dbip)/c/+ named \verb+COMPL(dbip)+, for children of \verb+$(dbip)/xt/+ named \verb+XTIME(dbip)+ and for changes to \verb+$(aiid)/xids/$(xid)+ named \verb+PREPAREDATA+; finally, we proceed with an \verb+UPDATE+ routine. Indeed, in this case when the behavior was not initially ready, it is important to first set all the necessary watches, then start processing already existing data in \verb+UPDATE+, in order not to miss any change in-between.

We define as \verb+dxid+ a generic execution and \verb+dcxid+ the current execution, both for the depending behavior. In general, the routines may perform any of these operations: concurrently update the common input arrival estimates, update the next execution time \verb+$(bip)/_nxtime+, create the execution time \verb+$xt/$(pxid)+, append the execution time and the dependent \verb+xid+s at \verb+$(aiid)/xids/$(xid)+, and create the ready flag \verb+$r/$(pxid)+. It must hold that the execution time is always created before any consequent update to the next execution time; also, the creation of the execution time and the resolution of the inputs must be performed before creating the ready flag. This ordering is necessary to avoid cyclic updates, on one hand, and state inconsistencies, on the other. Also, the update of the data in \verb+$(aiid)/xids/$(xid)+ must be performed synchronously.

Since the watch callbacks usually must be renewed, there exists the problem that watch events are missed between the callback and the creation of the new watch. For that reason, on a callback we will use the returned value only if we are certain that no renewal is necessary. Also, renewal does not mean that a new event will necessarily be produced for the current execution: we may receive an event that relates to a previous execution. Therefore in some cases we must provide a context equal to the execution identifier: in this way we can drop callbacks that we are not interested anymore in.

\begin{itemize}
\item \verb+UPDATE+: this function loads all the already produced data (of the various \verb+dbip+) whose output time is strictly greater than $T_{px}$; also, we evaluate the impact of the \verb+$(dbip)/_nxtime+ values. All the operations are feasible, in particular if the pending inputs are considerably ahead in respect to the current behavior;
\item \verb+NXTIME(dbip)+: if the next execution time value plus the propagation delay of \verb+dbip+ is greater than \verb+xt/$(cxid)+ for all the output ports of interest, we use such value, otherwise we re-read

\noindent
\verb+$(dbip)/_nxtime+ while setting a new \verb+NXTIME(dbip)+ watch. All the operations are feasible, in particular if such next execution time makes one input port resolve;
\item \verb+XTIME(dbip)+: if \verb+r/$(cxid)+ already exists, we use the obtained value, otherwise we get the children of \verb+$(dbip)/xt/+ again and set a new \verb+XTIME(dbip)+ watch. Since multiple new children can be present, we must keep track of the last one processed with a reference shared between the \verb+XTIME(dbip)+; the initial child of \verb+$(dbip)/xt/+ is the one with time greater or equal than \verb+$xt/$(pxid)+, i.e. $T_{px}$. All the operations are feasible, in particular if the propagation delay is fixed and no guard applies to the input: in that case we do not need to receive the output to evaluate its impact on readiness;
\item \verb+COMPL(dbip)+: if \verb+r/$(cxid)+ already exists, we use the obtained value, otherwise we get the children of \verb+$(dbip)/c/+ again and set a new of \verb+COMPL(dbip)+ watch. Again we need to consider all the children, since any number of new data sets may have been created between our notification and the setting of the new watch. We keep another shared reference between calls of \verb+COMPL(dbip)+ to store the most recent data set loaded, then we load data for every execution id of the depending behavior \verb+dxid+ having \verb+$(dbip)/xt/$(dxid)+ lesser or equal than \verb+$(bip)/xt/$(cxid)+. All the operations are feasible, since data allows to extend availability functions;
\item \verb+PREPAREDATA+: if \verb+$(aiid)/xids/$(xid)+ has data such that all ports are resolved, we use this value, otherwise we read \verb+$(aiid)/xids/$(xid)+ again and set a new \verb+PREPAREDATA+ watch; please note that the order of dependent executions in the data of a \verb+$(aiid)/xids/$(xid)+ znode is undefined. Then we identify which execution ids for the resolved input ports are present and load data accordingly, to be prepared for execution; if this is a meshed implementation, we must check whether this is a dimension for the mesh and whether the length is sufficient for execution; if it is not, then we must skip: if this is the master xjob, then the \verb+c/$(cxid)+ znode is created, otherwise we get the existence of \verb+c/$(cxid)+ and set a watch with a \verb+COMPLETED+ callback; if already existing, we just move to the Initialization phase;
\item \verb+WASREADY+: this function loads the data identified through the execution id dependencies in\\
\verb+$(aiid)/xids/$(xid)+, then it moves to the Execution phase;
\item \verb+NOWREADY+: the job simply moves to the Execution phase;
\item \verb+COMPLETED+: the job simply moves to the Initialization phase.
\end{itemize}

If the wnode fails at any time before setting \verb+xt/$(cxid)+, no problem occurs since it is only a matter of identifying the execution time again; the same holds for the appending of the execution time and the depending executions inside \verb+$(aiid)/xids/$(xid)+. The ready condition can be re-identified.

As for a slave xjob, it only deals with \verb+PREPAREDATA+, and either \verb+WASREADY+ or \verb+NOWREADY+ depending on the existence of \verb+r/$(cxid)+.
In practice, it must simply load data for execution as soon as it has been identified by the master job of the meshed implementation.

\subsubsection{Execution}

In the Execution phase, the distributed scheduler decides whether the behavior must be executed. After the results have been obtained, we move to the Storage phase. Since the execution does not involve any read or write operation on the synchronization layer, the phase is safe.

\subsubsection{Storage}

After execution, the output data set must be published under \verb+ds/$(cxid)/+. When all data has been stored, we need to validate the writing. First, we check whether this is a meshed implementation: if it is, we create a sequential znode under \verb+mc/$(cxid)/+ with prefix given by our \verb+coord+; if the returned sequence number is equal to the number of instances of the mesh minus one, then this is the last write and we are responsible with creating \verb+c/$(cxid)+, thus validating our overall results. If this is not a meshed implementation, we directly create \verb+c/$(cxid)+. After this phase, the job moves to the Initialization phase.

If the wnode fails at any time before creating \verb+c/$(cxid)+ but after creating \verb+mc/$(cxid)/+, we handle the cleanup in the Initialization phase directly. If no \verb+mc/$(cxid)/+ has been created, however, we need to repeat the whole execution.

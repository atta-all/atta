\section{Static analysis}
\label{sec:static-analysis}

The static analysis of a system consists in the analysis of a system outside its execution flow and it is an important part of Atta. Static analysis allows to perform some correctness checking, or to produce synthetic information on the topology of the system that will be used by the runtime. Since its results are a property of the system model not directly related to runtime, we cover this topic under the metamodel chapter.

\subsection{Minimum propagation delays}

Minimum propagation delays $pd^m$ are the minimum system times required for data produced by one behavior to arrive at another behavior, on a path through sensitive ports only; we call such a path a {refresh path}. We identify three different formats of the minimum propagation delay:

\begin{itemize}
\item[-] $pd^m(i,q)$ : the minimum propagation delay of a behavior $i$ from its input ports to the output port $q$;
\item[-] $pd^m(i,j,p)$ : the distance between behaviors $i$ and $j$, arriving at port $p = 1,\dots,N_I(j)$ of $j$, where $N_I(j)$ is the number of input ports of $j$;
\item[-] $pd^m(i,j,0)$ : the distance between behaviors $i$ and $j$, arriving at any sensitive port.
\end{itemize}

To obtain these values for a specific implementation instance, we can adapt the Floyd-Warshall algorithm that computes all the minimum distances between all the vertices of a graph, under the assumption that edges are weighted. Our vertices are the behaviors, and the weights are on behaviors in respect to their output ports; however, weights can be trivially translated into edges. The algorithm is iterative and assumes to number the behaviors from $0$ to $N-1$; as shown previously, the ports are instead numbered from 1, where port zero refers to the port-independent variant of the distance. The values for iteration $k=0$ are as follows:

\begin{equation}
pd^m_0(i,j,p) = \left\{
\begin{array}{ll}
\infty & \textrm{if $(i,j) \not\in E$} \\
pd^m(i,q) & \textrm{if $(i,j) \in E$}
\end{array}
\right.
\end{equation}

\begin{equation}
pd^m_0(i,j,0) = \left\{
\begin{array}{ll}
\infty & \textrm{if $(i,j) \not\in RE$} \\
pd^m(i,q) & \textrm{if $(i,j) \in RE$}
\end{array}
\right.
\end{equation}
where $q$ is the output port of $i$ that reaches $j$ (on $p$ or in general), $E$ is the set of directed edges between behaviors, and $RE \subseteq E$ has only sensitive ports as destinations. In respect to the basic Floyd-Warshall algorithm, we do not set the delay to zero if $i = j$, since we are also interested in cycles between one behavior and itself.

The generic $k$-th step becomes:
\begin{equation}
pd^m_k(i,j,p) = \min( pd^m_{k-1}(i,j,p),\, pd^m_{k-1}(i,k,0) + pd^m_{k-1}(k,j,p))\, , \, \forall p = 0,...,N_I(j)
\end{equation}

Particularly interesting is the fact that values for $k$ can overwrite values for $k-1$ as soon as they are obtained, without altering the final result. Consequently the space complexity is only $N^2$ while the time complexity is $N^3$, both for each port, where the number of ports depends on $j$.

\subsection{Guards}

Guards play an important role on the flow of execution. Due to the rather free semantics of execution, we must guarantee a correct definition of guards. In particular, we do not allow guards to be never satisfied. It could be the case since guards are defined for both edges and input ports, hierarchically: their intersection can be empty. That in turn would systematically prevent a switch, an initialization or an execution, regardless of the actual data involved. 

